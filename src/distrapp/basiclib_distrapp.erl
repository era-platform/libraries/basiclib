%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.04.2017
%%% @doc Implements service of load and run distributed applications.
%%%        Standard erlang dist_ac is weak, deadlock-ful and loose links after disconnect.
%%%        All of R's distributed applications turned to own strategy.

-module(basiclib_distrapp).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([assert_distributed_nodes/1,
         start_distributed_app/1,
         on_stop/2]).

-export([start_link_supv/2]).

-export([get_running_mode/1,
         get_active_node/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("basiclib_distrapp.hrl").

-define(RTimeout, 5000 + ?BU:random(5000)).

%% ====================================================================
%% API functions
%% ====================================================================

% check if all distributed nodes has the same configuration
assert_distributed_nodes(App) ->
    ?DistrNodes:assert_distributed_nodes(App).

% start application with check on deadlock of erlang application module (as it was earlier)
start_distributed_app(App) -> start_distributed_app(App,?RTimeout).
start_distributed_app(App,Timeout) ->
    ?BU:exec_fun_timered(fun() -> ?OUT('$info',"DistrApp '~ts' distributed starting...", [App]),
                                  application:ensure_all_started(App, permanent),
                                  ?OUT('$info',"DistrApp '~ts' distributed started!", [App]) end,
                         fun() -> ?OUT('$info',"DistrApp '~ts' found hang on start after rnd=~120p ms. Exit node",[App,Timeout]),
                                  ?BU:stop_node() end,
                         Timeout).

% on_stop(_App,_Opts) -> ok.
on_stop(App,Opts) ->
     case check_app_in_roles(App) of
         %false -> ok; % dialyzer says it can never be false (until check_app_in_roles() is actually implemented)
         true -> schedule_restart(App,Opts)
     end.

start_link_supv(App,RoleName) ->
    ?DistrAppSupv:start_link(App,RoleName).


% return current running mode
get_running_mode(App) ->
    case catch ?BLleader:get_state(?DistrAppSupv:reg_name_srv(App),local) of
        {ok,S} -> S;
        _ -> undefined
    end.

% return current active mode
get_active_node(App) ->
    case catch ?BLleader:get_active_node(?DistrAppSupv:reg_name_srv(App)) of
        undefined -> undefined;
        Node when is_atom(Node) -> Node;
        _ -> undefined
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @todo
%% TODO when function is implemented and can possibly return false, UNCOMMENT the false case in on_stop()! (was commented to remove dialyzer warning)
check_app_in_roles(_App) ->
    %?OUT('$info',"  DistrApp '~s' found in node roles", [App]),
    true.

schedule_restart(App,Opts) ->
    ?OUT('$info',"DistrApp schedule restart ~s...", [App]),
    {Self,Ref} = {self(),make_ref()},
    F = fun() ->
                ?BLopts:set_group_leader(),
                Self ! {ok,Ref},
                % boot app should run after application is removed from running.
                % wait a bit for app to stop fully.
                % wait more for mnesia to synchronize after connection restored.
                timer:sleep(5000),
                ?OUT('$info',"DistrApp booting apps...", []),
                case ?CFG:boot_app_function() of
                    F when is_function(F,2) -> F(App,Opts);
                    _ ->
                        Start1 = ?BU:function_exported(App,start,1),
                        Start0 = ?BU:function_exported(App,start,0),
                        case {Start1,Start0} of
                            {true,_} -> App:start(Opts);
                            {_,true} -> App:start();
                            _ -> application:start(App)
                        end,(Opts)
                end
        end,
    spawn(F),
    receive {ok,Ref} -> ok
    after 5000 -> ok
    end.

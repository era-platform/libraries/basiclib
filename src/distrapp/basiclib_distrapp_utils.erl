%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.03.2017
%%% @doc Utility functions for distributed applications

-module(basiclib_distrapp_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_distr_nodes/1,
         get_distr_nodes_apps/0,
         is_app_running/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("basiclib_distrapp.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% Return from current configuration distributed nodes for selected app
get_distr_nodes(App) ->
    case application:get_env(kernel,?KernelKeyDistrApps) of
        undefined -> undefined;
        {_,DApps} ->
            case lists:keyfind(App,1,DApps) of
                false -> undefined;
                {_,_,Nodes} -> Nodes
            end end.

%% Return from current configuration all nodes and their distributed apps
get_distr_nodes_apps() ->
    case application:get_env(kernel,?KernelKeyDistrApps) of
        undefined -> [];
        {ok,DApps} ->
            lists:foldl(fun({DApp,_,Nodes},Acc1) ->
                                lists:foldl(fun(N,Acc2) ->
                                                    case lists:keytake(N,1,Acc2) of
                                                        false -> [{N,[DApp]}|Acc2];
                                                        {value,{_,NDApps},X} -> [{N,[DApp|NDApps]}|X]
                                                    end
                                            end, Acc1,Nodes)
                        end, [], DApps) end.

%% Returns true if app is in running list
is_app_running(App) ->
    {_,RApps} = lists:keyfind(running,1,application:info()),
    case lists:keyfind(App,1,RApps) of
        false -> false;
        {_,_} -> true
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

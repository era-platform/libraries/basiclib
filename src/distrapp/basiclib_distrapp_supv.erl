%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.04.2017
%%% @doc Starting Distributed Applications Supervisor

-module(basiclib_distrapp_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/2,
         reg_name_srv/1]).
-export([init/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("basiclib_distrapp.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(App,RoleName) ->
    case check_exports(App) of
        ok ->
            Opts = [{app,App},{rolename,RoleName}],
            supervisor:start_link({local,reg_name_supv(App)}, ?MODULE, Opts);
        {error,_}=Err -> Err
    end.


reg_name_srv(App) -> ?BU:to_atom_new(?BU:to_list(App) ++ "_distr").

%% @private
reg_name_supv(App) -> ?BU:to_atom_new(?BU:to_list(App) ++ "_distr_supv").

%% ====================================================================
%% Callback functions (supervisor)
%% ====================================================================

init(Opts) ->
    [App,RoleName] = ?BU:extract_required_props([app,rolename], Opts),
    RegName = reg_name_srv(App),
    FunNodes = fun() -> distr_nodes(App,RoleName) end,
    FunStart = fun(RemoteState) -> start_top_supv(App,RoleName,RemoteState) end,
    FunStop = fun() -> stop_top_supv(App,RoleName) end,
    TOS = case {?BU:function_exported(App,get_takeover_state,2), ?BU:function_exported(App,get_ref,0)} of
              {true,true} -> fun(FromNode) -> App:get_takeover_state(App:get_ref(),FromNode) end;
              _ -> undefined
          end,
    OUTF = case ?BU:function_exported(App,out,2) of
               true -> fun(Fmt,Args) -> App:out(Fmt,Args) end;
               false -> undefined
           end,
    StartArgs = [
        {regname,RegName},
        {fun_nodes, FunNodes},
        {autosort, false}, % default true
        {fun_start, FunStart},
        {fun_stop, FunStop},
        {ping_interval, 2000}, % default 1000
        {waittimes, 5}, % default 0. -1 means no check after ping result, only initial ping_timeout (interval) and ping  while waiting heartbeat. Every waittime adds 4 sec (ping_timeout=2000 + ping time=1000 + checknode=1000). Also initial heartbeat timeout 2000, and last iteration less for 1000.
        {heartbeat, true}, % default false
        {heartbeat_interval, 1000}, % default 500
        {monitor, true}, % default false
        {takeover, true}, % default false
        {fun_takeover_state, TOS}, % default undefined
        {conflict_behaviour, 'restart_slave_node'}, % default 'stop_slave_service'
        {fun_out, OUTF} % default undefined
    ],
    StartArgs1 = case ?BU:function_exported(App,out,2) of
                     false -> StartArgs;
                     true -> [{out,fun(Fmt,Args) -> out(App,Fmt,Args) end} | StartArgs]
                 end,
    ChildSpec = [{RegName, {?BLleader, start_link, [StartArgs1]}, permanent, 1000, worker, [?BLleader]}],
    out(App, "~ts: ~p supervisor inited", [RoleName, self()]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
check_exports(App) ->
    Fname = get_top_supv_mfa,
    case ?BU:function_exported(App,Fname,1) of
        true -> ok;
        false ->
            case ?BU:function_exported(App,Fname,0) of
                true -> ok;
                false -> {error, ?BU:str("Application module should export function '~ts'",[Fname])}
            end end.

%% @private
distr_nodes(App,_RoleName) ->
    case ?DistrUtils:get_distr_nodes(App) of
        undefined -> [node()];
        Nodes -> Nodes
    end.

%% @private
start_top_supv(App,RoleName,RemoteState) ->
    out(App, "~ts. ~p Starting application master...", [RoleName, self()]),
    Fok = fun() -> App:out("~s: Application master started", [RoleName]), do_after_start(App) end,
    {M,F,A}= App:get_top_supv_mfa(RemoteState),
    ChildSpec = {top_supv, {M,F,A}, permanent, 1000, supervisor, [M]},
    SupvName = reg_name_supv(App),
    do_before_start(App),
    case catch supervisor:start_child(SupvName, ChildSpec) of
        {'EXIT',Err} ->
            App:out("~ts: Application master start exception: ~120tp, stop node..", [RoleName, Err]),
            ?BU:stop_node(1000);
        {error,_}=Err ->
            App:out("~ts: Application master start error: ~120tp, stop node..", [RoleName, Err]),
            ?BU:stop_node(1000);
        {ok,_} -> Fok(), ok;
        {ok,_,_} -> Fok(), ok
    end.

% @private
stop_top_supv(App,RoleName) ->
    SupvName = reg_name_supv(App),
    do_before_stop(App),
    catch supervisor:terminate_child(SupvName,top_supv),
    catch supervisor:delete_child(SupvName,top_supv),
    do_after_stop(App),
    out(App, "~ts. ~p Application master stopped", [RoleName, self()]).

%% ----------------------------

% callback before start
do_before_start(App) ->
    call(App,before_start,[]).

% callback after start
do_after_start(App) ->
    call(App,after_start,[]).

% callback before stop
do_before_stop(App) ->
    call(App,before_stop,[]).

% callback after start
do_after_stop(App) ->
    call(App,after_stop,[]).

%% print to log
out(App,Fmt,Args) ->
    call(App,out,[Fmt,Args]).

%% ------------------------
%% @private
%% callback on some
call(App,Fun,Args) ->
    case ?BU:function_exported(App,Fun,length(Args)) of
        true -> erlang:apply(App,Fun,Args);
        false -> ok
    end.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.04.2017
%%% @doc Definition and verification erlang nodes for distributed applications

-module(basiclib_distrapp_nodes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_distr_nodes/1,
         assert_distributed_nodes/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("basiclib_distrapp.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% Return from current configuration distributed nodes for selected app
get_distr_nodes(App) ->
    ?DistrUtils:get_distr_nodes(App).

assert_distributed_nodes(App) ->
    do_assert_distributed_nodes(App).

%% ====================================================================
%% Internal functions
%% ====================================================================

% -------------------------------------------------------
% @private
do_assert_distributed_nodes(App) ->
    % which nodes are distributed for app (from config)
    case ?DistrUtils:get_distr_nodes(App) of
        undefined -> ok;
        DNodes -> adn1(App,DNodes)
    end.

% @private
adn1(App,DNodes) ->
    % get application info from distr nodes [{N,{App,RunningInfo}}], find active app
    RNodes = DNodes--[node()],
    R1 = ?BLmulticall:call_direct(RNodes,{application,info,[]},1000),
    R2 = lists:filtermap(fun({_,undefined}) -> false;
                            ({N,I}) ->
                                 {_,RA}=lists:keyfind(running,1,I),
                                 case lists:keyfind(App,1,RA) of
                                     false -> false;
                                     T -> {true,{N,T}}
                                 end end, R1),
    % ask active distributed nodes for app for their config distr sequence
    RANodes = lists:map(fun({N,_}) -> N end, R2),
    T1 = ?BLmulticall:call_direct(RANodes,{?MODULE,get_distr_nodes,[App]},1000),
    % compare configuration
    case lists:all(fun({_,L}) -> L==DNodes end, T1) of
        true -> ok;
        false -> adn2(App,DNodes,T1)
    end.

% @private
adn2(App,DNodes,T1) ->
    timer:sleep(100),
    ?OUT('$error',"Application '~s'. Distributed nodes configuration sequence mistake.", [App]),
    % build closure of distributed nodes where App is known and registered
    C1 = lists:foldl(fun({_,undefined},Acc) -> Acc;
                        ({_,X},Acc) -> lists:umerge(Acc,lists:usort(X)) end, lists:usort(DNodes), T1),
    C2 = C1 -- [node()],
    % kill processes on closure nodes
    ?OUT('$info',"Application '~s'. Kill distributed closure nodes: ~120p", [App, C2]),
    ?BLmulticall:call_direct(C2,{?BU,kill_sysproc,[]},1000),
    % terminate self
    ?OUT('$info',"Application '~s'. Terminate self process",[App]),
    timer:sleep(100),
    ?BU:kill_sysproc(),
    % result
    {error, "Distributed nodes configuration sequence mistake"}.

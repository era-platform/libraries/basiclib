%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.10.2016
%%% @doc Implements service of unique strategy running. (choosing leader and activating it)
%%%        For example several nodes should share only one example of strategy gen_srv.
%%%        Then current srv executes and sync by configuration other nodes and applies state
%%%        Use mini fsm of undefined=inactive|'started'|'active'
%%%        Could be attached to any supv by local name. So any local name should not be used more than one time on node.
%%%        Options on start:
%%%             [regname (for local naming)] % default ?MODULE=basiclib_leader_svc_srv. Should be used to allow several servers in node.
%%%             fun_nodes (/0)				 % () -> [Node]. Function to get synchronized nodes in sequence of decreasing priority.
%%%             [autosort], 			     % default true. Set to use autosort nodes by name instead of direct sequence from fun_nodes().
%%%             fun_start (/0|/1)			 % ()|(RemoteState) -> ok | {error,Reason::term()}. Function to activate current example.
%%%             fun_stop (/0)				 % () -> ok. Function to deactivate current example on takeover.
%%%             [ping_timeout (ms)]			 % default 1000. Timeout on ping result. Should be greater than heartbeat timeout, because every heartbeat starts new ping timer.
%%%             [waittimes],			     % default 0. How many times ping to active node could fail before follower's node starts. And how many ping after heartbeat timeout without disconnect. -1 means no check active node.
%%%             [heartbeat],     			 % default false. Uses heartbeat from active to slaves.
%%%             [heartbeat_timeout],	     % default 500. Timeout between heartbeat from active to slaves. Should be less than ping_timeout.
%%%             [monitor],			    	 % default false. Use erlang:monitor_node(ActiveNode).
%%%             [takeover],		        	 % default false. Uses takeover mechanism by more prioritized node on start.
%%%             [fun_takeover_state (/1)],	 % default undefined, (FromNode::atom()) -> RemoteState::term(). Returns state from active node on takeover.
%%%             [conflict_behaviour],        % default stop_slave_service. When found 2 active nodes. Expected: none | stop_slave_service | restart_slave_node | restart_primary_service | restart_primary_node
%%%             [fun_out (/2)],			     % default undefined, (Fmt::string(),Args::list()) -> ok. Write to log/console operation.

-module(basiclib_leader_svc_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([get_state/2, check_node/2, takeover/2, please_activate_takeover/2, heartbeat/2, print/1, get_active_node/1]).

-export([debug_timeout/2,
         debug_check_disconnect_to_all/1,
         debug_throw/1]).

%% ====================================================================
%% Define
%% ====================================================================

-record(funs, {
    fun_nodes,
    fun_start,
    fun_stop,
    fun_takeover_state,
    fun_out
}).

-record(opts, {
    pause_on_init=0, % for non-leader nodes: wait a bit at start to wait for leader node to start, not to mistakeover, not to intercept activity on restarts.
    ping_timeout=1000,
    heartbeat_timeout=500,
    use_monitor=false,
    use_heartbeat=false,
    use_takeover=false,
    conflict_behaviour='stop_slave_service',
    max_wait_times=0, % how many times allowed to check active node before applying disconnect and start failover service (only when active is set and doesn't checked)
    max_ping_times=0, % how many times allowed to ping without disconnect after heartbeat failed (only when heartbeat)
    autosort=true
}).

-record(state, {
    regname,
    state = started,
    statestartts = 0,
    ets,
    funs=#funs{},
    opts=#opts{},
    last_nodes=[],
    active_node=undefined,
    wait_times=0, % how many times left of allowed to check active node before applying disconnect and start failover service (only when active is set and doesn't checked)
    ping_times=0, % how many times left of allowed to ping without disconnect after heartbeat failed (only when heartbeat)
    monitors=[],
    inited=false,
    ping_ref,
    takeover_ref,
    checknode_ref,
    heartbeat_ref,
    activate_ref,
    timerref
}).

-include("app.hrl").

-define(PING_TIMEOUT, 1000).
-define(START_PING_MSG, {start_ping_timer}).
-define(HEARTBEAT_TIMEOUT, 500).
-define(START_HEARTBEAT_MSG, {start_heartbeat_timer}).
-define(QUERY_TIMEOUT, 1000).
-define(TAKEOVER_TIMEOUT, 3000).

-define(OUTF(StateOrFun,Fmt,Args), out(StateOrFun,Fmt,Args)).

%% ====================================================================
%% API functions
%% ====================================================================

%% start gen_srv and link to supv or owner
% regname (for local naming)
% fun_nodes (/0)
% fun_start (/0 | /1)
% fun_stop (/0)
% ping_timeout (ms)
start_link(StartOpts) ->
    case ?BU:get_by_key(fun_nodes,StartOpts,undefined) of
        F when is_function(F,0) -> start_link_1(StartOpts);
        _ -> {error, {invalid_params, <<"Invalid 'fun_nodes'. Expected function of 0 args">>}}
    end.
start_link_1(StartOpts) ->
    case ?BU:get_by_key(fun_start,StartOpts,undefined) of
        F when is_function(F,0) -> start_link_2(StartOpts);
        F when is_function(F,1) -> start_link_2(StartOpts);
        _ -> {error, {invalid_params, <<"Invalid 'fun_start'. Expected function of 0/1 args">>}}
    end.
start_link_2(StartOpts) ->
    case ?BU:get_by_key(fun_stop,StartOpts,undefined) of
        F when is_function(F,0) -> start_link_3(StartOpts);
        _ -> {error, {invalid_params, <<"Invalid 'fun_stop'. Expected function of 0 args">>}}
    end.
start_link_3(StartOpts) ->
    RegName = ?BU:get_by_key(regname,StartOpts,?MODULE),
    Opts = case StartOpts of
               _ when is_map(StartOpts) -> maps:to_list(StartOpts);
               _ when is_list(StartOpts) -> StartOpts
           end,
    gen_server:start_link({local,RegName}, ?MODULE, Opts, []).

%% returns state of current sync FSM
%get_state(FromNode) -> get_state(?MODULE, FromNode).
get_state(RegName, FromNode) ->
    gen_server:call(RegName, {get_state,FromNode}).

% checks if node is ok, service is ok (process hangs)
check_node(RegName, _FromNode) ->
    case whereis(RegName) of
        undefined -> not_found;
        Pid -> {ok,Pid}
    end.

% stops node service to handle over other node
%takeover(FromNode) -> takeover(?MODULE, FromNode).
takeover(RegName, FromNode) ->
    gen_server:call(RegName, {takeover,FromNode}).

% active node send takeover event when heartbeat model is used and found more priority node
please_activate_takeover(RegName, FromNode) ->
    gen_server:cast(RegName, {please_activate_takeover,FromNode}).

% register heartbeat
heartbeat(RegName, FromNode) ->
    gen_server:call(RegName, {heartbeat,FromNode}).

%% Print state
print(RegName) ->
    gen_server:call(RegName, {print}).

%% returns last known active nodename after last ping
get_active_node(RegName) ->
    gen_server:call(RegName, {get_active_node}).

% @debug
debug_timeout(RegName,Timeout) ->
    gen_server:cast(RegName, {debug_timeout,Timeout}).

% @debug
debug_check_disconnect_to_all(RegName) ->
    gen_server:cast(RegName, {debug_check_disconnect_to_all}).

% @debug
debug_throw(RegName) ->
    gen_server:cast(RegName, {debug_throw}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [FunNodes,FunStart,FunStop] = ?BU:extract_required_props([fun_nodes,fun_start,fun_stop], Opts),
    [RegName, PauseOnInit, PingTimeout, HeartbeatTimeout, AutoSort, WaitTimes, UseMonitor, UseHeartbeat, UseTakeover, ConflictBehaviour, TOS, OUTF] =
        ?BU:extract_optional_default([{regname,?MODULE},
                                      {pause_on_init,0},
                                      {ping_timeout,?PING_TIMEOUT},
                                      {heartbeat_timeout,?HEARTBEAT_TIMEOUT},
                                      {autosort,true},
                                      {waittimes,0},
                                      {monitor,false},
                                      {heartbeat,false},
                                      {takeover,false},
                                      {conflict_behaviour,'stop_slave_service'},
                                      {fun_takeover_state,undefined},
                                      {fun_out,undefined}], Opts),
    Ref = make_ref(),
    State = #state{regname=RegName,
                   ets=ets:new(env, [set]),
                   state=started,
                   statestartts=?BU:timestamp(),
                   funs=#funs{fun_nodes=FunNodes,
                              fun_start=FunStart,
                              fun_stop=FunStop,
                              fun_takeover_state=TOS,
                              fun_out=OUTF},
                   opts=#opts{pause_on_init=PauseOnInit,
                              ping_timeout=max(PingTimeout,min(HeartbeatTimeout+100,HeartbeatTimeout*2)),
                              max_wait_times=WaitTimes,
                              max_ping_times=WaitTimes,
                              use_heartbeat=UseHeartbeat,
                              heartbeat_timeout=HeartbeatTimeout,
                              use_monitor=UseMonitor,
                              use_takeover=UseTakeover,
                              conflict_behaviour=ConflictBehaviour,
                              autosort=AutoSort},
                   ping_ref=Ref},
    gen_server:cast(self(), {'start_on_init',Ref}),
    ?OUTF(State, "DSERVICE '~s'. inited (~p)", [RegName, self()]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%%
handle_call({get_state,_FromNode}, _From, #state{state=S}=State) ->
    Reply = {ok,S},
    {reply, Reply, State};

%% handle takeover command from most priority
%% State: active. Others error
handle_call({takeover,FromNode}, _From, #state{state=active}=State) ->
    MyNode = node(),
    case sort_nodes([MyNode,FromNode], State) of
        [MyNode|_] ->
            ?OUTF(State, "DSERVICE '~s'. Blocked takeover by '~ts'.", [State#state.regname,FromNode]),
            {block, node()};
        [FromNode|_] ->
            ?OUTF(State, "DSERVICE '~s'. Stop on takeover.", [State#state.regname]),
            {ok,Res,State1} = stop_strategy_service(FromNode, State),
            {reply, Res, State1#state{takeover_ref=undefined}}
    end;
handle_call({takeover,_FromNode}, _From, #state{state=S}=State) ->
    Reply = {invalid_state, node(), S},
    {reply, Reply, State};

%% handle heartbeat from active
handle_call({heartbeat, Node}, _From, #state{state=started,timerref=undefined}=State) ->
    State1 = set_active_node(Node,State),
    Reply = {ok,started},
    {reply, Reply, State1#state{ping_times=0}};
handle_call({heartbeat, Node}, _From, #state{state=started,timerref=TimerRef}=State) ->
    erlang:cancel_timer(TimerRef),
    State1 = wait(set_active_node(Node,State)),
    Reply = {ok,started},
    {reply, Reply, State1#state{ping_times=0}};
handle_call({heartbeat, _Node}, _From, #state{state=S}=State) ->
    Reply = {ok,S},
    {reply, Reply, State};

%%
handle_call({print}, _From, #state{regname=RegName}=State) ->
    ?OUTF(State, "DSERVICE '~s'. State: ~120p", [RegName, State]),
    {reply, State, State};

handle_call({get_active_node}, _From, #state{active_node=ActiveNode, regname=_RegName}=State) ->
    %?OUTF(State, "DSERVICE '~s'. Active node: '~s'", [_RegName, ActiveNode]),
    {reply, ActiveNode, State};

%% other
handle_call(_Request, _From, State) ->
    %?OUTF(State, "DSERVICE '~s'. handle_call(~p)", [State#state.regname,_Request]),
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({'start_on_init',Ref}, #state{ping_ref=Ref,opts=#opts{pause_on_init=PauseOnInit}}=State) ->
    MyNode = node(),
    Ns = get_synchronized_nodes(State),
    State1 = State#state{last_nodes=Ns},
    Timeout = case sort_nodes(Ns, State1) of
                  [MyNode|_] -> 0;
                  _ -> PauseOnInit
              end,
    {noreply, State1#state{timerref=erlang:send_after(Timeout, self(), {?START_PING_MSG,Ref})}};

%% handle start takeover internal command
handle_cast({start_takeover, Node}, #state{state=takeover}=State) ->
    State1 = start_takeover(Node,State),
    {noreply, State1};

%% handle please_activate_takeover event from active node
handle_cast({please_activate_takeover, FromNode}, #state{state=started,active_node=FromNode,timerref=TimerRef}=State) ->
    UseTakeover = is_takeover(State),
    case TimerRef of
        _ when not UseTakeover ->
            ?OUTF(State, "DSERVICE '~s'. Got 'please_activate_takeover' signal from '~s'. Takeover is disabled!", [State#state.regname,FromNode]),
            {noreply, State};
        undefined ->
            ?OUTF(State, "DSERVICE '~s'. Got 'please_activate_takeover' signal from '~s'. Wait for next ping time...", [State#state.regname,FromNode]),
            {noreply, State};
        _ ->
            ?OUTF(State, "DSERVICE '~s'. Got 'please_activate_takeover' signal from '~s'. Ok, checking...", [State#state.regname,FromNode]),
            erlang:cancel_timer(TimerRef),
            State1 = start_ping_group(State#state{timerref=undefined}),
            {noreply, State1}
    end;
% #193
handle_cast({please_activate_takeover,FromNode}, #state{state='active',statestartts=STS,opts=#opts{conflict_behaviour=ConflictBehaviour}}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Got 'please_activate_takeover' signal from '~s' while active! Seems to be problem of global names (no conflict)", [State#state.regname,FromNode]),
    case ?BU:timestamp() of
        X when X > STS, X - STS > 5000 ->
            % Multi-active. First primary detected slave. Slave considered primary as not active.
            case ConflictBehaviour of
                C when C==restart_primary_service; C=='stop_slave_service' ->
                    ?OUTF(State, "DSERVICE '~s'. Skip instead of restart. Active for ~p sec. Send takeover ('~ts')...", [State#state.regname, X-STS div 1000,ConflictBehaviour]),
                    % stop follower node by emulating takeover (24.07.2024)
                    start_takeover_when_active(FromNode, State);
                'restart_primary_node' ->
                    ?OUTF(State, "DSERVICE '~s'. Stop node ('~ts')", [State#state.regname,ConflictBehaviour]),
                    init:stop();
                _ ->
                    ?OUTF(State, "DSERVICE '~s'. No Action ('~ts')", [State#state.regname,ConflictBehaviour]),
                    ok
            end;
        _ -> ok
    end,
    {noreply, State};
% #193
handle_cast({please_activate_takeover,FromNode}, #state{state=S}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Got 'please_activate_takeover' signal from '~s' while ~s!", [State#state.regname,FromNode,S]),
    {noreply, State};

%% @debug
handle_cast({debug_timeout,Timeout}, State) ->
    timer:sleep(Timeout),
    {noreply, State};

%% @debug
handle_cast({debug_check_disconnect_to_all}, State) ->
    check_disconnect_to_all(debug, State),
    {noreply, State};

%% @debug
handle_cast({debug_throw}, _State) ->
    throw(debug);

%% other
handle_cast(_Request, State) ->
    %?OUTF(State, "DSERVICE '~s'. handle_cast(~120p)", [State#state.regname,_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% ping timer
handle_info({?START_PING_MSG,Ref}, #state{state=started,inited=true,opts=#opts{use_heartbeat=true},ping_ref=Ref,ping_times=PingTimes}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Heartbeat timeout.", [State#state.regname]),
    State1 = start_ping_group(State),
    {noreply, State1#state{ping_times=PingTimes+1}};
handle_info({?START_PING_MSG,Ref}, #state{state=started,ping_ref=Ref}=State) ->
    State1 = start_ping_group(State),
    {noreply, State1};
handle_info({?START_PING_MSG,Ref}, #state{state=active,ping_ref=Ref}=State) ->
    State1 = start_ping_group(State),
    {noreply, State1};

% heartbeat timer
handle_info({?START_HEARTBEAT_MSG,Ref}, #state{state=active,heartbeat_ref=Ref}=State) ->
    State1 = start_heartbeat_group(State),
    {noreply, State1};

% ping reply event
handle_info({ping_reply,Ref,Res},#state{ping_ref=Ref,state=started,inited=false,timerref=undefined}=State) ->
    ?OUTF(State, "DSERVICE '~ts'. ping reply(i): ~200tp",[State#state.regname,Res]),
    State1 = do_handle_ping_result_on_started(Res,State#state{ping_ref=undefined}),
    {noreply, State1#state{inited=true}};
handle_info({ping_reply,Ref,Res},#state{ping_ref=Ref,state=started,timerref=undefined}=State) ->
    ?OUTF(State, "DSERVICE '~ts'. ping reply(s): ~200tp",[State#state.regname,Res]),
    State1 = do_handle_ping_result_on_started(Res,State#state{ping_ref=undefined}),
    {noreply, State1};
handle_info({ping_reply,Ref,Res},#state{ping_ref=Ref,state=active,timerref=undefined}=State) ->
    ?OUTF(State, "DSERVICE '~ts'. ping reply(a): ~200tp",[State#state.regname,Res]),
    State1 = do_handle_ping_result_on_active(Res,State#state{ping_ref=undefined}),
    {noreply, State1};

% takeover reply event
handle_info({takeover_reply,Ref,Res},#state{takeover_ref=Ref,state=takeover,timerref=undefined}=State) ->
    State1 = do_handle_result_takeover(Res,State#state{takeover_ref=undefined}),
    {noreply, State1};

%% takeover reply event on 'active' (takeover instead of restart of leader's node)
handle_info({takeover_reply_when_active,Ref,Res},#state{takeover_ref=Ref,state='active'}=State) ->
    State1 = do_handle_result_takeover_when_active(Res,State#state{takeover_ref=undefined}),
    {noreply, State1};

% checknode reply event
handle_info({checknode_reply,Ref,Res},#state{checknode_ref=Ref,state=started,timerref=undefined}=State) ->
    State1 = do_handle_checknode_result_started(Res,State#state{checknode_ref=undefined}),
    {noreply, State1};

% heartbeat reply event
handle_info({heartbeat_reply,Ref,Res},#state{heartbeat_ref=Ref,state=active}=State) ->
    State1 = do_handle_heartbeat_result_active(Res,State), % ref not cleared, same on timer and async reply
    {noreply, State1};

% activate reply event
handle_info({activate_reply,Ref,Res},#state{activate_ref=Ref,state=activating}=State) ->
    State1 = do_handle_activate(Res,State#state{activate_ref=undefined}),
    {noreply, State1};

% down spawned event
handle_info({'DOWN',_,process,_,normal},#state{}=State) ->
    {noreply, State};
handle_info({'DOWN',_,process,_,_},#state{state=takeover}=State) ->
    State1 = do_handle_result_takeover(undefined,State),
    {noreply, State1#state{takeover_ref=undefined}};
handle_info({'DOWN',_,process,_,_},#state{state=S}=State) when S==active;S==started->
    State1 = start_ping_group(State),
    {noreply, State1#state{ping_ref=undefined}};
handle_info({'DOWN',_,process,_,_},#state{state=activating}=State) ->
    {ok,_,State1} = stop_strategy_service(undefined, State),
    {noreply, State1#state{activate_ref=undefined}};

% down of active_node
handle_info({nodedown,Node}, #state{state=active,monitors=M}=State) ->
    % erlang:monitor_node(Node,false), % if cookie differs, then looped connect/down
    State1 = State#state{monitors=lists:keydelete(Node, 1, M)},
    {noreply, State1};
handle_info({nodedown,Node}, #state{active_node=Node,state=takeover}=State) ->
    handle_info({nodedown,Node}, State#state{state=started});
handle_info({nodedown,Node}, #state{active_node=Node,monitors=M,checknode_ref=CNRef,timerref=TimerRef}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Active node DOWN '~s'.", [State#state.regname,Node]),
    % erlang:monitor_node(Node,false), % locks for 5-6 sec timeout
    State1 = State#state{active_node=undefined,
                         monitors=lists:keydelete(Node, 1, M),
                         takeover_ref=undefined,
                         checknode_ref=undefined,
                         wait_times=0},
    case TimerRef of undefined -> ok; _ -> erlang:cancel_timer(TimerRef) end,
    case CNRef of undefined -> ok; _ -> check_disconnect_to_all(Node, State#state{checknode_ref=undefined}) end,
    State2 = start_ping_group(State1),
    {noreply, State2};

% other
handle_info(_Info, State) ->
    %?OUTF(State, "DSERVICE '~s'. handle_info(~120p)", [State#state.regname, _Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
% returns nodes to synchronize unique strategy example started
get_synchronized_nodes(#state{funs=#funs{fun_nodes=FNodes}}=_State)
  when is_function(FNodes,0) ->
    case catch FNodes() of
        {'EXIT',_} -> [];
        T -> T
    end.

%% @private
%% returns real takeover setting, based on deserviced case
is_takeover(#state{opts=#opts{use_takeover=UseTakeover0}}) ->
    UseTakeover0 orelse case get(takeover_override) of true -> true; _ -> false end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initiates ping of synchronized nodes
start_ping_group(#state{funs=#funs{fun_out=FunOut},opts=#opts{use_heartbeat=UseHeartbeat,max_ping_times=MaxPingTimes},ping_times=PingTimes}=State) ->
    % @todo from config or local nodes
    {Self,Ref,Node} = {self(),make_ref(),node()},
    Ns = get_synchronized_nodes(State),
    State1 = State#state{last_nodes=Ns},
    case sort_nodes(Ns, State1) of
        [] -> wait(State1);
        [Node] ->
            Self ! {ping_reply,Ref,[]},
            State1#state{ping_ref=Ref,timerref=undefined};
        AllNodes ->
            Nodes = lists:delete(Node, AllNodes),
            #state{regname=RegName}=State1,
            UseDisconnect = not UseHeartbeat orelse PingTimes >= MaxPingTimes,
            case UseHeartbeat of
                true when UseDisconnect -> ?OUTF(State, "DSERVICE '~ts'. Ping(t) ~200tp", [State#state.regname,Nodes]); % log rarely on problems of heartbeat
                true -> ?OUTF(State, "DSERVICE '~ts'. Ping(d) ~200tp", [State#state.regname,Nodes]); % log rarely on problems of heartbeat
                false -> ok
            end,
            spawn_monitor(fun() -> Self ! {ping_reply,Ref,get_states_from_nodes(Nodes,RegName,?QUERY_TIMEOUT,UseDisconnect,FunOut)} end),
            State1#state{ping_ref=Ref,timerref=undefined}
    end.

%% --------------------------------------------------
%% async ping result
%%    state = started
%% --------------------------------------------------
%%do_handle_ping_result_on_started(Res, #state{opts=#opts{use_takeover=false}}=State) ->
%%    do_handle_ping_result_on_started_1(Res,State).
do_handle_ping_result_on_started(Res, #state{state='started'}=State) ->
    case lists:keysearch(takeover, 2, Res) of
        false -> do_handle_ping_result_on_started_1(Res,State);
        {value, {TNode,_}} ->
            outstart({takeover,TNode},State),
            ?OUTF(State, "DSERVICE '~s'. Found takeover node '~s'.", [State#state.regname,TNode]),
            case lists:keysearch(active, 2, Res) of
                false -> wait(set_active_node(TNode,State));
                {value, {ANode1, active}} -> wait(set_active_node(ANode1,State))
            end end.
%
do_handle_ping_result_on_started_1(Res, State) ->
    case lists:keysearch(activating, 2, Res) of
        false -> do_handle_ping_result_on_started_2(Res,State);
        {value, {ANode,_}} ->
            outstart({activating,ANode},State),
            ?OUTF(State, "DSERVICE '~s'. Found activating node '~s'.", [State#state.regname,ANode]),
            case lists:keysearch(active, 2, Res) of
                false -> wait(set_active_node(ANode,State));
                {value, {ANode1, active}} -> wait(set_active_node(ANode1,State))
            end end.
%
do_handle_ping_result_on_started_2(Res, State) ->
    MyNode = node(),
    UseTakeover = is_takeover(State),
    % if any node found active - ok or takeover
    % several started nodes uses common algorythm to choose master
    case lists:keysearch(active, 2, Res) of
        false ->
            StartedNodes = lists:filtermap(fun({Node,started}) -> {true,Node}; (_) -> false end, Res),
            case sort_nodes([MyNode|StartedNodes], State) of
                [MyNode|_] ->
                    outstart(master,State),
                    ANode = State#state.active_node,
                    case ?BU:get_by_key(ANode, Res, undefined) of
                        'started' -> % remain only started state of significant
                            % known as active node is follower, we've got waste heartbeat from there
                            checknode_start(State#state{active_node=undefined});
                        _ -> % remain only undefined state and absent in Res
                            % try to check previous leader node several times (prior to activate current follower)
                            checknode_start(State) % start_strategy_service(undefined,State);
                    end;
                [Node|_] ->
                    outstart({wait_master,Node},State),
                    wait(State)
            end;
        {value, {ANode,active}} when ANode/=MyNode, UseTakeover ->
            StartedNodes = lists:filtermap(fun({Node,started}) -> {true,Node}; (_) -> false end, Res),
            case sort_nodes([ANode,MyNode|StartedNodes], State) of
                [MyNode|_] ->
                    outstart({slave,ANode},State),
                    gen_server:cast(self(), {start_takeover,ANode}),
                    State1 = set_active_node(ANode,State),
                    State1#state{state=takeover,
                                 statestartts=?BU:timestamp(),
                                 timerref=undefined};
                _ ->
                    outstart({active,ANode},State),
                    wait(set_active_node(ANode,State))
            end;
        {value, {ANode,active}} ->
            outstart({active,ANode},State),
            wait(set_active_node(ANode,State))
    end.

%% --------
%% async ping result
%%    state = active
%% Case: No heartbeat used only
do_handle_ping_result_on_active(Res, #state{state='active',regname=RegName,opts=#opts{conflict_behaviour=ConflictBehaviour}}=State) ->
    MyNode = node(),
    ActiveNodes = lists:filtermap(fun({Node,'active'}) -> {true,Node}; (_) -> false end, Res),
    case sort_nodes([MyNode|ActiveNodes], State) of
        [MyNode] -> wait_active(State);
        [MyNode,NextNode|_] ->
            % Ping reply (no heartbeat). Current is primary active.
            % Multi-active. First primary found slave
            case ConflictBehaviour of
                restart_primary_service ->
                    ?OUTF(State, "DSERVICE '~s'. Found conflict by ping result (double 'active', current is primary, conflict to '~ts'). Auto 'restart_primary_service'...", [RegName,NextNode]),
                    please_activate_takeover(RegName, NextNode),
                    {ok,_,State2} = stop_strategy_service(NextNode,State),
                    State2;
                restart_primary_node ->
                    ?OUTF(State, "DSERVICE '~s'. Found conflict by ping result (double 'active', current is primary, conflict to '~ts'). Auto 'restart_primary_node'...", [RegName,NextNode]),
                    ?OUTF(State, "DSERVICE '~s'. Stop node", [State#state.regname]),
                    init:stop(),
                    State;
                _ ->
                    ?OUTF(State, "DSERVICE '~s'. Found conflict by ping result (double 'active', current is primary, conflict to '~ts'). No Action ('~ts')...", [RegName,NextNode,ConflictBehaviour]),
                    wait_active(State)
            end;
        [PrimNode|_] ->
            % Ping reply (no heartbeat). Current is slave active.
            % Multi-active. First slave found primary.
            case ConflictBehaviour of
                stop_slave_service ->
                    ?OUTF(State, "DSERVICE '~s'. Found conflict by ping result (double 'active', current is slave, conflict to '~ts'). Auto 'stop_slave_service'...", [RegName,PrimNode]),
                    {ok,_,State1} = stop_strategy_service(undefined,State),
                    State1;
                restart_slave_node ->
                    ?OUTF(State, "DSERVICE '~s'. Found conflict by ping result (double 'active', current is slave, conflict to '~ts'). Auto 'restart_slave_node'...", [RegName,PrimNode]),
                    ?OUTF(State, "DSERVICE '~s'. Stop node", [State#state.regname]),
                    init:stop(),
                    State;
                _ ->
                    ?OUTF(State, "DSERVICE '~s'. Found conflict by ping result (double 'active', current is slave, conflict to '~ts'). No Action ('~ts')...", [RegName,PrimNode,ConflictBehaviour]),
                    wait_active(State)
            end
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initiates heartbeat of synchronized nodes
start_heartbeat_group(#state{state=active,heartbeat_ref=HBRef}=State) ->
    {Self,Node} = {self(),node()},
    Ref = case HBRef of undefined -> make_ref(); _ -> HBRef end,
    Ns = get_synchronized_nodes(State),
    State1 = State#state{last_nodes=Ns},
    case sort_nodes(Ns, State1) of
        [] -> wait_active(State1);
        [Node] -> wait_active(State1); % it's me
        AllNodes ->
            Nodes = lists:delete(Node, AllNodes),
            #state{regname=RegName}=State1,
            spawn_monitor(fun() -> Self ! {heartbeat_reply,Ref,send_heartbeat(Nodes,RegName,?QUERY_TIMEOUT)} end),
            wait_active(State1#state{heartbeat_ref=Ref})
    end.

% --------------------------------------------------
% async heartbeat result
do_handle_heartbeat_result_active(Res,#state{state='active',regname=RegName,opts=#opts{conflict_behaviour=ConflictBehaviour}}=State) ->
    case lists:keysearch('takeover',2,Res) of
        {_,_} -> State;
        false ->
            MyNode = node(),
            AliveNodes = lists:filtermap(fun({Node,'started'}) -> {true,Node};
                ({Node,'active'}) -> {true,Node};
                ({Node,'activating'}) -> {true,Node};
                (_) -> false end, Res),
            case sort_nodes([MyNode|AliveNodes], State) of
                [MyNode|_] ->
                    % Heartbeat reply. Current is primary active.
                    % Here could be Multi-active. First primary found slave.
                    case lists:keyfind('active',2,Res) of
                        false -> State;
                        {NextNode,_} ->
                            case ConflictBehaviour of
                                'restart_primary_service' ->
                                    ?OUTF(State, "DSERVICE '~s'. Found conflict by heartbeat result (double 'active', current is primary, conflict to '~ts'). Auto 'restart_primary_service'...", [RegName,NextNode]),
                                    please_activate_takeover(RegName, NextNode),
                                    {ok,_,State2} = stop_strategy_service(NextNode,State),
                                    State2;
                                'restart_primary_node' ->
                                    ?OUTF(State, "DSERVICE '~s'. Found conflict by heartbeat result (double 'active', current is primary, conflict to '~ts'). Auto 'restart_primary_node'...", [RegName,NextNode]),
                                    ?OUTF(State, "DSERVICE '~s'. Stop node", [State#state.regname]),
                                    init:stop(),
                                    State;
                                _ ->
                                    ?OUTF(State, "DSERVICE '~s'. Found conflict by heartbeat result (double 'active', current is primary, conflict to '~ts'). No action ('~ts')...", [RegName,NextNode,ConflictBehaviour]),
                                    State
                            end
                    end;
                [PrimNode|_] ->
                    UseTakeover = is_takeover(State),
                    case lists:keyfind(PrimNode,1,Res) of
                        {_,NS} when NS=='started' and UseTakeover ->
                            % Slave node found master is started -> send takeover event there.
                            % Almost happy-way
                            ?OUTF(State, "DSERVICE '~s'. Send takeover notify to '~s'.", [State#state.regname,PrimNode]),
                            spawn(fun() -> send_takeover_event_to_leader(PrimNode,RegName,1000) end),
                            State;
                        {_,NS} when NS=='started' ->
                            State;
                        {_,NS} when NS=='active'; NS=='activating' ->
                            % Heartbeat reply. Current is slave active.
                            % Multi-active. First slave found primary.
                            case ConflictBehaviour of
                                stop_slave_service ->
                                    ?OUTF(State, "DSERVICE '~s'. Found conflict by heartbeat result (double 'active', current is slave, conflict to '~ts'). Auto 'stop_slave_service'...", [RegName,PrimNode]),
                                    {ok,_,State2} = stop_strategy_service(undefined,State),
                                    State2;
                                restart_slave_node ->
                                    ?OUTF(State, "DSERVICE '~s'. Found conflict by heartbeat result (double 'active', current is slave, conflict to '~ts'). Auto 'restart_slave_node'...", [RegName,PrimNode]),
                                    ?OUTF(State, "DSERVICE '~s'. Stop node", [State#state.regname]),
                                    init:stop(),
                                    State;
                                _ ->
                                    ?OUTF(State, "DSERVICE '~s'. Found conflict by heartbeat result (double 'active', current is slave, conflict to '~ts'). No action ('~ts')...", [RegName,PrimNode,ConflictBehaviour]),
                                    State
                            end
                    end
            end
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% @private
%% start takeover operation
start_takeover(Node, #state{regname=RegName}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Start takeover from '~s'.", [State#state.regname,Node]),
    {Self,Ref} = {self(),make_ref()},
    spawn_monitor(fun() -> Self ! {takeover_reply,Ref,call_takeover(Node,RegName,?TAKEOVER_TIMEOUT)} end),
    State#state{state=takeover,
                statestartts=?BU:timestamp(),
                takeover_ref=Ref,
                timerref=undefined}.

%% start takeover operation (while active). Emulate for follower's takeover event handling instead of restart of leader's node.
start_takeover_when_active(Node, #state{regname=RegName,state='active'}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Emulate takeover from '~s'.", [State#state.regname,Node]),
    {Self,Ref} = {self(),make_ref()},
    spawn_monitor(fun() -> Self ! {takeover_reply_when_active,Ref,call_takeover(Node,RegName,?TAKEOVER_TIMEOUT)} end),
    State#state{takeover_ref=Ref}.

% --------------------------------------------------
% after got takeover result
% if state = takeover
do_handle_result_takeover(undefined, #state{state=takeover}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Apply takeover got undefined. Wait iteration.", [State#state.regname]),
    wait(State#state{state='started',
                     statestartts=?BU:timestamp()});
do_handle_result_takeover({invalid_state,AtNode,AtNodeState}, #state{state=takeover}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Apply takeover skipped by '~ts' in '~ts'. Wait iteration.", [State#state.regname,AtNode,AtNodeState]),
    wait(State#state{state=started,
                     statestartts=?BU:timestamp()});
do_handle_result_takeover({block,ByNode}, #state{state='takeover'}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Apply takeover blocked by '~ts'. Wait iteration.", [State#state.regname,ByNode]),
    wait(State#state{state='started',
                     statestartts=?BU:timestamp()});
do_handle_result_takeover({ok,RemoteState}, #state{state=takeover}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Apply takeover.", [State#state.regname]),
    start_strategy_service(RemoteState,State).

%% after got takeover result on emulated when 'active'
%% if state = 'active'
do_handle_result_takeover_when_active(undefined, #state{state='active'}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Emulated takeover got undefined. Ignore.", [State#state.regname]),
    State;
do_handle_result_takeover_when_active({block,ByNode}, #state{state='active'}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Emulated takeover blocked by '~ts'. Stop node...", [State#state.regname,ByNode]),
    init:stop(),
    State;
do_handle_result_takeover_when_active({invalid_state,AtNode,AtNodeState}, #state{state='active'}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Emulated takeover found invalid_state ~120tp on '~ts'. Ignore.", [State#state.regname, AtNodeState, AtNode]),
    State;
do_handle_result_takeover_when_active({ok,_RemoteState}, #state{state='active'}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Emulated takeover OK. Ignore.", [State#state.regname]),
    State.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% try to call active_node if no active found on ping
checknode_start(#state{state=started,active_node=undefined}=State) ->
    ?OUTF(State, "DSERVICE '~ts'. Start as master.", [State#state.regname]),
    start_strategy_service(undefined,State#state{wait_times=0});
checknode_start(#state{state=started,opts=#opts{max_wait_times=MaxWaitTimes}}=State) when MaxWaitTimes<0 ->
    ?OUTF(State, "DSERVICE '~ts'. Start as master.", [State#state.regname]),
    start_strategy_service(undefined,State#state{active_node=undefined,wait_times=0});
checknode_start(#state{regname=RegName,state=started,active_node=ANode}=State) ->
    ?OUTF(State, "DSERVICE '~ts'. Check node '~s'.", [State#state.regname,ANode]),
    {Self,Ref} = {self(),make_ref()},
    spawn_monitor(fun() -> Self ! {checknode_reply,Ref,call_check_node(ANode,RegName,?QUERY_TIMEOUT)} end),
    State#state{checknode_ref=Ref,timerref=undefined}.

% --------------------------------------------------
% async checknode result (when no active found, but active_node is setup)
do_handle_checknode_result_started({ok,_}=Res,#state{state='started',active_node=ANode}=State) ->
    ?OUTF(State, "DSERVICE '~ts'. Check node '~ts' found active (result: ~120p).", [State#state.regname,ANode,Res]),
    wait(State#state{wait_times=0});
do_handle_checknode_result_started(_Res,#state{state='started',active_node=ANode,wait_times=WaitTimes,opts=#opts{max_wait_times=MaxWaitTimes}}=State) when WaitTimes<MaxWaitTimes ->
    ?OUTF(State, "DSERVICE '~ts'. Check node '~ts' ~120p times remain (result: ~120p).", [State#state.regname,ANode,MaxWaitTimes-WaitTimes,_Res]),
    wait(State#state{wait_times=WaitTimes+1});
do_handle_checknode_result_started(_Res,#state{state=started,active_node=ANode}=State) ->
    ?OUTF(State, "DSERVICE '~ts'. Failover. (Check node '~ts' result: ~120tp)", [State#state.regname,ANode,_Res]),
    catch check_disconnect_to_all(ANode,State),
    case lists:member(ANode,nodes(connected)) of
        true -> wait(State#state{wait_times=0});
        false -> start_strategy_service(undefined,State#state{wait_times=0})
    end.

% @private
check_disconnect_to_all(undefined,_) -> ok;
check_disconnect_to_all(_ANode,#state{funs=#funs{fun_out=FunOut}}=State) ->
    MyNode = node(),
    RegName = State#state.regname,
    Nodes = get_synchronized_nodes(State),
    ?OUTF(State, "DSERVICE '~s'. SYNC GROUP check nodes.", [RegName]),
    Opts = #{disconnect => true,
             ondisconnect => fun(Text) -> ?OUTF(FunOut, "DSERVICE '~s'. SYNC GROUP. ~ts", [RegName, Text]) end,
             set_group_leader => true,
             out => ?BU:str("PING SYNC GROUP by '~s' from '~s'",[RegName, MyNode]),
             timeout => 800},
    {R,E} = ?BLmulticall:map_result_rpc_multicall(?BLmulticall:call_direct(Nodes, {?BLping, ping_open, [{500,100}, Opts]}, 1000)),
    ?OUTF(State, "DSERVICE '~s'. SYNC GROUP check nodes result: ~n\tok: ~120p~n\terr: ~120p", [RegName, lists:zip(Nodes--E,R), E]),
    {ok,R}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% wait for next ping timer event
wait(State) ->
    Ref = make_ref(),
    State#state{ping_ref=Ref,
                timerref=erlang:send_after((State#state.opts)#opts.ping_timeout, self(), {?START_PING_MSG,Ref})}.

% wait for next heartbeat timer event
wait_active(#state{opts=#opts{use_heartbeat=false}}=State) -> wait(State);
wait_active(#state{heartbeat_ref=undefined}=State) -> wait_active(State#state{heartbeat_ref=make_ref()});
wait_active(#state{heartbeat_ref=HBRef}=State) ->
    State#state{timerref=erlang:send_after((State#state.opts)#opts.heartbeat_timeout, self(), {?START_HEARTBEAT_MSG,HBRef})}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start strategy
start_strategy_service(RemoteState, #state{funs=#funs{fun_start=FStart}}=State)
  when is_function(FStart) ->
    {Self,Ref} = {self(),make_ref()},
    spawn_monitor(fun() -> Self ! {activate_reply,Ref,activate(RemoteState,FStart)} end),
    ?OUTF(State, "DSERVICE '~s'. Activating.", [State#state.regname]),
    State#state{state=activating,
                statestartts=?BU:timestamp(),
                activate_ref=Ref,
                timerref=undefined}.

% @private
activate(_,FStart) when is_function(FStart,0) -> catch FStart();
activate(RemoteState,FStart) when is_function(FStart,1) -> catch FStart(RemoteState).

% ----------------
do_handle_activate({error,_}=Res,State) -> do_handle_activate_err(Res,State);
do_handle_activate({'EXIT',_}=Res,State) -> do_handle_activate_err(Res,State);
do_handle_activate(Res,State) -> do_handle_activate_ok(Res,State).

do_handle_activate_err(Res,#state{funs=#funs{fun_stop=FStop}}=State) ->
    ?OUTF(State, "DSERVICE '~s'. Start error: ~120p", [State#state.regname, Res]),
    catch FStop(),
    State1 = State#state{state=started,
                         statestartts=?BU:timestamp(),
                         timerref=undefined},
    start_ping_group(State1).

do_handle_activate_ok(_Res,#state{regname=RegName,funs=#funs{fun_stop=FStop},last_nodes=Nodes}=State) ->
    MyNode = node(),
    State1 = State#state{state=active,
                         statestartts=?BU:timestamp(),
                         active_node=MyNode,
                         timerref=undefined},
    case FStop of
        _ when is_function(FStop,0) -> ?BLmonitor:append_fun(self(),RegName,fun() -> catch FStop() end)
    end,
    spawn_monitor(fun() -> send_heartbeat(Nodes--[MyNode],RegName,?QUERY_TIMEOUT) end),
    wait_active(State1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%

% stop strategy
stop_strategy_service(FromNode,#state{regname=RegName,funs=#funs{fun_stop=FStop,fun_takeover_state=FTOS},timerref=TimerRef}=State)
  when is_function(FStop,0) ->
    Res = case is_function(FTOS,1) of
              true -> {ok, case catch FTOS(FromNode) of {'EXIT',_} -> undefined; T -> T end};
              _ -> {ok,undefined}
          end,
    catch FStop(),
    ?BLmonitor:drop_fun(self(),RegName),
    case TimerRef of undefined -> ok; _ -> erlang:cancel_timer(TimerRef) end,
    State1 = set_active_node(FromNode,State),
    State2 = wait(State1#state{state=started,
                               statestartts=?BU:timestamp(),
                               ping_ref=undefined,
                               heartbeat_ref=undefined,
                               takeover_ref=undefined}),
    {ok,Res,State2};
stop_strategy_service(_,State) ->
    {ok,{error,<<"Stop function undefined">>},State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set_active_node(Node, #state{active_node=Node}=State) -> State#state{wait_times=0};
set_active_node(undefined, #state{}=State) -> State#state{active_node=undefined,wait_times=0};
set_active_node(Node, #state{opts=#opts{use_monitor=UseMonitor},monitors=M}=State) ->
    State1 = State#state{active_node=Node,wait_times=0},
    case lists:keyfind(Node,1,M) of
        {_,_} -> State1;
        false when UseMonitor ->
            MRef = erlang:monitor_node(Node, true),
            State1#state{monitors=lists:keystore(Node,1,M,{Node,MRef})};
        false -> State1
    end.

% -----
% @private
sort_nodes(Nodes,#state{opts=#opts{autosort=false},last_nodes=LNodes}) ->
    LNodes--LNodes--lists:usort(Nodes);
sort_nodes(Nodes,_State) ->
    lists:usort(Nodes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% @private
get_states_from_nodes(Nodes,RegName,Timeout,UseDisconnect,FunOut) ->
    SelfNode = node(),
    PingOpts = #{use_ping => true,
                 ping_timeout => 1000,
                 cache_timeout => 0,
                 disconnect => UseDisconnect,
                 ondisconnect => fun(Text) -> ?OUTF(FunOut, "DSERVICE '~s'. get_states. ~ts", [RegName, Text]) end},
    Res = ?BLmulticall:call_direct(Nodes, {?MODULE, get_state, [RegName,SelfNode]}, Timeout, PingOpts),
    %Res = rpc_multicall(Nodes, ?MODULE, get_state, [RegName,SelfNode], Timeout), % no disconnect
    lists:map(fun({Node,{ok,S}}) -> {Node,S}; (R) -> R end, Res).

%% @private
send_heartbeat(Nodes,RegName,Timeout) ->
    SelfNode = node(),
    %Res = ?BLmulticall:call_direct(Nodes, {?MODULE, heartbeat, [RegName,SelfNode]}, Timeout),
    Res = rpc_multicall(Nodes, ?MODULE, heartbeat, [RegName,SelfNode], Timeout), % no disconnect
    lists:map(fun({Node,{ok,S}}) -> {Node,S}; (R) -> R end, Res).

% @private
call_takeover(Node,RegName,Timeout) ->
    SelfNode = node(),
    rpc_call(Node, ?MODULE, takeover, [RegName,SelfNode], Timeout).

% @private
call_check_node(Node,RegName,Timeout) ->
    SelfNode = node(),
    rpc_call(Node, ?MODULE, check_node, [RegName,SelfNode], Timeout).

% @private
send_takeover_event_to_leader(Node,RegName,Timeout) ->
    SelfNode = node(),
    rpc_call(Node, ?MODULE, please_activate_takeover, [RegName,SelfNode], Timeout).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% @private
rpc_call(Node,Module,Fun,Args,Timeout) ->
    % rpc:call/5 doesn't hang on tcp misconnect, but forward exit, throw, error
    Default = undefined,
    case catch rpc:call(Node, Module, Fun, Args, Timeout) of
        {'EXIT',_} -> Default;
        %{badrpc,timeout} -> undefined; % rpc timeout. may be connection is failed
        %{badrpc,nodedown} -> undefined;
        {badrpc,_} -> Default;
        T -> T
    end.

%% @private
rpc_multicall(Nodes,Module,Fun,Args,Timeout) ->
    Funs = [{Node, fun() -> catch rpc:call(Node, Module, Fun, Args, Timeout) end} || Node <- Nodes],
    [{Node,Result} || {{Node,_},Result} <- ?BLmulticall:apply(Funs, Timeout)].

% @private
out(#state{funs=#funs{fun_out=undefined}},Fmt,Args) -> ?OUTC('$info',Fmt,Args);
out(#state{funs=#funs{fun_out=OutF}},Fmt,Args) when is_function(OutF,2) -> OutF(Fmt,Args);
out(undefined,Fmt,Args) -> ?OUTC('$info',Fmt,Args);
out(OutF,Fmt,Args) when is_function(OutF,2) -> OutF(Fmt,Args).

% @private
% log starting state after first ping
outstart(_,#state{inited=true}) -> ok;
outstart({takeover,Node},State) ->
    ?OUTF(State, "DSERVICE '~s'. START. Found takeover node '~s'.", [State#state.regname,Node]);
outstart({activating,Node},State) ->
    ?OUTF(State, "DSERVICE '~s'. START. Found activating node '~s'.", [State#state.regname,Node]);
outstart(master,State) ->
    ?OUTF(State, "DSERVICE '~s'. START. Empty, as master", [State#state.regname]);
outstart({wait_master,Node},State) ->
    ?OUTF(State, "DSERVICE '~s'. START. Empty, as slave, master node '~s'.", [State#state.regname,Node]);
outstart({slave,Node},State) ->
    ?OUTF(State, "DSERVICE '~s'. START. Found slave node '~s'.", [State#state.regname,Node]);
outstart({active,Node},State) ->
    ?OUTF(State, "DSERVICE '~s'. START. Found master node '~s'.", [State#state.regname,Node]).

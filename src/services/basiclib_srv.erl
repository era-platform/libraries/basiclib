%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.10.2015
%%% @doc Service of custom optional storage.
%%%        Set opts, get opts, del opts.
%%%        Also set group_leader to arbitrary process.

-module(basiclib_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1, stop/0,
         get_env/1, set_env/2, unset_env/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(state, {
    ets,
    groupleaderpid
  }).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, StartArgs, []).

stop() ->
    gen_server:stop(?MODULE).

%% -------------------------

%% Gets environment value
get_env(Key) ->
    gen_server:call(?MODULE, {get_env, [Key]}).

%% Set environment value
set_env(Key, Value) ->
    gen_server:cast(?MODULE, {set_env, [Key, Value]}).

%% Unset environment value
unset_env(Key) ->
    gen_server:cast(?MODULE, {unset_env, [Key]}).

%% Appends item to environment list-value
append_env(Key, Value) ->
    gen_server:cast(?MODULE, {append_env, [Key, Value]}).

%% Drops value item from environment list-value
dropvalue_env(Key, Value) ->
    gen_server:cast(?MODULE, {dropvalue_env, [Key, Value]}).

%% Print all values
print_all() ->
    gen_server:cast(?MODULE, {print_all}).

%% Get all
get_all() ->
    gen_server:call(?MODULE, {get_all}).

%% -------------------------

%% returns group_leader pid
get_group_leader() ->
    gen_server:call(?MODULE, {get_group_leader}).

%% set group_leader for calling pid
set_group_leader() ->
    group_leader(gen_server:call(?MODULE, {get_group_leader}), self()).

%% -------------------------

%% test crash
crash() ->
    gen_server:call(?MODULE, {crash}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Args) ->
    State = #state{ets=ets:new(env, [set]),
                   groupleaderpid=group_leader()},
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% get value
handle_call({get_env, [Key]}, _From, #state{ets=ETS}=State) ->
    Reply = case ets:lookup(ETS, Key) of
                [] -> undefined;
                [{_, Value}] -> {ok, Value}
            end,
    {reply, Reply, State};

%% set value
handle_call({set_env,_}=Cmd, _From, #state{}=State) ->
    handle_cast(Cmd, State),
    {reply, ok, State};

%% unset value
handle_call({unset_env,_}=Cmd, _From, #state{}=State) ->
    handle_cast(Cmd, State),
    {reply, ok, State};

%% append value to list
handle_call({append_env,_}=Cmd, _From, #state{}=State) ->
    handle_cast(Cmd, State),
    {reply, ok, State};

%% drop value from list
handle_call({dropvalue_env,_}=Cmd, _From, #state{}=State) ->
    handle_cast(Cmd, State),
    {reply, ok, State};

%% get all
handle_call({get_all}, _From, #state{ets=ETS}=State) ->
    Reply = ets:foldl(fun({Key, Val}, AccList) -> [{Key, Val}|AccList] end, [], ETS),
    {reply, Reply, State};

%% get group leader for console io operations
handle_call({get_group_leader}, _From, #state{groupleaderpid=Pid}=State) ->
    {reply, Pid, State};

%% crash test
handle_call({restart}, _From, State) ->
    {stop, normal, State};

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% set value
handle_cast({set_env, [Key, Value]}, #state{ets=ETS}=State) ->
    ets:insert(ETS, {Key, Value}),
    {noreply, State};

%% unset value
handle_cast({unset_env, [Key]}, #state{ets=ETS}=State) ->
    ets:delete(ETS, Key),
    {noreply, State};

%% append value to list
handle_cast({append_env, [Key, Value]}, #state{ets=ETS}=State) ->
    case ets:lookup(ETS, Key) of
        [] -> ets:insert(ETS, {Key, [Value]});
        [{_, List}] ->
            case lists:member(Value, List) of
                true -> ok;
                false -> ets:insert(ETS, {Key, [Value|List]})
            end
    end,
    {noreply, State};

%% drop value from list
handle_cast({dropvalue_env, [Key, Value]}, #state{ets=ETS}=State) ->
    case ets:lookup(ETS, Key) of
        [] -> ok;
        [{_, [Value]}] -> ets:delete(ETS, Key);
        [{_, List}] -> ets:insert(ETS, {Key, lists:delete(Value, List)})
    end,
    {noreply, State};

%% get all
handle_cast({print_all}, #state{ets=ETS}=State) ->
    List = ets:foldl(fun({Key, Val}, AccList) -> [{Key, Val}|AccList] end, [], ETS),
    ?OUTC('$force',"OptsSrv. All vars: ~120tp", [List]),
    {noreply, State};

%% restart
handle_cast({restart}, State) ->
     {stop, restart, State};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ====================================================================
%% Internal functions
%% ====================================================================

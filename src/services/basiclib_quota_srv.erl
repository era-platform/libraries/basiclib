%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2023 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 09.12.2023
%%% @doc Quotation srv to pass quota and handle queue of requests by groups
%%%      First setup_group, then request_quota, wait 'pass' info, work and release

-module(basiclib_quota_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([
    setup_group/2,
    request_quota/3,
    release_quota/2,
    release_quota/1
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(actor, {
    pid :: pid(), % pid of actor, requested quota
    monref :: reference(), % monitoring reference of actor
    info :: term() % some info about actor
}).

-record(group, {
    limit :: integer(), % max of passed quotas, setup before usage
    active = [] :: [#actor{}], % passed actors
    waiting = [] :: [#actor{}] % waiting actors
}).

-record(qstate, {
    ref :: reference(),
    ets :: ets:tab(), % {GroupKey, #group{}}
    links :: ets:tab() % % {MonRef, {GroupKey,Pid,MonRef}} | {Pid, {GroupKey,Pid,MonRef}}
}).

%% ====================================================================
%% Public functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% Setup group before usage
-spec setup_group(GroupKey::term(), Opts::map()) -> ok.
setup_group(GroupKey, Opts) ->
    gen_server:call(?MODULE, {setup_group, GroupKey, Opts}).

%% Some group's actor ask quota
-spec request_quota(GroupKey::term(), FromPid::pid(), Info::term()) -> {ok,SvcPid::pid()} | {error, group_not_found | already_exists}.
request_quota(GroupKey, FromPid, Info) ->
    gen_server:call(?MODULE, {request_quota, GroupKey, FromPid, Info}).

%% Some group's actor release quota
-spec release_quota(GroupKey::term(), FromPid::pid()) -> ok.
release_quota(GroupKey, FromPid) ->
    gen_server:cast(?MODULE, {release_quota, GroupKey, FromPid}).

%% Some group's actor release quota
-spec release_quota(FromPid::pid()) -> ok.
release_quota(FromPid) ->
    gen_server:cast(?MODULE, {release_quota, FromPid}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    Ref = make_ref(),
    State = #qstate{ref=Ref,
                    ets = ets:new(quota, [set]),
                    links = ets:new(lnk, [set])},
    self() ! {init,Ref},
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% setup group before usage
handle_call({setup_group,GroupKey,Opts}, _From, #qstate{ets=Ets}=State) ->
    Limit = ?BU:get_by_key(limit,Opts,1),
    case ets:lookup(Ets,GroupKey) of
        [] ->
            Group = #group{limit=Limit,
                           active=[],
                           waiting=[]},
            ets:insert(Ets, {GroupKey,Group});
        [{GroupKey,#group{limit=Limit0,waiting=W}=Group0}] ->
            Group = Group0#group{limit=Limit},
            case Limit > Limit0 of
                true when length(W) > 0 ->
                    [NewItem|RestW]=W,
                    Group1 = activate(NewItem, Group#group{waiting=RestW}),
                    ets:insert(Ets, {GroupKey, Group1});
                _ ->
                    ok
            end
    end,
    {reply, ok, State};

%% request of quota from some group's actor pid
handle_call({request_quota,GroupKey,FromPid,Info}, _From, #qstate{ets=Ets,links=Links}=State)
  when is_pid(FromPid) ->
    Fun = fun(Group0) ->
                #group{active=A,waiting=W,limit=Limit}=Group0,
                MonRef = erlang:monitor(process,FromPid),
                ets:insert(Links,{FromPid,{GroupKey,FromPid,MonRef}}),
                ets:insert(Links,{MonRef,{GroupKey,FromPid,MonRef}}),
                Item = #actor{pid=FromPid, monref=MonRef, info=Info},
                Group1 = case length(A) of
                             L when L >= Limit ->
                                 Group0#group{waiting = store(Item,W)};
                             _ ->
                                 activate(Item,Group0)
                         end,
                ets:insert(Ets,{GroupKey,Group1}),
                {reply, {ok,self()}, State}
          end,
    case ets:lookup(Links,FromPid) of
        [_] -> {reply, {error,already_exists}, State};
        [] ->
            case ets:lookup(Ets,GroupKey) of
                [] ->
                    %{reply, {error,group_not_found}, State};
                    Fun(#group{limit=1,waiting=[],active=[]});
                [{GroupKey, Group0}] ->
                    Fun(Group0)

            end
    end;

%% --------------
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% release of quota from some group's actor pid
handle_cast({release_quota,FromPid}, #qstate{links=Links}=State) ->
    case ets:lookup(Links,FromPid) of
        [] -> {noreply,State};
        [{_,{GroupKey,FromPid,_}}] ->
            handle_cast({release_quota,GroupKey,FromPid}, State)
    end;

handle_cast({release_quota,GroupKey,FromPid}, #qstate{ets=Ets, links=Links}=State) ->
    case ets:lookup(Ets,GroupKey) of
        [] -> ok;
        [{GroupKey,Group0}] ->
            FunDemonitor = fun(#actor{pid=Pid,monref=MonRef}) ->
                                erlang:demonitor(MonRef),
                                ets:delete(Links,Pid),
                                ets:delete(Links,MonRef)
                           end,
            #group{active=A,waiting=W,limit=Limit}=Group0,
            case take(FromPid,A) of
                {value, #actor{}=Item, RestA} when length(W)>0, length(RestA)<Limit ->
                    FunDemonitor(Item),
                    [NewItem|RestW]=W,
                    Group1 = activate(NewItem, Group0#group{active=RestA, waiting=RestW}),
                    ets:insert(Ets,{GroupKey,Group1});
                {value, #actor{}=Item, RestA} ->
                    FunDemonitor(Item),
                    Group1 = Group0#group{active = RestA},
                    ets:insert(Ets,{GroupKey,Group1});
                false ->
                    case take(FromPid,W) of
                        {value, #actor{}=Item, RestW} ->
                            FunDemonitor(Item),
                            Group1 = Group0#group{waiting = RestW},
                            ets:insert(Ets,{GroupKey,Group1});
                        false ->
                            ok
                    end
            end
    end,
    {noreply, State};

%% --------------
%% other
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({init,Ref}, #qstate{ref=Ref}=State) ->
    {noreply,State};

%% When request pid DOWN
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #qstate{ets=Ets,links=Links}=State) ->
    case ets:lookup(Links,MonRef) of
        [] -> ok;
        [{MonRef,{GroupKey,Pid,MonRef}}] ->
            ets:delete(Links,Pid),
            ets:delete(Links,MonRef),
            case ets:lookup(Ets,GroupKey) of
                [] -> ok;
                [{GroupKey,Group0}] ->
                    #group{active=A,waiting=W,limit=Limit}=Group0,
                    case take(Pid,A) of
                        {value, #actor{monref=MonRef}, RestA} when length(W)>0, length(RestA)<Limit ->
                            [NewItem|RestW]=W,
                            Group1 = activate(NewItem,Group0#group{active=RestA, waiting=RestW}),
                            ets:insert(Ets,{GroupKey,Group1});
                        {value, #actor{monref=MonRef}, RestA} ->
                            Group1 = Group0#group{active = RestA},
                            ets:insert(Ets,{GroupKey,Group1});
                        false ->
                            case take(Pid,W) of
                                {value, #actor{monref=MonRef}, RestW} ->
                                    Group1 = Group0#group{waiting = RestW},
                                    ets:insert(Ets,{GroupKey,Group1});
                                false ->
                                    ok
                            end
                    end
            end
    end,
    {noreply,State};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #qstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

store(#actor{pid=Pid}=Item,Coll) ->
    lists:keystore(Pid,1,Coll,Item).

take(Key,Coll) ->
    case lists:keytake(Key,#actor.pid,Coll) of
        false -> false;
        {value,#actor{}=Item,Rest} -> {value,Item,Rest}
    end.

%% -----------------
activate(#actor{pid=Pid}=Item, #group{active=A}=Group) ->
    Pid ! 'pass',
    Group#group{active = store(Item,A)}.
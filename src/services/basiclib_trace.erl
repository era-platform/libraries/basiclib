%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.12.2016
%%% @doc Service of tracing on spawned pid.
%%%        start/0 returns function/1 to trace
%%%        stop/1 takes this function to stop and return {ok,Trace::list()}

-module(basiclib_trace).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/0,start/1,
         stop/1,stop/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------
%% Starts trace process and return anonymous trace function (call with 'stop' to terminate, other put to trace)
%% ------------------------
-spec start() -> F::function().
%% ------------------------
start() ->
    start(10000).

%% ------------------------
-spec start(TimeoutWaitMs::integer()) -> F::function().
%% ------------------------
start(TimeoutWaitMs) ->
    Ref = make_ref(),
    TracePid = spawn(fun() -> trace(Ref,TimeoutWaitMs) end),
    fun({stop,TimeoutStopMs}) ->
            TracePid ! {stop,self(),Ref},
            receive
                {ok,Ref,Trace} -> {ok,Trace};
                ok -> {ok,[]}
            after TimeoutStopMs -> undefined
            end;
       (A) -> TracePid ! {trace,Ref,A}, ok
    end.

%% ------------------------
%% Stops trace process and return collected trace
%% ------------------------
-spec stop(F::function()) -> {ok,Trace::list()}.
%% ------------------------
stop(F) when is_function(F,1) ->
    stop(F,1000);
stop(_) -> {ok,[]}.

%% ------------------------
-spec stop(F::function(),TimeoutStopMs::integer()) -> {ok,Trace::list()}.
%% ------------------------
stop(F,TimeoutStopMs) when is_function(F,1) ->
    case F({stop,TimeoutStopMs}) of
        {ok,Trace} -> {ok,Trace};
        undefined -> undefined
    end;
stop(_,_) -> {ok,[]}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% -----------------------------
% Trace
% -----------------------------

% @private @spawned collector
trace(Ref,Timeout) ->
    trace_loop(Ref, Timeout, []).
trace_loop(Ref, Timeout, Acc) ->
    receive
        {stop,Pid,Ref} -> Pid ! {ok,Ref,Acc};
        {trace,Ref,K} -> trace_loop(Ref,Timeout,[K|Acc]);
        _ -> trace_loop(Ref,Timeout,Acc)
    after Timeout -> ok
    end.

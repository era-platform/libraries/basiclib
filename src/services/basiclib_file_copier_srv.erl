%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.06.2021
%%% @doc Service of file copier from one node to another.
%%%      Uses traffic limit,
%%%      Log errors.
%%%      TODO: allow to copy from remote site

-module(basiclib_file_copier_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([copy_file/3, copy_file/4, copy_file/5]).

-export([get_file_io/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(state, {
    file_count = 0,
    ref = make_ref()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).


%% ----------------------------
%% Copies file from from SrcPath of {Site,Node} into local DestPath. Make difference between remote site operations and local site operations.
%% ----------------------------
-spec copy_file(FromNode::node(), SrcPath::string(), DestPath::string()) -> ok | {error,Reason::term()}.
%% ----------------------------
copy_file(FromNode, FromPath, ToPath) ->
    gen_server:call(?MODULE, {copy_file, FromNode, FromPath, ToPath, fun(_,_,_) -> ok end, 65536000}, 600000).

%% ----------------------------
-spec copy_file(FromNode::node(), SrcPath::string(), DestPath::string(), FunLog::function()) -> ok | {error,Reason::term()}.
%% ----------------------------
copy_file(FromNode, FromPath, ToPath, FunLog)
  when (is_list(FromPath) orelse is_binary(FromPath)) andalso (is_list(ToPath) orelse is_binary(ToPath)), is_function(FunLog,3) ->
    gen_server:call(?MODULE, {copy_file, FromNode, ?BU:to_unicode_list(FromPath), ?BU:to_unicode_list(ToPath), FunLog, 65536000}, 600000).

%% ----------------------------
-spec copy_file(FromNode::node(), SrcPath::string(), DestPath::string(), FunLog::function(), Bandwidth::non_neg_integer()) -> ok | {error,Reason::term()}.
%% ----------------------------
copy_file(FromNode, FromPath, ToPath, FunLog, Bandwidth)
    when (is_list(FromPath) orelse is_binary(FromPath)) andalso (is_list(ToPath) orelse is_binary(ToPath)), is_function(FunLog,3) ->
    gen_server:call(?MODULE, {copy_file, FromNode, ?BU:to_unicode_list(FromPath), ?BU:to_unicode_list(ToPath), FunLog, Bandwidth}, 600000).

%% ----------------------------
%% Returns opened file io (could be used only in local site operations, when nodes could be connected directly)
%% ----------------------------
-spec get_file_io(FilePath::string()) -> {ok,RF::pid()} | {error,Reason::term()}.
%% ----------------------------
get_file_io(FilePath) when is_list(FilePath) ->
    ?BLopts:set_group_leader(),
    Self = self(),
    Ref = make_ref(),
    F = fun() ->
            case ?BLfilelib:is_file(FilePath) of
                false ->
                    Err = {error, file_not_found},
                    Self ! {reply, Err, Ref};
                true ->
                    case ?BLfile:read_file_info(FilePath) of
                        {error,_Reason}=Err ->
                            Self ! {reply, Err, Ref};
                        {ok, FI} ->
                            case ?BLfile:open(FilePath, [read, binary]) of
                                {error,_Reason}=Err ->
                                    Self ! {reply, Err, Ref};
                                {ok,RF} ->
                                    Self ! {reply, {ok,RF,FI}, Ref},
                                    erlang:link(RF),
                                    Ref1 = erlang:monitor(process,RF),
                                    receive
                                        {'DOWN',Ref1,process,RF,_} -> ok
                                    after
                                        3600000 -> ok
                                    end end end end end,
    {Pid,MonRef} = spawn_monitor(F),
    receive
        {reply,Reply,Ref} -> Reply;
        {'DOWN',MonRef,process,Pid,Reason} ->
            ?LOG('$error', "File open for read error: '~ts' -> ~120tp",[FilePath, Reason]),
            {error, {internal_error, <<"File open for read error">>}}
    after
        1000 ->
            ?LOG('$warning', "File open for read timeout: '~ts'",[FilePath]),
            {error, {internal_error, <<"File open for read timeout">>}}
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    State1 = #state{},
    ?LOG('$info', "~ts: filecopier srv inited", [?APP]),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call({copy_file, FromNode, FromPath, ToPath, FLOG, Bandwidth}, _From, #state{file_count=FC}=State) when is_atom(FromNode) ->
    case FC rem 100 of
        0 -> timer:sleep(10);
        _ -> ok
    end,
    Reply = do_copy_file(FromNode, FromPath, ToPath, FLOG, Bandwidth),
    {reply, Reply, State#state{file_count=FC+1}};

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({print}, State) ->
    ?LOG('$force', "FileCopier state: ~140p", [State]),
    {noreply, State};

handle_cast({restart}, State) ->
    {stop, normal, State};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
%% --------------------------------------
%% Copy from current site
%% --------------------------------------
do_copy_file(FromNode, FromPath, ToPath, FLOG, Bandwidth) ->
    %MFA = {?ENV, get_file_io, [SrcPath, [read, binary]]},
    {M,F,A} = {?MODULE, get_file_io, [FromPath]},
    State1 = case ?BLrpc:call(FromNode, M, F, A, 5000) of
                 {ok,RF,FI} ->
                     try do_copy_file_1(RF,FI,{FromNode,FromPath,ToPath,FLOG,Bandwidth})
                     catch _:_ ->
                         ?BLfile:close(RF),
                         {error,internal_error}
                     end;
                 {error,file_not_found}=Err ->
                     FLOG('$error', "Copy. Destination file '~ts:/~ts' not found",[FromNode,FromPath]),
                     Err;
                 {error,_}=Err ->
                     FLOG('$error', "Copy. Request for file '~ts:/~ts' failed: ~n\t~120tp",[FromNode,FromPath,Err]),
                     Err;
                 {badrpc,_}=BadRpc ->
                     FLOG('$warning', "Copy. Request for file '~ts:/~ts' failed: ~n\t~120tp",[FromNode,FromPath,BadRpc]),
                     {error,badrpc}
             end,
    State1.

%% @private
do_copy_file_1(RF,FI, {_FromNode,_FromPath,ToPath,FLOG,_Bandwidth}=P) ->
    ?BLfilelib:ensure_dir(ToPath),
    case ?BLfile:open(ToPath, [raw, write, {delayed_write,16384,100}]) of
        {error,_}=Err ->
            FLOG('$error', "Copy. File open for write error: ~120p -> ~120p",[ToPath, Err]),
            R = Err;
        {ok,WF} ->
            R = do_write_file(WF, RF, P, 1),
            ?BLfile:close(WF),
            case ?BLfile:write_file_info(ToPath, FI) of
                {error,_}=Err2 ->
                    FLOG('$error', "Copy. File open for write error: ~120p -> ~120p",[ToPath, Err2]),
                    R = Err2;
                ok -> R
            end end,
    ?BLfile:close(RF),
    R.

%% @private
do_write_file(WF, RF, {_FromNode,_FromPath,ToPath,FLOG,Bandwidth}=P, Cnt) ->
    case ?BLfile:read(RF, 65536) of
        eof ->
            FLOG('$info', "Copy. File copied: '~ts'", [ToPath]),
            ok;
        {error,_}=Err ->
            FLOG('$error', "Copy. File read error: '~ts', ~120tp", [ToPath, Err]),
            Err;
        {ok,Data} ->
            ?BLfile:write(WF, Data),
            pause_traffic(Cnt,Bandwidth), % limit speed/net traffic load
            do_write_file(WF, RF, P, Cnt+1)
    end.

%% @private
pause_traffic(Cnt,Bandwidth) ->
    % 10 portions / 10 ms -> <60 MB/sec
    Portion = case Bandwidth of
                  B when B =< 65536000 -> 10;
                  B when B =< 655360000 -> 100;
                  _ -> 1000
              end,
    Pause = ?BU:to_int((65536000 * Portion - 1)/ Bandwidth) + 1,
    case Cnt rem Portion of
        0 -> timer:sleep(Pause);
        _ -> ok
    end.
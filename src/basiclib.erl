%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.12.2015
%%% @doc Basliclib application.
%%%   Application could be started, then gen_servers initiate and services could be used:
%%%      monitor, store, ping, leader, distrapp, db_pg_access, bestinterface, encoding convertation
%%%   * Store allow to store/get/transaction modify/lazy functions with auto deletion or not.
%%%        Could be also used limitedly if app is not running. Store could be used as a template server by decorators.
%%%   * Monitor allow to register fin functions, that should be applied if selected process found down.
%%%   * Ping is applied in spawned mode to guarantee timeout. Result of ping is cached to avoid "DDoS".
%%%   * SyncStrategy allows to interact several examples of some distributed service and choose leader (supports failover,takeover).
%%%   * DistrApp allows to implement automatically distributed app (instead of erlang's default dapp, which has bugs on network problems).
%%%   * Workers allow to make limited parallel requests preserving some sequences (hashed by same key).
%%%   * Multicall allow to make parallel requests or to apply functions simultaneously and return integrated result.
%%%   * Log allow to write to console and to attached multi-file logger.
%%%   * DB allow to connect and make requests to postgresql.
%%%   * ... many other pure functions and helpers.
%%%   Application could be setup by application:set_env, by prepare(Opts), by start(Opts), by update_opts(Opts):
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','bl'}
%%%      boot_app_function
%%%             :: function() -> ok
%%%          Function is called when distrapp found that application is stopped.
%%%          Default: App:start(Opts) | App:start() | application:start(App)
%%%      node_index :: integer()
%%%          Node unique static index (0-1000000) in total cluster configuration to build addressing identifiers.
%%%          Default: phash2 % 1000000.
%%%      max_loglevel :: atom() | non_neg_integer()
%%%             :: '$crash' = 1 | '$error' = 2 | '$warning' = 3 | '$info' = 4 | '$trace' = 5 | '$debug' = 6
%%%          Current loglevel filter on node.
%%%          Default: '$info'
%%%      log_function :: function/2
%%%             :: function(LogFile::{Folder::atom(),FilePrefix::atom()}, Arg::string() | {Fmt::string(),Args::[term()]}
%%%          Called to write to some logger. Used by log facade.
%%%          Default: write to console
%%%      basic_log_dir :: string()
%%%          Absolute or cwd-relative path where log files are located.
%%%          Default: "logs"
%%%      log_dir_function
%%%             :: function(X::string()) -> string()
%%%          Called to detect absolute or cwd-relative folder of LogFile.
%%%          Default: basic_log_dir()/X
%%% -------------------------------------------------------------------
%%%
%%% -------------------------------------------------------------------

-module(basiclib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0,
         stop/0]).

-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% --------------------------------------
%% Starts application by loader
%% --------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% --------------------------------------
%% Stops application
%% --------------------------------------
stop() ->
    application:stop(?APP).

%% ===================================================================
%% Application callback functions
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() -> ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() -> ok.
%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.04.2016
%%% @doc Guid operations services

-module(basiclib_uuid).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([empty/0,
         new/0,
         new_v3/1,
         uuid_4122/0,
         luid/0]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

empty() -> <<"00000000-0000-0000-0000-000000000000">>.

new() -> uuid_4122().

uuid_4122() ->
    Rand = hex(crypto:strong_rand_bytes(4)),
    <<A:16/bitstring, B:16/bitstring, C:16/bitstring>> = <<(?BU:timestamp()):48>>,
    Hw = get_hwaddr(),
    <<Rand/binary, $-, (hex(A))/binary, $-, (hex(B))/binary, $-,
      (hex(C))/binary, $-, Hw/binary>>.

new_v3(Data) ->
    UUID = get_uuidv3(Data),
    uuid_to_string(UUID,binary_standard).


%% @doc Generates a new printable random UUID.
luid() -> lhash({make_ref(), os:timestamp()}).

%% ------------------------------------------------
% IID: InviteId
% Supported srvidx up to 281474976710656
%% ------------------------------------------------

%% ----------------------------
%% build id (uuid) by {random,time,srvidx}
%% ----------------------------
uuid_srvidx() ->
    Rand = hex(crypto:strong_rand_bytes(4)),
    %<<A:16/bitstring, B:16/bitstring, C:16/bitstring>> = <<(?BU:timestamp()):48>>,
    %<<A:16/bitstring, B:16/bitstring, C:16/bitstring, D:8/bitstring>> = <<(?BU:timestamp()):56>>,
    {X1,X2,X3} = os:timestamp(),
    <<A:16/bitstring,B:16/bitstring,C:16/bitstring,D:8/bitstring>> = <<X1:16,X2:20,X3:20>>,
    NodeIdx = case ?CFG:node_index() of
                  N when N > 0 -> N;
                  _ -> 0
              end,
    <<Rand/binary, $-,
      (hex(A))/binary, $-, (hex(B))/binary, $-, (hex(C))/binary, $-,
      (hex(D))/binary, (?BU:strbin("~10.16.0b",[NodeIdx]))/binary>>.

%% ----------------------------
%% return srvidx from uuid
%% ----------------------------
parse_uuid_srvidx(<<_:26/binary,X/binary>>) ->
    case catch erlang:binary_to_integer(X,16) of
        {'EXIT', _} -> -1;
        V -> V
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private Finds the MAC addr for enX or ethX, or a random one if none is found
-ifdef(TEST).
get_hwaddr() ->
    get_hw_addr_1().
-else.
get_hwaddr() ->
    OptsMap = #{store_timeout => 20000,
                expire_timeout => 10000,
                update_timeout => 1000,
                store_tran => true,
                sync_update => true},
    ?BLstore:lazy_t(hw_addrs,fun get_hw_addr_1/0,OptsMap).
-endif.

%% @private
get_hw_addr_1() ->
    {ok, Addrs} = inet:getifaddrs(),
    get_hwaddrs(Addrs).

%% @private
get_hwaddrs([{Name, Data}|Rest]) ->
    F = fun() ->
                case ?BU:get_by_key(hwaddr,Data,undefined) of
                    Hw when is_list(Hw), length(Hw)==6 -> hex(Hw);
                    _ -> get_hwaddrs(Rest)
                end end,
    case Name of
        "en"++_ -> F();
        "eth"++_ -> F();
        "\\DEVICE\\"++_ -> F();
        _ ->
            get_hwaddrs(Rest)
    end;

get_hwaddrs([]) ->
    hex(crypto:strong_rand_bytes(6)).



%% @private
-spec hex(binary()|string()) ->
                 binary().

hex(B) when is_binary(B) -> hex(binary_to_list(B), []);
hex(S) -> hex(S, []).


%% @private
hex([], Res) -> list_to_binary(lists:reverse(Res));
hex([N|Ns], Acc) -> hex(Ns, [digit(N rem 16), digit(N div 16)|Acc]).


%% @private
digit(D) when (D >= 0) and (D < 10) -> D + 48;
digit(D) -> D + 87.


%%-------------------------------------------------------------------------
%% UUID V3
%%-------------------------------------------------------------------------

%%-spec get_v3(Data :: binary()) -> uuid().
get_uuidv3(Data) when is_binary(Data) ->
    <<B1:48, B4a:4, B2:12, B4b:2, B3:14, B4c:48>> =
        crypto:hash(md5, Data),
    B4 = (B4a bxor B4b) bxor B4c,
    <<B1:48,
      0:1, 0:1, 1:1, 1:1,  % version 3 bits
      B2:12,
      1:1, 0:1,            % RFC 4122 variant bits
      B3:14,
      B4:48>>.

%%-------------------------------------------------------------------------
%% @doc
%% ===Convert a UUID to a string representation.===
%% @end
%%-------------------------------------------------------------------------

%%-spec uuid_to_string(Value :: uuid()) -> string().
uuid_to_string(Value) ->
    uuid_to_string(Value, standard).

%%-------------------------------------------------------------------------
%% @doc
%% ===Convert a UUID to a string representation based on an option.===
%% @end
%%-------------------------------------------------------------------------

%%-spec uuid_to_string(Value :: uuid(), Option :: standard | nodash | list_standard | list_nodash | binary_standard | binary_nodash) -> string() | binary().
uuid_to_string(<<Value:128/unsigned-integer>>, standard) ->
    [N01, N02, N03, N04, N05, N06, N07, N08,
     N09, N10, N11, N12,
     N13, N14, N15, N16,
     N17, N18, N19, N20,
     N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32] =
        int_to_hex_list(Value, 32),
    [N01, N02, N03, N04, N05, N06, N07, N08, $-,
     N09, N10, N11, N12, $-,
     N13, N14, N15, N16, $-,
     N17, N18, N19, N20, $-,
     N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32];

uuid_to_string(<<Value:128/unsigned-integer>>, nodash) ->
    int_to_hex_list(Value, 32);

uuid_to_string(Value, list_standard) ->
    uuid_to_string(Value, standard);

uuid_to_string(Value, list_nodash) ->
    uuid_to_string(Value, nodash);

uuid_to_string(<<Value:128/unsigned-integer>>, binary_standard) ->
    [N01, N02, N03, N04, N05, N06, N07, N08,
     N09, N10, N11, N12,
     N13, N14, N15, N16,
     N17, N18, N19, N20,
     N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32] =
        int_to_hex_list(Value, 32),
    <<N01, N02, N03, N04, N05, N06, N07, N08, $-,
      N09, N10, N11, N12, $-,
      N13, N14, N15, N16, $-,
      N17, N18, N19, N20, $-,
      N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32>>;

uuid_to_string(<<Value:128/unsigned-integer>>, binary_nodash) ->
    [N01, N02, N03, N04, N05, N06, N07, N08,
     N09, N10, N11, N12,
     N13, N14, N15, N16,
     N17, N18, N19, N20,
     N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32] =
        int_to_hex_list(Value, 32),
    <<N01, N02, N03, N04, N05, N06, N07, N08,
      N09, N10, N11, N12,
      N13, N14, N15, N16,
      N17, N18, N19, N20,
      N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32>>.

%% ---
int_to_hex_list(I, N) when is_integer(I), I >= 0 ->
    int_to_hex_list([], I, 1, N).

int_to_list_pad(L, 0) ->
    L;
int_to_list_pad(L, Count) ->
    int_to_list_pad([$0 | L], Count - 1).

int_to_hex_list(L, I, Count, N)
  when I < 16 ->
    int_to_list_pad([int_to_hex(I) | L], N - Count);
int_to_hex_list(L, I, Count, N) ->
    int_to_hex_list([int_to_hex(I rem 16) | L], I div 16, Count + 1, N).

int_to_dec(I) when 0 =< I, I =< 9 ->
    I + $0.

int_to_hex(I) when 0 =< I, I =< 9 ->
    I + $0;
int_to_hex(I) when 10 =< I, I =< 15 ->
    (I - 10) + $a.

%% -------------------------------------
%% @doc Generates a new printable SHA hash binary over `Base' (using 160 bits, 27 chars).
lhash(Base) ->
    <<I:160/integer>> = sha(term_to_binary(Base)),
    case encode_integer(I) of
        Hash when byte_size(Hash) == 27 -> Hash;
        Hash -> <<(binary:copy(<<"a">>, 27-byte_size(Hash)))/binary, Hash/binary>>
    end.

%% @private
sha(Term) -> crypto:hash(sha, Term).

%% @private
encode_integer(Int) -> list_to_binary(integer_to_list(Int, 62, [])).

%% @private

integer_to_list(I0, Base, R0) ->
    D = I0 rem Base,
    I1 = I0 div Base,
    R1 = if
        D >= 36 -> [D-36+$a|R0];
        D >= 10 -> [D-10+$A|R0];
        true -> [D+$0|R0]
    end,
    if
        I1 == 0 -> R1;
       true -> integer_to_list(I1, Base, R1)
    end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

f_test_() ->
    {"Guid test",
     [?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, empty()),
      ?_assertMatch(<<_:8/binary,"-",_:4/binary,"-",_:4/binary,"-",_:4/binary,"-",_:12/binary>>, new()),
      ?_assertEqual({match,[[{0,36}]]}, re:run(new(),<<"[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}">>,[global])),
      ?_assertEqual(true, new_v3(<<"a">>) == new_v3(<<"a">>))
     ]}.

-endif.
%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.03.2021
%%% @doc Prepare filter function to check if map() item is satisfies it.
%%%      By list params, ex. [<<"or">>,[<<"and">>,[<<">=">>,[<<"get">>,<<"i">>],1],[<<"=<">>,[<<"get">>,<<"i">>],10]],[<<"==">>,[<<"get">>,<<"i">>],15],[<<"in">>,[<<"get">>,<<"i">>],[<<"list">>,20,30,40,50,60]]]
%%%      Functions:
%%%         property,
%%%         and(&&), or(||), add(+), concat(.), join(..), list,
%%%         null, isnull, isnotnull, not, bool, string, integer, float, uuid, const, lower, upper,
%%%         add(+), sub(-), mul(*), ddiv(/), div(//), rem(%), equal(==), notequal(!=),
%%%         greater(>), greaterorequal(>=), less(<), lessorequal(<=), in, contains, like
%%%         between

-module(basiclib_filter_fun).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_filter_fun/1,
         build_expression_fun/1,
         build_aggregation_fun/1,
         finalize_aggregation_accumulator/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------------
%% Filter routines
%% ----------------------------------------------
%% [<<"or">>,[<<"and">>,[<<">=">>,[<<"get">>,<<"i">>],1],[<<"=<">>,[<<"get">>,<<"i">>],10]],[<<"==">>,[<<"get">>,<<"i">>],15],[<<"in">>,[<<"get">>,<<"i">>],[<<"list">>,20,30,40,50,60]]]
%% -----
%% [OR,
%%      [AND,
%%          [>=,A,1],
%%          [=<,A,10]],
%%      [=,A,15],
%%      [IN,A,[LIST,20,30,40,50,60]]]
%% -----
%% fun(Item) -> Or(And(Greater(A,1),Less(A,10)),Equal(A,15),In(A,List(20,30,40,50,60))) end,
%% fun(Item) -> Or([And([Greater([A,1]),Less([A,10])]),Equal([A,15]),In([A,List([20,30,40,50,60])])]) end
%% ------------------------------
build_filter_fun([]) -> fun(_) -> true end;
build_filter_fun(Filter) ->
    Fun = fun() -> do_build_filter_fun(Filter) end,
    ?BLstore:lazy_t({model_filter_fun,Filter},Fun,{120000,60000,2000},{true,false}).

%% @private
do_build_filter_fun(Filter) ->
    case catch expression_fun(Filter,"filter") of
        {'EXIT',Reason} ->
            ?LOG('$error',"build_filter_fun exception: ~120tp~n\tFilter: ~120tp",[Reason,Filter]),
            fun(_) -> false end;
        {error,_}=Err -> Err;
        P ->
            fun(undefined) -> true;
               (Item) ->
                    % Or(And(Greater(A,1),Less(A,10)),Equal(A,15),In(A,List(20,30,40,50,60)).
                    F = fun F({Fun,Args}) when is_function(Fun,1) -> Fun([F(Arg) || Arg <- Args]);
                            F({Fun,Args}) when is_function(Fun,2) -> Fun(Item,[F(Arg) || Arg <- Args]);
                            F(T) -> T
                        end,
                    try F(P) of
                        'null' -> false;
                        'undefined' -> false;
                        R -> ?BU:to_bool(R)
                    catch _:_ -> false
                    end
            end end.

%% -----------------------------------------
build_expression_fun(Expression) ->
    Fun = fun() -> do_build_expression_fun(Expression) end,
    ?BLstore:lazy_t({model_expression_fun,Expression},Fun,{20000,10000,2000},{true,false}).

%% @private
do_build_expression_fun(Expression) ->
    case catch expression_fun(Expression,"expression") of
        {'EXIT',Reason} ->
            ?LOG('$error',"build_expressiong_fun exception: ~120tp~n\tFilter: ~120tp",[Reason,Expression]),
            {error,{internal_error,<<"Exception while parsing expression">>}};
        {error,_}=Err -> Err;
        P ->
            fun(undefined) -> 'null';
               (Item) ->
                    % Or(And(Greater(A,1),Less(A,10)),Equal(A,15),In(A,List(20,30,40,50,60)).
                    F = fun F({Fun,Args}) when is_function(Fun,1) -> Fun([F(Arg) || Arg <- Args]);
                            F({Fun,Args}) when is_function(Fun,2) -> Fun(Item,[F(Arg) || Arg <- Args]);
                            F(T) -> T
                        end,
                    try F(P) of
                        'null' -> 'null';
                        'undefined' -> 'null';
                        R -> R
                    catch _:_ -> false
                    end
            end end.

%% -----------------------------------------
build_aggregation_fun(Aggr) ->
    Fun = fun() -> do_build_aggregation_fun(Aggr) end,
    ?BLstore:lazy_t({model_aggregation_fun,Aggr},Fun,{20000,10000,2000},{true,false}).

%% @private
do_build_aggregation_fun(Aggr) ->
    case catch aggr_expression_fun_and_acc0(Aggr,"aggregation expression") of
        {'EXIT',Reason} ->
            ?LOG('$error',"build_aggregation_fun exception: ~120tp~n\tFilter: ~120tp",[Reason,Aggr]),
            {error,{internal_error,<<"Exception while parsing aggr expression">>}};
        {error,_}=Err -> Err;
        {{AggrFun,Acc0},AggrArgs} when is_function(AggrFun,2) ->
            FunAcc = fun(Item,Acc) ->
                            % Or(And(Greater(A,1),Less(A,10)),Equal(A,15),In(A,List(20,30,40,50,60)).
                            F = fun F({aggr,Fun,Args}) -> Fun([F(Arg) || Arg <- Args],Acc);
                                    F({Fun,Args}) when is_function(Fun,1) -> Fun([F(Arg) || Arg <- Args]);
                                    F({Fun,Args}) when is_function(Fun,2) -> Fun(Item,[F(Arg) || Arg <- Args]);
                                    F(T) -> T
                                end,
                            F({aggr,AggrFun,AggrArgs})
                     end,
            {FunAcc,Acc0}
    end.

%% --------
finalize_aggregation_accumulator(R) ->
    case R of
        'null' -> 'null'; %
        'initial' -> 'null'; % 0 elements
        'undefined' -> 'null';
        {FunResult,Accumulators} when is_function(FunResult,2), is_list(Accumulators) ->
            FunResult('after',Accumulators); % for example for 'avg' -> {Fun/2, [Sum,Count])
        _ -> R
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------------------------
%% Expression functions
%% ----------------------------------------------------

%% @private
expression_fun([<<"property">>|Args], SubjectName) -> {get_field_fun(),[expression_fun(Arg,SubjectName) || Arg <- Args]};
expression_fun([<<"arglist">>,Arg1], X) when is_list(Arg1) -> lists:map(fun(Elt) -> expression_fun(Elt,X) end,Arg1);
expression_fun([O|Args], SubjectName) -> {get_expr_fun(O,SubjectName),[expression_fun(Arg,SubjectName) || Arg <- Args]};
expression_fun(undefined, _) -> 'null';
expression_fun(T, _) -> T.

%% @private
get_field_fun() ->
    fun(Item,Path) ->
        F = fun F(undefined,_) -> 'null';
                F('null',_) -> 'null';
                F(Data,[]) -> Data;
                F(Data,[First|Rest]) when is_list(Data), is_integer(First), length(Data)>First -> F(lists:nth(First+1,Data),Rest);
                F(Data,[First|Rest]) when is_map(Data) ->
                    Res = case maps:get(First,Data,'null') of
                              'null' when is_binary(First) ->
                                  try maps:get(binary_to_existing_atom(First),Data,'null')
                                  catch _:_ -> 'null'
                                  end;
                              'null' when is_atom(First) ->
                                  try maps:get(atom_to_binary(First),Data,'null')
                                  catch _:_ -> 'null'
                                  end;
                              R -> R
                          end,
                    F(Res,Rest);
                F(_,_) -> 'null'
            end,
        F(Item,Path)
    end.

%% @private
%% Return Fun::function/1 (Args::list()) -> term()
get_expr_fun(O,SubjectName) ->
    case maps:get(O,synonyms(),O) of
        % arity *
        <<"&&">> -> fun(Args) -> lists:all(fun(Arg) -> ?BU:to_bool(Arg) end, Args) end;
        <<"||">> -> fun(Args) -> lists:any(fun(Arg) -> ?BU:to_bool(Arg) end, Args) end;
        <<"+">> -> fun(Args) -> lists:foldl(fun('null',_) -> 'null';
                                               (Arg,Acc) -> Arg + Acc
                                            end, 0, Args) end;

        <<"concat">> -> fun(Args) -> case lists:member('null',Args) of
                                         true -> 'null';
                                         _ -> ?BU:join_binary(Args,<<>>)
                                     end end;

        <<"join">> -> fun(Args) -> case lists:member('null',Args) of
                                       true -> 'null';
                                       _ -> [Delim|ArgsN] = Args, ?BU:join_binary(ArgsN,Delim)
                                   end end;
        <<"list">> -> fun([Arg]) when is_map(Arg) -> maps:keys(Arg);
                         ([Arg]) when is_list(Arg) -> Arg;
                         (Args) when is_list(Args) -> Args
                      end;
        <<"array">> -> fun([Arg]) when is_map(Arg) -> maps:keys(Arg);
                          ([Arg]) when is_list(Arg) -> Arg;
                          (Args) when is_list(Args) -> Args
                       end;
        % arity 0
        <<"null">> -> fun(_) -> 'null' end;
        <<"now">> -> fun(_) -> ?BU:strdatetime3339() end;
        % arity 1
        <<"isnull">> -> fun([Arg1]) -> Arg1 =:= 'null' orelse Arg1 =:= 'undefined' end;

        <<"isnotnull">> -> fun([Arg1]) -> Arg1 =/= 'null' andalso Arg1 =/= 'undefined' end;

        <<"not">> -> fun(['null']) -> 'null';
                        ([Arg1]) -> not Arg1
                     end;

        <<"bool">> -> fun(['null']) -> 'null';
                         ([Arg1]) -> ?BU:to_bool(Arg1)
                      end;

        <<"string">> -> fun(['null']) -> 'null';
                           ([Arg1]) -> ?BU:to_binary(Arg1)
                        end;

        <<"integer">> -> fun(['null']) -> 'null';
                            ([Arg1]) -> ?BU:to_int(Arg1)
                         end;

        <<"float">> -> fun(['null']) -> 'null';
                          ([Arg1]) -> ?BU:to_float(Arg1)
                       end;

        <<"uuid">> -> fun(['null']) -> 'null';
                         ([Arg1]) -> ?BU:to_guid(Arg1)
                      end;

        <<"const">> -> fun([Arg1]) -> Arg1 end;

        <<"lower">> -> fun(['null']) -> 'null';
                          ([Arg1]) -> ?BU:to_binary(string:lowercase(?BU:to_unicode_list(Arg1)))
                       end;

        <<"upper">> -> fun(['null']) -> 'null';
                          ([Arg1]) -> ?BU:to_binary(string:uppercase(?BU:to_unicode_list(Arg1)))
                       end;
        <<"year">> -> fun(['null']) -> 'null';
                         ([Arg1]) when is_binary(Arg1) -> {{Y,_,_},_,_} = ?BU:parse_datetime(Arg1), Y;
                         (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                      end;
        <<"quarter">> -> fun(['null']) -> 'null';
                            ([Arg1]) when is_binary(Arg1) -> {{_,M,_},_,_} = ?BU:parse_datetime(Arg1), ((M-1) div 3) + 1;
                            (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                         end;
        <<"month">> -> fun(['null']) -> 'null';
                          ([Arg1]) when is_binary(Arg1) -> {{_,M,_},_,_} = ?BU:parse_datetime(Arg1), M;
                          (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                       end;
        <<"day">> -> fun(['null']) -> 'null';
                        ([Arg1]) when is_binary(Arg1) -> {{_,_,D},_,_} = ?BU:parse_datetime(Arg1), D;
                        (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                     end;
        <<"hour">> -> fun(['null']) -> 'null';
                        ([Arg1]) when is_binary(Arg1) -> {_,{H,_,_},_} = ?BU:parse_datetime(Arg1), H;
                        (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                     end;
        <<"minute">> -> fun(['null']) -> 'null';
                           ([Arg1]) when is_binary(Arg1) -> {_,{_,Mi,_},_} = ?BU:parse_datetime(Arg1), Mi;
                           (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                      end;
        <<"second">> -> fun(['null']) -> 'null';
                           ([Arg1]) when is_binary(Arg1) -> {_,{_,_,S},_} = ?BU:parse_datetime(Arg1), S;
                           (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                      end;
        <<"weekday">> -> fun(['null']) -> 'null';
                            ([Arg1]) when is_binary(Arg1) -> {Dt,_,_} = ?BU:parse_datetime(Arg1), calendar:day_of_the_week(Dt);
                            (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                         end;
        <<"toYYYYMM">> -> fun(['null']) -> 'null';
                             ([Arg1]) when is_binary(Arg1) -> {{Y,M,_},_,_} = ?BU:parse_datetime(Arg1), Y*100+M;
                             (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                          end;
        <<"toYYYYMMDD">> -> fun(['null']) -> 'null';
                               ([Arg1]) when is_binary(Arg1) -> {{Y,M,D},_,_} = ?BU:parse_datetime(Arg1), Y*10000+M*100+D;
                               (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value",[O])}})
                            end;

        % arity 2
        <<"-">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                      ([Arg1,Arg2]) -> Arg1 - Arg2
                   end;

        <<"*">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                      ([Arg1,Arg2]) -> Arg1 * Arg2
                   end;

        <<"/">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                      ([Arg1,Arg2]) -> Arg1 / Arg2
                   end;

        <<"//">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                       ([Arg1,Arg2]) -> Arg1 div Arg2
                    end;

        <<"%">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                      ([Arg1,Arg2]) -> Arg1 rem Arg2
                   end;

        <<"==">> -> fun%([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null'; % !!! emulate postgre's "is not distinct from"
                       ([Arg1,Arg2]) -> Arg1 == Arg2
                    end;

        <<"!=">> -> fun%([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null'; % !!! emulate postgre's "is distinct from"
                       ([Arg1,Arg2]) -> Arg1 /= Arg2
                    end;

        <<">">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                      ([Arg1,Arg2]) -> Arg1 > Arg2
                   end;

        <<">=">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                       ([Arg1,Arg2]) -> Arg1 >= Arg2
                    end;

        <<"<">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                      ([Arg1,Arg2]) -> Arg1 < Arg2
                   end;

        <<"<=">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                       ([Arg1,Arg2]) -> Arg1 =< Arg2
                    end;

        <<"in">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                       ([Arg1,Arg2]) when is_list(Arg2) -> lists:member(Arg1,Arg2);
                       ([Arg1,Arg2]) when is_map(Arg2) -> maps:is_key(Arg1,Arg2)
                    end;

        <<"inlist">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                           ([Arg1,Arg2]) when is_list(Arg2) -> lists:member(Arg1,Arg2);
                           ([Arg1,Arg2]) when is_map(Arg2) -> maps:is_key(Arg1,Arg2)
                        end;

        <<"hasAny">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                           ([Arg1,Arg2]) when (is_list(Arg1) orelse is_map(Arg1)) andalso (is_list(Arg2) orelse is_map(Arg2)) ->
                                Arg1X = case is_map(Arg1) of true -> maps:keys(Arg1); false -> Arg1 end,
                                Arg2X = case is_map(Arg2) of true -> maps:keys(Arg2); false -> Arg2 end,
                                ordsets:intersection(ordsets:from_list(Arg1X), ordsets:from_list(Arg2X)) /= []
                        end;

        <<"hasAll">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                           ([Arg1,Arg2]) when (is_list(Arg1) orelse is_map(Arg1)) andalso (is_list(Arg2) orelse is_map(Arg2)) ->
                               Arg1X = case is_map(Arg1) of true -> maps:keys(Arg1); false -> Arg1 end,
                               Arg2X = case is_map(Arg2) of true -> maps:keys(Arg2); false -> Arg2 end,
                               ordsets:is_subset(ordsets:from_list(Arg1X), ordsets:from_list(Arg2X))
                        end;

        <<"contains">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                             ([Arg1,Arg2]) -> case binary:match(lowercase(Arg1),lowercase(Arg2)) of nomatch -> false; _ -> true end
                          end;

        <<"like">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                         ([Arg1,Arg2]) ->
                              Mask = make_mask(lowercase(Arg2),[unicode,dotall,{newline,anycrlf}]),
                              case re:run(lowercase(Arg1),Mask) of
                                  nomatch -> false;
                                  _ -> true
                              end end;
        <<"interval_in_minutes">> -> fun(['null',_]) -> 'null';
                                        ([_,'null']) -> 'null';
                                        ([Arg1,Arg2]) when is_binary(Arg1), is_integer(Arg2), Arg2 > 0 ->
                                            IntervalSec = Arg2 * 60,
                                            ?BU:strdatetime3339(calendar:gregorian_seconds_to_datetime((?BU:gregsecond(?BU:parse_datetime(Arg1)) div IntervalSec) * IntervalSec));
                                        ([Arg1,Arg2]) when is_integer(Arg1), is_integer(Arg2), Arg2 > 0 ->
                                            IntervalSec = Arg2 * 60,
                                            (((Arg1 div 1000) div IntervalSec) * IntervalSec) * 1000;
                                        (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected Arg1: datetime/string or integer timestamp value and Arg2: integer value",[O])}})
                                     end;

        <<"dateadd">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                            ([Arg1,Arg2]) when is_binary(Arg1), is_integer(Arg2) ->
                                ?BU:strdatetime3339(calendar:gregorian_seconds_to_datetime(?BU:gregsecond(?BU:parse_datetime(Arg1)) + Arg2));
                            (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value and integer value",[O])}})
                         end;
        <<"datediff">> -> fun([Arg1,Arg2]) when Arg1=:='null';Arg2=:='null' -> 'null';
                             ([Arg1,Arg2]) when is_binary(Arg1), is_binary(Arg2) ->
                                 ?BU:gregsecond(?BU:parse_datetime(Arg2)) - ?BU:gregsecond(?BU:parse_datetime(Arg1));
                             (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected two datetime/string values",[O])}})
                         end;
        % arity 3
        <<"between">> -> fun(['null',_,_]) -> 'null';
                            ([Arg1,Arg2,Arg3]) when Arg2=:='null';Arg3=:='null' ->
                                    if (Arg2=:='null') and (Arg3=:='null') -> 'null';
                                       (Arg2=:='null') and (Arg1>Arg3) -> false;
                                       (Arg1<Arg2) and (Arg3=:='null') -> false;
                                       true -> 'null'
                                    end;
                            ([Arg1,Arg2,Arg3]) -> Arg1 >= Arg2 andalso Arg1 =< Arg3
                         end;
        % other
        _ ->
            throw({error,{invalid_params,?BU:strbin("Invalid ~ts. Unknown function: '~ts'. Expected: ~ts",[SubjectName,?BU:to_binary(O),expected_functions()])}})
    end.

%% @private
lowercase(Arg) ->
    string:lowercase(Arg).

%% @private
make_mask(FilterV,ReOpts) ->
    F = fun($[,Acc) -> [$],$[,$\\,$[ | Acc];
           ($],Acc) -> [$],$],$\\,$[ | Acc];
           ($\\,Acc) -> [$],$\\,$\\,$[ | Acc];
           ($*,[$],$\\,$\\,$[ | Acc]) -> [$],$*,$[ | Acc];
           ($*,Acc) -> [$*,$. | Acc];
           ($?,[$],$\\,$\\,$[ | Acc]) -> [$],$?,$[ | Acc];
           ($?,Acc) -> [$. | Acc];
           (A,Acc) -> [$],A,$[ | Acc]
        end,
    Mask = "^" ++ lists:reverse(lists:foldl(F, [], ?BU:to_unicode_list(FilterV))) ++ "$",
    {ok,Re} = re:compile(?BU:to_binary(Mask),ReOpts),
    Re.

%% -------------------------------------
%% Descriptors for expression functions
%% -------------------------------------

%% @private
expected_functions() ->
    FDs = lists:map(fun({Name,Arity,[]}) -> ?BU:strbin("'~ts'/~p",[Name,Arity]);
                       ({Name,Arity,Synonyms}) ->
                            SynStr = ?BU:join_binary([<<"'",Syn/binary,"'">> || Syn <- Synonyms],<<",">>),
                            ?BU:strbin("'~ts'/~p (~ts)",[Name,Arity,SynStr])
                    end, functions()),
    ?BU:join_binary(FDs,<<", ">>).

%% @private
functions() ->
    [
        {<<"isnull">>,1,[]},
        {<<"isnotnull">>,1,[]},
        {<<"not">>,1,[<<"!">>]},
        {<<"bool">>,1,[]},
        {<<"integer">>,1,[]},
        {<<"float">>,1,[]},
        {<<"string">>,1,[]},
        {<<"uuid">>,1,[]},
        {<<"const">>,1,[]},
        {<<"lower">>,1,[]},
        {<<"upper">>,1,[]},
        {<<"year">>,1,[<<"toYYYY">>,<<"toYear">>]},
        {<<"quarter">>,1,[]},
        {<<"month">>,1,[]},
        {<<"day">>,1,[]},
        {<<"hour">>,1,[]},
        {<<"minute">>,1,[]},
        {<<"second">>,1,[]},
        {<<"weekday">>,1,[]},
        {<<"toYYYYMM">>,1,[]},
        {<<"toYYYYMMDD">>,1,[]},
        {<<"concat">>,any,[<<".">>]},
        {<<"join">>,any,[<<"..">>]},
        {<<"list">>,any,[]},
        {<<"array">>,any,[]},
        {<<"null">>,0,[]},
        {<<"now">>,0,[]},
        {<<"+">>,any,[<<"add">>]},
        {<<"-">>,2,[<<"sub">>]},
        {<<"*">>,2,[<<"mul">>]},
        {<<"/">>,2,[<<"div">>]},
        {<<"//">>,2,[<<"ddiv">>]},
        {<<"%">>,2,[<<"rem">>]},
        {<<"&&">>,any,[<<"and">>]},
        {<<"||">>,any,[<<"or">>]},
        {<<"==">>,2,[<<"equal">>,<<"equals">>]},
        {<<"!=">>,2,[<<"notequal">>,<<"notequals">>,<<"<>">>]},
        {<<">">>,2,[<<"greater">>]},
        {<<">=">>,2,[<<"notless">>,<<"greaterorequal">>]},
        {<<"<">>,2,[<<"less">>]},
        {<<"<=">>,2,[<<"notgreater">>,<<"lessorequal">>]},
        {<<"in">>,2,[]},
        {<<"inlist">>,2,[]},
        {<<"hasAny">>,2,[]},
        {<<"hasAll">>,2,[]},
        {<<"contains">>,2,[]},
        {<<"like">>,2,[]},
        {<<"interval_in_minutes">>,2,[]},
        {<<"dateadd">>,2,[]},
        {<<"datediff">>,2,[]},
        {<<"between">>,3,[]}
    ].

synonyms() ->
    #{
        <<"!">> => <<"not">>,
        <<".">> => <<"concat">>,
        <<"..">> => <<"join">>,
        <<"add">> => <<"+">>,
        <<"sub">> => <<"-">>,
        <<"mul">> => <<"*">>,
        <<"div">> => <<"/">>,
        <<"ddiv">> => <<"//">>,
        <<"rem">> => <<"%">>,
        <<"and">> => <<"&&">>,
        <<"or">> => <<"||">>,
        <<"equal">> => <<"==">>,
        <<"equals">> => <<"==">>,
        <<"notequal">> => <<"!=">>,
        <<"notequals">> => <<"!=">>,
        <<"<>">> => <<"!=">>,
        <<"greater">> => <<">">>,
        <<"greaterorequal">> => <<">=">>,
        <<"notless">> => <<">=">>,
        <<"less">> => <<"<">>,
        <<"notgreater">> => <<"<=">>,
        <<"lessorequal">> => <<"<=">>,
        <<"=<">> => <<"<=">>,
        <<"toYYYY">> => <<"year">>,
        <<"toYear">> => <<"year">>
    }.

%% ----------------------------------------------------
%% Aggregation functions
%% ----------------------------------------------------

aggr_expression_fun_and_acc0([O|Args], SubjectName) -> {get_aggr_expr_fun_and_acc0(O,SubjectName),[expression_fun(Arg,SubjectName) || Arg <- Args]};
aggr_expression_fun_and_acc0(_, _SubjectName) ->
    throw({error,{invalid_params,?BU:strbin("Invalid aggregation function. Expected specific list, starting with one of aggregation functions: ~ts",[expected_aggr_functions()])}}).

%% @private
%% Return {AggrFun::function/2, Acc0::term()}, when AggrFun::function/2 (Args::list(), Acc::term()) -> NewAcc::term()
get_aggr_expr_fun_and_acc0(O,SubjectName) ->
    case maps:get(O,aggr_synonyms(),O) of
        % arity 1
        <<"count">> -> {fun(_,Acc) -> Acc+1 end, 0};

        <<"sum">> -> {fun(_,'null') -> 'null'; % already nas null
                         ([Arg],_) when Arg=:='null' -> 'null';
                         ([Arg],Acc) -> case ?BU:as_number(Arg,'null') of 'null' -> 'null'; X -> Acc + X end
                      end, 0};

        <<"min">> -> {fun(_,'null') -> 'null'; % already nas null
                         ([Arg],_) when Arg=:='null' -> 'null'; % first null element
                         ([Arg],'initial') -> Arg; % first element
                         ([Arg],Acc) when Arg < Acc -> Arg; % happy way, step 2+
                         (_,Acc) -> Acc % happy way, step 2+
                      end, 'initial'};

        <<"max">> -> {fun(_,'null') -> 'null'; % already nas null
                         ([Arg],_) when Arg=:='null' -> 'null'; % first null element
                         ([Arg],'initial') -> Arg; % first element
                         ([Arg],Acc) when Arg > Acc -> Arg; % happy way, step 2+
                         (_,Acc) -> Acc % happy way, step 2+
                      end, 'initial'};

        <<"avg">> -> {fun(_,'null') -> 'null'; % already nas null
                         ([Arg], _) when Arg=:='null' -> 'null'; % first null element
                         ([Arg],'initial') -> case ?BU:as_number(Arg,'null') of 'null' -> 'null'; X -> {fun('after',[Sum,Cnt]) -> Sum / Cnt end, [X, 1]} end; % first element
                         ([Arg], {FunResult,[AccSum,AccCnt]}) -> case ?BU:as_number(Arg,'null') of 'null' -> 'null'; X -> {FunResult,[AccSum+X,AccCnt+1]} end % happy way, step 2+
                      end, 'initial'};

        <<"and">> -> {fun(_,'null') -> 'null'; % already nas null
                         ([Arg], _) when Arg=:='null' -> 'null'; % first null element
                         ([Arg],'initial') -> ?BU:to_bool(Arg); % first element
                         ([Arg], Acc) -> Acc and ?BU:to_bool(Arg) % happy way, step 2+
                      end, 'initial'};

        <<"or">> -> {fun(_,'null') -> 'null'; % already nas null
                        ([Arg], _) when Arg=:='null' -> 'null'; % first null element
                        ([Arg],'initial') -> ?BU:to_bool(Arg); % first element
                        ([Arg], Acc) -> Acc or ?BU:to_bool(Arg) % happy way, step 2+
                     end, 'initial'};

        <<"join">> -> {fun(_,'null') -> 'null'; % already nas null
                          ([Arg,Delim], _) when Arg=:='null'; Delim=:='null' -> 'null'; % first null element
                          ([Arg,_],'initial') -> ?BU:to_binary(Arg); % first element
                          ([Arg,Delim], Acc) -> <<Acc/binary, (?BU:to_binary(Delim))/binary, (?BU:to_binary(Arg))/binary>> % happy way, step 2+
                       end, 'initial'};
        % other
        _ ->
            throw({error,{invalid_params,?BU:strbin("Invalid ~ts. Unknown function: '~ts'. Expected: ~ts",[SubjectName,?BU:to_binary(O),expected_aggr_functions()])}})
    end.

%% --------------------------------------
%% Descriptors for aggregation functions
%% --------------------------------------

%% @private
expected_aggr_functions() ->
    FDs = lists:map(fun({Name,Arity,[]}) -> ?BU:strbin("'~ts'/~p",[Name,Arity]);
                       ({Name,Arity,Synonyms}) ->
                            SynStr = ?BU:join_binary([<<"'",Syn/binary,"'">> || Syn <- Synonyms],<<",">>),
                            ?BU:strbin("'~ts'/~p (~ts)",[Name,Arity,SynStr])
                    end, aggr_functions()),
    ?BU:join_binary(FDs,<<", ">>).

%% @private
aggr_functions() ->
    [
        {<<"count">>,1,[]},
        {<<"sum">>,1,[]},
        {<<"min">>,1,[]},
        {<<"max">>,1,[]},
        {<<"avg">>,1,[]},
        {<<"and">>,1,[]},
        {<<"or">>,1,[]},
        {<<"join">>,2,[]}
    ].

%% @private
aggr_synonyms() ->
    #{}.
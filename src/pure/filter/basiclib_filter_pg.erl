%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.03.2021
%%% @doc Prepare filter string for postgre SELECT request (section WHERE).
%%%      By list params, ex. [<<"or">>,[<<"and">>,[<<">=">>,[<<"get">>,<<"i">>],1],[<<"=<">>,[<<"get">>,<<"i">>],10]],[<<"==">>,[<<"get">>,<<"i">>],15],[<<"in">>,[<<"get">>,<<"i">>],[<<"list">>,20,30,40,50,60]]]
%%%      Functions:
%%%         property,
%%%         and(&&), or(||), add(+), concat(.), join(..), list,
%%%         null, isnull, isnotnull, not, bool, string, integer, float, uuid, const, lower, upper,
%%%         add(+), sub(-), mul(*), ddiv(/), div(//), rem(%), equal(==), notequal(!=),
%%%         greater(>), greaterorequal(>=), less(<), lessorequal(<=), in, contains, like
%%%         between

-module(basiclib_filter_pg).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_filter_string/1]).
-export([build_expression_string/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------------
%% Filter routines
%% ----------------------------------------------
%% [<<"or">>,[<<"and">>,[<<">=">>,[<<"get">>,<<"i">>],1],[<<"=<">>,[<<"get">>,<<"i">>],10]],[<<"==">>,[<<"get">>,<<"i">>],15],[<<"in">>,[<<"get">>,<<"i">>],[<<"list">>,20,30,40,50,60]]]
%% -----
%% [OR,
%%      [AND,
%%          [>=,A,1],
%%          [=<,A,10]],
%%      [=,A,15],
%%      [IN,A,[LIST,20,30,40,50,60]]]
%% -----
%% ((A>=1) AND (A<=10)) OR (A = 15) OR (A IN (20,30,50,60))
%% ------------------------------
build_filter_string(Filter) ->
    Fun = fun() -> do_build_expression_string(Filter,"filter") end,
    ?BLstore:lazy_t({model_expression_pg,Filter},Fun,{120000,60000,2000},{true,false}).

build_expression_string(Expression) ->
    Fun = fun() -> do_build_expression_string(Expression,"expression") end,
    ?BLstore:lazy_t({model_expression_pg,Expression},Fun,{120000,60000,2000},{true,false}).

%% @private
do_build_expression_string(Filter,SubjectName) ->
    case catch expression_fun(preparse(Filter),quote) of
        {'EXIT',_}=Ex ->
            ?LOG('$error', "Caught exception on build_expression_string (~120p):~n\t ~120p", [Filter,Ex]),
            {error,{invalid_params,?BU:strbin("Invalid ~ts",[SubjectName])}};
        {error,_}=Err -> Err;
        P ->
            F = fun F({Fun,Args}) when is_function(Fun,1) -> Fun([F(Arg) || Arg <- Args]);
                    F(T) -> T
                end,
            case catch F(P) of
                {'EXIT',_}=Ex ->
                    ?LOG('$error', "Caught exception on build_expression_string (~120p):~n\t ~120p", [Filter,Ex]),
                    {error,{invalid_params,?BU:strbin("Invalid ~ts",[SubjectName])}};
                Bin -> Bin
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private hasAny and hasAll expect array, not list as 2nd arg.
preparse([O|Args]) when O==<<"hasAny">>; O==<<"hasAll">> ->
    case Args of
        [[<<"list">>|Rest],A2] -> [O,preparse(A2),[<<"array">> | [preparse(Arg) || Arg <- Rest]]];
        [A1,[<<"list">>|Rest]] -> [O,preparse(A1),[<<"array">> | [preparse(Arg) || Arg <- Rest]]];
        _ -> [O | [preparse(Arg) || Arg <- Args]]
    end;
preparse([O|Args]) when is_binary(O) -> [O | [preparse(Arg) || Arg <- Args]];
preparse(Arg) -> Arg.

%% @private
expression_fun([<<"property">>|Args],_) -> {get_field_fun(),[expression_fun(Arg,unquote) || Arg <- Args]};
expression_fun([<<"arglist">>,Arg1],_) when is_list(Arg1) ->
    Elements = Arg1,
    F = fun(T) -> expression_fun(T,quote) end,
    <<"(",(?BU:join_binary(lists:map(F,Elements),","))/binary,")">>;
expression_fun([O|Args],_) -> {get_expr_fun(O),[expression_fun(Arg,quote) || Arg <- Args]};
expression_fun(T,quote) when is_binary(T) -> <<"'",(binary:replace(T,<<"'">>,<<"''">>,[global]))/binary,"'">>;
expression_fun(T,unquote) when is_binary(T) -> T;
expression_fun(T,_) when is_integer(T) -> T;
expression_fun(T,_) when is_float(T) -> T;
expression_fun(T,_) when is_number(T) -> T;
expression_fun(T,_) when is_boolean(T) -> T;
expression_fun(undefined,_) -> <<"NULL">>;
expression_fun(T,_) -> throw({error,{invalid_params,?BU:strbin("Invalid term: ~120tp. Expected constant or function name",[T])}}).

%% @private
get_field_fun() ->
    fun([]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (fun 'property'). Expected property name",[])}});
       ([First]) when is_binary(First) -> <<"\"",First/binary,"\"">>;
       ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (fun 'property'). Expected string property name",[])}});
       ([First|Rest]) when is_binary(First) ->
           Fld = <<"\"",First/binary,"\"">>,
           Fld1 = lists:foldl(fun%(Path,Acc) when is_binary(Path) -> <<"(",Acc/binary,"::jsonb ->> '",Path/binary,"')">>; % FIX: Found that it's too long to get string and convert to jsonb everytime
                                 %(Path,Acc) when is_integer(Path) -> <<"(",Acc/binary,"::jsonb ->> ",(b(Path))/binary,")">>; % FIX: Found that it's too long to get string and convert to jsonb everytime
                                 (Path,Acc) when is_binary(Path) -> <<"(",Acc/binary,"::jsonb -> '",Path/binary,"')">>;
                                 (Path,Acc) when is_integer(Path) -> <<"(",Acc/binary,"::jsonb -> ",(b(Path))/binary,")">>;
                                 (_,_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (fun 'property'). Expected string or integer property path parts",[])}})
                              end,Fld,lists:droplast(Rest)),
           case lists:last(Rest) of
               Last when is_binary(Last) -> <<"(",Fld1/binary,"::jsonb ->> '",Last/binary,"')">>;
               Last when is_integer(Last) -> <<"(",Fld1/binary,"::jsonb ->> ",(b(Last))/binary,")">>;
               _ -> throw({error,{invalid_params,?BU:strbin("Invalid filter (fun 'property'). Expected string or integer property path parts",[])}})
           end;
       ([_,_|_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (fun 'property'). Expected string property name starting path",[])}})
    end.

%% @private
get_expr_fun(O) ->
    case maps:get(O,synonyms(),O) of
        % arity *
        <<"&&">> -> fun([]) -> <<"true">>;
                       (Args) -> xfun(any,Args,<<"AND">>) end;
        <<"||">> -> fun([]) -> <<"false">>;
                       (Args) -> xfun(any,Args,<<"OR">>) end;
        <<"+">> -> fun([]) -> <<"0">>;
                      (Args) -> xfun(any,Args,<<"+">>) end;
        <<"concat">> -> fun([]) -> <<"''">>;
                           (Args) -> xfun(any,Args,<<"||">>) end;
        <<"join">> -> fun([_]) -> <<"''">>;
                         ([Delim|ArgsN]) when is_binary(Delim) -> xfun(any,ArgsN,<<"|| ",Delim/binary," ||">>);
                         (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string delimiter",[O])}})
                      end;
        <<"list">> -> fun(Args) -> <<"(",(?BU:join_binary(Args,","))/binary,")">> end;
        <<"array">> -> fun(Args) -> <<"array[",(?BU:join_binary(Args,","))/binary,"]">> end;
        % arity 0
        <<"null">> -> fun(_) -> <<"NULL">> end;
        <<"now">> -> fun(_) -> <<"(now() :: timestamp)">> end;
        % arity 1
        <<"isnull">> -> fun([Arg1|_]) -> <<"(",(b(Arg1))/binary," IS NULL)">> end;
        <<"isnotnull">> -> fun([Arg1|_]) -> <<"(",(b(Arg1))/binary," IS NOT NULL)">> end;
        <<"not">> -> fun([Arg1|_]) -> <<"(NOT ",(b(Arg1))/binary,")">> end;
        <<"bool">> -> fun([Arg1|_]) -> <<"(",(b(Arg1))/binary,")::boolean">> end;
        <<"string">> -> fun([Arg1|_]) -> <<"(",(b(Arg1))/binary,")::varchar">> end;
        <<"integer">> -> fun([Arg1|_]) -> <<"(",(b(Arg1))/binary,")::integer">> end;
        <<"float">> -> fun([Arg1|_]) -> <<"(",(b(Arg1))/binary,")::float">> end;
        <<"uuid">> -> fun([Arg1|_]) -> <<"(",Arg1/binary,")::uuid">> end;
        <<"const">> -> fun([Arg1]) when is_binary(Arg1) -> <<"'",(string:trim(Arg1,both,"$'"))/binary,"'">>;
                          ([Arg1]) when is_number(Arg1) -> b(Arg1);
                          ([Arg1]) when is_boolean(Arg1) -> b(Arg1);
                          (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                       end;
        <<"lower">> -> fun([Arg1]) when is_binary(Arg1) -> <<"lower(",Arg1/binary,")">>;
                          ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                          (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                       end;
        <<"upper">> -> fun([Arg1]) when is_binary(Arg1) -> <<"upper(",Arg1/binary,")">>;
                          ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                          (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'. Expected single value",[O])}})
                       end;
        <<"year">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(year from ",Arg1/binary," :: timestamp)">>;
                         ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                         (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                      end;
        <<"quarter">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(quarter from ",Arg1/binary," :: timestamp)">>;
                            ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                            (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                         end;
        <<"month">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(month from ",Arg1/binary," :: timestamp)">>;
                          ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                          (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                       end;
        <<"day">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(day from ",Arg1/binary," :: timestamp)">>;
                        ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                        (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                     end;
        <<"hour">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(hour from ",Arg1/binary," :: timestamp)">>;
                         ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                         (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                     end;
        <<"minute">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(minute from ",Arg1/binary," :: timestamp)">>;
                           ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                           (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                      end;
        <<"second">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(second from ",Arg1/binary," :: timestamp)">>;
                           ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                           (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                      end;
        <<"weekday">> -> fun([Arg1]) when is_binary(Arg1) -> <<"extract(dow from ",Arg1/binary," :: timestamp)">>;
                            ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                            (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                         end;
        <<"toYYYYMM">> -> fun([Arg1]) when is_binary(Arg1) -> <<"(extract(year from ",Arg1/binary," :: timestamp)*100 + extract(month from ",Arg1/binary," :: timestamp))">>;
                             ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                             (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                          end;
        <<"toYYYYMMDD">> -> fun([Arg1]) when is_binary(Arg1) -> <<"(extract(year from ",Arg1/binary," :: timestamp)*10000 + extract(month from ",Arg1/binary," :: timestamp)*100 + extract(day from ",Arg1/binary," :: timestamp))">>;
                               ([_]) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected string value",[O])}});
                               (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected single value",[O])}})
                            end;
        % arity 2
        <<"-">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"-">>) end;
        <<"*">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"*">>) end;
        <<"/">> -> fun([Arg1,Arg2]) -> <<"((",(b(Arg1))/binary, ")::float / ",(b(Arg2))/binary,")">> end; % TODO ICP
        <<"//">> -> fun([Arg1,Arg2]) -> <<"floor(",(b(Arg1))/binary, " / ",(b(Arg2))/binary,")">> end;
        <<"%">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"%">>) end;
        <<"==">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"is not distinct from">>) end;
        <<"!=">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"is distinct from">>)  end;
        <<">">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<">">>)  end;
        <<">=">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<">=">>)  end;
        <<"<">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"<">>)  end;
        <<"<=">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"<=">>)  end;
        <<"in">> -> fun([Arg1,Arg2]) -> xfun(2,[Arg1,Arg2],<<"IN">>) end;
        <<"inlist">> -> fun([Arg1,Arg2]) -> <<"(",Arg2/binary, "::jsonb ? ",Arg1/binary,")">> end;
        <<"hasAny">> -> fun([Arg1,<<"array[",_/binary>>=Arg2]) -> <<"(",Arg1/binary, "::jsonb ?| ",Arg2/binary,")">>;
                           ([<<"array[",_/binary>>=Arg1,Arg2]) -> <<"(",Arg2/binary, "::jsonb ?| ",Arg1/binary,")">>;
                           ([Arg1,Arg2]) -> <<"(",Arg1/binary, "::jsonb ?& ",Arg2/binary,")">>
                        end;
        <<"hasAll">> -> fun([Arg1,<<"array[",_/binary>>=Arg2]) -> <<"(",Arg1/binary, "::jsonb ?& ",Arg2/binary,")">>;
                           ([<<"array[",_/binary>>=Arg1,Arg2]) -> <<"(",Arg2/binary, "::jsonb ?& ",Arg1/binary,")">>;
                           ([Arg1,Arg2]) -> <<"(",Arg1/binary, "::jsonb ?& ",Arg2/binary,")">>
                        end;
        <<"contains">> -> fun([<<"NULL">>,_Arg2]) -> false;
                             ([Arg1,Arg2]) -> <<"(strpos(lower(",(b(Arg1))/binary,"),lower(",(b(Arg2))/binary,")) > 0)">> end;
        <<"like">> -> fun([<<"NULL">>,_Arg2]) -> false;
                         ([Arg1,Arg2]) ->
                             XArg2 = <<"replace(",(b(Arg2))/binary,",'*','%')">>,
                             xfun(2,[Arg1,XArg2],<<"LIKE">>)
                      end;
        <<"interval_in_minutes">> -> fun([Arg1,Arg2]) when is_binary(Arg1), is_integer(Arg2), Arg2 > 0 ->
                                            IntervalSec = Arg2 * 60,
                                            ?BU:strbin("to_timestamp(floor(extract(epoch from ~ts :: timestamp) / ~p) * ~p) AT TIME ZONE 'UTC'",[Arg1,IntervalSec,IntervalSec]);
                                        ([Arg1,Arg2]) when is_integer(Arg1), is_integer(Arg2), Arg2 > 0 ->
                                            IntervalSec = Arg2 * 60,
                                            ?BU:strbin("floor(~p :: timestamp / ~p) * ~p",[Arg1,IntervalSec,IntervalSec]);
                                        (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value and integer value",[O])}})
                                     end;
        <<"dateadd">> -> fun([Arg1,Arg2]) when is_binary(Arg1) ->
                                ?BU:strbin("(~ts :: timestamp + (~p :: integer) * INTERVAL '1 sec')",[Arg1,Arg2]);
                                (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value and integer value",[O])}})
                         end;
        <<"datediff">> -> fun([Arg1,Arg2]) when is_binary(Arg1), is_binary(Arg2) ->
                                ?BU:strbin("extract(epoch from (~ts :: timestamp) - (~ts :: timestamp)) :: integer",[Arg2,Arg1]);
                                (_) -> throw({error,{invalid_params,?BU:strbin("Invalid filter (function '~ts'). Expected datetime/string value and integer value",[O])}})
                          end;
        % 3
        <<"between">> -> fun([<<"NULL">>,_Arg2,_Arg3]) -> false;
                            ([undefined,_Arg2,_Arg3]) -> false;
                            ([Arg1,Arg2,Arg3]) -> <<"(",(b(Arg1))/binary," BETWEEN ",(b(Arg2))/binary," AND ",(b(Arg3))/binary,")">> end;
        % other
        _ ->
            throw({error,{invalid_params,?BU:strbin("Invalid filter. Unknown function: '~ts'. Expected: ~ts",[?BU:to_binary(O),expected_functions()])}})
    end.

%% @private
xfun(2,[Arg1,Arg2],Operation) -> <<"(",(b(Arg1))/binary, " ", Operation/binary, " ",(b(Arg2))/binary,")">>;
xfun(any,Args,Operation) -> <<"(",(?BU:join_binary([b(Arg) || Arg <- Args], <<" ", Operation/binary, " ">>))/binary,")">>.

%% @private
b(Arg) -> ?BU:to_binary(Arg).

%% ----------------------------------------------------
%% Descriptors
%% ----------------------------------------------------

%% @private
expected_functions() ->
    FDs = lists:map(fun({Name,Arity,[]}) -> ?BU:strbin("'~ts'/~p",[Name,Arity]);
                       ({Name,Arity,Synonyms}) ->
                            SynStr = ?BU:join_binary([<<"'",Syn/binary,"'">> || Syn <- Synonyms],<<",">>),
                            ?BU:strbin("'~ts'/~p (~ts)",[Name,Arity,SynStr])
                    end, functions()),
    ?BU:join_binary(FDs,<<", ">>).

%% @private
functions() ->
    [
        {<<"isnull">>,1,[]},
        {<<"isnotnull">>,1,[]},
        {<<"not">>,1,[<<"!">>]},
        {<<"bool">>,1,[]},
        {<<"integer">>,1,[]},
        {<<"float">>,1,[]},
        {<<"string">>,1,[]},
        {<<"uuid">>,1,[]},
        {<<"const">>,1,[]},
        {<<"lower">>,1,[]},
        {<<"upper">>,1,[]},
        {<<"year">>,1,[<<"toYYYY">>,<<"toYear">>]},
        {<<"quarter">>,1,[]},
        {<<"month">>,1,[]},
        {<<"day">>,1,[]},
        {<<"hour">>,1,[]},
        {<<"minute">>,1,[]},
        {<<"second">>,1,[]},
        {<<"weekday">>,1,[]},
        {<<"toYYYYMM">>,1,[]},
        {<<"toYYYYMMDD">>,1,[]},
        {<<"concat">>,any,[<<".">>]},
        {<<"join">>,any,[<<"..">>]},
        {<<"list">>,any,[]},
        {<<"array">>,any,[]},
        {<<"null">>,0,[]},
        {<<"now">>,0,[]},
        {<<"+">>,any,[<<"add">>]},
        {<<"-">>,2,[<<"sub">>]},
        {<<"*">>,2,[<<"mul">>]},
        {<<"/">>,2,[<<"div">>]},
        {<<"//">>,2,[<<"ddiv">>]},
        {<<"%">>,2,[<<"rem">>]},
        {<<"&&">>,any,[<<"and">>]},
        {<<"||">>,any,[<<"or">>]},
        {<<"==">>,2,[<<"equal">>,<<"equals">>]},
        {<<"!=">>,2,[<<"notequal">>,<<"notequals">>,<<"<>">>]},
        {<<">">>,2,[<<"greater">>]},
        {<<">=">>,2,[<<"notless">>,<<"greaterorequal">>]},
        {<<"<">>,2,[<<"less">>]},
        {<<"<=">>,2,[<<"notgreater">>,<<"lessorequal">>,<<"=<">>]},
        {<<"in">>,2,[]},
        {<<"inlist">>,2,[]},
        {<<"hasAny">>,2,[]},
        {<<"hasAll">>,2,[]},
        {<<"contains">>,2,[]},
        {<<"like">>,2,[]},
        {<<"interval_in_minutes">>,2,[]},
        {<<"dateadd">>,2,[]},
        {<<"datediff">>,2,[]},
        {<<"between">>,3,[]}
    ].

synonyms() ->
    #{
        <<"!">> => <<"not">>,
        <<".">> => <<"concat">>,
        <<"..">> => <<"join">>,
        <<"add">> => <<"+">>,
        <<"sub">> => <<"-">>,
        <<"mul">> => <<"*">>,
        <<"div">> => <<"/">>,
        <<"ddiv">> => <<"//">>,
        <<"rem">> => <<"%">>,
        <<"and">> => <<"&&">>,
        <<"or">> => <<"||">>,
        <<"equal">> => <<"==">>,
        <<"equals">> => <<"==">>,
        <<"notequal">> => <<"!=">>,
        <<"notequals">> => <<"!=">>,
        <<"<>">> => <<"!=">>,
        <<"greater">> => <<">">>,
        <<"greaterorequal">> => <<">=">>,
        <<"notless">> => <<">=">>,
        <<"less">> => <<"<">>,
        <<"notgreater">> => <<"<=">>,
        <<"lessorequal">> => <<"<=">>,
        <<"=<">> => <<"<=">>,
        <<"toYYYY">> => <<"year">>,
        <<"toYear">> => <<"year">>
    }.

%% ====================================================================
%% Filepath helpers Test
%% ====================================================================

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

do_build_expression_string_test_() ->
    P0 = [<<"null">>],
    R0 = <<"NULL">>,
    %
    P1 = [<<"isnull">>,<<"a">>],
    R1 = <<"('a' IS NULL)">>,
    %
    P2 = [<<"like">>,[<<"property">>,<<"a">>],<<"abc*">>],
    R2 = <<"(\"a\" LIKE replace('abc*','*','%'))">>,
    %
    P3 = [<<"between">>,[<<"property">>,<<"a">>],1,10],
    R3 = <<"(\"a\" BETWEEN 1 AND 10)">>,
    %
    P4 = [<<"==">>,[<<"string">>,[<<"property">>,<<"b/field">>]],<<"abc">>],
    %R4 = <<"((\"b/field\")::varchar = 'abc')">>,
    R4 = <<"((\"b/field\")::varchar is not distinct from 'abc')">>,
    %
    P5 = [<<"and">>,[<<"between">>,[<<"property">>,<<"a">>],1,10],[<<"==">>,[<<"string">>,[<<"property">>,<<"b/field">>]],<<"abc">>],true],
    %R5 = <<"((\"a\" BETWEEN 1 AND 10) AND ((\"b/field\")::varchar = 'abc') AND true)">>,
    R5 = <<"((\"a\" BETWEEN 1 AND 10) AND ((\"b/field\")::varchar is not distinct from 'abc') AND true)">>,
    %
    P6 = [<<"and">>,[<<"like">>,[<<"property">>,<<"data">>,<<"items">>,1],[<<"concat">>,<<"a">>,<<"b">>,<<"c">>]],[<<"||">>,[<<"equal">>,[<<"property">>,<<"a">>],<<"25">>],[<<"in">>,<<"3">>,[<<"list">>,1,2,3,4,5]]]],
    %R6 = <<"((((\"data\"::jsonb ->> 'items')::jsonb ->> 1) LIKE replace(('a' || 'b' || 'c'),'*','%')) AND ((\"a\" = '25') OR ('3' IN (1,2,3,4,5))))">>,
    %R6 = <<"((((\"data\"::jsonb ->> 'items')::jsonb ->> 1) LIKE replace(('a' || 'b' || 'c'),'*','%')) AND ((\"a\" is not distinct from '25') OR ('3' IN (1,2,3,4,5))))">>,
    R6 = <<"((((\"data\"::jsonb -> 'items')::jsonb ->> 1) LIKE replace(('a' || 'b' || 'c'),'*','%')) AND ((\"a\" is not distinct from '25') OR ('3' IN (1,2,3,4,5))))">>,
    %
    P7 = [<<"||">>,true,<<"false">>,<<"false">>],
    R7 = <<"(true OR 'false' OR 'false')">>,
    %
    P8 = [<<"&&">>,[<<"or">>],[<<"or">>,true],[<<"==">>,[<<"property">>,<<"x">>],false],[<<"or">>,true,false,<<"false">>]],
    %R8 = <<"(false AND (true) AND (\"x\" = false) AND (true OR false OR 'false'))">>,
    R8 = <<"(false AND (true) AND (\"x\" is not distinct from false) AND (true OR false OR 'false'))">>,
    %
    P9 = [<<".">>,[<<".">>],[<<".">>,<<"a">>],[<<".">>,<<"b">>,<<"c">>]],
    R9 = <<"('' || ('a') || ('b' || 'c'))">>,
    %
    P10 = [<<"..">>,<<",">>,[<<".">>],[<<".">>,<<"a">>],[<<".">>,<<"b">>,<<"c">>]],
    R10 = <<"('' || ',' || ('a') || ',' || ('b' || 'c'))">>,
    %
    P11 = [<<"or">>,[<<"contains">>,[<<"upper">>,[<<"property">>,<<"name">>]],<<"abc">>],[<<"in">>,<<"a">>,[<<"list">>,1,2,<<"3">>,true]],[<<"and">>,[<<"!=">>,[<<"/">>,5,10],1],[<<"<=">>,[<<"//">>,[<<"float">>,<<"3.5">>],2.5],0.5]]],
    %R11 = <<"((strpos(lower(upper(\"name\")),lower('abc')) > 0) OR ('a' IN (1,2,'3',true)) OR ((((5)::float / 10) != 1) AND (floor(('3.5')::float / 2.5) <= 0.5)))">>,
    R11 = <<"((strpos(lower(upper(\"name\")),lower('abc')) > 0) OR ('a' IN (1,2,'3',true)) OR ((((5)::float / 10) is distinct from 1) AND (floor(('3.5')::float / 2.5) <= 0.5)))">>,
    %
    P12 = [<<"const">>,<<"2">>],
    R12 = <<"'2'">>,
    %
    P13 = [<<"string">>,2],
    R13 = <<"(2)::varchar">>,
    %
    {"do_build_expression_string test",
        [
         ?_assertEqual(R0, do_build_expression_string(P0,"expression")),
         ?_assertEqual(R1, do_build_expression_string(P1,"expression")),
         ?_assertEqual(R2, do_build_expression_string(P2,"expression")),
         ?_assertEqual(R3, do_build_expression_string(P3,"expression")),
         ?_assertEqual(R4, do_build_expression_string(P4,"expression")),
         ?_assertEqual(R5, do_build_expression_string(P5,"expression")),
         ?_assertEqual(R6, do_build_expression_string(P6,"expression")),
         ?_assertEqual(R7, do_build_expression_string(P7,"expression")),
         ?_assertEqual(R8, do_build_expression_string(P8,"expression")),
         ?_assertEqual(R9, do_build_expression_string(P9,"expression")),
         ?_assertEqual(R10, do_build_expression_string(P10,"expression")),
         ?_assertEqual(R11, do_build_expression_string(P11,"expression")),
         ?_assertEqual(R12, do_build_expression_string(P12,"expression")),
         ?_assertEqual(R13, do_build_expression_string(P13,"expression"))
        ]}.
-endif.

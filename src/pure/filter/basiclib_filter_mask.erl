%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Regex creation for mask
%%% @todo

-module(basiclib_filter_mask).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([do_make_re/1,
         do_make_route_re/1,
         check_modifier_re/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------------------------------
%% make regex search object by mask
%% -----------------------------------------------------------

%% --------------------------------------------
%% create regex filter by mask
%% "*", "?", "$"
%% --------------------------------------------
-spec do_make_re(Mask::string()|binary()) -> {re_pattern,_,_,_,_} | {error, Reason::string()}.
%% --------------------------------------------
do_make_re(M) ->
    F = fun($+,{false,Acc}) -> {false,lists:reverse("[+]")++Acc};
           ($+,{true,Acc}) -> {true,[$+|Acc]};
           ($*,{false,Acc}) -> {false,lists:reverse(".*")++Acc};
           ($*,{true,Acc}) -> {true,[$*|Acc]};
           ($.,{false,Acc}) -> {false,lists:reverse("[.]")++Acc};
           ($.,{true,Acc}) -> {true,[$.|Acc]};
           ($[,{false,Acc}) -> {true,[$[|Acc]};
           ($[,{_,_}) -> throw({error,"Invalid mask ([)"});
           ($],{true,Acc}) -> {false,[$]|Acc]};
           ($],{_,_}) -> throw({error,"Invalid mask (])"});
           ($X,{false,Acc}) -> {false,[$.|Acc]};
           (C,{Br,Acc}) when C>=$0, C=<$9 -> {Br,[C|Acc]};
           (C,{Br,Acc}) when C>=$a, C=<$z -> {Br,[C|Acc]};
           (C,{Br,Acc}) when C>=$A, C=<$Z -> {Br,[C|Acc]};
           ($-,{Br,Acc}) -> {Br,[$-|Acc]};
           ($_,{Br,Acc}) -> {Br,[$_|Acc]};
           ($#,{Br,Acc}) -> {Br,[$#|Acc]};
           ($@,{Br,Acc}) -> {Br,[$@|Acc]};
           % --- for domains
           ($?,{false,Acc}) -> {false,lists:reverse("[^.]")++Acc};
           ($?,{true,Acc}) -> {true,[$?|Acc]};
           ($$,{false,Acc}) -> {false,lists:reverse("[^.]+")++Acc};
           ($$,{true,Acc}) -> {true,[$$|Acc]};
           % ---
           (A,_) -> throw({error,"Invalid mask ("++[A]++")"})
        end,
    M1 = ?BU:to_list(M),
    case try lists:foldl(F,{false,[]},M1) catch throw:R -> R end of
        {error,_}=Err -> Err;
        {_,M2} ->
            M3 = lists:reverse(M2),
            case catch re:compile("^"++M3++"$") of
                {'EXIT',_} -> {error, "Invalid mask (compile)"};
                {ok,FRe} -> FRe
            end
    end.

%% --------------------------------------------
%% create regex filter function by mask (for using in routes)
%% --------------------------------------------
-spec do_make_route_re(Mask::string()) ->
    {re, Regex::term()} |
    {fun2, MatchFun::function()} |
    {error, Reason::string()}.
%% --------------------------------------------
do_make_route_re(M) ->
    B1 = ?BU:to_binary(M),
    B2 = binary:replace(binary:replace(B1, <<"{F}">>, <<21>>, [global]), <<"{f}">>, <<21>>, [global]),
    B3 = binary:replace(binary:replace(B2, <<"{T}">>, <<22>>, [global]), <<"{t}">>, <<22>>, [global]),
    B4 = binary:replace(binary:replace(B2, <<"{E}">>, <<23>>, [global]), <<"{e}">>, <<23>>, [global]),
    M1 = ?BU:to_list(B4),
    % true - in sq brackets, false - out of sq brackets
    F = fun($+,{false,Acc}) -> {false,lists:reverse("[+]")++Acc};
           ($+,{true,Acc}) -> {true,[$+|Acc]};
           ($*,{false,Acc}) -> {false,lists:reverse(".*")++Acc};
           ($*,{true,Acc}) -> {true,[$*|Acc]};
           ($.,{false,Acc}) -> {false,lists:reverse("[.]")++Acc};
           ($.,{true,Acc}) -> {true,[$.|Acc]};
           ($[,{false,Acc}) -> {true,[$[|Acc]};
           ($[,{_,_}) -> throw({error,"Invalid mask ([)"});
           ($],{true,Acc}) -> {false,[$]|Acc]};
           ($],{_,_}) -> throw({error,"Invalid mask (])"});
           ($X,{false,Acc}) -> {false,[$.|Acc]};
           ($?,{false,Acc}) -> {false,[$.|Acc]};
           (C,{Br,Acc}) when C>=$0, C=<$9 -> {Br,[C|Acc]};
           (C,{Br,Acc}) when C>=$a, C=<$z -> {Br,[C|Acc]};
           (C,{Br,Acc}) when C>=$A, C=<$Z -> {Br,[C|Acc]};
           ($-,{Br,Acc}) -> {Br,[$-|Acc]};
           ($_,{Br,Acc}) -> {Br,[$_|Acc]};
           ($#,{Br,Acc}) -> {Br,[$#|Acc]};
           ($@,{Br,Acc}) -> {Br,[$@|Acc]};
           % --- for routes
           (21,{Br,Acc}) -> {Br,[21|Acc]};    % from
           (22,{Br,Acc}) -> {Br,[22|Acc]};    % to
           (23,{Br,Acc}) -> {Br,Acc};         % empty
           % ---
           (A,_) -> throw({error,"Invalid mask ("++[A]++")"})
        end,
    case try lists:foldl(F,{false,[]},M1) catch throw:R -> R end of
        {error,_}=Err -> Err;
        {_,M2} ->
            M3 = lists:reverse(M2),
            case {B1==B2,B2==B3} of % {NoFromLink, NoToLink}
                {true,true} ->
                    case catch re:compile("^"++M3++"$") of
                        {'EXIT',_} -> {error, "Invalid mask (compile 1)"};
                        {ok,FRe} -> {fun2, fun(X,_) ->
                                                case re:run(X, FRe) of
                                                    nomatch -> false;
                                                    {match,_} -> true
                                                end end}
                    end;
                _ ->
                    M3B1 = ?BU:to_binary(M3),
                    M3B2 = binary:replace(M3B1,<<21>>,<<"(\\1)">>,[global]),
                    M3B3 = binary:replace(M3B2,<<22>>,<<"(\\2)">>,[global]),
                    M4 = ?BU:to_list(M3B3),
                    Re = "^(.*)>(.*)<"++M4++"$",
                    case catch re:compile(Re) of
                        {'EXIT',_} -> {error, "Invalid mask (compile 2)"};
                        {ok,FRe} -> {fun2, fun(X,{From,To}) ->
                                                V = ?BU:to_list(From)++">"++?BU:to_list(To)++"<"++?BU:to_list(X),
                                                case re:run(V, FRe) of
                                                    nomatch -> false;
                                                    {match,_} -> true
                                                end end}
                    end
            end
    end.

%% --------------------------------------------
%% check correctness of regex modifier mask
%% --------------------------------------------
check_modifier_re(<<"/reg/",_/bitstring>>=Mod) ->
    lists:foldl(fun(_, {error,_}=Err) -> Err;
        (R, _) ->
            case binary:split(R, [<<"/">>], [global]) of
                [Pattern,_,_] ->
                    case catch re:compile(?BU:to_list(Pattern)) of
                        {ok,_} -> ok;
                        {'EXIT',_} -> {error, {pattern, Pattern}};
                        _ -> {error, {pattern, Pattern}}
                    end;
                _ -> {error, {format, <<"/reg/",R/bitstring>>}}
            end end, ok, binary:split(Mod, [<<"/reg/">>], [global, trim_all]));
check_modifier_re(Mod) when is_binary(Mod) -> ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

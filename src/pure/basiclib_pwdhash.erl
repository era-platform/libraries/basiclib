%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.10.2016.
%%% @doc Service of encryption and decryption of passwords. To use in open sources (config, etc.)

-module(basiclib_pwdhash).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enhash/1, % TODO rename to 'encrypt' or something
         dehash/1, % TODO rename to 'decrypt' (there's NO such thing as dehashing, because HASH is supposed to be one-way!)
         encrypt_rsa_deprecated/1,
         decrypt_rsa_deprecated/1,
         encrypt_rsa/1,
         decrypt_rsa/1,
         sign_dsa/1,
         verify_dsa/2,
         verify_dsa_3/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(PREFIX, "ha_v=1;").

%% ====================================================================
%% API functions
%% ====================================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ---------------------------
%% encrypt value with RSA
%% ---------------------------
enhash(<<?PREFIX,_/bitstring>> = EncData) -> EncData;
enhash(Data) when is_binary(Data) ->
    X1 = encrypt_rsa(Data),
    X2 = <<3,7,3,7,3,7,3,9,X1/bitstring>>,
    X3 = ?BU:to_binary(?BU:bin_to_hexstr(X2)),
    _X4 = <<?PREFIX,X3/bitstring>>.

%% ---------------------------
%% decrypt value with RSA
%% ---------------------------
dehash(<<?PREFIX,Data/bitstring>>) ->
    case catch ?BU:hexstr_to_bin(?BU:to_list(Data)) of
        {'EXIT',_} -> Data;
        <<3,7,3,7,3,7,3,9,X1/bitstring>> ->
            case catch decrypt_rsa(X1) of
                {'EXIT',_} -> Data;
                X2 when is_binary(X2) -> X2
            end;
        <<3,7,3,7,3,7,3,8,X1/bitstring>> ->
            case catch decrypt_rsa_deprecated(X1) of
                {'EXIT',_} -> Data;
                X2 when is_binary(X2) -> X2
            end
    end;
dehash(Data) when is_binary(Data) -> Data.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ---------------------------
%% encrypt rsa by public key1
%% ---------------------------
encrypt_rsa(Data) when is_binary(Data) ->
    [RSAEntry] = public_key:pem_decode(list_to_binary(public_key(1))),
    PrivKey = public_key:pem_entry_decode(RSAEntry),
    _Sign = public_key:encrypt_public(Data, PrivKey).

%% ---------------------------
%% decrypt rsa by private key1
%% ---------------------------
decrypt_rsa(CipherText) when is_binary(CipherText) ->
    [RSAEntry] = public_key:pem_decode(list_to_binary(private_key(1))),
    PubKey = public_key:pem_entry_decode(RSAEntry),
    public_key:decrypt_private(CipherText, PubKey).

%% ---------------------------
%% encrypt rsa by private key1
%% ---------------------------
encrypt_rsa_deprecated(Data) when is_binary(Data) ->
    [RSAEntry] = public_key:pem_decode(list_to_binary(private_key(1))),
    PrivKey = public_key:pem_entry_decode(RSAEntry),
    _Sign = public_key:encrypt_private(Data, PrivKey).

%% ---------------------------
%% decrypt rsa by public key1
%% ---------------------------
decrypt_rsa_deprecated(CipherText) when is_binary(CipherText) ->
    [RSAEntry] = public_key:pem_decode(list_to_binary(public_key(1))),
    PubKey = public_key:pem_entry_decode(RSAEntry),
    public_key:decrypt_public(CipherText, PubKey).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ---------------------------
%% sign data by private key1
%% ---------------------------
sign_dsa(Data) ->
    [RSAEntry] = public_key:pem_decode(list_to_binary(private_key(1))),
    PrivKey = public_key:pem_entry_decode(RSAEntry),
    _Sign = public_key:sign(Data, sha, PrivKey).

%% ---------------------------
%% verify signature by public key1
%% ---------------------------
verify_dsa(Data,Sign) ->
    [RSAEntry] = public_key:pem_decode(list_to_binary(public_key(1))),
    PubKey = public_key:pem_entry_decode(RSAEntry),
    public_key:verify(Data, sha, Sign, PubKey).

%% ---------------------------
%% verify signature by key3
%% ---------------------------
verify_dsa_3(Data,Sign) ->
    [RSAEntry] = public_key:pem_decode(list_to_binary(public_key(3))),
    PubKey = public_key:pem_entry_decode(RSAEntry),
    public_key:verify(Data, sha, Sign, PubKey).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Keys
%% ====================================================================

% @private
-define(PUBLIC_KEY_1, "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEWZ5thEtdMYB4EQv+5eiqso6R
Z2aDvP/Cbcej+Ub235KJaFRJXQKwm8IJIfIUHrIlymaAWbQdTe4QsGjM9RBFNhXo
PndVorAzI36oVjhkBUsAplv197pglkqVpg9Dav66jAIeVGlbUWhkct2kVAF7T/BC
ju0q2qxFh8eQOWBX2QIDAQAB
-----END PUBLIC KEY-----").

-define(PRIVATE_KEY_1, "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQDEWZ5thEtdMYB4EQv+5eiqso6RZ2aDvP/Cbcej+Ub235KJaFRJ
XQKwm8IJIfIUHrIlymaAWbQdTe4QsGjM9RBFNhXoPndVorAzI36oVjhkBUsAplv1
97pglkqVpg9Dav66jAIeVGlbUWhkct2kVAF7T/BCju0q2qxFh8eQOWBX2QIDAQAB
AoGAEBg4gEmj09PG8BCVpyM5WySfwxNyt8G6RTmyb6JzSrEw1cqTfh/9pHRgB1ec
/EE34RkiJcs0lGJa7UtIi+RanOzo0+JZ9Hs4nC0dIE3wsUqGwc0Vz6ChzgCJODGN
0WeGDK0tiuBjAWUZNOk6+YxKBuNzLpWobuAAF6eoboqY7gECQQDvOg7C/TaygfQh
rJ2P40eg5xT5XDS+1LsHMe+d9B+hzPDwEs7euT43fAaMpoywS2H9TT5LHFQ6FX6c
FlxUeSFJAkEA0h30x6L3/v8xM1GwRpIlMqL/44jGcXE+g4F0Je/rTI5rGXJDKxEN
qAAYZe9JndKKSfXWqsvbX0GWUzGTDw4SEQJAJ6nmnKnDEeG+Cn9GOH8kdycZMSb6
j9FRmSXMZtH9FKSYJhXYOtFX4Xcs1tEebnRxMMBH3cg6ps69iT+j21WiEQJACFA5
HCD9kPBF4RLtV412vU8nsrf5qgliANABaXLZq3jDEgmsUKrnEGuYi4fxMLZg1FZO
vQk9z8IHkZkXiXP0EQJBAOlKbpI9GaoRwVmVmr3GCyjuP/RQxNs/EqzZ8XkH0Eoc
KDgzRSbSMvRN2gE332Zl2EPStUiNvwkpvHi9aP2N14Q=
-----END RSA PRIVATE KEY-----").


-define(PUBLIC_KEY_2, "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYmpfJywyHiHrsZ598Or4yNBn1
c2G2MJ4wOMVoleY8CcVJUDuiuR2INsvi1ma4BD2c7UGcDwbB9MJw9jzH7mjq4i7i
189vUiiTlR/vcJN2QSu6Uk9NzlWiTRh9illubNbCFUZtqYURVmtM779sdFvOEw3N
z5f4ZFx7t3JeEdedcQIDAQAB
-----END PUBLIC KEY-----").

%% -define(PRIVATE_KEY_2, "-----BEGIN RSA PRIVATE KEY-----
%% MIICWwIBAAKBgQCYmpfJywyHiHrsZ598Or4yNBn1c2G2MJ4wOMVoleY8CcVJUDui
%% uR2INsvi1ma4BD2c7UGcDwbB9MJw9jzH7mjq4i7i189vUiiTlR/vcJN2QSu6Uk9N
%% zlWiTRh9illubNbCFUZtqYURVmtM779sdFvOEw3Nz5f4ZFx7t3JeEdedcQIDAQAB
%% AoGAGTL9+Z6D9SV+VudNmLAClDuyRavF6NXiaUgWoQyPtj5Oy6pqDpbO351c0Zph
%% 3+UjX3R2LxT0IrcOHKUsqD8gJ9fmwrvf24IB+prprO9WP45XiPGLehyK0QSWKeam
%% KQgFmkOt4QZXy1EZaF+w+6G+/b3GAwbccBaD3rDaBIYARlkCQQDF7fvjspy1emXy
%% RVfS9zxmEKMNj/l+Scl/OBDj/CTMSQva1fwDO1IpJMo5evU4RiYb9wsqHko+ChbQ
%% KRtwpCavAkEAxWBNOZk30EL0+9bbuDfnzfmQdDC99S/oho0NA9l8k6uC0XPhiO+U
%% ey33+FQwZJFvFEkgrhk86W79Ih/UoY+F3wJAKsh0SBWVlgys+J9QioNyhMVX2Vij
%% n06nxV4DGKSp95Zaf6T9Yl1yNv6Cz8XqN43CMw1FVodRmPNpVvlruv9gdQJAGcYf
%% j3vwi+OSdihyfLXgMUd7k5Ch3Zip9pgIsQ3/dJEa/hrxwp32d7djACyOPTO4m5+J
%% AvMMvzbdG02oyIgjZQJATCaqz/ZpofxfA2tF8DBXWxGoCCAwDcfBZwQUi5pZakXK
%% u8W4st8+z/2XDwgh9ejHS971ZwprOVTvZnMydtaIMg==
%% -----END RSA PRIVATE KEY-----").


-define(PUBLIC_KEY_3, "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDgqIMB+7m0zoDLfRu6AfETzKg0
jexuOpeoawYX1qUxqNOAtJLqk/kN2I2OWHHoiaEl5Y+f2sBvkla3qlN7BtrV20Ku
NMIrx+R4QGZA4zGUpXHa44MW+YRmGJWtA4iLZAYDygrjuP+FsjPHOz3czp3x/2JP
ZdtSOkMv78SafvIoRQIDAQAB
-----END PUBLIC KEY-----").

%% -define(PRIVATE_KEY_3, "-----BEGIN RSA PRIVATE KEY-----
%% MIICXgIBAAKBgQDgqIMB+7m0zoDLfRu6AfETzKg0jexuOpeoawYX1qUxqNOAtJLq
%% k/kN2I2OWHHoiaEl5Y+f2sBvkla3qlN7BtrV20KuNMIrx+R4QGZA4zGUpXHa44MW
%% +YRmGJWtA4iLZAYDygrjuP+FsjPHOz3czp3x/2JPZdtSOkMv78SafvIoRQIDAQAB
%% AoGBAM0mVGvNOOn44PfxSQ50k3bnHnrPVyXZb4AmKyw9oCBW8LzJKJf5hCa2p51i
%% +rHEbFm05ZCuK6g20+VoLk9cFRwaxfsvd3Zh9BDrjETTimPgRkv5rZvTc8nmqwb/
%% mqwRAVBhGgXAOtcvrzdNWoSfu3lKX5s8GOUcZCOxcbvqWS4JAkEA9L6vZhYwp0IG
%% 5lBFY8eHNY1qWU0qU9VtwRGMM09Cmt7mUsQl1dGH3qp4Lu8KmUMrWj1VT33hXjFW
%% L1PjRvXsFwJBAOr9WjKCYJHwXFMbtE0nwGBw1LSR1gXAo7gyDNWFWc/qBA4Px2Za
%% /H1MG8p3llH/TpSuIxO4in6OFxYdjWz9PAMCQQDDgoRZN8a8Bim+2EbmM2EWbZaU
%% KelzON7JSXxHDLz3PLKfUWOsEArK3EDPtVMxJZA/MRmyO6vJ8/2WJxw69n8XAkEA
%% nDBZS0ktNtM0CmlXphDN5UwZlQt53cj1CPD7ZtCDqZY5q78BIh9Hw2Ba+HAUEdxI
%% Nrqnxeva4dM/tQtffRm0UQJAeTlrO6wpIvc+Js4JZnpl+1yz5ymGEAvdnjcNsX1i
%% 7XrtTxrH/8bWnwnVfCl/T8SxKUEGE5HTKhPlvcC2+dnDDg==
%% -----END RSA PRIVATE KEY-----").

public_key(1) -> ?PUBLIC_KEY_1;
%public_key(2) -> ?PUBLIC_KEY_2; % dialyzer says this case is never used.
public_key(3) -> ?PUBLIC_KEY_3.

private_key(1) -> ?PRIVATE_KEY_1.
%private_key(2) -> ?PRIVATE_KEY_2. % dialyzer says this case is never used.
%private_key(3) -> ?PRIVATE_KEY_3.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

f_test_() ->
    {"pwdhash test",
     [?_assertMatch(<<"ha_v=1;",_/binary>>, enhash(<<"asdf">>)),
      ?_assertEqual(<<"asdf">>, dehash(enhash(<<"asdf">>))),
      ?_assertEqual(<<"asdf">>, dehash(enhash(enhash(<<"asdf">>)))),
      ?_assertMatch(<<"asdf">>, dehash(<<"ha_v=1;03070307030703092431619E75DB8DA84AB1200CD04B0DB12B606AFAE3CBD58A4CF4737F534EA6BACFD79A1FD98278BB89BDE9A01121653A1A6087C3AFBEAC5A7FC893DDE95BFBE0193137F1512400E106A4CB8563763A9BB5509161739926A4A34203F3FD70948B33DF131CE1C8CA5C3EDACE82AEC9093979F9389A021EAA008940BB9B125F3CC9">>)),
      ?_assertMatch(<<"asdf">>, dehash(<<"ha_v=1;03070307030703087523CAB5FD0B4E47CF591831BC4C9217A1B7055FCDA47F6E8DB08B19EE0D62B2957CCA8D45740534C8100DE81D11E079C2443CA81B4C0185C0AAEA46B77816A46D06AF1685E8B4DE24D1305D96C22C162E25158D958A742D8905B6BF2D42E3F3DB8560F27451BE6C33885646370BD917AFBB3B0B39A5F360DD8F235A632D9C89">>)),
      ?_assertMatch(<<"asdf">>, dehash(dehash(<<"ha_v=1;0307030703070309211F36E709488BA805379D5EFC62F4D54E5D437A267F8896056E690F47D3100B439D618C89F2CFDCF8CE3866E61E2C5B005937F673FE8435DF88B5445E7B2BF5E278CF04F87FAF78E2F9A2A7AFE606E3C582206EA6CAC73024516BEC242AC770FB0C96E13274BBFC3DC50962DC15F3AAC78D160EAD4113EA2B307B6B2F73B2F6">>))),
      ?_assertMatch(X when is_binary(X), encrypt_rsa(<<"asdf">>)),
      ?_assertEqual(<<"asdf">>, decrypt_rsa(encrypt_rsa(<<"asdf">>))),
      ?_assertMatch(X when is_binary(X), sign_dsa(<<"asdf">>)),
      ?_assertEqual(true, verify_dsa(<<"asdf">>,sign_dsa(<<"asdf">>)))
      ]}.

-endif.


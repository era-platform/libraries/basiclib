%% ====================================================================
%% Modules
%% ====================================================================

-define(Parse, basiclib_txtnum_parser).

%% ====================================================================
%% Number Text Type
%% ====================================================================

-define(NttHours,                   0).
-define(NttMinutes,                 1).
-define(NttSeconds,                 2).
-define(NttDays,                    3).
-define(NttMonths,                  4).
-define(NttYears,                   5).
-define(NttDollars,                 6).
-define(NttCents,                   7).
-define(NttEurs,                    8).
-define(NttEurCents,                9).
-define(NttRoubles,                 10).
-define(NttCopecs,                  11).
-define(NttCurrencyNationalMaj,     12).
-define(NttCurrencyNationalMin,     13).
-define(NttCurrencyCustomMaj,       14).
-define(NttCurrencyCustomMin,       15).
-define(NttElements,                16).
-define(NttOperators,               17).
-define(NttTasks,                   18).
-define(NttProjects,                19).
-define(NttLists,                   20).
-define(NttTables,                  21).
-define(NttUsers,                   22).
-define(NttNumbers,                 23).
-define(NttFiles,                   24).
-define(NttTrunk,                   25).
-define(NttLines,                   26).
-define(NttScripts,                 27).
-define(NttRows,                    28).
-define(NttColumns,                 29).
-define(NttWorkDays,                30).
-define(NttWorkDaysShort,           31).
-define(NttDaysExtraShort,          32).
-define(NttOperations,              33).
-define(NttRoutes,                  34).
-define(NttNews,                    35).
-define(NttPercent,                 36).

%% ====================================================================
%% Number representation
%% ====================================================================

-record(number_representation,
        {
         lang = 'ru' :: 'ru'|'en',
         lang_module = basiclib_txtnum_ru :: basiclib_txtnum_ru | basiclib_txtnum_en,
         text_module = basiclib_txtnum_ru_txt :: basiclib_txtnum_ru_txt | basiclib_txtnum_en_txt,
         mode = 'byformat' :: 'byformat'|'quantity'|'ordinal'|'phone'|'date'|'time'|'datetime'|'weekday'|'timewo0b'|'timewo0e'|'timesec'|'moneyusd'|'moneyeur'|'moneyrub'|'percent',
         format = 'sym1' :: 'sym1'|'sym2'|'sym3'|'number'|'letters'|'words',
         gender = 'masculine' :: 'masculine'|'feminine'|'neuter',
         downTone = false :: 'true'|'false'
        }).

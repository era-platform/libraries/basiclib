%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2018
%%% @doc Database morphemes of the English language.

-module(basiclib_txtnum_en_txt).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("../basiclib_txtnum.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
major_names(0) -> <<"">>;
major_names(1) -> <<"thousand">>;
major_names(2) -> <<"million">>;
major_names(3) -> <<"billion">>;
major_names(4) -> <<"trillion">>;
major_names(5) -> <<"quadrillion">>;
major_names(6) -> <<"quintillion">>;
major_names(7) -> <<"sextillion">>;
major_names(8) -> <<"septillion">>;
major_names(9) -> <<"octillion">>;
major_names(10) -> <<"nonillion">>;
major_names(11) -> <<"decillion">>.

%% --------------------------------------
%%
%% --------------------------------------
major_names_th(0) -> <<"">>;
major_names_th(1) -> <<"thousandth">>;
major_names_th(2) -> <<"millionth">>;
major_names_th(3) -> <<"billionth">>;
major_names_th(4) -> <<"trillionth">>;
major_names_th(5) -> <<"quadrillionth">>;
major_names_th(6) -> <<"quintillionth">>;
major_names_th(7) -> <<"sextillionth">>;
major_names_th(8) -> <<"septillionth">>;
major_names_th(9) -> <<"octillionth">>;
major_names_th(10) -> <<"nonillionth">>;
major_names_th(11) -> <<"decillionth">>.

%% --------------------------------------
%%
%% --------------------------------------
tens_names(0) -> <<"">>;
tens_names(1) -> <<"ten">>;
tens_names(2) -> <<"twenty">>;
tens_names(3) -> <<"thirty">>;
tens_names(4) -> <<"fourty">>;
tens_names(5) -> <<"fifty">>;
tens_names(6) -> <<"sixty">>;
tens_names(7) -> <<"seventy">>;
tens_names(8) -> <<"eighty">>;
tens_names(9) -> <<"ninety">>.

%% --------------------------------------
%%
%% --------------------------------------
tens_names_th(0) -> <<"">>;
tens_names_th(1) -> <<"tenth">>;
tens_names_th(2) -> <<"twentieth">>;
tens_names_th(3) -> <<"thirtieth">>;
tens_names_th(4) -> <<"fourtieth">>;
tens_names_th(5) -> <<"fiftieth">>;
tens_names_th(6) -> <<"sixtieth">>;
tens_names_th(7) -> <<"seventieth">>;
tens_names_th(8) -> <<"eightieth">>;
tens_names_th(9) -> <<"ninetieth">>.

%% --------------------------------------
%%
%% --------------------------------------
num_names(0) -> <<"">>;
num_names(1) -> <<"one">>;
num_names(2) -> <<"two">>;
num_names(3) -> <<"three">>;
num_names(4) -> <<"four">>;
num_names(5) -> <<"five">>;
num_names(6) -> <<"six">>;
num_names(7) -> <<"seven">>;
num_names(8) -> <<"eight">>;
num_names(9) -> <<"nine">>;
num_names(10) -> <<"ten">>;
num_names(11) -> <<"eleven">>;
num_names(12) -> <<"twelve">>;
num_names(13) -> <<"thirteen">>;
num_names(14) -> <<"fourteen">>;
num_names(15) -> <<"fifteen">>;
num_names(16) -> <<"sixteen">>;
num_names(17) -> <<"seventeen">>;
num_names(18) -> <<"eighteen">>;
num_names(19) -> <<"nineteen">>.

%% --------------------------------------
%%
%% --------------------------------------
num_names_th(0) -> <<"">>;
num_names_th(1) -> <<"first">>;
num_names_th(2) -> <<"second">>;
num_names_th(3) -> <<"third">>;
num_names_th(4) -> <<"fourth">>;
num_names_th(5) -> <<"fifth">>;
num_names_th(6) -> <<"sixth">>;
num_names_th(7) -> <<"seventh">>;
num_names_th(8) -> <<"eighth">>;
num_names_th(9) -> <<"ninth">>;
num_names_th(10) -> <<"tenth">>;
num_names_th(11) -> <<"eleventh">>;
num_names_th(12) -> <<"twelfth">>;
num_names_th(13) -> <<"thirteenth">>;
num_names_th(14) -> <<"fourteenth">>;
num_names_th(15) -> <<"fifteenth">>;
num_names_th(16) -> <<"sixteenth">>;
num_names_th(17) -> <<"seventeenth">>;
num_names_th(18) -> <<"eighteenth">>;
num_names_th(19) -> <<"nineteenth">>.

%% --------------------------------------
%%
%% --------------------------------------
zero() -> <<"zero">>.
zeroth() -> <<"zeroth">>.
minus() -> <<"negative">>.
point() -> <<"point">>.
o0() -> <<"o0">>.
asterisk() -> <<"asterisk">>.
lattice() -> <<"lattice">>.
dot() -> <<"dot">>.
comma() -> <<"comma">>.
slash() -> <<"slash">>.
backslash() -> <<"backslash">>.
dash() -> <<"dash">>.
double() -> <<"double">>.
tripple() -> <<"tripple">>.
hundred() -> <<"hundred">>.
hundredth() -> <<"hundredth">>.
and_str() -> <<"and">>.
sharp() -> <<"sharp">>.

%% --------
pm() -> <<"pm">>.
am() -> <<"am">>.
the_txt() -> <<"the">>.
of_txt() -> <<"of">>.

%% --------------------------------------
%%
%% --------------------------------------
names(?NttDollars, Count) -> get_number(Count, <<"dollar">>, <<"dollars">>);
names(?NttCents, Count) -> get_number(Count, <<"cent">>, <<"cents">>);
names(?NttEurs, Count) -> get_number(Count, <<"euro">>, <<"euros">>);
names(?NttCurrencyNationalMaj, Count) -> get_number(Count, <<"rouble">>, <<"roubles">>);
names(?NttCurrencyNationalMin, Count) -> get_number(Count, <<"copec">>, <<"copecs">>);
names(?NttCurrencyCustomMaj, Count) -> get_number(Count, <<"pound">>, <<"pounds">>);
names(?NttCurrencyCustomMin, Count) -> get_number(Count, <<"penny">>, <<"pennies">>);
names(?NttPercent, Count) -> get_number(Count, <<"percent">>, <<"percents">>);
names(?NttHours, Count) -> get_number(Count, <<"hour">>, <<"hours">>);
names(?NttMinutes, Count) -> get_number(Count, <<"minute">>, <<"minutes">>);
names(?NttSeconds, Count) -> get_number(Count, <<"second">>, <<"seconds">>);
names(?NttYears, Count) -> get_number(Count, <<"year">>, <<"years">>);
names(_, _) -> <<>>.

%% --------------------------------------
%%
%% --------------------------------------
name_month(1) -> <<"january">>;
name_month(2) -> <<"february">>;
name_month(3) -> <<"march">>;
name_month(4) -> <<"april">>;
name_month(5) -> <<"may">>;
name_month(6) -> <<"june">>;
name_month(7) -> <<"july">>;
name_month(8) -> <<"august">>;
name_month(9) -> <<"september">>;
name_month(10) -> <<"october">>;
name_month(11) -> <<"november">>;
name_month(12) -> <<"december">>;
name_month(_) -> <<>>.

%% --------------------------------------
%%
%% --------------------------------------
name_week_day(1) -> <<"monday">>;
name_week_day(2) -> <<"tuesday">>;
name_week_day(3) -> <<"wednesday">>;
name_week_day(4) -> <<"thursday">>;
name_week_day(5) -> <<"friday">>;
name_week_day(6) -> <<"saturday">>;
name_week_day(7) -> <<"sunday">>;
name_week_day(Str) when is_binary(Str) ->
    StrLo = string:lowercase(Str),
    case re:run(StrLo, <<"^(monday|tuesday|wednesday|thirsday|friday|saturday|sunday)$">>) of
        nomatch -> case re:run(StrLo, "\\A[1|2|3|4|5|6|7|8|9]\\Z") of
                       nomatch -> <<>>;
                       _ -> name_week_day(?BU:to_int(StrLo))
                   end;
        _ -> StrLo
    end;
name_week_day(_) -> <<>>.

%% ====================================================================
%% Internal functions
%% ====================================================================
get_number(1, S1, _SN) -> S1;
get_number(_, _S1, SN) -> SN.


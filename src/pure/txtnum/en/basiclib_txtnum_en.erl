%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2018
%%% @doc The logic of decomposition of a number into morphemes and the formation of an array of words, in accordance with the format. For English

-module(basiclib_txtnum_en).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build/2,
    parse_number_split_by_symbols_count/3,
    parse_number/2,
    parse_number_ordinal/2,
    parse_float_simple_count_wf/4,
    parse_time/4,
    parse_date/2,
    parse_date/3,
    parse_date_time/2,
    parse_phone_simple/2,
    parse_3x_number_with_zeroes/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("../basiclib_txtnum.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
-spec build(Number::any(), Representation::#number_representation{}) -> Result::list().
%% --------------------------------------
build(Number, #number_representation{mode='byformat'}=R) ->
    ?Parse:number_by_type(Number, R);
build(Number, #number_representation{mode='quantity'}=R) ->
    ?Parse:float_simple_count(Number, R);
build(Number, #number_representation{mode='ordinal'}=R) ->
    ?Parse:float_simple_ordinal(Number, R);
build(Number, #number_representation{mode='date'}=R) ->
    ?Parse:date(Number, R);
build(Number, #number_representation{mode='time'}=R) ->
    ?Parse:time(Number, R);
build(Number, #number_representation{mode='timewo0b'}=R) ->
    ?Parse:time_wo0b(Number, R);
build(Number, #number_representation{mode='timewo0e'}=R) ->
    ?Parse:time_wo0e(Number, R);
build(Number, #number_representation{mode='timesec'}=R) ->
    ?Parse:time_in_seconds(Number, R);
build(Number, #number_representation{mode='datetime'}=R) ->
    ?Parse:date_time(Number, R);
build(Number, #number_representation{mode='phone'}=R) ->
    ?Parse:phone(Number, R);
build(Number, #number_representation{mode='weekday'}=R) ->
    ?Parse:week_day(Number, R);
build(Number, #number_representation{mode='moneyusd'}=R) ->
    ?Parse:money_USD(Number, R);
build(Number, #number_representation{mode='moneyeur'}=R) ->
    ?Parse:money_euro(Number, R);
build(Number, #number_representation{mode='moneyrub'}=R) ->
    ?Parse:money_national(Number, R);
build(Number, #number_representation{mode='percent'}=R) ->
    ?Parse:percent(Number, R).

%% --------------------------------------
%%
%% --------------------------------------
parse_number(Number, R)->
    parse_number(Number, ?Parse:txt_function(zero,[],R), R).
parse_number(<<>>, Zero, _R) -> [Zero];
parse_number(Number, Zero, R) when is_binary(Number) ->
    case re:run(Number, "^[0-9]+$") of
        nomatch -> [];
        _ -> case ?BU:to_int(Number) of
                 0 ->
                    F = fun(_, Acc) -> lists:append(Acc,[?Parse:txt_function(zero,[],R)]) end,
                    lists:foldl(F, [], lists:seq(1, string:length(Number)));
                 _ -> convert(Number, false, Zero, R)
             end
    end;
parse_number(Number, Zero, R) ->
    parse_number(?BU:to_binary(Number), Zero, R).

%% --------------------------------------
%%
%% --------------------------------------
parse_number_ordinal(Number, R) when is_binary(Number) ->
    convert(Number, true, ?Parse:txt_function(zero,[],R), R).

%% --------------------------------------
%%
%% --------------------------------------
parse_number_split_by_symbols_count(Number, 1, Repr) ->
    parse_XX_number(1, fun parse_3x_number_with_zeroes/2, [], Number, Repr);
parse_number_split_by_symbols_count(Number, 2, Repr) ->
    {Acc1, Rest} = parse_XX_number_first(2, Number, Repr),
    parse_XX_number(2, fun parse_2x_number_with_zeroes/2, Acc1, Rest, Repr);
parse_number_split_by_symbols_count(Number, 3, Repr) ->
    {Acc1, Rest} = parse_XX_number_first(3, Number, Repr),
    parse_XX_number(3, fun parse_3x_number_with_zeroes/2, Acc1, Rest, Repr).
%% ------------------
%% @private
%% ------------------
parse_XX_number_first(Index, Number, Repr) ->
    L = string:length(Number),
    Fst = L rem Index,
    S1 = binary:part(Number, 0, Fst),
    Rest = binary:part(Number, Fst, string:length(Number)-Fst),
    {parse_2x_number_with_zeroes(S1, Repr), Rest}.
%% ------------------
%% @private
%% ------------------
parse_XX_number(Index, Fun, Acc, Number, Repr) ->
    F = fun(_, {AccIn,R2}) ->
                RR = binary:part(R2, Index, string:length(R2)-Index),
                SS = binary:part(R2, 0, Index),
                NumZ = Fun(SS, Repr),
                {lists:append(AccIn, NumZ), RR}
        end,
    {Result, <<>>} = lists:foldl(F, {Acc,Number}, lists:seq(0, (string:length(Number) div Index) - 1)),
    Result.

%% --------------------------------------
%%
%% --------------------------------------
parse_float_simple_count_wf(NumberW, NumberF, W0, R) ->
    Acc1 = case W0 of false -> parse_number(NumberW, R); true -> [] end,
    Acc2 = lists:append(Acc1, [?Parse:txt_function(point,[],R)]),
    List = ?BU:to_list(NumberF),
    F = fun($0, {AccIn,Z}) -> {AccIn,Z+1};
           (El, {AccIn,Z}) ->
                    UpAccIn = case Z of
                                  1 -> lists:append(AccIn, [?Parse:txt_function(o0,[],R)]);
                                  2 -> lists:append(lists:append(AccIn, [?Parse:txt_function(double,[],R)]), [?Parse:txt_function(o0,[],R)]);
                                  3 -> lists:append(lists:append(AccIn, [?Parse:txt_function(tripple,[],R)]), [?Parse:txt_function(o0,[],R)]);
                                  _ -> AccIn
                              end,
                    {lists:append(UpAccIn, parse_number(<<El>>, R)), 0}
        end,
    {Result,_} = lists:foldl(F, {Acc2,0}, List),
    Result.

%% --------------------------------------
%% time
%% --------------------------------------
parse_time({H,Min,_S}, false, false, R) ->
    UpH = case H of 0 -> 12; _ -> H rem 12 end,
    PmOrAm = case H < 12 of true -> ?Parse:txt_function(am, [], R); false -> ?Parse:txt_function(pm, [], R) end,
    case Min of
        0 -> lists:append(parse_number(UpH, ?Parse:txt_function(o0, [], R), R), [PmOrAm]);
        _ -> Acc1 = parse_number(UpH, ?Parse:txt_function(o0, [], R), R),
             MinBin = ?BU:to_binary(Min),
             MinStr = case Min > 9 of true -> MinBin; false -> <<"0", MinBin/binary>> end,
             Acc2 = lists:append(Acc1, parse_3x_number_with_zeroes(MinStr, ?Parse:txt_function(o0, [], R), R)),
             lists:append(Acc2, [PmOrAm])
    end;
parse_time({H,Min,S}, Filterzeroesatbegin, Filterzeroesatend, R) ->
    Acc1 = add_hour(H, Filterzeroesatend, R),
    Acc2 = add_minute(Min, Filterzeroesatbegin, R),
    Acc3 = add_second(S, Filterzeroesatbegin, H+Min, R),
    Acc4 = add_sharp(S, Filterzeroesatbegin, Filterzeroesatend, Min+S, H+Min, R),
    lists:append([Acc1,Acc2,Acc3,Acc4]).
%% ------------------
%% @private
%% ------------------
add_hour(H, _, R) when (H > 0) -> add_hour_do(H, R);
add_hour(H, true, R) -> add_hour_do(H, R);
add_hour(_, _, _) -> [].
add_hour_do(H, R) ->
    Acc = parse_number(H, R),
    lists:append(Acc, [?Parse:txt_function(names, [?NttHours, H], R)]).
%% ------------------
%% @private
%% ------------------
add_minute(Min, _, R) when (Min > 0) -> add_minute_do(Min, R);
add_minute(_, _, _) -> [].
add_minute_do(Min, R) ->
    Acc = parse_number(Min, R),
    lists:append(Acc, [?Parse:txt_function(names, [?NttMinutes, Min], R)]).
%% ------------------
%% @private
%% ------------------
add_second(S, _, _, R) when (S > 0) -> add_second_do(S, R);
add_second(S, true, 0, R) -> add_second_do(S, R);
add_second(_, _, _, _) -> [].
add_second_do(S, R) ->
    Acc = parse_number(S, R),
    lists:append(Acc, [?Parse:txt_function(names, [?NttSeconds, S], R)]).
%% ------------------
%% @private
%% ------------------
add_sharp(_, _, true, 0, _, R) -> add_sharp_do(R);
add_sharp(0, true, _, _, HMin, R) when (HMin > 0) -> add_sharp_do(R);
add_sharp(_S, _B, _E, _MS, _HM, _) -> [].
add_sharp_do(R) -> [?Parse:txt_function(sharp, [], R)].

%% ----------------------------
%% date
%% ----------------------------
parse_date(Date, R) ->
    parse_date(Date, false, R).
parse_date({Y,M,D}, Withtime, R) ->
    Acc1 = [?Parse:txt_function(name_month, [M], R)],
    Acc2 = lists:append(Acc1, [?Parse:txt_function(the_txt, [], R)]),
    Acc3 = lists:append(Acc2, parse_number_th(D, R)),
    lists:append(Acc3, parse_number_year(Y, Withtime, R)).

%% --------------------------------------
%% datetime
%% --------------------------------------
parse_date_time({{_,_,_}=D,{_,_,_}=T,_}, R) ->
    Acc1 = parse_date(D, true, R),
    Acc2 = lists:append(Acc1, parse_time(T, false, true, R)),
    Acc2.

%% ----------------------------
%% phone
%% ----------------------------
parse_phone_simple(Number, R) ->
    Arr = re:split(Number, "([\\*#\\d]+)", [group]),
    N = lists:foldl(fun([_],Acc) -> Acc;
                       ([_,Match],Acc) -> <<Acc/binary, Match/binary>> end, <<>>, Arr),
    F = fun(I,L,2) -> parse_2x_number_with_zeroes(binary:part(N,I,L), R);
           (I,L,3) -> parse_3x_number_with_zeroes(binary:part(N,I,L), R) end,
    case string:length(N) of
        Min when Min>=0,Min<3 -> parse_2x_number_with_zeroes(N, R);
        3 -> case binary:part(N,1,2) of
                 <<"00">> -> parse_3x_number_with_zeroes(N, R);
                 _ -> lists:append([F(0,1,2),F(1,2,2)])
             end;
        4 -> case binary:part(N,1,2) of
                 <<"00">> -> lists:append([F(0,1,3),F(1,3,3)]);
                 _ -> lists:append([F(0,2,2),F(2,2,2)])
             end;
        Count ->
            St = Count rem 2,
            S = binary:part(N, 0, St),
            Acc1 = parse_2x_number_with_zeroes(S, R),
            End = binary:part(N, St, Count-St),
            FIn = fun(Ind, AccIn) ->
                        SiN = binary:part(End, Ind * 2, 2),
                        lists:append(AccIn, parse_2x_number_with_zeroes(SiN, R))
                end,
            lists:foldl(FIn, Acc1, lists:seq(0, (Count div 2) - 1))
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
convert_less_than_one_thousand(Number, First1000, R) when is_number(Number) ->
    Rem100 = Number rem 100,
    {SoFar, UpNumber} = case (Rem100 < 20) of
                            true -> {case ?Parse:txt_function(num_names, [Rem100], R) of <<>> -> []; SoFar1 -> [SoFar1] end, Number div 100};
                            false ->
                                Rem10 = Number rem 10,
                                Acc1 = case ?Parse:txt_function(num_names, [Rem10], R) of <<>> -> []; SoFar2 -> [SoFar2] end,
                                Div10 = Number div 10,
                                Acc2 = case ?Parse:txt_function(tens_names,[Div10 rem 10], R) of <<>> -> []; SoFar3 -> [SoFar3] end,
                                {lists:append(Acc2,Acc1), Div10 div 10}
                        end,
    convert_less_than_one_thousand_format(UpNumber, SoFar, First1000, R).
%% ------------------
%% @private
%% ------------------
convert_less_than_one_thousand_format(0, SoFar, true, _R) -> SoFar;
convert_less_than_one_thousand_format(0, SoFar, false, R) ->
    And = ?Parse:txt_function(and_str,[],R),
    lists:append([And], SoFar);
convert_less_than_one_thousand_format(Number, [], _, R) ->
    NumAcc = case ?Parse:txt_function(num_names,[Number],R) of <<>> -> []; NumBin -> [NumBin] end,
    Hundred = ?Parse:txt_function(hundred,[],R),
    lists:append(NumAcc, [Hundred]);
convert_less_than_one_thousand_format(Number, SoFar, _, R) ->
    NumAcc = case ?Parse:txt_function(num_names,[Number],R) of <<>> -> []; NumBin -> [NumBin] end,
    And = ?Parse:txt_function(and_str,[],R),
    Hundred = ?Parse:txt_function(hundred,[],R),
    lists:append([NumAcc, [Hundred], [And], SoFar]).

%% --------------------------------------
%%
%% --------------------------------------
convert_less_than_one_thousand_th(Number, First1000, R) when is_number(Number) ->
    Rem100 = Number rem 100,
    {SoFar, UpNumber} = case (Rem100 < 20) of
                            true -> {case ?Parse:txt_function(num_names_th,[Rem100], R) of <<>> -> []; SoFar1 -> [SoFar1] end, Number div 100};
                            false ->
                                Rem10 = Number rem 10,
                                Acc1 = case ?Parse:txt_function(num_names_th,[Rem10],R) of <<>> -> []; SoFar2 -> [SoFar2] end,
                                Div10 = Number div 10,
                                UpSoFar = case Acc1 of
                                              [] -> Acc2 = case ?Parse:txt_function(tens_names_th,[Div10 rem 10],R) of <<>> -> []; SoFar3 -> [SoFar3] end,
                                                    lists:append(Acc2, Acc1);
                                              _ -> Acc3 = case ?Parse:txt_function(tens_names,[Div10 rem 10],R) of <<>> -> []; SoFar4 -> [SoFar4] end,
                                                   lists:append(Acc3, Acc1)
                                          end,
                                {UpSoFar, Div10 div 10}
                        end,
    convert_less_than_one_thousand_format_th(UpNumber, SoFar, First1000, R).
%% ------------------
%% @private
%% ------------------
convert_less_than_one_thousand_format_th(0, SoFar, true, _R) ->    SoFar;
convert_less_than_one_thousand_format_th(0, SoFar, false, R) ->
    And = ?Parse:txt_function(and_str,[],R),
    lists:append([And], SoFar);
convert_less_than_one_thousand_format_th(Number, [], _, R) ->
    NumAcc = case ?Parse:txt_function(num_names,[Number],R) of <<>> -> []; NumBin -> [NumBin] end,
    Hundred = ?Parse:txt_function(hundredth,[],R),
    lists:append(NumAcc, [Hundred]);
convert_less_than_one_thousand_format_th(Number, SoFar, _, R) ->
    NumAcc = case ?Parse:txt_function(num_names,[Number],R) of <<>> -> []; NumBin -> [NumBin] end,
    And = ?Parse:txt_function(and_str,[],R),
    Hundred = ?Parse:txt_function(hundred,[],R),
    lists:append([NumAcc, [Hundred], [And], SoFar]).

%% --------------------------------------
%%
%% --------------------------------------
convert(Numstr, Th, Zero, R) ->
    Number = ?BU:to_int(Numstr),
    case Number of
        0 -> case Th of false -> [Zero]; true -> [?Parse:txt_function(zeroth,[],R)] end;
        _ -> {Prefix,UpNumber} = case Number < 0 of true -> {[?Parse:txt_function(minus,[],R)], Number * -1}; false -> {[],Number} end,
             SoFar = convert_do(UpNumber, Th, R),
             lists:append(Prefix, SoFar)
    end.
convert_do(Number, Th, R) -> convert_do(Number, [], 0, Th, R).
convert_do(0, SoFar, _, _, _) -> SoFar;
convert_do(Number, SoFar, Place, Th, R) ->
    N = Number rem 1000,
    UpNum = Number div 1000,
    case N of
        0 -> convert_do(UpNum, SoFar, Place+1, Th, R);
        _ ->
            S = convert_less(Place, Th, N, UpNum, R),
            UpSoFar = update_sofar(SoFar, Place, Th, S, R),
            convert_do(UpNum, UpSoFar, Place+1, Th, R)
    end.
%% ------------------
%% @private
%% ------------------
convert_less(Place, Th, N, Num, R) when (Place > 0); (Th == false) ->
    convert_less_than_one_thousand(N, Num == 0, R);
convert_less(_, _, N, Num, R) ->
    convert_less_than_one_thousand_th(N, Num == 0, R).
%% ------------------
%% @private
%% ------------------
update_sofar(SoFar, Place, Th, S, R) when (SoFar =/= []); (Th == false) ->
    NameAcc = case ?Parse:txt_function(major_names,[Place],R) of <<>> -> []; Name -> [Name] end,
    lists:append([S, NameAcc, SoFar]);
update_sofar(_S, Place, _, S, R) ->
    NameAcc = case ?Parse:txt_function(major_names_th,[Place],R) of <<>> -> []; Name -> [Name] end,
    lists:append(S,NameAcc).


%% --------------------------------------
%%
%% --------------------------------------
parse_2x_number_with_zeroes(<<>>, _R) -> [];
parse_2x_number_with_zeroes(Number, R) when is_binary(Number) ->
    parse_2x_number_with_zeroes(Number, ?Parse:txt_function(zero,[],R), R);
parse_2x_number_with_zeroes(Number, R) ->
    parse_2x_number_with_zeroes(?BU:to_binary(Number), R).
%% ------------------
%%
%% ------------------
parse_2x_number_with_zeroes(<<"00">>, Zero, R) ->
    lists:append([?Parse:txt_function(double,[],R)],[Zero]);
parse_2x_number_with_zeroes(Number, Zero, R) ->
    parse_3x_number_with_zeroes(Number, Zero, R).

%% --------------------------------------
%%
%% --------------------------------------
parse_3x_number_with_zeroes(<<>>, _R) -> [];
parse_3x_number_with_zeroes(Number, R) when is_binary(Number) ->
    parse_3x_number_with_zeroes(Number, ?Parse:txt_function(zero,[],R), R);
parse_3x_number_with_zeroes(Number, R) ->
    parse_3x_number_with_zeroes(?BU:to_binary(Number), R).
%% ------------------
%%
%% ------------------
parse_3x_number_with_zeroes(<<"000">>, Zero, R) ->
    lists:append([?Parse:txt_function(tripple,[],R)],[Zero]);
parse_3x_number_with_zeroes(<<"***">>, _Zero, R) ->
    lists:append([?Parse:txt_function(tripple,[],R)],[?Parse:txt_function(asterisk,[],R)]);
parse_3x_number_with_zeroes(<<"###">>, _Zero, R) ->
    lists:append([?Parse:txt_function(tripple,[],R)],[?Parse:txt_function(lattice,[],R)]);
parse_3x_number_with_zeroes(<<"00", Num/bitstring>>, Zero, R) ->
    Acc1 = lists:append([?Parse:txt_function(double,[],R)],[Zero]),
    lists:append(Acc1, parse_3x_number_with_zeroes(Num, R));
parse_3x_number_with_zeroes(<<"**", Num/bitstring>>, _Zero, R) ->
    Acc1 = lists:append([?Parse:txt_function(double,[],R)],[?Parse:txt_function(asterisk,[],R)]),
    lists:append(Acc1, parse_3x_number_with_zeroes(Num, R));
parse_3x_number_with_zeroes(<<"##", Num/bitstring>>, _Zero, R) ->
    Acc1 = lists:append([?Parse:txt_function(double,[],R)],[?Parse:txt_function(lattice,[],R)]),
    lists:append(Acc1, parse_3x_number_with_zeroes(Num, R));
parse_3x_number_with_zeroes(<<"0", Num/bitstring>>, Zero, R) ->
    lists:append([Zero], parse_3x_number_with_zeroes(Num, R));
parse_3x_number_with_zeroes(<<"*", Num/bitstring>>, _Zero, R) ->
    lists:append([?Parse:txt_function(asterisk,[],R)], parse_3x_number_with_zeroes(Num, R));
parse_3x_number_with_zeroes(<<"#", Num/bitstring>>, _Zero, R) ->
    lists:append([?Parse:txt_function(lattice,[],R)], parse_3x_number_with_zeroes(Num, R));
parse_3x_number_with_zeroes(Number, Zero, R) ->
    try convert(Number, false, Zero, R) of
        Result -> Result
    catch
        _:_ ->
            Num = binary:part(Number,1,byte_size(Number)-1),
            Acc1 = case binary:first(Number) of
                       $* -> [?Parse:txt_function(asterisk,[],R)];
                       $# -> [?Parse:txt_function(lattice,[],R)];
                       First when (First>48),(First<58) ->
                           convert(Num, false, Zero, R);
                       _ -> []
                   end,
            lists:append(Acc1, parse_3x_number_with_zeroes(Num, Zero, R))
    end.
%% --------------------------------------
%%
%% --------------------------------------
parse_number_th(Number, R) -> convert(Number, true, ?Parse:txt_function(zero,[],R), R).
%% --------------------------------------
%%
%% --------------------------------------
parse_number_year(Number, Withtime, R) when is_number(Number) ->
    Low = Number rem 100,
    High = Number div 100,
    ArrF = convert(?BU:to_binary(Number), false, ?Parse:txt_function(o0,[],R), R),
    ArrH = convert(?BU:to_binary(High), false, ?Parse:txt_function(o0,[],R), R),
    ArrL = convert(?BU:to_binary(Low), false, ?Parse:txt_function(o0,[],R), R),
    parse_number_year_do(High, Low, Withtime, ArrH, ArrF, ArrL, R).
%% ------------------
%% @private
%% ------------------
parse_number_year_do(High, 0, true, ArrH, _ArrF, _ArrL, R) when High >= 10 ->
    % 1100, 1200, ... 1900, 2000, 2100
    lists:append(ArrH, [?Parse:txt_function(hundred,[],R)]);
parse_number_year_do(High, Low, true, ArrH, ArrF, ArrL, R) when High >= 10 ->
    case High rem 10 of
        0 ->
            % 1001, 1002, ... 1099, 2001, 2001, .. 2099, 3001, ...
            And = ?Parse:txt_function(and_str,[],R),
            Filter = fun(El) -> case El of And -> false; _ -> true end end,
            lists:filter(Filter, ArrF);
        _ ->
            % 1895, 1953, 1999, 2102, ..
            Acc1 = lists:append(ArrH, [?Parse:txt_function(hundred,[],R)]),
            Acc2 = case Low < 10 of true -> lists:append(Acc1, [?Parse:txt_function(o0,[],R)]); false -> Acc1 end,
            lists:append(Acc2, ArrL)
    end;
parse_number_year_do(High, 0, false, ArrH, _ArrF, _ArrL, R) when High >= 10 ->
    lists:append(ArrH, [?Parse:txt_function(hundred,[],R)]);
parse_number_year_do(High, Low, false, ArrH, _ArrF, ArrL, R) when High >= 10 ->
    Acc2 = case Low < 10 of true -> lists:append(ArrH, [?Parse:txt_function(o0,[],R)]); false -> ArrH end,
    lists:append(Acc2, ArrL);
parse_number_year_do(High, _Low, _, _ArrH, ArrF, _ArrL, R) when High >= 1 ->
    % 600 - six hundred
    % 612 - six hundred twelve
    And = ?Parse:txt_function(and_str,[],R),
    Filter = fun(El) -> case El of And -> false; _ -> true end end,
    lists:filter(Filter, ArrF);
parse_number_year_do(_High, Low, _, _ArrH, _ArrF, ArrL, R) ->
    % 12 - o o twelve
    % 9 - o o o nine
    Acc1 = [?Parse:txt_function(o0,[],R),?Parse:txt_function(o0,[],R)],
    Acc2 = case Low < 10 of true -> lists:append(Acc1, [?Parse:txt_function(o0,[],R)]); false -> Acc1 end,
    lists:append(Acc2, ArrL).

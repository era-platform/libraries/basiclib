%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2018
%%% @doc Database morphemes of the Russian language.

-module(basiclib_txtnum_ru_txt).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("../basiclib_txtnum.hrl").

-define(M, basiclib_txtnum_ru).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
round_ordinal(Num, G) ->
    K = Num div 100 * 100,
    round_ordinal_do(K, G).

%% --------------------------------------
%%
%% --------------------------------------
hundreeds_ordinal_prefix(Num) ->
    K = Num div 100 * 100,
    hundreeds_ordinal_prefix_do(K).

%% --------------------------------------
%%
%% --------------------------------------
tens_ordinal_prefix(Num) ->
    K = Num div 10 * 10,
    tens_ordinal_prefix_do(K).

%% --------------------------------------
%%
%% --------------------------------------
idents_ordinal_prefix(Num) ->
    K = case Num > 19 of true -> Num rem 10; false -> Num end,
    idents_ordinal_prefix_do(K).

%% --------------------------------------
%%
%% --------------------------------------
ordinal_suffix(0, masculine) -> <<"тысячный"/utf8>>;
ordinal_suffix(0, feminine) -> <<"тысячная"/utf8>>;
ordinal_suffix(0, neuter) -> <<"тысячное"/utf8>>;
ordinal_suffix(1, masculine) -> <<"миллионный"/utf8>>;
ordinal_suffix(1, feminine) -> <<"миллионная"/utf8>>;
ordinal_suffix(1, neuter) -> <<"миллионное"/utf8>>;
ordinal_suffix(2, masculine) -> <<"миллиардный"/utf8>>;
ordinal_suffix(2, feminine) -> <<"миллиардная"/utf8>>;
ordinal_suffix(2, neuter) -> <<"миллиардное"/utf8>>;
ordinal_suffix(3, masculine) -> <<"триллионный"/utf8>>;
ordinal_suffix(3, feminine) -> <<"триллионная"/utf8>>;
ordinal_suffix(3, neuter) -> <<"триллионное"/utf8>>;
ordinal_suffix(4, masculine) -> <<"квадриллионный"/utf8>>;
ordinal_suffix(4, feminine) -> <<"квадриллионная"/utf8>>;
ordinal_suffix(4, neuter) -> <<"квадриллионное"/utf8>>;
ordinal_suffix(5, masculine) -> <<"квинтиллионный"/utf8>>;
ordinal_suffix(5, feminine) -> <<"квинтиллионная"/utf8>>;
ordinal_suffix(5, neuter) -> <<"квинтиллионное"/utf8>>;
ordinal_suffix(6, masculine) -> <<"секстиллионный"/utf8>>;
ordinal_suffix(6, feminine) -> <<"секстиллионная"/utf8>>;
ordinal_suffix(6, neuter) -> <<"секстиллионное"/utf8>>;
ordinal_suffix(7, masculine) -> <<"септиллионный"/utf8>>;
ordinal_suffix(7, feminine) -> <<"септиллионная"/utf8>>;
ordinal_suffix(7, neuter) -> <<"септиллионное"/utf8>>;
ordinal_suffix(8, masculine) -> <<"октиллионный"/utf8>>;
ordinal_suffix(8, feminine) -> <<"октиллионная"/utf8>>;
ordinal_suffix(8, neuter) -> <<"октиллионное"/utf8>>;
ordinal_suffix(9, masculine) -> <<"нониллионный"/utf8>>;
ordinal_suffix(9, feminine) -> <<"нониллионная"/utf8>>;
ordinal_suffix(9, neuter) -> <<"нониллионное"/utf8>>;
ordinal_suffix(10, masculine) -> <<"Дециллионный"/utf8>>;
ordinal_suffix(10, feminine) -> <<"Дециллионная"/utf8>>;
ordinal_suffix(10, neuter) -> <<"Дециллионное"/utf8>>;
ordinal_suffix(_, _) -> <<>>.

%% --------------------------------------
%%
%% --------------------------------------
hundreeds_ordinal(Num, G) ->
    K = Num div 100 * 100,
    round_ordinal_do(K, G).

%% --------------------------------------
%%
%% --------------------------------------
tens_ordinal(Num, G) ->
    K = Num div 10 * 10,
    tens_ordinal_do(K, G).

%% --------------------------------------
%%
%% --------------------------------------
idents_ordinal(Num, G) ->
    K = case Num > 19 of true -> Num rem 10; false -> Num end,
    idents_ordinal_do(K, G).

%% --------------------------------------
%%
%% --------------------------------------
zero_ordinal(masculine) -> <<"нулевой"/utf8>>;
zero_ordinal(feminine) -> <<"нулевая"/utf8>>;
zero_ordinal(neuter) ->  <<"нулевое"/utf8>>;
zero_ordinal(_) ->  <<>>.

%% --------------------------------------
%%
%% --------------------------------------
hundreeds(Num) ->
    K = Num div 100 * 100,
    hundreeds_do(K).

%% --------------------------------------
%%
%% --------------------------------------
tens(Num) ->
    K = Num div 10 * 10,
    tens_do(K).

%% --------------------------------------
%%
%% --------------------------------------
idents(Num, G) ->
    K = case Num > 19 of true -> Num rem 10; false -> Num end,
    idents_do(K, G).

%% --------------------------------------
%%
%% --------------------------------------
name_1000(Num3X, Count) ->
    K = Num3X rem 10,
    K1 = Num3X rem 100,
    name_1000_do(K, K1, Count).

%% --------------------------------------
%%
%% --------------------------------------
zero() -> <<"ноль"/utf8>>.
asterisk() -> <<"звездочка"/utf8>>.
lattice() -> <<"решетка"/utf8>>.
dot() -> <<"точка"/utf8>>.
comma() -> <<"запятая"/utf8>>.
slash() -> <<"слэш"/utf8>>.
backslash() -> <<"бэкслэш"/utf8>>.
dash() -> <<"тире"/utf8>>.
minus() -> <<"минус"/utf8>>.

%% --------------------------------------
%%
%% --------------------------------------
name_wholes(Number) ->
    K = ?BU:to_int(Number),
    get_number(K, <<"целая"/utf8>>, <<"целых"/utf8>>, <<"целых"/utf8>>).

%% --------------------------------------
%%
%% --------------------------------------
name_fractales(Number) ->
    K = ?BU:to_int(Number),
    K1 = K rem 10,
    K10 = K rem 100 / 10,
    name_fractales_do(string:length(Number), K1, K10).

%% --------------------------------------
%%
%% --------------------------------------
names(ENames, Count) ->
    get_number(Count, ENames).

%% --------------------------------------
%%
%% --------------------------------------
name_month(1) -> <<"января"/utf8>>;
name_month(2) -> <<"февраля"/utf8>>;
name_month(3) -> <<"марта"/utf8>>;
name_month(4) -> <<"апреля"/utf8>>;
name_month(5) -> <<"мая"/utf8>>;
name_month(6) -> <<"июня"/utf8>>;
name_month(7) -> <<"июля"/utf8>>;
name_month(8) -> <<"августа"/utf8>>;
name_month(9) -> <<"сентября"/utf8>>;
name_month(10) -> <<"октября"/utf8>>;
name_month(11) -> <<"ноября"/utf8>>;
name_month(12) -> <<"декабря"/utf8>>;
name_month(_) -> <<>>.

%% --------------------------------------
%%
%% --------------------------------------
name_week_day(1) -> <<"понедельник"/utf8>>;
name_week_day(2) -> <<"вторник"/utf8>>;
name_week_day(3) -> <<"среда"/utf8>>;
name_week_day(4) -> <<"четверг"/utf8>>;
name_week_day(5) -> <<"пятница"/utf8>>;
name_week_day(6) -> <<"суббота"/utf8>>;
name_week_day(7) -> <<"воскресенье"/utf8>>;
name_week_day(Str) when is_binary(Str) ->
    StrLo = string:lowercase(Str),
    case re:run(StrLo, <<"^(понедельник|вторник|среда|четверг|пятница|суббота|воскресенье)$"/utf8>>) of
        nomatch -> case re:run(StrLo, "\\A[1|2|3|4|5|6|7|8|9]\\Z") of
                       nomatch -> <<>>;
                       _ -> name_week_day(?BU:to_int(StrLo))
                   end;
        _ -> StrLo
    end;
name_week_day(_) -> <<>>.

%% --------------------------------------
%%
%% --------------------------------------
parse_number_th(1) -> [<<"первое"/utf8>>];
parse_number_th(2) -> [<<"второе"/utf8>>];
parse_number_th(3) -> [<<"третье"/utf8>>];
parse_number_th(4) -> [<<"четвертое"/utf8>>];
parse_number_th(5) -> [<<"пятое"/utf8>>];
parse_number_th(6) -> [<<"шестое"/utf8>>];
parse_number_th(7) -> [<<"седьмое"/utf8>>];
parse_number_th(8) -> [<<"восьмое"/utf8>>];
parse_number_th(9) -> [<<"девятое"/utf8>>];
parse_number_th(10) -> [<<"десятое"/utf8>>];
parse_number_th(11) -> [<<"одиннадцатое"/utf8>>];
parse_number_th(12) -> [<<"двенадцатое"/utf8>>];
parse_number_th(13) -> [<<"тринадцатое"/utf8>>];
parse_number_th(14) -> [<<"четырнадцатое"/utf8>>];
parse_number_th(15) -> [<<"пятнадцатое"/utf8>>];
parse_number_th(16) -> [<<"шестнадцатое"/utf8>>];
parse_number_th(17) -> [<<"семнадцатое"/utf8>>];
parse_number_th(18) -> [<<"восемнадцатое"/utf8>>];
parse_number_th(19) -> [<<"девятнадцатое"/utf8>>];
parse_number_th(20) -> [<<"двадцатое"/utf8>>];
parse_number_th(21) -> [<<"двадцать"/utf8>>, <<"первое"/utf8>>];
parse_number_th(22) -> [<<"двадцать"/utf8>>, <<"второе"/utf8>>];
parse_number_th(23) -> [<<"двадцать"/utf8>>, <<"третье"/utf8>>];
parse_number_th(24) -> [<<"двадцать"/utf8>>, <<"четвертое"/utf8>>];
parse_number_th(25) -> [<<"двадцать"/utf8>>, <<"пятое"/utf8>>];
parse_number_th(26) -> [<<"двадцать"/utf8>>, <<"шестое"/utf8>>];
parse_number_th(27) -> [<<"двадцать"/utf8>>, <<"седьмое"/utf8>>];
parse_number_th(28) -> [<<"двадцать"/utf8>>, <<"восьмое"/utf8>>];
parse_number_th(29) -> [<<"двадцать"/utf8>>, <<"девятое"/utf8>>];
parse_number_th(30) -> [<<"тридцатое"/utf8>>];
parse_number_th(31) -> [<<"тридцать"/utf8>>, <<"первое"/utf8>>];
parse_number_th(_) -> [].

%% --------------------------------------
%%
%% --------------------------------------
parse_number_genitive(2000) -> [<<"двухтысячного"/utf8>>];
parse_number_genitive(Num) ->
    K = Num rem 1000,
    Acc1 = case Num < 2000 of true -> [<<"тысяча"/utf8>>]; false -> case Num > 2000 of true -> [<<"две"/utf8>>, <<"тысячи"/utf8>>]; false -> [] end end,
    lists:append(Acc1, parse_number_genitive_hundredths(K)).

%% ====================================================================
%% Internal functions
%% ====================================================================
round_ordinal_do(100, masculine) -> <<"сотый"/utf8>>;
round_ordinal_do(100, feminine) -> <<"сотая"/utf8>>;
round_ordinal_do(100, neuter) ->  <<"сотое"/utf8>>;
round_ordinal_do(200, masculine) -> <<"двухсотый"/utf8>>;
round_ordinal_do(200, feminine) -> <<"двухсотая"/utf8>>;
round_ordinal_do(200, neuter) ->  <<"двухсотое"/utf8>>;
round_ordinal_do(300, masculine) -> <<"трехсотый"/utf8>>;
round_ordinal_do(300, feminine) -> <<"трехсотая"/utf8>>;
round_ordinal_do(300, neuter) ->  <<"трехсотое"/utf8>>;
round_ordinal_do(400, masculine) -> <<"четырехсотый"/utf8>>;
round_ordinal_do(400, feminine) -> <<"четырехсотая"/utf8>>;
round_ordinal_do(400, neuter) ->  <<"четырехсотое"/utf8>>;
round_ordinal_do(500, masculine) -> <<"пятисотый"/utf8>>;
round_ordinal_do(500, feminine) -> <<"пятисотая"/utf8>>;
round_ordinal_do(500, neuter) ->  <<"пятисотое"/utf8>>;
round_ordinal_do(600, masculine) -> <<"шестисотый"/utf8>>;
round_ordinal_do(600, feminine) -> <<"шестисотая"/utf8>>;
round_ordinal_do(600, neuter) ->  <<"шестисотое"/utf8>>;
round_ordinal_do(700, masculine) -> <<"семисотый"/utf8>>;
round_ordinal_do(700, feminine) -> <<"семисотая"/utf8>>;
round_ordinal_do(700, neuter) ->  <<"семисотое"/utf8>>;
round_ordinal_do(800, masculine) -> <<"восьмисотый"/utf8>>;
round_ordinal_do(800, feminine) -> <<"восьмисотая"/utf8>>;
round_ordinal_do(800, neuter) ->  <<"восьмисотое"/utf8>>;
round_ordinal_do(900, masculine) -> <<"девятисотый"/utf8>>;
round_ordinal_do(900, feminine) -> <<"девятисотая"/utf8>>;
round_ordinal_do(900, neuter) ->  <<"девятисотое"/utf8>>;
round_ordinal_do(_, _) ->  <<>>.
%% --------------------------------------
%%
%% --------------------------------------
hundreeds_ordinal_prefix_do(100) -> <<"сто-"/utf8>>;
hundreeds_ordinal_prefix_do(200) -> <<"двухсот-"/utf8>>;
hundreeds_ordinal_prefix_do(300) -> <<"трехсот-"/utf8>>;
hundreeds_ordinal_prefix_do(400) -> <<"четырехсот-"/utf8>>;
hundreeds_ordinal_prefix_do(500) -> <<"пятисот-"/utf8>>;
hundreeds_ordinal_prefix_do(600) -> <<"шестисот-"/utf8>>;
hundreeds_ordinal_prefix_do(700) -> <<"семисот-"/utf8>>;
hundreeds_ordinal_prefix_do(800) -> <<"восьмисот-"/utf8>>;
hundreeds_ordinal_prefix_do(900) -> <<"девятисот-"/utf8>>;
hundreeds_ordinal_prefix_do(_) -> <<>>.
%% --------------------------------------
%%
%% --------------------------------------
tens_ordinal_prefix_do(20) -> <<"двадцати-"/utf8>>;
tens_ordinal_prefix_do(30) -> <<"тридцати-"/utf8>>;
tens_ordinal_prefix_do(40) -> <<"сорока-"/utf8>>;
tens_ordinal_prefix_do(50) -> <<"пятидесяти-"/utf8>>;
tens_ordinal_prefix_do(60) -> <<"шестидесяти-"/utf8>>;
tens_ordinal_prefix_do(70) -> <<"семидесяти-"/utf8>>;
tens_ordinal_prefix_do(80) -> <<"восьмидесяти-"/utf8>>;
tens_ordinal_prefix_do(90) -> <<"девяносто-"/utf8>>;
tens_ordinal_prefix_do(_) -> <<>>.
%% --------------------------------------
%%
%% --------------------------------------
idents_ordinal_prefix_do(1) -> <<"одно-"/utf8>>;
idents_ordinal_prefix_do(2) -> <<"двух-"/utf8>>;
idents_ordinal_prefix_do(3) -> <<"трех-"/utf8>>;
idents_ordinal_prefix_do(4) -> <<"четырех-"/utf8>>;
idents_ordinal_prefix_do(5) -> <<"пяти-"/utf8>>;
idents_ordinal_prefix_do(6) -> <<"шести-"/utf8>>;
idents_ordinal_prefix_do(7) -> <<"семи-"/utf8>>;
idents_ordinal_prefix_do(8) -> <<"восьми-"/utf8>>;
idents_ordinal_prefix_do(9) -> <<"девяти-"/utf8>>;
idents_ordinal_prefix_do(10) -> <<"десяти-"/utf8>>;
idents_ordinal_prefix_do(11) -> <<"одиннадцати-"/utf8>>;
idents_ordinal_prefix_do(12) -> <<"двенадцати-"/utf8>>;
idents_ordinal_prefix_do(13) -> <<"тринадцати-"/utf8>>;
idents_ordinal_prefix_do(14) -> <<"четырнадцати-"/utf8>>;
idents_ordinal_prefix_do(15) -> <<"пятнадцати-"/utf8>>;
idents_ordinal_prefix_do(16) -> <<"шестнадцати-"/utf8>>;
idents_ordinal_prefix_do(17) -> <<"семнадцати-"/utf8>>;
idents_ordinal_prefix_do(18) -> <<"восемнадцати-"/utf8>>;
idents_ordinal_prefix_do(19) -> <<"девятнадцати-"/utf8>>;
idents_ordinal_prefix_do(_) -> <<>>.
%% --------------------------------------
%%
%% --------------------------------------
tens_ordinal_do(20, masculine) -> <<"двадцатый"/utf8>>;
tens_ordinal_do(20, feminine) -> <<"двадцатая"/utf8>>;
tens_ordinal_do(20, neuter) ->  <<"двадцатое"/utf8>>;
tens_ordinal_do(30, masculine) -> <<"тридцатый"/utf8>>;
tens_ordinal_do(30, feminine) -> <<"тридцатая"/utf8>>;
tens_ordinal_do(30, neuter) ->  <<"тридцатое"/utf8>>;
tens_ordinal_do(40, masculine) -> <<"сороковой"/utf8>>;
tens_ordinal_do(40, feminine) -> <<"сороковая"/utf8>>;
tens_ordinal_do(40, neuter) ->  <<"сороковое"/utf8>>;
tens_ordinal_do(50, masculine) -> <<"пятидесятый"/utf8>>;
tens_ordinal_do(50, feminine) -> <<"пятидесятая"/utf8>>;
tens_ordinal_do(50, neuter) ->  <<"пятидесятое"/utf8>>;
tens_ordinal_do(60, masculine) -> <<"шестидесятый"/utf8>>;
tens_ordinal_do(60, feminine) -> <<"шестидесятая"/utf8>>;
tens_ordinal_do(60, neuter) ->  <<"шестидесятое"/utf8>>;
tens_ordinal_do(70, masculine) -> <<"семидесятый"/utf8>>;
tens_ordinal_do(70, feminine) -> <<"семидесятая"/utf8>>;
tens_ordinal_do(70, neuter) ->  <<"семидесятое"/utf8>>;
tens_ordinal_do(80, masculine) -> <<"восьмидесятый"/utf8>>;
tens_ordinal_do(80, feminine) -> <<"восьмидесятая"/utf8>>;
tens_ordinal_do(80, neuter) ->  <<"восьмидесятое"/utf8>>;
tens_ordinal_do(90, masculine) -> <<"девяностый"/utf8>>;
tens_ordinal_do(90, feminine) -> <<"девяностая"/utf8>>;
tens_ordinal_do(90, neuter) ->  <<"девяностое"/utf8>>;
tens_ordinal_do(_, _) ->  <<>>.
%% --------------------------------------
%%
%% --------------------------------------
idents_ordinal_do(1, masculine) -> <<"первый"/utf8>>;
idents_ordinal_do(1, feminine) -> <<"первая"/utf8>>;
idents_ordinal_do(1, neuter) ->  <<"первое"/utf8>>;
idents_ordinal_do(2, masculine) -> <<"второй"/utf8>>;
idents_ordinal_do(2, feminine) -> <<"вторая"/utf8>>;
idents_ordinal_do(2, neuter) ->  <<"второе"/utf8>>;
idents_ordinal_do(3, masculine) -> <<"третий"/utf8>>;
idents_ordinal_do(3, feminine) -> <<"третья"/utf8>>;
idents_ordinal_do(3, neuter) ->  <<"третье"/utf8>>;
idents_ordinal_do(4, masculine) -> <<"четвертый"/utf8>>;
idents_ordinal_do(4, feminine) -> <<"четвертая"/utf8>>;
idents_ordinal_do(4, neuter) ->  <<"четвертое"/utf8>>;
idents_ordinal_do(5, masculine) -> <<"пятый"/utf8>>;
idents_ordinal_do(5, feminine) -> <<"пятая"/utf8>>;
idents_ordinal_do(5, neuter) ->  <<"пятое"/utf8>>;
idents_ordinal_do(6, masculine) -> <<"шестой"/utf8>>;
idents_ordinal_do(6, feminine) -> <<"шестая"/utf8>>;
idents_ordinal_do(6, neuter) ->  <<"шестое"/utf8>>;
idents_ordinal_do(7, masculine) -> <<"седьмой"/utf8>>;
idents_ordinal_do(7, feminine) -> <<"седьмая"/utf8>>;
idents_ordinal_do(7, neuter) ->  <<"седьмое"/utf8>>;
idents_ordinal_do(8, masculine) -> <<"восьмой"/utf8>>;
idents_ordinal_do(8, feminine) -> <<"восьмая"/utf8>>;
idents_ordinal_do(8, neuter) ->  <<"восьмое"/utf8>>;
idents_ordinal_do(9, masculine) -> <<"девятый"/utf8>>;
idents_ordinal_do(9, feminine) -> <<"девятая"/utf8>>;
idents_ordinal_do(9, neuter) ->  <<"девятое"/utf8>>;
idents_ordinal_do(10, masculine) -> <<"десятый"/utf8>>;
idents_ordinal_do(10, feminine) -> <<"десятая"/utf8>>;
idents_ordinal_do(10, neuter) ->  <<"десятое"/utf8>>;
idents_ordinal_do(11, masculine) -> <<"одиннадцатый"/utf8>>;
idents_ordinal_do(11, feminine) -> <<"одиннадцатая"/utf8>>;
idents_ordinal_do(11, neuter) ->  <<"одиннадцатое"/utf8>>;
idents_ordinal_do(12, masculine) -> <<"двенадцатый"/utf8>>;
idents_ordinal_do(12, feminine) -> <<"двенадцатая"/utf8>>;
idents_ordinal_do(12, neuter) ->  <<"двенадцатое"/utf8>>;
idents_ordinal_do(13, masculine) -> <<"тринадцатый"/utf8>>;
idents_ordinal_do(13, feminine) -> <<"тринадцатая"/utf8>>;
idents_ordinal_do(13, neuter) ->  <<"тринадцатое"/utf8>>;
idents_ordinal_do(14, masculine) -> <<"четырнадцатый"/utf8>>;
idents_ordinal_do(14, feminine) -> <<"четырнадцатая"/utf8>>;
idents_ordinal_do(14, neuter) ->  <<"четырнадцатое"/utf8>>;
idents_ordinal_do(15, masculine) -> <<"пятнадцатый"/utf8>>;
idents_ordinal_do(15, feminine) -> <<"пятнадцатая"/utf8>>;
idents_ordinal_do(15, neuter) ->  <<"пятнадцатое"/utf8>>;
idents_ordinal_do(16, masculine) -> <<"шестнадцатый"/utf8>>;
idents_ordinal_do(16, feminine) -> <<"шестнадцатая"/utf8>>;
idents_ordinal_do(16, neuter) ->  <<"шестнадцатое"/utf8>>;
idents_ordinal_do(17, masculine) -> <<"семнадцатый"/utf8>>;
idents_ordinal_do(17, feminine) -> <<"семнадцатая"/utf8>>;
idents_ordinal_do(17, neuter) ->  <<"семнадцатое"/utf8>>;
idents_ordinal_do(18, masculine) -> <<"восемнадцатый"/utf8>>;
idents_ordinal_do(18, feminine) -> <<"восемнадцатая"/utf8>>;
idents_ordinal_do(18, neuter) ->  <<"восемнадцатое"/utf8>>;
idents_ordinal_do(19, masculine) -> <<"девятнадцатый"/utf8>>;
idents_ordinal_do(19, feminine) -> <<"девятнадцатая"/utf8>>;
idents_ordinal_do(19, neuter) ->  <<"девятнадцатое"/utf8>>;
idents_ordinal_do(_, _) -> <<>>.
%% --------------------------------------
%%
%% --------------------------------------
hundreeds_do(100) -> <<"сто"/utf8>>;
hundreeds_do(200) -> <<"двести"/utf8>>;
hundreeds_do(300) -> <<"триста"/utf8>>;
hundreeds_do(400) -> <<"четыреста"/utf8>>;
hundreeds_do(500) -> <<"пятьсот"/utf8>>;
hundreeds_do(600) -> <<"шестьсот"/utf8>>;
hundreeds_do(700) -> <<"семьсот"/utf8>>;
hundreeds_do(800) -> <<"восемьсот"/utf8>>;
hundreeds_do(900) -> <<"девятьсот"/utf8>>;
hundreeds_do(_) -> <<>>.
%% --------------------------------------
%%
%% --------------------------------------
tens_do(20) -> <<"двадцать"/utf8>>;
tens_do(30) -> <<"тридцать"/utf8>>;
tens_do(40) -> <<"сорок"/utf8>>;
tens_do(50) -> <<"пятьдесят"/utf8>>;
tens_do(60) -> <<"шестьдесят"/utf8>>;
tens_do(70) -> <<"семьдесят"/utf8>>;
tens_do(80) -> <<"восемьдесят"/utf8>>;
tens_do(90) -> <<"девяносто"/utf8>>;
tens_do(_) -> <<>>.
%% --------------------------------------
%%
%% --------------------------------------
idents_do(1, masculine) -> <<"один"/utf8>>;
idents_do(1, feminine) -> <<"одна"/utf8>>;
idents_do(1, neuter) ->  <<"одно"/utf8>>;
idents_do(2, feminine) -> <<"две"/utf8>>;
idents_do(2, _) -> <<"два"/utf8>>;
idents_do(3, _) -> <<"три"/utf8>>;
idents_do(4, _) -> <<"четыре"/utf8>>;
idents_do(5, _) -> <<"пять"/utf8>>;
idents_do(6, _) -> <<"шесть"/utf8>>;
idents_do(7, _) -> <<"семь"/utf8>>;
idents_do(8, _) -> <<"восемь"/utf8>>;
idents_do(9, _) -> <<"девять"/utf8>>;
idents_do(10, _) -> <<"десять"/utf8>>;
idents_do(11, _) -> <<"одиннадцать"/utf8>>;
idents_do(12, _) -> <<"двенадцать"/utf8>>;
idents_do(13, _) -> <<"тринадцать"/utf8>>;
idents_do(14, _) -> <<"четырнадцать"/utf8>>;
idents_do(15, _) -> <<"пятнадцать"/utf8>>;
idents_do(16, _) -> <<"шестнадцать"/utf8>>;
idents_do(17, _) -> <<"семнадцать"/utf8>>;
idents_do(18, _) -> <<"восемнадцать"/utf8>>;
idents_do(19, _) -> <<"девятнадцать"/utf8>>;
idents_do(_, _) -> <<>>.
%% --------------------------------------
%%
%% --------------------------------------
name_1000_do(0, _K1, Count) -> name_1000_do_1(Count);
name_1000_do(5, _K1, Count) -> name_1000_do_1(Count);
name_1000_do(6, _K1, Count) -> name_1000_do_1(Count);
name_1000_do(7, _K1, Count) -> name_1000_do_1(Count);
name_1000_do(8, _K1, Count) -> name_1000_do_1(Count);
name_1000_do(9, _K1, Count) -> name_1000_do_1(Count);
name_1000_do(_, K1, Count) when (K1>10), (K1<20) -> name_1000_do_1(Count);
name_1000_do(1, _K1, Count) -> name_1000_do_2(Count);
name_1000_do(_K, _K1, Count) -> name_1000_do_3(Count).
%% ------------------
%% @private
%% ------------------
name_1000_do_1(0) -> <<"тысяч"/utf8>>;
name_1000_do_1(1) -> <<"миллионов"/utf8>>;
name_1000_do_1(2) -> <<"миллиардов"/utf8>>;
name_1000_do_1(3) -> <<"триллионов"/utf8>>;
name_1000_do_1(4) -> <<"квадриллионов"/utf8>>;
name_1000_do_1(5) -> <<"квинтиллионов"/utf8>>;
name_1000_do_1(6) -> <<"секстиллионов"/utf8>>;
name_1000_do_1(7) -> <<"септиллионов"/utf8>>;
name_1000_do_1(8) -> <<"октиллионов"/utf8>>;
name_1000_do_1(9) -> <<"нониллионов"/utf8>>;
name_1000_do_1(10) -> <<"дециллионов"/utf8>>;
name_1000_do_1(_) -> <<>>.
%% ------------------
%% @private
%% ------------------
name_1000_do_2(0) -> <<"тысяча"/utf8>>;
name_1000_do_2(1) -> <<"миллион"/utf8>>;
name_1000_do_2(2) -> <<"миллиард"/utf8>>;
name_1000_do_2(3) -> <<"триллион"/utf8>>;
name_1000_do_2(4) -> <<"квадриллион"/utf8>>;
name_1000_do_2(5) -> <<"квинтиллион"/utf8>>;
name_1000_do_2(6) -> <<"секстиллион"/utf8>>;
name_1000_do_2(7) -> <<"септиллион"/utf8>>;
name_1000_do_2(8) -> <<"октиллион"/utf8>>;
name_1000_do_2(9) -> <<"нониллион"/utf8>>;
name_1000_do_2(10) -> <<"дециллион"/utf8>>;
name_1000_do_2(_) -> <<>>.
%% ------------------
%% @private
%% ------------------
name_1000_do_3(0) -> <<"тысячи"/utf8>>;
name_1000_do_3(1) -> <<"миллиона"/utf8>>;
name_1000_do_3(2) -> <<"миллиарда"/utf8>>;
name_1000_do_3(3) -> <<"триллиона"/utf8>>;
name_1000_do_3(4) -> <<"квадриллиона"/utf8>>;
name_1000_do_3(5) -> <<"квинтиллиона"/utf8>>;
name_1000_do_3(6) -> <<"секстиллиона"/utf8>>;
name_1000_do_3(7) -> <<"септиллиона"/utf8>>;
name_1000_do_3(8) -> <<"октиллиона"/utf8>>;
name_1000_do_3(9) -> <<"нониллиона"/utf8>>;
name_1000_do_3(10) -> <<"дециллиона"/utf8>>;
name_1000_do_3(_) -> <<>>.

%% --------------------------------------
%%
%% --------------------------------------
get_number(Count, ENumberText) ->
    get_number(Count, false, ENumberText).
%% ------------------
%%
%% ------------------
get_number(Count, Self0, ?NttHours) -> get_number(Count, Self0, <<"0 часов"/utf8>>, <<"час"/utf8>>, <<"часа"/utf8>>, <<"часов"/utf8>>);
get_number(Count, Self0, ?NttMinutes) -> get_number(Count, Self0, <<"0 минут"/utf8>>, <<"минута"/utf8>>, <<"минуты"/utf8>>, <<"минут"/utf8>>);
get_number(Count, Self0, ?NttSeconds) -> get_number(Count, Self0, <<"0 секунд"/utf8>>, <<"секунда"/utf8>>, <<"секунды"/utf8>>, <<"секунд"/utf8>>);
get_number(Count, Self0, ?NttDays) -> get_number(Count, Self0, <<"0 дней"/utf8>>, <<"день"/utf8>>, <<"дня"/utf8>>, <<"дней"/utf8>>);
get_number(Count, Self0, ?NttMonths) -> get_number(Count, Self0, <<"0 месяцев"/utf8>>, <<"месяц"/utf8>>, <<"месяца"/utf8>>, <<"месяцев"/utf8>>);
get_number(_Count, _Self0, ?NttYears) -> <<"года"/utf8>>;
get_number(Count, Self0, ?NttDollars) -> get_number(Count, Self0, <<"0 долларов"/utf8>>, <<"доллар"/utf8>>, <<"доллара"/utf8>>, <<"долларов"/utf8>>);
get_number(Count, Self0, ?NttCents) -> get_number(Count, Self0, <<"0 центов"/utf8>>, <<"цент"/utf8>>, <<"цента"/utf8>>, <<"центов"/utf8>>);
get_number(Count, Self0, ?NttEurs) -> get_number(Count, Self0, <<"0 евро"/utf8>>, <<"евро"/utf8>>, <<"евро"/utf8>>, <<"евро"/utf8>>);
get_number(Count, Self0, ?NttEurCents) -> get_number(Count, Self0, <<"0 евроцентов"/utf8>>, <<"евроцент"/utf8>>, <<"евроцента"/utf8>>, <<"евроцентов"/utf8>>);
get_number(Count, Self0, ?NttRoubles) -> get_number(Count, Self0, <<"0 рублей"/utf8>>, <<"рубль"/utf8>>, <<"рубля"/utf8>>, <<"рублей"/utf8>>);
get_number(Count, Self0, ?NttCopecs) -> get_number(Count, Self0, <<"0 копеек"/utf8>>, <<"копейка"/utf8>>, <<"копейки"/utf8>>, <<"копеек"/utf8>>);
get_number(Count, Self0, ?NttCurrencyNationalMaj) -> get_number(Count, Self0, <<"0 рублей"/utf8>>, <<"рубль"/utf8>>, <<"рубля"/utf8>>, <<"рублей"/utf8>>);
get_number(Count, Self0, ?NttCurrencyNationalMin) -> get_number(Count, Self0, <<"0 копеек"/utf8>>, <<"копейка"/utf8>>, <<"копейки"/utf8>>, <<"копеек"/utf8>>);
get_number(Count, Self0, ?NttCurrencyCustomMaj) -> get_number(Count, Self0, <<"0 гривен"/utf8>>, <<"гривна"/utf8>>, <<"гривны"/utf8>>, <<"гривен"/utf8>>);
get_number(Count, Self0, ?NttCurrencyCustomMin) -> get_number(Count, Self0, <<"0 копеек"/utf8>>, <<"копейка"/utf8>>, <<"копейки"/utf8>>, <<"копеек"/utf8>>);
get_number(Count, Self0, ?NttElements) -> get_number(Count, Self0, <<"0 элементов"/utf8>>, <<"элемент"/utf8>>, <<"элемента"/utf8>>, <<"элементов"/utf8>>);
get_number(Count, Self0, ?NttOperators) -> get_number(Count, Self0, <<"нет операторов"/utf8>>, <<"оператор"/utf8>>, <<"оператора"/utf8>>, <<"операторов"/utf8>>);
get_number(Count, Self0, ?NttTasks) -> get_number(Count, Self0, <<"нет задач"/utf8>>, <<"задача"/utf8>>, <<"задачи"/utf8>>, <<"задач"/utf8>>);
get_number(Count, Self0, ?NttProjects) -> get_number(Count, Self0, <<"нет проектов"/utf8>>, <<"проект"/utf8>>, <<"проекта"/utf8>>, <<"проектов"/utf8>>);
get_number(Count, Self0, ?NttLists) -> get_number(Count, Self0, <<"нет списков"/utf8>>, <<"список"/utf8>>, <<"списка"/utf8>>, <<"списков"/utf8>>);
get_number(Count, Self0, ?NttTables) -> get_number(Count, Self0, <<"нет таблиц"/utf8>>, <<"таблица"/utf8>>, <<"таблицы"/utf8>>, <<"таблиц"/utf8>>);
get_number(Count, Self0, ?NttUsers) -> get_number(Count, Self0, <<"нет пользователей"/utf8>>, <<"пользователь"/utf8>>, <<"пользователя"/utf8>>, <<"пользователей"/utf8>>);
get_number(Count, Self0, ?NttNumbers) -> get_number(Count, Self0, <<"нет номеров"/utf8>>, <<"номер"/utf8>>, <<"номера"/utf8>>, <<"номеров"/utf8>>);
get_number(Count, Self0, ?NttFiles) -> get_number(Count, Self0, <<"нет файлов"/utf8>>, <<"файл"/utf8>>, <<"файла"/utf8>>, <<"файлов"/utf8>>);
get_number(Count, Self0, ?NttTrunk) -> get_number(Count, Self0, <<"0 транков"/utf8>>, <<"транк"/utf8>>, <<"транка"/utf8>>, <<"транков"/utf8>>);
get_number(Count, Self0, ?NttLines) -> get_number(Count, Self0, <<"нет линий"/utf8>>, <<"линия"/utf8>>, <<"линии"/utf8>>, <<"линий"/utf8>>);
get_number(Count, Self0, ?NttScripts) -> get_number(Count, Self0, <<"нет сценариев"/utf8>>, <<"сценарий"/utf8>>, <<"сценария"/utf8>>, <<"сценариев"/utf8>>);
get_number(Count, Self0, ?NttRows) -> get_number(Count, Self0, <<"0 строк"/utf8>>, <<"строка"/utf8>>, <<"строки"/utf8>>, <<"строк"/utf8>>);
get_number(Count, Self0, ?NttColumns) -> get_number(Count, Self0, <<"0 столбцов"/utf8>>, <<"столбец"/utf8>>, <<"столбца"/utf8>>, <<"столбцов"/utf8>>);
get_number(Count, Self0, ?NttWorkDays) -> get_number(Count, Self0, <<"0 рабочих дней"/utf8>>, <<"рабочий день"/utf8>>, <<"рабочих дня"/utf8>>, <<"рабочих дней"/utf8>>);
get_number(Count, Self0, ?NttWorkDaysShort) -> get_number(Count, Self0, <<"0 раб. дней"/utf8>>, <<"раб. день"/utf8>>, <<"раб. дня"/utf8>>, <<"раб. дней"/utf8>>);
get_number(Count, Self0, ?NttDaysExtraShort) -> get_number(Count, Self0, <<"0 дн"/utf8>>, <<"дн"/utf8>>, <<"дн"/utf8>>, <<"дн"/utf8>>);
get_number(Count, Self0, ?NttOperations) -> get_number(Count, Self0, <<"Операции не назначены"/utf8>>, <<"операция"/utf8>>, <<"операции"/utf8>>, <<"операций"/utf8>>);
get_number(Count, Self0, ?NttRoutes) -> get_number(Count, Self0, <<"Направления не назначены"/utf8>>, <<"направление"/utf8>>, <<"направления"/utf8>>, <<"направлений"/utf8>>);
get_number(Count, Self0, ?NttNews) -> get_number(Count, Self0, <<"Новостей нет"/utf8>>, <<"новость"/utf8>>, <<"новости"/utf8>>, <<"новостей"/utf8>>);
get_number(Count, Self0, ?NttPercent) -> get_number(Count, Self0, <<"Ноль процентов"/utf8>>, <<"процент"/utf8>>, <<"процента"/utf8>>, <<"процентов"/utf8>>).
%% ------------------
%%
%% ------------------
get_number(0, true, S0, _S1, _S2, _S7) -> S0;
get_number(Count, _Self0, _S0, S1, S2, S7) -> get_number(Count, S1, S2, S7).
%% ------------------
%%
%% ------------------
get_number(X, S1, S2, S7) ->
    case X rem 100 of
        X100 when (X100 < 5); (X100 > 20) ->
            case X100 rem 10 of
                1 -> S1;
                X10 when (X10 > 1), (X10 < 5) -> S2;
                _ -> S7
            end;
        _ -> S7
    end.
%% --------------------------------------
%%
%% --------------------------------------
name_fractales_do(0, _K1, _K10) -> <<""/utf8>>;
name_fractales_do(1, 1, K10) when (K10 =/= 1) -> <<"десятая"/utf8>>;
name_fractales_do(1, _K1, _K10) -> <<"десятых"/utf8>>;
name_fractales_do(2, 1, K10) when (K10 =/= 1) -> <<"сотая"/utf8>>;
name_fractales_do(2, _K1, _K10) -> <<"сотых"/utf8>>;
name_fractales_do(3, 1, K10) when (K10 =/= 1) -> <<"тысячная"/utf8>>;
name_fractales_do(3, _K1, _K10) -> <<"тысячных"/utf8>>;
name_fractales_do(4, 1, K10) when (K10 =/= 1) -> <<"десятитысячная"/utf8>>;
name_fractales_do(4, _K1, _K10) -> <<"десятитысячных"/utf8>>;
name_fractales_do(_, _K1, _K10) -> <<>>.

%% --------------------------------------
%%
%% --------------------------------------
parse_number_genitive_hundredths(900) -> [<<"девятисотого"/utf8>>];
parse_number_genitive_hundredths(800) -> [<<"восьмисотого"/utf8>>];
parse_number_genitive_hundredths(700) -> [<<"семисотого"/utf8>>];
parse_number_genitive_hundredths(600) -> [<<"шестисотого"/utf8>>];
parse_number_genitive_hundredths(500) -> [<<"пятисотого"/utf8>>];
parse_number_genitive_hundredths(400) -> [<<"четырехсотого"/utf8>>];
parse_number_genitive_hundredths(300) -> [<<"трехсотого"/utf8>>];
parse_number_genitive_hundredths(200) -> [<<"двухсотого"/utf8>>];
parse_number_genitive_hundredths(100) -> [<<"сотого"/utf8>>];
parse_number_genitive_hundredths(Num) ->
    K = Num div 100 * 100,
    Acc1 = case K == 0 of false -> ?M:parse_number(K, #number_representation{gender='masculine'}); true -> [] end,
    K10 = Num rem 100,
    lists:append(Acc1, parse_number_genitive_tenths(K10)).
%% ------------------
%%
%% ------------------
parse_number_genitive_tenths(90) -> [<<"девяностого"/utf8>>];
parse_number_genitive_tenths(80) -> [<<"восьмидесятого"/utf8>>];
parse_number_genitive_tenths(70) -> [<<"семидесятого"/utf8>>];
parse_number_genitive_tenths(60) -> [<<"шестидесятого"/utf8>>];
parse_number_genitive_tenths(50) -> [<<"пятидесятого"/utf8>>];
parse_number_genitive_tenths(40) -> [<<"сорокового"/utf8>>];
parse_number_genitive_tenths(30) -> [<<"тридцатого"/utf8>>];
parse_number_genitive_tenths(20) -> [<<"двадцатого"/utf8>>];
parse_number_genitive_tenths(19) -> [<<"девятнадцатого"/utf8>>];
parse_number_genitive_tenths(18) -> [<<"восемнадцатого"/utf8>>];
parse_number_genitive_tenths(17) -> [<<"семнадцатого"/utf8>>];
parse_number_genitive_tenths(16) -> [<<"шестнадцатого"/utf8>>];
parse_number_genitive_tenths(15) -> [<<"пятнадцатого"/utf8>>];
parse_number_genitive_tenths(14) -> [<<"четырнадцатого"/utf8>>];
parse_number_genitive_tenths(13) -> [<<"тринадцатого"/utf8>>];
parse_number_genitive_tenths(12) -> [<<"двенадцатого"/utf8>>];
parse_number_genitive_tenths(11) -> [<<"одиннадцатого"/utf8>>];
parse_number_genitive_tenths(10) -> [<<"десятого"/utf8>>];
parse_number_genitive_tenths(Num) ->
    K = Num div 10 * 10,
    Acc1 = case K == 0 of false -> ?M:parse_number(K, #number_representation{gender='masculine'}); true -> [] end,
    K1 = Num rem 10,
    lists:append(Acc1, parse_number_genitive_single(K1)).
%% ------------------
%%
%% ------------------
parse_number_genitive_single(9) -> [<<"девятого"/utf8>>];
parse_number_genitive_single(8) -> [<<"восьмого"/utf8>>];
parse_number_genitive_single(7) -> [<<"седьмого"/utf8>>];
parse_number_genitive_single(6) -> [<<"шестого"/utf8>>];
parse_number_genitive_single(5) -> [<<"пятого"/utf8>>];
parse_number_genitive_single(4) -> [<<"четвертого"/utf8>>];
parse_number_genitive_single(3) -> [<<"третьего"/utf8>>];
parse_number_genitive_single(2) -> [<<"второго"/utf8>>];
parse_number_genitive_single(1) -> [<<"первого"/utf8>>];
parse_number_genitive_single(_) -> [].


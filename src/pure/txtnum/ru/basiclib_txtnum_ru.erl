%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2018
%%% @doc The logic of decomposition of a number into morphemes and the formation of an array of words, in accordance with the format. For Russian

-module(basiclib_txtnum_ru).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build/2,
    parse_number/2,
    parse_number_ordinal/2,
    parse_float_simple_count_wf/4,
    parse_number_split_by_symbols_count/3,
    parse_3x_number_with_zeroes/2,
    parse_phone_simple/2,
    parse_date/2,
    parse_time/4,
    parse_date_time/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("../basiclib_txtnum.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
-spec build(Number::any(), Representation::#number_representation{}) -> Result::list().
%% --------------------------------------
build(Number, #number_representation{mode='byformat'}=R) ->
    ?Parse:number_by_type(Number, R);
build(Number, #number_representation{mode='quantity'}=R) ->
    ?Parse:float_simple_count(Number, R);
build(Number, #number_representation{mode='ordinal'}=R) ->
    ?Parse:float_simple_ordinal(Number, R);
build(Number, #number_representation{mode='date'}=R) ->
    ?Parse:date(Number, R);
build(Number, #number_representation{mode='time'}=R) ->
    ?Parse:time(Number, R);
build(Number, #number_representation{mode='timewo0b'}=R) ->
    ?Parse:time_wo0b(Number, R);
build(Number, #number_representation{mode='timewo0e'}=R) ->
    ?Parse:time_wo0e(Number, R);
build(Number, #number_representation{mode='timesec'}=R) ->
    ?Parse:time_in_seconds(Number, R);
build(Number, #number_representation{mode='datetime'}=R) ->
    ?Parse:date_time(Number, R);
build(Number, #number_representation{mode='phone'}=R) ->
    ?Parse:phone(Number, R);
build(Number, #number_representation{mode='weekday'}=R) ->
    ?Parse:week_day(Number, R);
build(Number, #number_representation{mode='moneyusd'}=R) ->
    ?Parse:money_USD(Number, R);
build(Number, #number_representation{mode='moneyeur'}=R) ->
    ?Parse:money_euro(Number, R);
build(Number, #number_representation{mode='moneyrub'}=R) ->
    ?Parse:money_national(Number, R);
build(Number, #number_representation{mode='percent'}=R) ->
    ?Parse:percent(Number, R).

%% --------------------------------------
%% accurate
%% --------------------------------------
parse_number(<<>>, R) -> [?Parse:txt_function(zero,[],R)];
parse_number(0, R) -> [?Parse:txt_function(zero,[],R)];
parse_number(Number, R) when is_binary(Number) ->
    case re:run(Number, "^[0-9]+$") of
        {match,[{0,_}]} ->
            case ?BU:to_int(Number) of
                0 ->
                    F = fun(_I, Acc) -> lists:append(Acc,[?Parse:txt_function(zero,[],R)]) end,
                    lists:foldl(F, [], lists:seq(1, string:length(Number)));
                _ ->
                    S = ?Parse:add_zero_left_do(Number),
                    Rankmax = string:length(S) div 3 - 2,
                    F = fun(I, Acc) ->
                                S1 = binary:part(S, I * 3, 3),
                                N3 = ?BU:to_int(S1),
                                ResRank = rank_3X(Rankmax - I, N3, R),
                                lists:append(Acc, ResRank)
                        end,
                    lists:foldl(F,[],lists:seq(0, Rankmax + 1))
            end;
        _ -> []
    end;
parse_number(Number, R) ->
    parse_number(?BU:to_binary(Number), R).


%% --------------------------------------
%% ordinal
%% --------------------------------------
parse_number_ordinal(Number, #number_representation{gender=G}=R) when is_binary(Number) ->
    case string:length(Number) of
        0 -> [?Parse:txt_function(zero_ordinal,[G],R)];
        _ ->
            case re:run(Number, "^[0-9]+$") of
                {match,[{0,_}]} ->
                    case ?BU:to_int(Number) of
                        0 -> [?Parse:txt_function(zero_ordinal,[G],R)];
                        _ ->
                            S =  ?Parse:add_zero_left_do(Number),
                            Rankmax = string:length(S) div 3 - 2,
                            F = fun(I, Acc) ->
                                        S1 = binary:part(S, I * 3, 3),
                                        N3 = ?BU:to_int(S1),
                                        Index = ( I + 1 ) * 3,
                                        Srest = binary:part(S, Index, (byte_size(S) - Index)),
                                        InAcc = case re:run(Srest, "^0*$") of
                                                    {match,_} -> rank_3X_ordinal(Rankmax - I, N3, R);
                                                    _ -> rank_3X(Rankmax - I, N3, R)
                                                end,
                                        lists:append(Acc, InAcc)
                                end,
                            lists:foldl(F,[],lists:seq(0, Rankmax + 1))
                    end;
                _ -> []
            end
    end;
parse_number_ordinal(Number, R) ->
    parse_number_ordinal(?BU:to_binary(Number), R).

%% --------------------------------------
%%
%% --------------------------------------
parse_float_simple_count_wf(NumberW, NumberF, _W0, R) ->
    Acc1 = parse_number(NumberW, R#number_representation{gender=feminine}),
    Acc2 = lists:append(Acc1, [?Parse:txt_function(name_wholes,[NumberW],R)]),
    Acc3 = lists:append(Acc2, parse_number(NumberF, R#number_representation{gender=feminine})),
    lists:append(Acc3, [?Parse:txt_function(name_fractales, [NumberF], R)]).

%% --------------------------------------
%%
%% --------------------------------------
parse_number_split_by_symbols_count(Number, Part, Repr) ->
    L = string:length(Number),
    F = fun(_, {Acc,R2}) ->
                RR = binary:part(R2,Part,string:length(R2)-Part),
                SS = binary:part(R2, 0, Part),
                NumZ = parse_3x_number_with_zeroes(SS, Repr),
                {lists:append(Acc, NumZ), RR}
        end,
    case lists:foldl(F, {[],Number}, lists:seq(0, (L div Part)-1)) of
        {Result, <<>>} -> Result;
        {Result, Residue} -> lists:append(Result,parse_3x_number_with_zeroes(Residue, Repr))
    end.

%% --------------------------------------
%%
%% --------------------------------------
parse_phone_simple(Number, R) ->
    Arr = re:split(Number, "([\\*#\\d]+)", [group]),
    N = lists:foldl(fun([_],Acc) -> Acc;
                       ([_,Match],Acc) -> <<Acc/binary, Match/binary>> end, <<>>, Arr),
    F = fun(I,L) -> parse_3x_number_with_zeroes(binary:part(N,I,L), R) end,
    case string:length(N) of
        Min when Min>=0,Min<4 -> parse_3x_number_with_zeroes(N, R);
        4 -> lists:append([F(0,2),F(2,2)]);
        5 -> lists:append([F(0,2),F(2,3)]);
        6 -> lists:append([F(0,2),F(2,2),F(4,2)]);
        7 -> lists:append([F(0,3),F(3,2),F(5,2)]);
        8 -> lists:append([F(0,2),F(2,2),F(4,2),F(6,2)]);
        9 -> lists:append([F(0,2),F(2,2),F(4,1),F(5,2),F(7,2)]);
        10 -> lists:append([F(0,3),F(3,3),F(6,2),F(8,2)]);
        11 -> lists:append([F(0,1),F(1,3),F(4,3),F(7,2),F(9,2)]);
        Count ->
            Fun = fun(Ind, Acc) ->
                          S = binary:part(N, Ind * 3, 3),
                          lists:append(Acc, [parse_3x_number_with_zeroes(S, R)])
                  end,
            PreRes = lists:foldl(Fun, [], lists:seq(0, Count div 3)),
            case (Count rem 3) of
                0 -> PreRes;
                Last -> lists:append(PreRes, [F(byte_size(N) - Last,Last)])
            end
    end.
%% ------------------------------------------------
%% Calendar
%% ------------------------------------------------

%% --------------------------------------
%% time
%% --------------------------------------
parse_date({Y,M,D}, R) ->
    Acc1 = ?Parse:txt_function(parse_number_th,[D], R),
    Acc2 = lists:append(Acc1, [?Parse:txt_function(name_month,[M], R)]),
    Acc3 = lists:append(Acc2, ?Parse:txt_function(parse_number_genitive,[Y], R)),
    Acc4 = lists:append(Acc3, [?Parse:txt_function(names,[?NttYears, Y], R)]),
    Acc4.

%% --------------------------------------
%% time
%% --------------------------------------
parse_time({H,Min,S}, Filterzeroesatbegin, Filterzeroesatend, R) ->
    Acc1 = add_hour(H, Filterzeroesatbegin, R),
    Acc2 = add_minute(Min, Filterzeroesatbegin, Filterzeroesatend, R#number_representation{gender=feminine}),
    Acc3 = add_second(S, Filterzeroesatbegin, Filterzeroesatend, H+Min, R#number_representation{gender=feminine}),
    lists:append([Acc1,Acc2,Acc3]).
%% ------------------
%% @private
%% ------------------
add_hour(H, _, R) when (H > 0) -> add_hour_do(H, R);
add_hour(H, false, R) -> add_hour_do(H, R);
add_hour(_, _, _) -> [].
add_hour_do(H, R) ->
    Acc = parse_number(H, R),
    lists:append(Acc, [?Parse:txt_function(names, [?NttHours, H], R)]).
%% ------------------
%% @private
%% ------------------
add_minute(Min, _, _, R) when (Min > 0) -> add_minute_do(Min, R);
add_minute(Min, false, false, R) -> add_minute_do(Min, R);
add_minute(_, _, _, _) -> [].
add_minute_do(Min, R) ->
    Acc = parse_number(Min, R),
    lists:append(Acc, [?Parse:txt_function(names, [?NttMinutes, Min], R)]).
%% ------------------
%% @private
%% ------------------
add_second(S, _, _, _, R) when (S > 0) -> add_second_do(S, R);
add_second(S, false, false, _, R) -> add_second_do(S, R);
add_second(S, true, _, 0, R) -> add_second_do(S, R);
add_second(_, _, _, _, _) -> [].
add_second_do(S, R) ->
    Acc = parse_number(S, R),
    lists:append(Acc, [?Parse:txt_function(names, [?NttSeconds, S], R)]).

%% --------------------------------------
%% datetime
%% --------------------------------------
parse_date_time({{_,_,_}=D,{_,_,_}=T,_}, R) ->
    Acc1 = parse_date(D, R),
    Acc2 = lists:append(Acc1, parse_time(T, false, true, R)),
    Acc2.

%% ====================================================================
%% Internal functions
%% ====================================================================
rank_3X(_, 0, _) -> [];
rank_3X(Count, Num3x,  #number_representation{gender=G}=R) ->
    N100 = ( Num3x rem 1000 ) div 100 * 100,
    N10 = ( Num3x rem 100 ) div 10 * 10,
    N1 = ( Num3x rem 100 ),

    Acc1 = case ?Parse:txt_function(hundreeds,[N100], R) of <<>> -> []; S100 -> [S100] end,
    Acc2 = case ?Parse:txt_function(tens,[N10],R) of <<>> -> Acc1; S10 -> lists:append(Acc1, [S10]) end,

    % Только тысяча делает предстоящую ей единицу женским родом,
    % остальные - миллионы, миллиарды - мужики.
    % А отрицательные - это то как сказали. Зависит уже от морковок следом за числом.
    UpSexNumberPlay = case (Count == 0) of true -> 'feminine'; false -> case (Count > 0) of true -> 'masculine'; false -> G end end,

    Acc3 = case ?Parse:txt_function(idents,[N1, UpSexNumberPlay], R) of <<>> -> Acc2; S1 -> lists:append(Acc2, [S1]) end,
    Acc4 = case ?Parse:txt_function(name_1000,[Num3x, Count], R) of <<>> -> Acc3; S -> lists:append(Acc3, [S]) end,
    Acc4.

%% ----------------------------
%%     третий
%%     двенадцатое
%%     двадцать вторая
%%     сотое
%%     сто первый
%%     сто пятидесятый
%%     двухсотый
%%     триста седьмой
%%     девятьсот сороковой
%%     одно-тысячный
%%     пятнадцати-тысячный
%%     сто-восьми-тысячный
%%     трехсот-восьмидесяти-одно-миллионный
%% ----------------------------
rank_3X_ordinal(_Count, 0, _R) -> [];
rank_3X_ordinal(Count, Num3x, #number_representation{gender=G}=R) ->
     N100 = ( Num3x rem 1000 ) div 100 * 100,
     N10 = ( Num3x rem 100 ) div 10 * 10,
    N1 = ( Num3x rem 100 ),

    case (Count < 0) of
        true -> % первая тысяча / отдельные слова
            S100 = case N100 of Num3x -> ?Parse:txt_function(hundreeds_ordinal,[N100, G],R); _ -> ?Parse:txt_function(hundreeds,[N100],R) end,
            Acc1 = case S100 of <<>> -> []; _ -> [S100] end,

            S10 = case (N100 + N10) of Num3x -> ?Parse:txt_function(tens_ordinal,[N10, G],R); _ -> ?Parse:txt_function(tens,[N10],R) end,
            Acc2 = case S10 of <<>> -> Acc1; _ -> lists:append(Acc1, [S10]) end,

            Acc3 = case ?Parse:txt_function(idents_ordinal,[N1, G], R) of <<>> -> Acc2; S1 -> lists:append(Acc2, [S1]) end,
            Acc3;
        false -> % не первая тысяча / склеенные слова
            Acc1 = case ?Parse:txt_function(hundreeds_ordinal_prefix,[N100], R) of <<>> -> []; S100 -> [S100] end,
            Acc2 = case ?Parse:txt_function(tens_ordinal_prefix,[N10], R) of <<>> -> Acc1; S10 -> lists:append(Acc1, [S10]) end,
            Acc3 = case ?Parse:txt_function(idents_ordinal_prefix,[N1], R) of <<>> -> Acc2; S1 -> lists:append(Acc2, [S1]) end,
            Acc4 = case ?Parse:txt_function(ordinal_suffix,[Count, G], R) of <<>> -> Acc3; S -> lists:append(Acc3, [S]) end,
            Acc4
    end.

%% --------------------------------------
%%
%% --------------------------------------
parse_3x_number_with_zeroes(Number, Repr) ->
    try ?BU:to_int(Number) of
        T ->
            Acc1 = add_zeros(Number, [], Repr),
            lists:append(Acc1, rank_3X(-1, T, Repr))
    catch
        _:_ ->
            Fun = fun(Index) -> parse_3x_number_with_zeroes(binary:part(Number, Index, byte_size(Number)-Index), Repr) end,
            case binary:first(Number) of
                $* ->
                    RpeRes = Fun(1),
                    Asterisk = ?Parse:txt_function(asterisk,[],Repr),
                    lists:append([Asterisk],[RpeRes]);
                $# ->
                    RpeRes = Fun(1),
                    Lattice = ?Parse:txt_function(lattice,[],Repr),
                    lists:append([Lattice],[RpeRes]);
                S ->
                    case binary:part(Number, 1, 1) of
                        S1 when S1==$*; S1==$# ->
                            RpeRes1 = parse_3x_number_with_zeroes(S,Repr),
                            RpeRes2 = Fun(1),
                            lists:append(RpeRes1,RpeRes2);
                        S12 ->
                            RpeRes1 = parse_3x_number_with_zeroes(<<S/binary,S12/binary>>,Repr),
                            RpeRes2 = Fun(2), % Parse3xNumberWithZeroes( ref L, number.Substring(2, 1), sex )
                            lists:append(RpeRes1,RpeRes2)
                    end
            end
    end.
%% ------------------
%% @private
%% ------------------
add_zeros(<<$0,Num/bitstring>>, Acc, R) ->
    add_zeros(Num, lists:append(Acc, [?Parse:txt_function(zero,[],R)]), R);
add_zeros(_, Acc, _R) -> Acc.




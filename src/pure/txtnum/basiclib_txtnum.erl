%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2018
%%% @doc Facade for the algorithm layout of numerals in the text

-module(basiclib_txtnum).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("basiclib_txtnum.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%% Возвращает массив слов текущего языка, означающими числительное с дополнительными
%% описаниями ( рубли, время и т.п. ).    Принимает - род, формат, тип проигрывания
%% и объект ( число, строка, дата-время )
%% Преобразует к числительному в текущем установленном языке
%% --------------------------------------
-spec build(Number::any(), Opts::map()) -> Result::list().
%% --------------------------------------
build(Number, Opts) when is_map(Opts) ->
    R = #number_representation{
                               lang = maps:get(lang, Opts, 'ru'),
                               mode = maps:get(mode, Opts, 'byformat'),
                               format = maps:get(format, Opts, 'sym1'),
                               gender = maps:get(gender, Opts, 'masculine'),
                               downTone = maps:get(downTone, Opts, 'false')
                               },
    build(Number, R);
%% --------------------------------------
build(Number, #number_representation{}=Representation) ->
    case check_representation_opts(Representation) of
        {error, Reason} ->
            ?LOG('$error', "~120p -- string_representation_of_number: ERROR - ~120p.", [?MODULE, Reason]),
            [];
        {ok, UpRep} -> down_tone(get_files_on_number_lang(Number, UpRep), Representation)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
check_representation_opts(#number_representation{mode=M}) when
  M=/='byformat',M=/='quantity',M=/='ordinal',M=/='phone',M=/='date',M=/='time',M=/='datetime',
  M=/='weekday',M=/='timewo0b',M=/='timewo0e',M=/='timesec',M=/='moneyusd',M=/='moneyeur',M=/='moneyrub',M=/='percent' ->
    {error, <<"Unsupported number play format">>};
check_representation_opts(#number_representation{format=F}) when
  F=/='sym1',F=/='sym2',F=/='sym3',F=/='number',F=/='letters',F=/='words' ->
    {error, <<"Unsupported number play type">>};
check_representation_opts(#number_representation{gender=G}) when
  G=/='masculine',G=/='feminine',G=/='neuter' ->
    {error, <<"Unsupported gender">>};
check_representation_opts(Representation) -> {ok ,Representation}.

%% --------------------------------------
%% lang module
%% --------------------------------------
get_files_on_number_lang(Number, #number_representation{lang='ru'}=Representation) ->
    M = basiclib_txtnum_ru,
    M:build(Number, Representation#number_representation{lang_module=M, text_module=basiclib_txtnum_ru_txt});
get_files_on_number_lang(Number, #number_representation{lang='en'}=Representation) ->
    M = basiclib_txtnum_en,
    M:build(Number, Representation#number_representation{lang_module=M, text_module=basiclib_txtnum_en_txt});
get_files_on_number_lang(_Number, #number_representation{lang = Lang}) ->
    ?LOG('$error', "~120p -- string_representation_of_number: ERROR - Unsupported lang ~120p.", [?MODULE, Lang]),
    [].

%% --------------------------------------
%%
%% --------------------------------------
down_tone(ResultList, #number_representation{downTone=false}) -> ResultList;
down_tone([], #number_representation{downTone=true}) -> [];
down_tone(ResultList, #number_representation{downTone=true}) ->
    Last = lists:last(ResultList),
    UpResult = lists:droplast(ResultList),
    UpLast = <<Last/binary, $_>>,
    lists:append(UpResult,[UpLast]).


%% ====================================================================
%% Test functions
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% ---------------------------------------
%% ru
%% ---------------------------------------
basiclib_txtnum_ru_test_() ->
    A = fun(L) -> string:join(lists:map(fun(X) -> ?BU:to_unicode_list(X) end, L), " ") end,
    FMap = fun([_N,M,undefined,undefined,D]) -> #{mode => M, downTone => D};
              ([_N,M,undefined,G,D]) -> #{mode => M, gender => G, downTone => D};
              ([_N,M,F,G,D]) -> #{mode => M, format => F, gender => G, downTone => D}
           end,
    X = fun(_I,N,Args) -> A(build(N,FMap(Args))) end,
    {"txtnum ru test",
        [?_assertEqual(Res, X(_I,N,Args)) || {_I,[N,_M,_F,_G,_D]=Args,Res} <- get_tests_ru()]
    }.

get_tests_ru() ->
    [{0, ["0", quantity, undefined, neuter, false], "ноль"},
     {1, ["00", quantity, undefined, neuter, false], "ноль ноль"},
     {2, ["00000", quantity, undefined, masculine, false], "ноль ноль ноль ноль ноль"},
     {3, ["00001", quantity, undefined, masculine, false], "один"},
     {4, ["1", quantity, undefined, feminine, false], "одна"},
     {5, ["1", quantity, undefined, neuter, false], "одно"},
     {6, ["abcd1", quantity, undefined, neuter, false], "ноль"},
     {7, ["1234567", quantity, undefined, masculine, false], "один миллион двести тридцать четыре тысячи пятьсот шестьдесят семь"},
     {8, ["1234567", quantity, undefined, masculine, true], "один миллион двести тридцать четыре тысячи пятьсот шестьдесят семь_"},
     {9, ["4321", quantity, undefined, feminine, false], "четыре тысячи триста двадцать одна"},
     {10, ["04321", quantity, undefined, feminine, false], "четыре тысячи триста двадцать одна"},
     {11, ["0.0", quantity, undefined, neuter, false], "ноль"},
     {12, ["2.1", quantity, undefined, feminine, false], "две целых одна десятая"},
     {13, ["-2.2", quantity, undefined, feminine, false], "минус две целых две десятых"},
     {14, ["13.3", quantity, undefined, feminine, false], "тринадцать целых три десятых"},
     {15, ["-13.01", quantity, undefined, feminine, false], "минус тринадцать целых одна сотая"},
     {16, ["- 13.01", quantity, undefined, feminine, false], "минус тринадцать целых одна сотая"},
     {17, ["2.21", quantity, undefined, feminine, false], "две целых двадцать одна сотая"},
     {18, ["2.423", quantity, undefined, feminine, false], "две целых четыреста двадцать три тысячных"},
     {19, ["2.4231", quantity, undefined, feminine, false], "две целых четыре тысячи двести тридцать одна десятитысячная"},
     {20, ["2.4231", quantity, undefined, masculine, false], "две целых четыре тысячи двести тридцать одна десятитысячная"},
     {21, ["2.4231", quantity, undefined, neuter, false], "две целых четыре тысячи двести тридцать одна десятитысячная"},
     {22, ["17.12", ordinal, undefined, feminine, false], "семнадцатая"},
     {23, ["0", ordinal, undefined, masculine, false], "нулевой"},
     {24, ["00", ordinal, undefined, masculine, false], "нулевой"},
     {25, ["2935879", ordinal, undefined, masculine, false], "два миллиона девятьсот тридцать пять тысяч восемьсот семьдесят девятый"},
     {26, ["5134", ordinal, undefined, neuter, true], "пять тысяч сто тридцать четвертое_"},
     {27, ["17", ordinal, undefined, feminine, false], "семнадцатая"},
     {28, ["100", ordinal, undefined, neuter, false], "сотое"},
     {29, ["150", ordinal, undefined, masculine, false], "сто пятидесятый"},
     {30, ["200", ordinal, undefined, masculine, false], "двухсотый"},
     {31, ["307", ordinal, undefined, masculine, false], "триста седьмой"},
     {32, ["940", ordinal, undefined, masculine, false], "девятьсот сороковой"},
     {33, ["1000", ordinal, undefined, masculine, false], "одно- тысячный"},
     {34, ["15000", ordinal, undefined, masculine, false], "пятнадцати- тысячный"},
     {35, ["108000", ordinal, undefined, masculine, false], "сто- восьми- тысячный"},
     {36, ["381000000", ordinal, undefined, masculine, false], "трехсот- восьмидесяти- одно- миллионный"},
     {37, ["0.5", byformat, sym1, neuter, false], "ноль пять"},
     {38, ["0", byformat, sym1, neuter, false], "ноль"},
     {39, ["00", byformat, sym1, neuter, false], "ноль ноль"},
     {40, ["00", byformat, sym2, neuter, false], "ноль ноль"},
     {41, ["222", byformat, sym1, feminine, false], "две две две"},
     {42, ["111", byformat, sym1, masculine, false], "один один один"},
     {43, ["111", byformat, sym1, neuter, false], "одно одно одно"},
     {44, ["111", byformat, sym1, feminine, false], "одна одна одна"},
     {45, ["212121", byformat, sym1, feminine, true], "две одна две одна две одна_"},
     {46, ["212121", byformat, sym2, neuter, false], "двадцать одно двадцать одно двадцать одно"},
     {47, ["212121", byformat, sym3, masculine, false], "двести двенадцать сто двадцать один"},
     {48, ["1234567890", byformat, sym1, masculine, false], "один два три четыре пять шесть семь восемь девять ноль"},
     {49, ["1234567890", byformat, sym1, feminine, false], "одна две три четыре пять шесть семь восемь девять ноль"},
     {50, ["1234567890", byformat, sym2, masculine, true], "двенадцать тридцать четыре пятьдесят шесть семьдесят восемь девяносто_"},
     {51, ["1234567890", byformat, sym3, masculine, false], "сто двадцать три четыреста пятьдесят шесть семьсот восемьдесят девять ноль"},
     {52, ["1234567890", byformat, number, masculine, true], "один миллиард двести тридцать четыре миллиона пятьсот шестьдесят семь тысяч восемьсот девяносто_"},
     {53, ["abcd efg", byformat, letters, masculine, true], "a b c d e f g_"},
     {54, ["*#  .,/\\-", byformat, letters, masculine, true], "звездочка решетка точка запятая слэш бэкслэш тире_"},
     {55, ["фыва", byformat, letters, masculine, false], "ф ы в а"},
     {56, ["добрый день мы рады вас видеть", byformat, words, masculine, false], "добрый день мы рады вас видеть"},
     {57, ["добрый день, мы рады вас видеть", byformat, words, masculine, false], "добрый день, мы рады вас видеть"},
     {58, ["Добрый    день!
Мы рады вас видеть!", byformat, words, masculine, false], "добрый день! мы рады вас видеть!"},
     {59, ["8452", phone, undefined, undefined, false], "восемьдесят четыре пятьдесят два"},
     {60, ["12345", phone, undefined, undefined, false], "двенадцать триста сорок пять"},
     {61, ["123456", phone, undefined, undefined, false], "двенадцать тридцать четыре пятьдесят шесть"},
     {62, ["210006", phone, undefined, undefined, false], "двадцать один ноль ноль ноль шесть"},
     {63, ["3123456", phone, undefined, undefined, false], "триста двенадцать тридцать четыре пятьдесят шесть"},
     {64, ["31aBcD625", phone, undefined, undefined, false], "тридцать один a b c d шестьсот двадцать пять"},
     {65, ["*01", phone, undefined, undefined, false], "звездочка ноль один"},
     {66, ["#12", phone, undefined, undefined, false], "решетка двенадцать"},
     {67, ["#345", phone, undefined, undefined, false], "решетка триста сорок пять"},
     {68, ["#3457", phone, undefined, undefined, false], "решетка тридцать четыре пятьдесят семь"},
     {69, ["3#4567", phone, undefined, undefined, false], "три решетка сорок пять шестьдесят семь"},
     {70, ["34*567", phone, undefined, undefined, false], "тридцать четыре звездочка пятьсот шестьдесят семь"},
     {71, ["*1*25*123#", phone, undefined, undefined, false], "звездочка один звездочка двадцать пять звездочка сто двадцать три решетка"},
     {72, ["*1*1*123#", phone, undefined, undefined, false], "звездочка один звездочка один звездочка сто двадцать три решетка"},
     {73, ["*12", phone, undefined, undefined, false], "звездочка двенадцать"},
     {74, ["2213890", phone, undefined, undefined, false], "двести двадцать один тридцать восемь девяносто"},
     {75, ["89253120035", phone, undefined, undefined, false], "восемь девятьсот двадцать пять триста двенадцать ноль ноль тридцать пять"},
     {76, ["+79253120035", phone, undefined, undefined, false], "семь девятьсот двадцать пять триста двенадцать ноль ноль тридцать пять"},
     {77, ["+7(925)3120035", phone, undefined, undefined, false], "семь девятьсот двадцать пять триста двенадцать ноль ноль тридцать пять"},
     {78, ["+7(925)312-00-35", phone, undefined, undefined, false], "семь девятьсот двадцать пять триста двенадцать ноль ноль тридцать пять"},
     {79, ["+7 (925) 312-00-35", phone, undefined, undefined, false], "семь девятьсот двадцать пять триста двенадцать ноль ноль тридцать пять"},
     {80, ["+7 (8552) 21-12-80", phone, undefined, undefined, false], "семь восемьсот пятьдесят пять двести двадцать один двенадцать восемьдесят"},
     {81, ["+7 (85) 5221-12-80", phone, undefined, undefined, false], "семь восемьсот пятьдесят пять двести двадцать один двенадцать восемьдесят"},
     {82, ["2018-05-08 12:35:00", date, undefined, undefined, false], "восьмое мая две тысячи восемнадцатого года"},
     {83, ["2018-05-08T12:35:00.52Z", date, undefined, undefined, false], "восьмое мая две тысячи восемнадцатого года"},
     {84, ["28.12.2018", date, undefined, undefined, false], "двадцать восьмое декабря две тысячи восемнадцатого года"},
     {85, ["28.12.2018 22:00:00", date, undefined, undefined, false], "двадцать восьмое декабря две тысячи восемнадцатого года"},
     {86, ["2018-03-28", time, undefined, undefined, false], "ноль часов ноль минут ноль секунд"},
     {87, ["2018-03-28 12:01", time, undefined, undefined, false], "двенадцать часов одна минута ноль секунд"},
     {88, ["2018-03-28 00:01:02", time, undefined, undefined, false], "ноль часов одна минута две секунды"},
     {89, ["2018-03-28 12:21:00", time, undefined, undefined, false], "двенадцать часов двадцать одна минута ноль секунд"},
     {90, ["00:02", time, undefined, undefined, false], "ноль часов две минуты ноль секунд"},
     {91, ["00:15:35", time, undefined, undefined, false], "ноль часов пятнадцать минут тридцать пять секунд"},
     {92, ["15:00", time, undefined, undefined, false], "пятнадцать часов ноль минут ноль секунд"},
     {93, ["2018-03-28", timewo0b, undefined, undefined, false], "ноль секунд"},
     {94, ["2018-03-28 12:01", timewo0b, undefined, undefined, false], "двенадцать часов одна минута"},
     {95, ["2018-03-28 00:01:02", timewo0b, undefined, undefined, false], "одна минута две секунды"},
     {96, ["2018-03-28 12:21:00", timewo0b, undefined, undefined, false], "двенадцать часов двадцать одна минута"},
     {97, ["00:02", timewo0b, undefined, undefined, false], "две минуты"},
     {98, ["00:15:35", timewo0b, undefined, undefined, false], "пятнадцать минут тридцать пять секунд"},
     {99, ["15:00", timewo0b, undefined, undefined, false], "пятнадцать часов"},
     {100, ["2018-03-28", timewo0e, undefined, undefined, false], "ноль часов"},
     {101, ["2018-03-28 12:01", timewo0e, undefined, undefined, false], "двенадцать часов одна минута"},
     {102, ["2018-03-28 00:01:02", timewo0e, undefined, undefined, false], "ноль часов одна минута две секунды"},
     {103, ["2018-03-28 12:21:00", timewo0e, undefined, undefined, false], "двенадцать часов двадцать одна минута"},
     {104, ["00:02", timewo0e, undefined, undefined, false], "ноль часов две минуты"},
     {105, ["00:15:35", timewo0e, undefined, undefined, false], "ноль часов пятнадцать минут тридцать пять секунд"},
     {106, ["15:00", timewo0e, undefined, undefined, false], "пятнадцать часов"},
     {107, ["200", timesec, undefined, undefined, false], "двести секунд"},
     {108, ["102", timesec, undefined, undefined, false], "сто две секунды"},
     {109, ["81", timesec, undefined, undefined, false], "восемьдесят одна секунда"},
     {110, ["2018-03-28", datetime, undefined, undefined, false], "двадцать восьмое марта две тысячи восемнадцатого года ноль часов"},
     {111, ["13:22:25", datetime, undefined, undefined, false], "первое января тысяча девятьсот семидесятого года тринадцать часов двадцать две минуты двадцать пять секунд"},
     {112, ["25.12.1981 12:30:00", datetime, undefined, undefined, true], "двадцать пятое декабря тысяча девятьсот восемьдесят первого года двенадцать часов тридцать минут_"},
     {113, ["1981-12-25T12:30:00+03:00", datetime, undefined, undefined, true], "двадцать пятое декабря тысяча девятьсот восемьдесят первого года двенадцать часов тридцать минут_"},
     {114, ["1", weekday, undefined, undefined, false], "понедельник"},
     {115, ["2", weekday, undefined, undefined, true], "вторник_"},
     {116, ["7", weekday, undefined, undefined, false], "воскресенье"},
     {117, ["2", moneyusd, undefined, undefined, false], "два доллара"},
     {118, ["1.2", moneyusd, undefined, undefined, false], "один доллар двадцать центов"},
     {119, ["5.25", moneyusd, undefined, undefined, false], "пять долларов двадцать пять центов"},
     {120, ["1382.31", moneyusd, undefined, undefined, false], "одна тысяча триста восемьдесят два доллара тридцать один цент"},
     {121, ["11800.92", moneyusd, undefined, undefined, false], "одиннадцать тысяч восемьсот долларов девяносто два цента"},
     {122, ["2000000000", moneyusd, undefined, undefined, false], "два миллиарда долларов"},
     {123, ["19000000000000", moneyusd, undefined, undefined, false], "девятнадцать триллионов долларов"},
     {124, ["2", moneyeur, undefined, undefined, false], "два евро"},
     {125, ["1.2", moneyeur, undefined, undefined, false], "один евро двадцать евроцентов"},
     {126, ["5.25", moneyeur, undefined, undefined, false], "пять евро двадцать пять евроцентов"},
     {127, ["1482.31", moneyeur, undefined, undefined, false], "одна тысяча четыреста восемьдесят два евро тридцать один евроцент"},
     {128, ["12600.92", moneyeur, undefined, undefined, false], "двенадцать тысяч шестьсот евро девяносто два евроцента"},
     {129, ["2", moneyrub, undefined, undefined, false], "два рубля"},
     {130, ["1.2", moneyrub, undefined, undefined, false], "один рубль двадцать копеек"},
     {131, ["5.25", moneyrub, undefined, undefined, false], "пять рублей двадцать пять копеек"},
     {132, ["1582.31", moneyrub, undefined, undefined, false], "одна тысяча пятьсот восемьдесят два рубля тридцать одна копейка"},
     {133, ["13700.92", moneyrub, undefined, undefined, false], "тринадцать тысяч семьсот рублей девяносто две копейки"},
     {134, ["2", percent, undefined, undefined, false], "два процента"},
     {135, ["1.2", percent, undefined, undefined, false], "одна целая две десятых процента"},
     {136, ["5.25", percent, undefined, undefined, false], "пять целых двадцать пять сотых процента"},
     {137, ["100", percent, undefined, undefined, false], "сто процентов"},
     {138, ["146", percent, undefined, undefined, false], "сто сорок шесть процентов"}].

%% ---------------------------------------
%% en
%% ---------------------------------------
basiclib_txtnum_en_test_() ->
    A = fun(L) -> string:join(lists:map(fun(X) -> ?BU:to_unicode_list(X) end, L), " ") end,
    FMap = fun([_N,M,undefined,undefined,D]) -> #{mode => M, downTone => D, lang => en};
              ([_N,M,undefined,G,D]) -> #{mode => M, gender => G, downTone => D, lang => en};
              ([_N,M,F,G,D]) -> #{mode => M, format => F, gender => G, downTone => D, lang => en}
           end,
    X = fun(_I,N,Args) -> A(build(N,FMap(Args))) end,
    {"txtnum ru test",
        [?_assertEqual(Res, X(_I,N,Args)) || {_I,[N,_M,_F,_G,_D]=Args,Res} <- get_tests_en()]
    }.

get_tests_en() ->
    [{0, ["0", quantity, undefined, neuter, false], "zero"},
     {1, ["00", quantity, undefined, neuter, false], "zero zero"},
     {2, ["00000", quantity, undefined, masculine, false], "zero zero zero zero zero"},
     {3, ["00001", quantity, undefined, masculine, false], "one"},
     {4, ["1", quantity, undefined, feminine, false], "one"},
     {5, ["1", quantity, undefined, neuter, false], "one"},
     {6, ["abcd1", quantity, undefined, neuter, false], "zero"},
     {7, ["1234567", quantity, undefined, masculine, true], "one million two hundred and thirty four thousand five hundred and sixty seven_"},
     {8, ["1234567", quantity, undefined, masculine, true], "one million two hundred and thirty four thousand five hundred and sixty seven_"},
     {9, ["4321", quantity, undefined, feminine, false], "four thousand three hundred and twenty one"},
     {10, ["04321", quantity, undefined, feminine, false], "four thousand three hundred and twenty one"},
     {11, ["0.0", quantity, undefined, neuter, false], "zero"},
     {12, ["2.1", quantity, undefined, feminine, false], "two point one"},
     {13, ["-2.2", quantity, undefined, feminine, false], "negative two point two"},
     {14, ["13.3", quantity, undefined, feminine, false], "thirteen point three"},
     {15, ["-13.01", quantity, undefined, feminine, false], "negative thirteen point o0 one"},
     {16, ["- 13.01", quantity, undefined, feminine, false], "negative thirteen point o0 one"},
     {17, ["2.21", quantity, undefined, feminine, false], "two point two one"},
     {18, ["2.423", quantity, undefined, feminine, false], "two point four two three"},
     {19, ["2.4231", quantity, undefined, feminine, false], "two point four two three one"},
     {20, ["2.4231", quantity, undefined, masculine, false], "two point four two three one"},
     {21, ["2.4231", quantity, undefined, neuter, false], "two point four two three one"},
     {22, ["17.12", ordinal, undefined, feminine, false], "seventeenth"},
     {23, ["0", ordinal, undefined, masculine, false], "zeroth"},
     {24, ["00", ordinal, undefined, masculine, false], "zeroth"},
     {25, ["2935879", ordinal, undefined, masculine, false], "two million nine hundred and thirty five thousand eight hundred and seventy ninth"},
     {26, ["5134", ordinal, undefined, neuter, true], "five thousand one hundred and thirty fourth_"},
     {27, ["17", ordinal, undefined, feminine, false], "seventeenth"},
     {28, ["100", ordinal, undefined, neuter, false], "one hundredth"},
     {29, ["150", ordinal, undefined, masculine, false], "one hundred and fiftieth"},
     {30, ["200", ordinal, undefined, masculine, false], "two hundredth"},
     {31, ["307", ordinal, undefined, masculine, false], "three hundred and seventh"},
     {32, ["940", ordinal, undefined, masculine, false], "nine hundred and fourtieth"},
     {33, ["1000", ordinal, undefined, masculine, false], "one thousandth"},
     {34, ["15000", ordinal, undefined, masculine, false], "fifteen thousandth"},
     {35, ["108000", ordinal, undefined, masculine, false], "one hundred and eight thousandth"},
     {36, ["381000000", ordinal, undefined, masculine, false], "three hundred and eighty one millionth"},
     {37, ["0.5", byformat, sym1, neuter, false], "zero five"},
     {38, ["0", byformat, sym1, neuter, false], "zero"},
     {39, ["00", byformat, sym1, neuter, false], "zero zero"},
     {40, ["00", byformat, sym2, neuter, false], "double zero"},
     {41, ["222", byformat, sym1, feminine, false], "two two two"},
     {42, ["111", byformat, sym1, masculine, false], "one one one"},
     {43, ["111", byformat, sym1, neuter, false], "one one one"},
     {44, ["111", byformat, sym1, feminine, false], "one one one"},
     {45, ["212121", byformat, sym1, feminine, true], "two one two one two one_"},
     {46, ["212121", byformat, sym2, neuter, false], "twenty one twenty one twenty one"},
     {47, ["212121", byformat, sym3, masculine, false], "two hundred and twelve one hundred and twenty one"},
     {48, ["1234567890", byformat, sym1, masculine, false], "one two three four five six seven eight nine zero"},
     {49, ["1234567890", byformat, sym1, feminine, false], "one two three four five six seven eight nine zero"},
     {50, ["1234567890", byformat, sym2, masculine, true], "twelve thirty four fifty six seventy eight ninety_"},
     {51, ["1234567890", byformat, sym3, masculine, false], "one two hundred and thirty four five hundred and sixty seven eight hundred and ninety"},
     {52, ["1234567890", byformat, number, masculine, true], "one billion two hundred and thirty four million five hundred and sixty seven thousand eight hundred and ninety_"},
     {53, ["abcd efg", byformat, letters, masculine, true], "a b c d e f g_"},
     {54, ["*#  .,/\\-", byformat, letters, masculine, true], "asterisk lattice dot comma slash backslash dash_"},
     {55, ["фыва", byformat, letters, masculine, false], "ф ы в а"},
     {56, ["добрый день мы рады вас видеть", byformat, words, masculine, false], "добрый день мы рады вас видеть"},
     {57, ["добрый день, мы рады вас видеть", byformat, words, masculine, false], "добрый день, мы рады вас видеть"},
     {58, ["Добрый    день!
Мы рады вас видеть!", byformat, words, masculine, false], "добрый день! мы рады вас видеть!"},
     {59, ["8452", phone, undefined, undefined, false], "eighty four fifty two"},
     {60, ["12345", phone, undefined, undefined, false], "one twenty three fourty five"},
     {61, ["123456", phone, undefined, undefined, false], "twelve thirty four fifty six"},
     {62, ["210006", phone, undefined, undefined, false], "twenty one double zero zero six"},
     {63, ["3123456", phone, undefined, undefined, false], "three twelve thirty four fifty six"},
     {64, ["31aBcD625", phone, undefined, undefined, false], "thirty one a b c d six twenty five"},
     {65, ["*01", phone, undefined, undefined, false], "asterisk zero one"},
     {66, ["#12", phone, undefined, undefined, false], "lattice twelve"},
     {67, ["#345", phone, undefined, undefined, false], "lattice three fourty five"},
     {68, ["#3457", phone, undefined, undefined, false], "lattice thirty four fifty seven"},
     {69, ["3#4567", phone, undefined, undefined, false], "three lattice fourty five sixty seven"},
     {70, ["34*567", phone, undefined, undefined, false], "thirty four asterisk five sixty seven"},
     {71, ["*1*25*123#", phone, undefined, undefined, false], "asterisk one asterisk twenty five asterisk one twenty three lattice"},
     {72, ["*1*1*123#", phone, undefined, undefined, false], "asterisk one asterisk one asterisk one twenty three lattice"},
     {73, ["*12", phone, undefined, undefined, false], "asterisk twelve"},
     {74, ["2213890", phone, undefined, undefined, false], "two twenty one thirty eight ninety"},
     {75, ["89253120035", phone, undefined, undefined, false], "eight ninety two fifty three twelve double zero thirty five"},
     {76, ["+79253120035", phone, undefined, undefined, false], "seven ninety two fifty three twelve double zero thirty five"},
     {77, ["+7(925)3120035", phone, undefined, undefined, false], "seven ninety two fifty three twelve double zero thirty five"},
     {78, ["+7(925)312-00-35", phone, undefined, undefined, false], "seven ninety two fifty three twelve double zero thirty five"},
     {79, ["+7 (925) 312-00-35", phone, undefined, undefined, false], "seven ninety two fifty three twelve double zero thirty five"},
     {80, ["+7 (8552) 21-12-80", phone, undefined, undefined, false], "seven eighty five fifty two twenty one twelve eighty"},
     {81, ["+7 (85) 5221-12-80", phone, undefined, undefined, false], "seven eighty five fifty two twenty one twelve eighty"},
     {82, ["2018-05-08 12:35:00", date, undefined, undefined, false], "may the eighth twenty eighteen"},
     {83, ["2018-05-08T12:35:00.52Z", date, undefined, undefined, false], "may the eighth twenty eighteen"},
     {84, ["28.12.2018", date, undefined, undefined, false], "december the twenty eighth twenty eighteen"},
     {85, ["28.12.2018 22:00:00", date, undefined, undefined, false], "december the twenty eighth twenty eighteen"},
     {86, ["2018-03-28", time, undefined, undefined, false], "twelve am"},
     {87, ["2018-03-28 12:01", time, undefined, undefined, false], "zero o0 one pm"},
     {88, ["2018-03-28 00:01:02", time, undefined, undefined, false], "twelve o0 one am"},
     {89, ["2018-03-28 12:21:00", time, undefined, undefined, false], "zero twenty one pm"},
     {90, ["00:02", time, undefined, undefined, false], "twelve o0 two am"},
     {91, ["00:15:35", time, undefined, undefined, false], "twelve fifteen am"},
     {92, ["15:00", time, undefined, undefined, false], "three pm"},
     {93, ["2018-03-28", timewo0b, undefined, undefined, false], "zero seconds"},
     {94, ["2018-03-28 12:01", timewo0b, undefined, undefined, false], "twelve hours one minute sharp"},
     {95, ["2018-03-28 00:01:02", timewo0b, undefined, undefined, false], "one minute two seconds"},
     {96, ["2018-03-28 12:21:00", timewo0b, undefined, undefined, false], "twelve hours twenty one minutes sharp"},
     {97, ["00:02", timewo0b, undefined, undefined, false], "two minutes sharp"},
     {98, ["00:15:35", timewo0b, undefined, undefined, false], "fifteen minutes thirty five seconds"},
     {99, ["15:00", timewo0b, undefined, undefined, false], "fifteen hours sharp"},
     {100, ["2018-03-28", timewo0e, undefined, undefined, false], "zero hours sharp"},
     {101, ["2018-03-28 12:01", timewo0e, undefined, undefined, false], "twelve hours one minute"},
     {102, ["2018-03-28 00:01:02", timewo0e, undefined, undefined, false], "zero hours one minute two seconds"},
     {103, ["2018-03-28 12:21:00", timewo0e, undefined, undefined, false], "twelve hours twenty one minutes"},
     {104, ["00:02", timewo0e, undefined, undefined, false], "zero hours two minutes"},
     {105, ["00:15:35", timewo0e, undefined, undefined, false], "zero hours fifteen minutes thirty five seconds"},
      {106, ["15:00", timewo0e, undefined, undefined, false], "fifteen hours sharp"},
     {107, ["200", timesec, undefined, undefined, false], "two hundred seconds"},
     {108, ["102", timesec, undefined, undefined, false], "one hundred and two seconds"},
     {109, ["81", timesec, undefined, undefined, false], "eighty one seconds"},
     {110, ["2018-03-28", datetime, undefined, undefined, false], "march the twenty eighth two thousand eighteen zero hours sharp"},
     {111, ["13:22:25", datetime, undefined, undefined, false], "january the first nineteen hundred seventy thirteen hours twenty two minutes twenty five seconds"},
     {112, ["25.12.1981 12:30:00", datetime, undefined, undefined, true], "december the twenty fifth nineteen hundred eighty one twelve hours thirty minutes_"},
     {113, ["1981-12-25T12:30:00+03:00", datetime, undefined, undefined, true], "december the twenty fifth nineteen hundred eighty one twelve hours thirty minutes_"},
     {114, ["1", weekday, undefined, undefined, false], "monday"},
     {115, ["2", weekday, undefined, undefined, true], "tuesday_"},
     {116, ["7", weekday, undefined, undefined, false], "sunday"},
     {117, ["2", moneyusd, undefined, undefined, false], "two dollars"},
     {118, ["1.2", moneyusd, undefined, undefined, false], "one dollar twenty cents"},
     {119, ["5.25", moneyusd, undefined, undefined, false], "five dollars twenty five cents"},
     {120, ["1382.31", moneyusd, undefined, undefined, false], "one thousand three hundred and eighty two dollars thirty one cents"},
     {121, ["11800.92", moneyusd, undefined, undefined, false], "eleven thousand eight hundred dollars ninety two cents"},
     {122, ["2000000000", moneyusd, undefined, undefined, false], "two billion dollars"},
     {123, ["19000000000000", moneyusd, undefined, undefined, false], "nineteen trillion dollars"},
     {124, ["2", moneyeur, undefined, undefined, false], "two euros"},
     {125, ["1.2", moneyeur, undefined, undefined, false], "one euro twenty "},
     {126, ["5.25", moneyeur, undefined, undefined, false], "five euros twenty five "},
     {127, ["1482.31", moneyeur, undefined, undefined, false], "one thousand four hundred and eighty two euros thirty one "},
     {128, ["12600.92", moneyeur, undefined, undefined, false], "twelve thousand six hundred euros ninety two "},
     {129, ["2", moneyrub, undefined, undefined, false], "two roubles"},
     {130, ["1.2", moneyrub, undefined, undefined, false], "one rouble twenty copecs"},
     {131, ["5.25", moneyrub, undefined, undefined, false], "five roubles twenty five copecs"},
     {132, ["1582.31", moneyrub, undefined, undefined, false], "one thousand five hundred and eighty two roubles thirty one copecs"},
     {133, ["13700.92", moneyrub, undefined, undefined, false], "thirteen thousand seven hundred roubles ninety two copecs"},
     {134, ["2", percent, undefined, undefined, false], "two percents"},
     {135, ["1.2", percent, undefined, undefined, false], "one point two percents"},
     {136, ["5.25", percent, undefined, undefined, false], "five point two five percents"},
     {137, ["100", percent, undefined, undefined, false], "one hundred percents"},
     {138, ["146", percent, undefined, undefined, false], "one hundred and fourty six percents"}].


-endif.
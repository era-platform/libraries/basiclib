%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2018
%%% @doc Common functions are the expansion of numbers into morphemes and the formation of an array of words, according to the format.

-module(basiclib_txtnum_parser).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([number_by_type/2,
    float_simple_count/2,
    float_simple_ordinal/2,
    date/2,
    time/2,
    time_wo0b/2,
    time_wo0e/2,
    time_in_seconds/2,
    date_time/2,
    phone/2,
    week_day/2,
    money_USD/2,
    money_euro/2,
    money_national/2,
    money_custom/2,
    percent/2,
    % supporting
    txt_function/3,
    %
    add_zeros_left/1,
    add_zero_left_do/1,
    delete_zeros_right/1,
    parse_1x_letter_and_number/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("basiclib_txtnum.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%% Type
%% --------------------------------------
number_by_type(Number, R) ->
    parse_number_by_type(?BU:to_unicode_binary(Number), R).

%% --------------------------------------
%% Float
%% --------------------------------------
float_simple_count(Number, R) when is_binary(Number) ->
    parse_float_simple_count(Number, R);
float_simple_count(Number, R) ->
    parse_float_simple_count(?BU:to_binary(Number), R).

float_simple_ordinal(Number, R) when is_binary(Number) ->
    parse_float_simple_ordinal(Number, R);
float_simple_ordinal(Number, R) ->
    float_simple_ordinal(?BU:to_binary(Number), R).

%% --------------------------------------
%% Calendar
%% --------------------------------------
date({{0,0,0},{_,_,_},_}, R) -> date({1970,1,1}, R);
date({{_,_,_}=Date,{_,_,_},_}, R) -> date(Date, R);
date({_,_,_}=Date, R) -> lang_function(parse_date, [Date], R);
date(Value, R) -> date(?BU:parse_datetime(?BU:to_binary(Value)), R).

date_time({{0,0,0},{_,_,_}=T,Ms}, R) ->    date_time({{1970,1,1},T,Ms}, R);
date_time({{_,_,_},{_,_,_},_}=DateTime, R) -> lang_function(parse_date_time,[DateTime], R);
date_time(Value, R) -> date_time(?BU:parse_datetime(?BU:to_binary(Value)), R).

time({_,{_,_,_}=Time,_}, R) -> time(Time, R);
time({_,_,_}=Time, R) -> lang_function(parse_time,[Time, false, false], R);
time(Seconds, R) when is_integer(Seconds) -> time(calendar:seconds_to_time(Seconds), R);
time(Value, R) -> time(?BU:parse_datetime(?BU:to_binary(Value)), R).

time_wo0b({_,{_,_,_}=Time,_}, R) -> time_wo0b(Time, R);
time_wo0b({_,_,_}=Time, R) -> lang_function(parse_time,[Time, true, false], R);
time_wo0b(Seconds, R) when is_integer(Seconds) -> time_wo0b(calendar:seconds_to_time(Seconds), R);
time_wo0b(Value, R) -> time_wo0b(?BU:parse_datetime(?BU:to_binary(Value)), R).

time_wo0e({_,{_,_,_}=Time,_}, R) -> time_wo0e(Time, R);
time_wo0e({_,_,_}=Time, R) -> lang_function(parse_time,[Time, false, true], R);
time_wo0e(Seconds, R) when is_integer(Seconds) -> time_wo0e(calendar:seconds_to_time(Seconds), R);
time_wo0e(Value, R) -> time_wo0e(?BU:parse_datetime(?BU:to_binary(Value)), R).

time_in_seconds(Seconds, R) when is_integer(Seconds) ->
    Acc = lang_function(parse_number, [Seconds], R#number_representation{gender=feminine}),
    lists:append(Acc, [txt_function(names, [?NttSeconds, Seconds], R)]);
time_in_seconds(Value, R) -> time_in_seconds(?BU:to_int(Value), R).

week_day(Day, R) when is_integer(Day) -> [txt_function(name_week_day, [Day], R)];
week_day({{0,0,0},{_,_,_},_}, R) -> week_day({1970,1,1}, R);
week_day({{_,_,_}=Date,{_,_,_},_}, R) -> week_day(Date, R);
week_day({_,_,_}=Date, R) ->
    Day = calendar:day_of_the_week(Date),
    week_day(Day,R);
week_day(Value, R) -> [txt_function(name_week_day, [?BU:to_binary(Value)], R)].

%% --------------------------------------
%% Phone
%% --------------------------------------
phone(Number, R) when is_binary(Number) ->
    parse_phone(Number, R);
phone(Number, R) ->
    phone(?BU:to_binary(Number), R).

%% --------------------------------------
%% Money
%% --------------------------------------
money_USD(Number, R) when is_binary(Number) ->
    parse_money(Number, ?NttDollars, ?NttCents, R);
money_USD(Number, R) -> money_USD(?BU:to_binary(Number), R).

money_euro(Number, R) when is_binary(Number) ->
    parse_money(Number, ?NttEurs, ?NttEurCents, R);
money_euro(Number, R) -> money_euro(?BU:to_binary(Number), R).

money_national(Number, R) when is_binary(Number) ->
    parse_money(Number, ?NttCurrencyNationalMaj, ?NttCurrencyNationalMin, R);
money_national(Number, R) -> money_national(?BU:to_binary(Number), R).

money_custom(Number, R) when is_binary(Number) ->
    parse_money(Number, ?NttCurrencyCustomMaj, ?NttCurrencyCustomMin, R);
money_custom(Number, R) -> money_custom(?BU:to_binary(Number), R).

%% --------------------------------------
%% Percent
%% --------------------------------------
percent(Number, R) when is_binary(Number) ->
    Acc1 = parse_float_simple_count(Number, R),
    {NumberW, _NumberF, _W0, F0} = divide_float_parts(Number),
    case F0 of
        false -> lists:append(Acc1, [txt_function(names, [?NttPercent, 2], R)]);
        true -> lists:append(Acc1, [txt_function(names, [?NttPercent, ?BU:to_int(NumberW)], R)])
    end;
percent(Number, R) -> percent(?BU:to_binary(Number), R).

%% ====================================================================
%% Supporting functions
%% ====================================================================
txt_function(letter, [<<"а"/utf8>>], _R) -> <<"а"/utf8>>;
txt_function(letter, [<<"б"/utf8>>], _R) -> <<"б"/utf8>>;
txt_function(letter, [<<"в"/utf8>>], _R) -> <<"в"/utf8>>;
txt_function(letter, [<<"г"/utf8>>], _R) -> <<"г"/utf8>>;
txt_function(letter, [<<"д"/utf8>>], _R) -> <<"д"/utf8>>;
txt_function(letter, [<<"е"/utf8>>], _R) -> <<"е"/utf8>>;
txt_function(letter, [<<"ё"/utf8>>], _R) -> <<"ё"/utf8>>;
txt_function(letter, [<<"ж"/utf8>>], _R) -> <<"ж"/utf8>>;
txt_function(letter, [<<"з"/utf8>>], _R) -> <<"з"/utf8>>;
txt_function(letter, [<<"и"/utf8>>], _R) -> <<"и"/utf8>>;
txt_function(letter, [<<"й"/utf8>>], _R) -> <<"й"/utf8>>;
txt_function(letter, [<<"к"/utf8>>], _R) -> <<"к"/utf8>>;
txt_function(letter, [<<"л"/utf8>>], _R) -> <<"л"/utf8>>;
txt_function(letter, [<<"м"/utf8>>], _R) -> <<"м"/utf8>>;
txt_function(letter, [<<"н"/utf8>>], _R) -> <<"н"/utf8>>;
txt_function(letter, [<<"о"/utf8>>], _R) -> <<"о"/utf8>>;
txt_function(letter, [<<"п"/utf8>>], _R) -> <<"п"/utf8>>;
txt_function(letter, [<<"р"/utf8>>], _R) -> <<"р"/utf8>>;
txt_function(letter, [<<"с"/utf8>>], _R) -> <<"с"/utf8>>;
txt_function(letter, [<<"т"/utf8>>], _R) -> <<"т"/utf8>>;
txt_function(letter, [<<"у"/utf8>>], _R) -> <<"у"/utf8>>;
txt_function(letter, [<<"ф"/utf8>>], _R) -> <<"ф"/utf8>>;
txt_function(letter, [<<"х"/utf8>>], _R) -> <<"х"/utf8>>;
txt_function(letter, [<<"ц"/utf8>>], _R) -> <<"ц"/utf8>>;
txt_function(letter, [<<"ч"/utf8>>], _R) -> <<"ч"/utf8>>;
txt_function(letter, [<<"ш"/utf8>>], _R) -> <<"ш"/utf8>>;
txt_function(letter, [<<"щ"/utf8>>], _R) -> <<"щ"/utf8>>;
txt_function(letter, [<<"ъ"/utf8>>], _R) -> <<"ъ"/utf8>>;
txt_function(letter, [<<"ы"/utf8>>], _R) -> <<"ы"/utf8>>;
txt_function(letter, [<<"ь"/utf8>>], _R) -> <<"ь"/utf8>>;
txt_function(letter, [<<"э"/utf8>>], _R) -> <<"э"/utf8>>;
txt_function(letter, [<<"ю"/utf8>>], _R) -> <<"ю"/utf8>>;
txt_function(letter, [<<"я"/utf8>>], _R) -> <<"я"/utf8>>;
%%
txt_function(letter, [<<"a"/utf8>>], _R) -> <<"a"/utf8>>;
txt_function(letter, [<<"b"/utf8>>], _R) -> <<"b"/utf8>>;
txt_function(letter, [<<"c"/utf8>>], _R) -> <<"c"/utf8>>;
txt_function(letter, [<<"d"/utf8>>], _R) -> <<"d"/utf8>>;
txt_function(letter, [<<"e"/utf8>>], _R) -> <<"e"/utf8>>;
txt_function(letter, [<<"f"/utf8>>], _R) -> <<"f"/utf8>>;
txt_function(letter, [<<"g"/utf8>>], _R) -> <<"g"/utf8>>;
txt_function(letter, [<<"h"/utf8>>], _R) -> <<"h"/utf8>>;
txt_function(letter, [<<"i"/utf8>>], _R) -> <<"i"/utf8>>;
txt_function(letter, [<<"j"/utf8>>], _R) -> <<"j"/utf8>>;
txt_function(letter, [<<"k"/utf8>>], _R) -> <<"k"/utf8>>;
txt_function(letter, [<<"l"/utf8>>], _R) -> <<"l"/utf8>>;
txt_function(letter, [<<"m"/utf8>>], _R) -> <<"m"/utf8>>;
txt_function(letter, [<<"n"/utf8>>], _R) -> <<"n"/utf8>>;
txt_function(letter, [<<"o"/utf8>>], _R) -> <<"o"/utf8>>;
txt_function(letter, [<<"p"/utf8>>], _R) -> <<"p"/utf8>>;
txt_function(letter, [<<"q"/utf8>>], _R) -> <<"q"/utf8>>;
txt_function(letter, [<<"r"/utf8>>], _R) -> <<"r"/utf8>>;
txt_function(letter, [<<"s"/utf8>>], _R) -> <<"s"/utf8>>;
txt_function(letter, [<<"t"/utf8>>], _R) -> <<"t"/utf8>>;
txt_function(letter, [<<"u"/utf8>>], _R) -> <<"u"/utf8>>;
txt_function(letter, [<<"v"/utf8>>], _R) -> <<"v"/utf8>>;
txt_function(letter, [<<"w"/utf8>>], _R) -> <<"w"/utf8>>;
txt_function(letter, [<<"x"/utf8>>], _R) -> <<"x"/utf8>>;
txt_function(letter, [<<"y"/utf8>>], _R) -> <<"y"/utf8>>;
txt_function(letter, [<<"z"/utf8>>], _R) -> <<"z"/utf8>>;
%%
txt_function(letter, [<<"*"/utf8>>], R) -> txt_function(asterisk, [], R);
txt_function(letter, [<<"#"/utf8>>], R) -> txt_function(lattice, [], R);
txt_function(letter, [<<"."/utf8>>], R) -> txt_function(dot, [], R);
txt_function(letter, [<<","/utf8>>], R) -> txt_function(comma, [], R);
txt_function(letter, [<<"/"/utf8>>], R) -> txt_function(slash, [], R);
txt_function(letter, [<<"\\"/utf8>>], R) -> txt_function(backslash, [], R);
txt_function(letter, [<<"-"/utf8>>], R) -> txt_function(dash, [], R);
%%
txt_function(FunName, Params, #number_representation{text_module=TM}) ->
    erlang:apply(TM, FunName, Params).
%% ----------------------------
lang_function(FunName, Params, #number_representation{lang_module=LM}=R) ->
    UpParams = lists:append(Params, [R]),
    erlang:apply(LM, FunName, UpParams).

%% ------------------
%%
%% ------------------
add_zeros_left(Number) when is_binary(Number) ->
    L = string:length(Number),
    case (L < 6) of
        true -> case ?BU:to_int(Number) of 0 -> Number; _ -> add_zero_left_do(Number) end;
        false -> add_zero_left_do(Number)
    end.
%% --------
%%
%% --------
add_zero_left_do(Number) ->
    case (string:length(Number) rem 3) of
        0 -> Number;
        _ -> add_zero_left_do(<<"0", Number/binary>>)
    end.
%% ------------------
%%
%% ------------------
delete_zeros_right(<<>>) -> <<>>;
delete_zeros_right(Number) when is_binary(Number) ->
    case binary:last(Number) of
        $0 -> delete_zeros_right(binary:part(Number, 0, (byte_size(Number) - 1)));
        _ -> Number
    end.

%% --------------------------------------
%%
%% --------------------------------------
parse_1x_letter_and_number(Number, R) ->
    try ?BU:to_int(Number) of
        _ -> lang_function(parse_3x_number_with_zeroes, [Number], R)
    catch
        _:_ ->
            % если сюда приехали - значит буквенный символ.
            Numberlow = string:lowercase(Number),
            All = <<"абвгдеёжзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyz.,\\/-*#"/utf8>>,
            case binary:match(All, Numberlow) of
                nomatch  -> [];
                _ -> [?Parse:txt_function(letter,[Numberlow],R)]
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
parse_number_by_type(Number, #number_representation{format='number'}=R) ->
    float_simple_count(Number, R);
parse_number_by_type(Number, #number_representation{format='words'}) ->
    NumberLow = string:lowercase(Number),
    Arr = re:split(NumberLow, "([^\\f\\n\\r\\t\\v ?\\*]+)", [group]),
    F = fun([_],Acc) -> Acc;
           ([_,M],Acc) -> lists:append(Acc, [M])
        end,
    lists:foldl(F, [], Arr);
parse_number_by_type(Number, #number_representation{format='letters'}=R) ->
    List = ?BU:to_unicode_list(Number),
    Trim = string:trim(List),
    F = fun(S, Acc) ->
                Result = parse_1x_letter_and_number(<<S/utf8>>, R),
                lists:append(Acc, Result)
        end,
    lists:foldl(F, [], Trim);
parse_number_by_type(Number, #number_representation{format=Format}=R) ->
    NumberTrim = string:trim(Number),
    Matches = re:split(NumberTrim, "([#\\*\\d]+)", [group]),
    N = lists:foldl(fun([_],Acc) -> Acc;
                       ([_,M],Acc) -> <<Acc/binary, M/binary>> end, <<>>, Matches),
    F = fun(I) -> lang_function(parse_number_split_by_symbols_count, [N, I], R) end,
    case Format of
        sym1 -> F(1);
        sym2 -> F(2);
        sym3 -> F(3)
    end.

%% --------------------------------------
%% Float
%% --------------------------------------
parse_float_simple_count(Number, R) when is_binary(Number) ->
    case string:length(Number) of
        0 -> txt_function(zero,[],R);
        _ ->
            {UpNumber,Acc} = check_and_append_minus(Number, [], R),
            {NumberW, NumberF, W0, F0} = divide_float_parts(UpNumber),
            case F0 of
                true -> lists:append(Acc, lang_function(parse_number, [NumberW], R));
                false -> lists:append(Acc, lang_function(parse_float_simple_count_wf, [NumberW, NumberF, W0], R))
            end
    end.
%% --------
%% @private
%% --------
check_and_append_minus(<<$-, Number/bitstring>>, Acc, R) ->
    {Number, lists:append(Acc, [txt_function(minus,[],R)])};
check_and_append_minus(Number, Acc, _) ->
    {Number, Acc}.

%% ----------------------------
%%
%% ----------------------------
-spec divide_float_parts(Number::binary()) -> {Whole::binary(), Fract::binary(), WholeIsZero::boolean(), FractIsZero::boolean()}.
%% ----------------------------
divide_float_parts(Num) when is_binary(Num) ->
    Number = string:trim(Num),
    FunWhole = fun(PartWhole) ->
                       NWhole = binary:part(Number, PartWhole),
                       WholeZ = add_zeros_left(NWhole),
                       IsZero = case string:length(WholeZ) < 6 of true -> case ?BU:to_int(WholeZ) of 0 -> true; _ -> false end; false -> false end,
                       {IsZero, WholeZ}
               end,
    case re:run(Number, "^([0-9]+)((,|.)[0-9]+)*$") of
        {match,[{0,_},PartWhole]} ->
            {WholeIsZero, ResultWhole} = FunWhole(PartWhole),
            {ResultWhole,<<>>,WholeIsZero,true};
        {match,[{0,_},PartWhole,{S,L},_]} ->
            NumberFract = binary:part(Number, S+1, L-1),
            NumberFractWithoutZeros = delete_zeros_right(NumberFract),
            {WholeIsZero, ResultWhole} = FunWhole(PartWhole),
            UpNumberFract = case (string:length(NumberFractWithoutZeros) > 4) of
                                true -> binary:part(NumberFractWithoutZeros, 0, 4);
                                false -> NumberFractWithoutZeros
                            end,
            {FractIsZero, ResultFract} = case (string:length(UpNumberFract) == 0) of
                                             true -> {true,<<>>};
                                             false -> case ?BU:to_int(UpNumberFract) of 0 -> {true,<<>>}; _ -> {false, UpNumberFract} end
                                         end,
            {ResultWhole,ResultFract,WholeIsZero,FractIsZero};
        _R -> {<<>>, <<>>, true, true}
    end.

%% ----------------------------
%%
%% ----------------------------
parse_float_simple_ordinal(Number, R) ->
    case string:length(Number) of
        0 -> [txt_function(zero,[],R)];
        _ ->
            {UpNum, Acc} = check_and_append_minus(Number, [], R),
            {NumberW, _NumberF, _W0, _F0} = divide_float_parts(UpNum),
            lists:append(Acc, lang_function(parse_number_ordinal, [NumberW], R))
    end.

%% --------------------------------------
%% phone
%% --------------------------------------
parse_phone(Number, R) ->
    Arr = re:split(Number, "([\\*#\dABCDEFabcdef]+)"),
    F = fun(<<>>, Acc) -> Acc;
           (Part, Acc) ->
                case re:run(Part, "([\\*#ABCDEFabcdef]+)") of
                    nomatch -> lists:append(Acc, lang_function(parse_phone_simple, [Part], R));
                    _ ->
                        lists:append(Acc, parse_number_by_type(Part, R#number_representation{format='letters'}))
                end
        end,
    lists:foldl(F, [], Arr).

%% --------------------------------------
%% Money
%% --------------------------------------
parse_money(Number, CurrencyMaj, CurrencyMin, #number_representation{mode=M}=R) ->
    case string:length(Number) of
        0 -> lists:append(txt_function(zero, [], R), txt_function(names, [CurrencyMaj, 0], R));
        _ ->
            {NumberW, NumberF, _W0, F0} = divide_float_parts(Number),
            {UpNumberF, UpF0} = update_number_f(NumberF,F0),
            case UpF0 of
                true ->
                    Acc1 = lang_function(parse_number, [NumberW], R#number_representation{gender=masculine}),
                    lists:append(Acc1, [txt_function(names, [CurrencyMaj, ?BU:to_int(NumberW)], R)]);
                false ->
                    Acc1 = lang_function(parse_number, [NumberW], R#number_representation{gender=masculine}),
                    Acc2 = lists:append(Acc1, [txt_function(names, [CurrencyMaj, ?BU:to_int(NumberW)], R)]),
                    G = case M of moneyrub -> feminine; _ -> masculine end,
                    Acc3 = lists:append(Acc2, lang_function(parse_number,[UpNumberF], R#number_representation{gender=G})),
                    Acc4 = lists:append(Acc3, [txt_function(names, [CurrencyMin, ?BU:to_int(UpNumberF)], R)]),
                    Acc4
            end
    end.
%% ------------------
%% @private
%% ------------------
update_number_f(<<"00",_>>, _) -> {<<>>, true};
update_number_f(<<N:8/bitstring>>, _) -> {<<N/binary,"0">>, false};
update_number_f(NumberF, F0) -> {NumberF,F0}.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.02.2017
%%% @doc Service of pure functions to modify values (routing, representative, redirect, displayname on from, etc.)

-module(basiclib_modifier).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([apply_modifier_regex/2,
         apply_modifier/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------------------------
%% Applies char modifier to value
%%  Mask could be text and regex (/reg/Pattern/Replace/Opts)
%%  BraceKeys should be list of opts [{Char,Value::function()|binary()|string()|integer()}]
%%  Opts could contain 't', 'x', 'asterisk', 'ebrace' to enable some options
-spec apply_modifier(Modifier::binary(),
                     Value::string()|binary(),
                     BraceKeys::[{Char::integer(),Value::function()|binary()|string()|integer()}],
                     Opts::list())
        -> Result::binary().

apply_modifier(<<"/reg/",_/bitstring>>=M, Value, _, _) ->
    % @todo? check 'regex' option
    apply_modifier_regex(M,Value);

apply_modifier(Modifier, Value, BraceKeys, Opts)
  when is_binary(Modifier) ->
    V = ?BU:to_unicode_list(Value),
    M = ?BU:to_unicode_list(Modifier),
    % ---
    UseT = lists:member('t', Opts),
    UseX = lists:member('x', Opts),
    UseAst = lists:member('asterisk', Opts),
    UseQuestion = lists:member('question', Opts),
    UseEbrace = lists:member('ebrace', Opts),
    % ---
    F = fun($[,{Rest,Acc,false}) -> {Rest, Acc, sqbrace};
           ($[,{_,_,sqbrace}) -> throw({error,"Invalid mask ([)"});
           ($],{Rest,Acc,sqbrace}) -> {Rest, Acc, false};
           ($],{_,_,_}) -> throw({error,"Invalid mask (])"});
           (A,{Rest,Acc,sqbrace}) -> {Rest, [A|Acc], sqbrace};
           % ---
           ($/, {Rest,Acc,false}) when UseX -> {Rest, Acc, skip};
           ($/, {Rest,Acc,skip}) when UseX -> {Rest, Acc, false};
           ($X, {[Sym|Rest],Acc,false}) when UseX -> {Rest, [Sym|Acc], false};
           ($X, {[_Sym|Rest],Acc,skip}) when UseX -> {Rest, Acc, skip};
           ($X, {[],Acc,F}) when UseX -> {[], Acc, F};
           ($?, {[Sym|Rest],Acc,false}) when UseQuestion -> {Rest, [Sym|Acc], false};
           ($?, {[_Sym|Rest],Acc,skip}) when UseQuestion -> {Rest, Acc, skip};
           ($?, {[],Acc,F}) when UseQuestion -> {[], Acc, F};
           % ---
           ($*, {Rest,Acc,false}) when UseAst -> {[], lists:reverse(Rest) ++ Acc, false};
           ($*, {_,Acc,skip}) when UseAst -> {[], Acc, skip};
           ($T, {Rest,Acc,false}) when UseT -> {Rest, lists:reverse(V) ++ Acc, skip};
           % ---
           (${, {Rest,Acc,false}) -> {Rest, Acc, brace1};
           ($E, {Rest,Acc,brace1}) when UseEbrace -> {Rest, Acc, brace2};
           ($e, {Rest,Acc,brace1}) when UseEbrace -> {Rest, Acc, brace2};
           (Ch, {Rest,Acc,brace1}) ->
                case lists:keyfind(Ch,1,BraceKeys) of
                    false -> throw(Value);
                    {_,FX} when is_function(FX,0) -> {Rest, lists:reverse(?BU:to_list(FX())) ++ Acc, brace2};
                    {_,VX} when is_binary(VX); is_list(VX) -> {Rest, lists:reverse(?BU:to_list(VX)) ++ Acc, brace2}
                end;
           ($}, {Rest,Acc,brace2}) -> {Rest, Acc, false};
           ($}, {_,_,false}) -> throw(Value);
           (_, {_,_,brace1}) -> throw(Value);
           (_, {_,_,brace2}) -> throw(Value);
           % ---
           (A, {Rest,Acc,false}) -> {Rest, [A|Acc], false};
           (_A, {Rest,Acc,skip}) -> {Rest, Acc, skip};
           (_, Acc) -> Acc
        end,
    try {_,Res,_} = lists:foldl(F, {V,[],false}, M),
        ?BU:to_binary(lists:reverse(Res))
    catch throw:R -> ?BU:to_binary(R)
    end.


%% ----------------------------------------------------------
%% Applies regex modifier to value
%%  allows to apply several regex modifiers sequently
%%    every modifier contains "/reg/Pattern/Replace/Opts", where Opts:: of ""|"g"|"i"|"gi"
-spec apply_modifier_regex(RegModifiers::binary(), Value::binary()) -> Result::binary().

apply_modifier_regex(<<"/reg/",_/bitstring>>=Mod, Value) ->
    Fopts = fun(Opts) -> lists:foldl(fun($g, Acc) -> [global|Acc];
                                        ($i, Acc) -> [caseless|Acc];
                                        (_, Acc) -> Acc
                                     end, [], ?BU:to_list(Opts)) end,
    Frep = fun(Val,Pat,Rep,Opts) ->
                   case catch re:replace(?BU:to_list(Val), ?BU:to_list(Pat), ?BU:to_list(Rep), [{return,binary}|Opts]) of
                       {'EXIT',_R} -> ?LOG('$error', "Error regex modifier val=~120p, pat=~120p, rep=~120p, opt=~120p, res=~120p", [Val,Pat,Rep,Opts,_R]), {error,Val};
                       R -> R
                   end end,
    R = lists:foldl(fun(_, {error,_}=Err) -> Err;
                       (R, Acc) ->
                            case binary:split(R, [<<"/">>], [global]) of
                                [Pattern,Replace,Opts] -> Frep(Acc,Pattern,Replace,Fopts(Opts));
                                _ -> {error,Acc}
                            end end, ?BU:to_binary(Value), binary:split(Mod, [<<"/reg/">>], [global, trim_all])),
    case R of
        {error,Val} -> Val;
        _ -> R
    end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

regex_test_() ->
    {"modifier regex test",
     [?_assertEqual(<<"QWEREY">>, apply_modifier(<<"/reg/T/E/g">>, <<"QWERTY">>, null, null)),
      ?_assertEqual(<<"qwerEyE">>, apply_modifier(<<"/reg/T/E/gi">>, <<"qwertyt">>, null, null)),
      ?_assertEqual(<<"qwertytE">>, apply_modifier(<<"/reg/T/E/g">>, <<"qwertytT">>, null, null)),
      ?_assertEqual(<<"qwertytE">>, apply_modifier(<<"/reg/T/E/g">>, <<"qwertytT">>, null, null)),
      %
      ?_assertEqual(<<"_qwerty_">>, apply_modifier(<<"/reg/(\\w+)/_\\1_/g">>, <<"qwerty">>, null, null)),
      ?_assertEqual(<<"sqwers-sasdfs">>, apply_modifier(<<"/reg/(\\w+)/s\\1s/g">>, <<"qwer-asdf">>, null, null)),
      ?_assertEqual(<<"sqwers-asdf">>, apply_modifier(<<"/reg/(\\w+)/s\\1s/">>, <<"qwer-asdf">>, null, null)),
      %
      ?_assertEqual(<<"aEy,qwerEy">>, apply_modifier(<<"/reg/t/E/g /reg/qwer/a/">>, <<"qwerty,qwerty">>, null, null))
      ]}.

symbol_test_() ->
    % Modifier, Value, BraceKeys, Opts
    % 't','x','asterisk','ebrace'
    {"modifier symbol test",
     [?_assertEqual(<<"asdf">>, apply_modifier(<<"asdf">>, <<"QWERTY">>, [], [])),
      ?_assertEqual(<<"фыва"/utf8>>, apply_modifier(<<"фыва"/utf8>>, <<"йцук"/utf8>>, [], [])),
      %
      ?_assertEqual(<<"*">>, apply_modifier(<<"*">>, <<"QWERTY">>, [], [])),
      ?_assertEqual(<<"*">>, apply_modifier(<<"[*]">>, <<"QWERTY">>, [], [])),
      ?_assertEqual(<<"QWERTY">>, apply_modifier(<<"*">>, <<"QWERTY">>, [], [asterisk])),
      ?_assertEqual(<<"QWERTY">>, apply_modifier(<<"**">>, <<"QWERTY">>, [], [asterisk])),
      ?_assertEqual(<<"*QWERTY">>, apply_modifier(<<"[*]*">>, <<"QWERTY">>, [], [asterisk])),
      ?_assertEqual(<<"QWERTYA">>, apply_modifier(<<"*A">>, <<"QWERTY">>, [], [asterisk])),
      ?_assertEqual(<<"фыва"/utf8>>, apply_modifier(<<"*">>, <<"фыва"/utf8>>, [], [asterisk])),
      %
      ?_assertEqual(<<"/X/XX">>, apply_modifier(<<"/X/XX">>, <<"QWERTY">>, [], [])),
      ?_assertEqual(<<"WE">>, apply_modifier(<<"/X/XX">>, <<"QWERTY">>, [], [x])),
      ?_assertEqual(<<"WXE">>, apply_modifier(<<"/X/X[X]X">>, <<"QWERTY">>, [], [x])),
      ?_assertEqual(<<"ва"/utf8>>, apply_modifier(<<"/XX/XX">>, <<"фыва"/utf8>>, [], [x])),
      ?_assertEqual(<<"asdf">>, apply_modifier(<<"X*">>, <<"asdf">>, [], [asterisk,x])),
      ?_assertEqual(<<"asd">>, apply_modifier(<<"XXX">>, <<"asdf">>, [], [x])),
      ?_assertEqual(<<"asdf">>, apply_modifier(<<"XXXXX">>, <<"asdf">>, [], [x])),
      %
      ?_assertEqual(<<"aT">>, apply_modifier(<<"XT">>, <<"asdf">>, [], [x])),
      ?_assertEqual(<<"aasdf">>, apply_modifier(<<"XT">>, <<"asdf">>, [], [x,t])),
      ?_assertEqual(<<"aasdf">>, apply_modifier(<<"XTT">>, <<"asdf">>, [], [x,t])),
      %
      ?_assertEqual(<<"asdf">>, apply_modifier(<<"{Y}">>, <<"asdf">>, [], [x,t])),
      ?_assertEqual(<<"asdf">>, apply_modifier(<<"qwer{E}">>, <<"asdf">>, [], [x,t])),
      ?_assertEqual(<<"asdf">>, apply_modifier(<<"qwer{Y}zxcv">>, <<"asdf">>, [], [x,t,ebrace])),
      ?_assertEqual(<<"ayyysdf">>, apply_modifier(<<"X{Y}XXX">>, <<"asdf">>, [{$Y,<<"yyy">>}], [x,t,ebrace])),
      ?_assertEqual(<<"azzzsdf">>, apply_modifier(<<"X{Z}XXX">>, <<"asdf">>, [{$Z,fun() -> <<"zzz">> end}], [x,t,ebrace])),
      ?_assertEqual(<<"qwerasdf">>, apply_modifier(<<"qwer{E}XXXX">>, <<"asdf">>, [], [x,t,ebrace])),
      %
      ?_assertEqual(<<"/XT*/XT*">>, apply_modifier(<<"[/][X][T][*][/XT*]">>, <<"asdf">>, [], [asterisk,x,t]))
      ]}.

-endif.
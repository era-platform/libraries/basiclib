%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.08.2019
%%% @doc WAV file and audio functions

-module(basiclib_wav).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

get_wav_header(IsFloatingPoint,ChannelCount,BitDepth,SampleRate,TotalSamples) ->
    <<"RIFF", (BitDepth div 8*TotalSamples+36):32/unsigned-little-integer, "WAVE",
      % sub-chunk 1
      "fmt ",  %  sub-chunk 1 id
      16:32/unsigned-little-integer, % sub-chunk 1 size
      (case IsFloatingPoint of true -> 3; false -> 1 end):16/unsigned-little-integer, % audio fmt - floating point (3) or PCM (1)
      ChannelCount:16/unsigned-little-integer, % channels
      SampleRate:32/unsigned-little-integer, % sample rate
      (SampleRate * ChannelCount * BitDepth div 8):32/unsigned-little-integer, % bytes rate
      (ChannelCount * BitDepth div 8):16/unsigned-little-integer, % bytes rate
      BitDepth:16/unsigned-little-integer, % bits per sample
      % sub-chunk 2
      "data", % sub-chunk 2 id
      (BitDepth div 8 * TotalSamples):32/unsigned-little-integer % sub-chunk 2 size
    >>.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

get_wav_header_test_() ->
    {"transform to binary", [
        ?_assertEqual(44, size(get_wav_header(false,1,16,8000,1000))),
        ?_assertEqual(<<82,73,70,70,244,7,0,0,87,65,86,69,102,109,116,32,16,0,0,0,1,0,1,0,64,31,0,0,128,62,0,0,2,0,16,0,100,97,116,97,208,7,0,0>>,
            get_wav_header(false,1,16,8000,1000))
     ]}.

-endif.

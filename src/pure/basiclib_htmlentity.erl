%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.11.2017.
%%% @doc Utils to encode/decode html-entities

-module(basiclib_htmlentity).
-author('Peter Bukashin <tbotc@yandex.ru>').

% -compile([export_all]).
-export([encode/1,
         decode/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

encode(S) when is_list(S) -> ?BU:to_list(encode(?BU:to_binary(S)));
encode(S) when is_binary(S) -> do_encode(S,<<>>).

decode(S) when is_list(S) -> unicode:characters_to_list(decode(?BU:to_binary(S)));
decode(S) when is_binary(S) -> do_decode(S).

%% ====================================================================
%% Internal functions
%% ====================================================================

do_encode(Str,Acc) when is_binary(Str) ->
    do_encode(unicode:characters_to_list(Str),Acc);
do_encode([],Acc) -> Acc;
do_encode([A|Rest],Acc) ->
    X = unicode:characters_to_binary([A]),
    Symbl = case code_alias(A) of
                A when A>127 -> <<"&#x",(?BU:to_binary(?BU:to_upper(?BU:str("~.16.0b",[A]))))/binary,";">>;
                A -> X;
                Alias -> Alias
            end,
    do_encode(Rest,<<Acc/binary,Symbl/binary>>).


% @private
do_decode(Str) when is_binary(Str) ->
    Descriptors = parse_descriptors(Str),
    F = fun({From,Len,Type},{Shift,Data,Acc}) ->
                XLen = From-Shift,
                <<Prefix:XLen/binary,Elt:Len/binary,Rest/binary>> = Data,
                Sym = decode_symbol(Elt,Type),
                {From+Len, Rest, <<Acc/binary,Prefix/binary,Sym/binary>>}
        end,
    {_,Rest,Acc} = lists:foldl(F,{0,Str,<<>>},Descriptors),
    <<Acc/binary,Rest/binary>>.

% @private
parse_descriptors(Str) when is_binary(Str) ->
    X = [],
    X1 = case re:run(Str,<<"&#x([\\dA-Fa-f]{1,5});">>,[global]) of
             nomatch -> X;
             {match,Hex} ->
                 ordsets:union(X,lists:map(fun([{From,Len},{DF,DL}]) -> {From,Len,{hex,DF-From,DL}} end, Hex))
         end,
    X2 = case re:run(Str,<<"&#(\\d{1,5});">>,[global]) of
             nomatch -> X1;
             {match,Dec} ->
                 ordsets:union(X1,lists:map(fun([{From,Len},{DF,DL}]) -> {From,Len,{dec,DF-From,DL}} end, Dec))
         end,
    X3 = case re:run(Str,<<"&(\\w{1,8});">>,[global]) of
               nomatch -> X2;
             {match,Alias} ->
                 ordsets:union(X2,lists:map(fun([{From,Len},{DF,DL}]) -> {From,Len,{alias,DF-From,DL}} end, Alias))
         end,
    X3.

% @private
decode_symbol(Elt,Type) ->
    case Type of
        {hex,DF,DL} -> decode_hex(Elt,DF,DL);
        {dec,DF,DL} -> decode_dec(Elt,DF,DL);
        {alias,DF,DL} -> decode_alias(Elt,DF,DL)
    end.

% @private
decode_hex(Elt,DF,DL) ->
    <<_:DF/binary,Code:DL/binary,_/binary>> = Elt,
    Pow = 16,
    Fd = fun(A) when A>=$0, A=<$9 -> A-$0;
            (A) when A>=$A, A=<$F -> A-$A+10;
            (A) when A>=$a, A=<$f -> A-$a+10
         end,
    {Res,_} = lists:foldr(fun(X,{Acc,Tens}) -> {Acc+Fd(X)*Tens,Tens*Pow} end, {0,1}, ?BU:to_list(Code)),
    unicode:characters_to_binary([Res]).

% @private
decode_dec(Elt,DF,DL) ->
    <<_:DF/binary,Code:DL/binary,_/binary>> = Elt,
    unicode:characters_to_binary([?BU:to_int(Code)]).

% @private
decode_alias(Elt,_DF,_DL) ->
    unicode:characters_to_binary([alias_code(Elt)]).

% -------------------------------

% @private
alias_code(X) ->
    case X of
        <<"&quot;">> -> 34;
        <<"&amp;">> -> 38;
        <<"&apos;">> -> 39;
        <<"&lt;">> -> 60;
        <<"&gt;">> -> 62;
        <<"&nbsp;">> -> 160;
        <<"&iexcl;">> -> 161;
        <<"&cent;">> -> 162;
        <<"&pound;">> -> 163;
        <<"&curren;">> -> 164;
        <<"&yen;">> -> 165;
        <<"&brvbar;">> -> 166;
        <<"&sect;">> -> 167;
        <<"&uml;">> -> 168;
        <<"&copy;">> -> 169;
        <<"&ordf;">> -> 170;
        <<"&laquo;">> -> 171;
        <<"&not;">> -> 172;
        <<"&shy;">> -> 173;
        <<"&reg;">> -> 174;
        <<"&macr;">> -> 175;
        <<"&deg;">> -> 176;
        <<"&plusmn;">> -> 177;
        <<"&sup2;">> -> 178;
        <<"&sup3;">> -> 179;
        <<"&acute;">> -> 180;
        <<"&micro;">> -> 181;
        <<"&para;">> -> 182;
        <<"&middot;">> -> 183;
        <<"&cedil;">> -> 184;
        <<"&sup1;">> -> 185;
        <<"&ordm;">> -> 186;
        <<"&raquo;">> -> 187;
        <<"&frac14;">> -> 188;
        <<"&frac12;">> -> 189;
        <<"&frac34;">> -> 190;
        <<"&iquest;">> -> 191;
        <<"&Agrave;">> -> 192;
        <<"&Aacute;">> -> 193;
        <<"&Acirc;">> -> 194;
        <<"&Atilde;">> -> 195;
        <<"&Auml;">> -> 196;
        <<"&Aring;">> -> 197;
        <<"&AElig;">> -> 198;
        <<"&Ccedil;">> -> 199;
        <<"&Egrave;">> -> 200;
        <<"&Eacute;">> -> 201;
        <<"&Ecirc;">> -> 202;
        <<"&Euml;">> -> 203;
        <<"&Igrave;">> -> 204;
        <<"&Iacute;">> -> 205;
        <<"&Icirc;">> -> 206;
        <<"&Iuml;">> -> 207;
        <<"&ETH;">> -> 208;
        <<"&Ntilde;">> -> 209;
        <<"&Ograve;">> -> 210;
        <<"&Oacute;">> -> 211;
        <<"&Ocirc;">> -> 212;
        <<"&Otilde;">> -> 213;
        <<"&Ouml;">> -> 214;
        <<"&times;">> -> 215;
        <<"&Oslash;">> -> 216;
        <<"&Ugrave;">> -> 217;
        <<"&Uacute;">> -> 218;
        <<"&Ucirc;">> -> 219;
        <<"&Uuml;">> -> 220;
        <<"&Yacute;">> -> 221;
        <<"&THORN;">> -> 222;
        <<"&szlig;">> -> 223;
        <<"&agrave;">> -> 224;
        <<"&aacute;">> -> 225;
        <<"&acirc;">> -> 226;
        <<"&atilde;">> -> 227;
        <<"&auml;">> -> 228;
        <<"&aring;">> -> 229;
        <<"&aelig;">> -> 230;
        <<"&ccedil;">> -> 231;
        <<"&egrave;">> -> 232;
        <<"&eacute;">> -> 233;
        <<"&ecirc;">> -> 234;
        <<"&euml;">> -> 235;
        <<"&igrave;">> -> 236;
        <<"&iacute;">> -> 237;
        <<"&icirc;">> -> 238;
        <<"&iuml;">> -> 239;
        <<"&eth;">> -> 240;
        <<"&ntilde;">> -> 241;
        <<"&ograve;">> -> 242;
        <<"&oacute;">> -> 243;
        <<"&ocirc;">> -> 244;
        <<"&otilde;">> -> 245;
        <<"&ouml;">> -> 246;
        <<"&divide;">> -> 247;
        <<"&oslash;">> -> 248;
        <<"&ugrave;">> -> 249;
        <<"&uacute;">> -> 250;
        <<"&ucirc;">> -> 251;
        <<"&uuml;">> -> 252;
        <<"&yacute;">> -> 253;
        <<"&thorn;">> -> 254;
        <<"&yuml;">> -> 255;
        <<"&OElig;">> -> 338;
        <<"&oelig;">> -> 339;
        <<"&Scaron;">> -> 352;
        <<"&scaron;">> -> 353;
        <<"&Yuml;">> -> 376;
        <<"&fnof;">> -> 402;
        <<"&circ;">> -> 710;
        <<"&tilde;">> -> 732;
        <<"&Alpha;">> -> 913;
        <<"&Beta;">> -> 914;
        <<"&Gamma;">> -> 915;
        <<"&Delta;">> -> 916;
        <<"&Epsilon;">> -> 917;
        <<"&Zeta;">> -> 918;
        <<"&Eta;">> -> 919;
        <<"&Theta;">> -> 920;
        <<"&Iota;">> -> 921;
        <<"&Kappa;">> -> 922;
        <<"&Lambda;">> -> 923;
        <<"&Mu;">> -> 924;
        <<"&Nu;">> -> 925;
        <<"&Xi;">> -> 926;
        <<"&Omicron;">> -> 927;
        <<"&Pi;">> -> 928;
        <<"&Rho;">> -> 929;
        <<"&Sigma;">> -> 931;
        <<"&Tau;">> -> 932;
        <<"&Upsilon;">> -> 933;
        <<"&Phi;">> -> 934;
        <<"&Chi;">> -> 935;
        <<"&Psi;">> -> 936;
        <<"&Omega;">> -> 937;
        <<"&alpha;">> -> 945;
        <<"&beta;">> -> 946;
        <<"&gamma;">> -> 947;
        <<"&delta;">> -> 948;
        <<"&epsilon;">> -> 949;
        <<"&zeta;">> -> 950;
        <<"&eta;">> -> 951;
        <<"&theta;">> -> 952;
        <<"&iota;">> -> 953;
        <<"&kappa;">> -> 954;
        <<"&lambda;">> -> 955;
        <<"&mu;">> -> 956;
        <<"&nu;">> -> 957;
        <<"&xi;">> -> 958;
        <<"&omicron;">> -> 959;
        <<"&pi;">> -> 960;
        <<"&rho;">> -> 961;
        <<"&sigmaf;">> -> 962;
        <<"&sigma;">> -> 963;
        <<"&tau;">> -> 964;
        <<"&upsilon;">> -> 965;
        <<"&phi;">> -> 966;
        <<"&chi;">> -> 967;
        <<"&psi;">> -> 968;
        <<"&omega;">> -> 969;
        <<"&thetasym;">> -> 977;
        <<"&upsih;">> -> 978;
        <<"&piv;">> -> 982;
        <<"&ensp;">> -> 8194;
        <<"&emsp;">> -> 8195;
        <<"&thinsp;">> -> 8201;
        <<"&zwnj;">> -> 8204;
        <<"&zwj;">> -> 8205;
        <<"&lrm;">> -> 8206;
        <<"&rlm;">> -> 8207;
        <<"&ndash;">> -> 8211;
        <<"&mdash;">> -> 8212;
        <<"&lsquo;">> -> 8216;
        <<"&rsquo;">> -> 8217;
        <<"&sbquo;">> -> 8218;
        <<"&ldquo;">> -> 8220;
        <<"&rdquo;">> -> 8221;
        <<"&bdquo;">> -> 8222;
        <<"&dagger;">> -> 8224;
        <<"&Dagger;">> -> 8225;
        <<"&bull;">> -> 8226;
        <<"&hellip;">> -> 8230;
        <<"&permil;">> -> 8240;
        <<"&prime;">> -> 8242;
        <<"&Prime;">> -> 8243;
        <<"&lsaquo;">> -> 8249;
        <<"&rsaquo;">> -> 8250;
        <<"&oline;">> -> 8254;
        <<"&frasl;">> -> 8260;
        <<"&euro;">> -> 8364;
        <<"&weierp;">> -> 8472;
        <<"&image;">> -> 8465;
        <<"&real;">> -> 8476;
        <<"&trade;">> -> 8482;
        <<"&alefsym;">> -> 8501;
        <<"&larr;">> -> 8592;
        <<"&uarr;">> -> 8593;
        <<"&rarr;">> -> 8594;
        <<"&darr;">> -> 8595;
        <<"&harr;">> -> 8596;
        <<"&crarr;">> -> 8629;
        <<"&lArr;">> -> 8656;
        <<"&uArr;">> -> 8657;
        <<"&rArr;">> -> 8658;
        <<"&dArr;">> -> 8659;
        <<"&hArr;">> -> 8660;
        <<"&forall;">> -> 8704;
        <<"&part;">> -> 8706;
        <<"&exist;">> -> 8707;
        <<"&empty;">> -> 8709;
        <<"&nabla;">> -> 8711;
        <<"&isin;">> -> 8712;
        <<"&notin;">> -> 8713;
        <<"&ni;">> -> 8715;
        <<"&prod;">> -> 8719;
        <<"&sum;">> -> 8721;
        <<"&minus;">> -> 8722;
        <<"&lowast;">> -> 8727;
        <<"&radic;">> -> 8730;
        <<"&prop;">> -> 8733;
        <<"&infin;">> -> 8734;
        <<"&ang;">> -> 8736;
        <<"&and;">> -> 8743;
        <<"&or;">> -> 8744;
        <<"&cap;">> -> 8745;
        <<"&cup;">> -> 8746;
        <<"&int;">> -> 8747;
        <<"&there4;">> -> 8756;
        <<"&sim;">> -> 8764;
        <<"&cong;">> -> 8773;
        <<"&asymp;">> -> 8776;
        <<"&ne;">> -> 8800;
        <<"&equiv;">> -> 8801;
        <<"&le;">> -> 8804;
        <<"&ge;">> -> 8805;
        <<"&sub;">> -> 8834;
        <<"&sup;">> -> 8835;
        <<"&nsub;">> -> 8836;
        <<"&sube;">> -> 8838;
        <<"&supe;">> -> 8839;
        <<"&oplus;">> -> 8853;
        <<"&otimes;">> -> 8855;
        <<"&perp;">> -> 8869;
        <<"&sdot;">> -> 8901;
        <<"&lceil;">> -> 8968;
        <<"&rceil;">> -> 8969;
        <<"&lfloor;">> -> 8970;
        <<"&rfloor;">> -> 8971;
        <<"&lang;">> -> 9001;
        <<"&rang;">> -> 9002;
        <<"&loz;">> -> 9674;
        <<"&spades;">> -> 9824;
        <<"&clubs;">> -> 9827;
        <<"&hearts;">> -> 9829;
        <<"&diams;">> -> 9830;
        _ -> 32
    end.

code_alias(X) ->
    case X of
        34 -> <<"&quot;">>;
        38 -> <<"&amp;">>;
        39 -> <<"&apos;">>;
        60 -> <<"&lt;">>;
        62 -> <<"&gt;">>;
        160 -> <<"&nbsp;">>;
        161 -> <<"&iexcl;">>;
        162 -> <<"&cent;">>;
        163 -> <<"&pound;">>;
        164 -> <<"&curren;">>;
        165 -> <<"&yen;">>;
        166 -> <<"&brvbar;">>;
        167 -> <<"&sect;">>;
        168 -> <<"&uml;">>;
        169 -> <<"&copy;">>;
        170 -> <<"&ordf;">>;
        171 -> <<"&laquo;">>;
        172 -> <<"&not;">>;
        173 -> <<"&shy;">>;
        174 -> <<"&reg;">>;
        175 -> <<"&macr;">>;
        176 -> <<"&deg;">>;
        177 -> <<"&plusmn;">>;
        178 -> <<"&sup2;">>;
        179 -> <<"&sup3;">>;
        180 -> <<"&acute;">>;
        181 -> <<"&micro;">>;
        182 -> <<"&para;">>;
        183 -> <<"&middot;">>;
        184 -> <<"&cedil;">>;
        185 -> <<"&sup1;">>;
        186 -> <<"&ordm;">>;
        187 -> <<"&raquo;">>;
        188 -> <<"&frac14;">>;
        189 -> <<"&frac12;">>;
        190 -> <<"&frac34;">>;
        191 -> <<"&iquest;">>;
        192 -> <<"&Agrave;">>;
        193 -> <<"&Aacute;">>;
        194 -> <<"&Acirc;">>;
        195 -> <<"&Atilde;">>;
        196 -> <<"&Auml;">>;
        197 -> <<"&Aring;">>;
        198 -> <<"&AElig;">>;
        199 -> <<"&Ccedil;">>;
        200 -> <<"&Egrave;">>;
        201 -> <<"&Eacute;">>;
        202 -> <<"&Ecirc;">>;
        203 -> <<"&Euml;">>;
        204 -> <<"&Igrave;">>;
        205 -> <<"&Iacute;">>;
        206 -> <<"&Icirc;">>;
        207 -> <<"&Iuml;">>;
        208 -> <<"&ETH;">>;
        209 -> <<"&Ntilde;">>;
        210 -> <<"&Ograve;">>;
        211 -> <<"&Oacute;">>;
        212 -> <<"&Ocirc;">>;
        213 -> <<"&Otilde;">>;
        214 -> <<"&Ouml;">>;
        215 -> <<"&times;">>;
        216 -> <<"&Oslash;">>;
        217 -> <<"&Ugrave;">>;
        218 -> <<"&Uacute;">>;
        219 -> <<"&Ucirc;">>;
        220 -> <<"&Uuml;">>;
        221 -> <<"&Yacute;">>;
        222 -> <<"&THORN;">>;
        223 -> <<"&szlig;">>;
        224 -> <<"&agrave;">>;
        225 -> <<"&aacute;">>;
        226 -> <<"&acirc;">>;
        227 -> <<"&atilde;">>;
        228 -> <<"&auml;">>;
        229 -> <<"&aring;">>;
        230 -> <<"&aelig;">>;
        231 -> <<"&ccedil;">>;
        232 -> <<"&egrave;">>;
        233 -> <<"&eacute;">>;
        234 -> <<"&ecirc;">>;
        235 -> <<"&euml;">>;
        236 -> <<"&igrave;">>;
        237 -> <<"&iacute;">>;
        238 -> <<"&icirc;">>;
        239 -> <<"&iuml;">>;
        240 -> <<"&eth;">>;
        241 -> <<"&ntilde;">>;
        242 -> <<"&ograve;">>;
        243 -> <<"&oacute;">>;
        244 -> <<"&ocirc;">>;
        245 -> <<"&otilde;">>;
        246 -> <<"&ouml;">>;
        247 -> <<"&divide;">>;
        248 -> <<"&oslash;">>;
        249 -> <<"&ugrave;">>;
        250 -> <<"&uacute;">>;
        251 -> <<"&ucirc;">>;
        252 -> <<"&uuml;">>;
        253 -> <<"&yacute;">>;
        254 -> <<"&thorn;">>;
        255 -> <<"&yuml;">>;
        338 -> <<"&OElig;">>;
        339 -> <<"&oelig;">>;
        352 -> <<"&Scaron;">>;
        353 -> <<"&scaron;">>;
        376 -> <<"&Yuml;">>;
        402 -> <<"&fnof;">>;
        710 -> <<"&circ;">>;
        732 -> <<"&tilde;">>;
        913 -> <<"&Alpha;">>;
        914 -> <<"&Beta;">>;
        915 -> <<"&Gamma;">>;
        916 -> <<"&Delta;">>;
        917 -> <<"&Epsilon;">>;
        918 -> <<"&Zeta;">>;
        919 -> <<"&Eta;">>;
        920 -> <<"&Theta;">>;
        921 -> <<"&Iota;">>;
        922 -> <<"&Kappa;">>;
        923 -> <<"&Lambda;">>;
        924 -> <<"&Mu;">>;
        925 -> <<"&Nu;">>;
        926 -> <<"&Xi;">>;
        927 -> <<"&Omicron;">>;
        928 -> <<"&Pi;">>;
        929 -> <<"&Rho;">>;
        931 -> <<"&Sigma;">>;
        932 -> <<"&Tau;">>;
        933 -> <<"&Upsilon;">>;
        934 -> <<"&Phi;">>;
        935 -> <<"&Chi;">>;
        936 -> <<"&Psi;">>;
        937 -> <<"&Omega;">>;
        945 -> <<"&alpha;">>;
        946 -> <<"&beta;">>;
        947 -> <<"&gamma;">>;
        948 -> <<"&delta;">>;
        949 -> <<"&epsilon;">>;
        950 -> <<"&zeta;">>;
        951 -> <<"&eta;">>;
        952 -> <<"&theta;">>;
        953 -> <<"&iota;">>;
        954 -> <<"&kappa;">>;
        955 -> <<"&lambda;">>;
        956 -> <<"&mu;">>;
        957 -> <<"&nu;">>;
        958 -> <<"&xi;">>;
        959 -> <<"&omicron;">>;
        960 -> <<"&pi;">>;
        961 -> <<"&rho;">>;
        962 -> <<"&sigmaf;">>;
        963 -> <<"&sigma;">>;
        964 -> <<"&tau;">>;
        965 -> <<"&upsilon;">>;
        966 -> <<"&phi;">>;
        967 -> <<"&chi;">>;
        968 -> <<"&psi;">>;
        969 -> <<"&omega;">>;
        977 -> <<"&thetasym;">>;
        978 -> <<"&upsih;">>;
        982 -> <<"&piv;">>;
        8194 -> <<"&ensp;">>;
        8195 -> <<"&emsp;">>;
        8201 -> <<"&thinsp;">>;
        8204 -> <<"&zwnj;">>;
        8205 -> <<"&zwj;">>;
        8206 -> <<"&lrm;">>;
        8207 -> <<"&rlm;">>;
        8211 -> <<"&ndash;">>;
        8212 -> <<"&mdash;">>;
        8216 -> <<"&lsquo;">>;
        8217 -> <<"&rsquo;">>;
        8218 -> <<"&sbquo;">>;
        8220 -> <<"&ldquo;">>;
        8221 -> <<"&rdquo;">>;
        8222 -> <<"&bdquo;">>;
        8224 -> <<"&dagger;">>;
        8225 -> <<"&Dagger;">>;
        8226 -> <<"&bull;">>;
        8230 -> <<"&hellip;">>;
        8240 -> <<"&permil;">>;
        8242 -> <<"&prime;">>;
        8243 -> <<"&Prime;">>;
        8249 -> <<"&lsaquo;">>;
        8250 -> <<"&rsaquo;">>;
        8254 -> <<"&oline;">>;
        8260 -> <<"&frasl;">>;
        8364 -> <<"&euro;">>;
        8472 -> <<"&weierp;">>;
        8465 -> <<"&image;">>;
        8476 -> <<"&real;">>;
        8482 -> <<"&trade;">>;
        8501 -> <<"&alefsym;">>;
        8592 -> <<"&larr;">>;
        8593 -> <<"&uarr;">>;
        8594 -> <<"&rarr;">>;
        8595 -> <<"&darr;">>;
        8596 -> <<"&harr;">>;
        8629 -> <<"&crarr;">>;
        8656 -> <<"&lArr;">>;
        8657 -> <<"&uArr;">>;
        8658 -> <<"&rArr;">>;
        8659 -> <<"&dArr;">>;
        8660 -> <<"&hArr;">>;
        8704 -> <<"&forall;">>;
        8706 -> <<"&part;">>;
        8707 -> <<"&exist;">>;
        8709 -> <<"&empty;">>;
        8711 -> <<"&nabla;">>;
        8712 -> <<"&isin;">>;
        8713 -> <<"&notin;">>;
        8715 -> <<"&ni;">>;
        8719 -> <<"&prod;">>;
        8721 -> <<"&sum;">>;
        8722 -> <<"&minus;">>;
        8727 -> <<"&lowast;">>;
        8730 -> <<"&radic;">>;
        8733 -> <<"&prop;">>;
        8734 -> <<"&infin;">>;
        8736 -> <<"&ang;">>;
        8743 -> <<"&and;">>;
        8744 -> <<"&or;">>;
        8745 -> <<"&cap;">>;
        8746 -> <<"&cup;">>;
        8747 -> <<"&int;">>;
        8756 -> <<"&there4;">>;
        8764 -> <<"&sim;">>;
        8773 -> <<"&cong;">>;
        8776 -> <<"&asymp;">>;
        8800 -> <<"&ne;">>;
        8801 -> <<"&equiv;">>;
        8804 -> <<"&le;">>;
        8805 -> <<"&ge;">>;
        8834 -> <<"&sub;">>;
        8835 -> <<"&sup;">>;
        8836 -> <<"&nsub;">>;
        8838 -> <<"&sube;">>;
        8839 -> <<"&supe;">>;
        8853 -> <<"&oplus;">>;
        8855 -> <<"&otimes;">>;
        8869 -> <<"&perp;">>;
        8901 -> <<"&sdot;">>;
        8968 -> <<"&lceil;">>;
        8969 -> <<"&rceil;">>;
        8970 -> <<"&lfloor;">>;
        8971 -> <<"&rfloor;">>;
        9001 -> <<"&lang;">>;
        9002 -> <<"&rang;">>;
        9674 -> <<"&loz;">>;
        9824 -> <<"&spades;">>;
        9827 -> <<"&clubs;">>;
        9829 -> <<"&hearts;">>;
        9830 -> <<"&diams;">>;
        _ -> X
    end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

f_test_() ->
    {"HTML entity test",
     [% decode bin
      ?_assertEqual(<<>>, decode(<<>>)),
      ?_assertEqual(<<" ">>, decode(<<" ">>)),
      ?_assertEqual(<<"R">>, decode(<<"R">>)),
      ?_assertEqual(<<"&">>, decode(<<"&amp;">>)),
      ?_assertEqual(<<"и"/utf8>>, decode(<<"&#x438;">>)),
      ?_assertEqual(<<"к"/utf8>>, decode(<<"&#x43a;">>)),
      ?_assertEqual(<<"к"/utf8>>, decode(<<"&#x43A;">>)),
      ?_assertEqual(<<"И"/utf8>>, decode(<<"&#1048;">>)),
      ?_assertEqual(<<"as&иzx<qwИty"/utf8>>, decode(<<"as&amp;&#x438;zx&lt;qw&#1048;ty">>)),
      ?_assertEqual(<<"as&кzкx<qwИty&;*"/utf8>>, decode(<<"as&amp;&#x43a;z&#x43A;x&lt;qw&#1048;ty&;*">>)),
      % encode bin
      ?_assertEqual(<<>>, encode(<<>>)),
      ?_assertEqual(<<" ">>, encode(<<" ">>)),
      ?_assertEqual(<<"R">>, encode(<<"R">>)),
      ?_assertEqual(<<"&amp;">>, encode(<<"&">>)),
      ?_assertEqual(<<"&#x438;">>, encode(<<"и"/utf8>>)),
      ?_assertEqual(<<"&#x43A;">>, encode(<<"к"/utf8>>)),
      ?_assertEqual(<<"2&#x444;&#x44B;&#x432;&#x430;&#x444;.asdf,&amp;&gt;">>, encode(<<"2фываф.asdf,&>"/utf8>>)),
      %
      ?_assertEqual("&и", decode("&amp;&#x438;")),
      ?_assertEqual("&amp;&#x43A;", encode("&к"))
      ]}.

-endif.
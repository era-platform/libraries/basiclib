%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 09.08.2019
%%% @doc Implements algorithm of selecting items from any collection.
%%%      Uses filter, order, mask, flatting, offset and limit, counting, formatting.
%%%      Collection could be list, ets, fold_function or dict. Contains entities in maps or proplist forms. Keys could be atoms or binaries.
%%%      Algorithm:
%%%         1. Filtering
%%%         2. If countonly then return
%%%         3. Sorting
%%%         4. Offset
%%%         5. Limit
%%%         6. Formatting: take masking fields + flat composite fields + format to map|list

-module(basiclib_collection_select).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([select/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ModFilterFun, basiclib_filter_fun).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------------------------
%% Return selected elements (filtering, ordering, masking, flatting, mapping, offset+limit)
%% Or only count by filter
%% Collection could be list, ets, fold_function or dict. Contains entities in maps or proplist forms. Keys could be atoms or binaries.
%% Returns {ok,Count::integer} or {ok,[Item::map()]}, Keys remain atoms or binaries except flat=true on map values (always become binaries with dot).
%% OPTS: filter, order, offset, limit, mask, flat, format, composite_fields, fun_map_value_filter, fun_map_item.
%%   <<"filter">>:
%%          [{a1,10},{a2,<<"10">>},{a2,<<"~-15;-10~-8.8;-2~3;100~">>}]
%%          [{b1,true},{b2,1},{b3,<<"TRuE">>}]
%%          [{c1,<<"abc">>},{c2,<<"a%bc%">>},{c3,<<"abc|def|ghi">>},{c4,<<"/reg/abc.*">>},{c5,<<"/reg/i/abc.*},{c5,<<"/reg//http://.*}]
%%          #{a => 1, b=><<"a%bc%">>}
%%          <<"{\"a\":\"a%bc%\"}">>
%%          <<"{\"opts.a\":\"a%bc%\"}">>
%%          <<"{\"opts\":{\"a\":\"a%bc%\"}}">>
%%          [<<"&&">>,[<<"like">>,[<<"property">>,<<"a">>],<<"*ABC*">>],[<<">=">>,[<<"property">>,<<"b">>],10]]
%%   <<"order">>:
%%          [a,b]
%%          [a,<<"b">>]
%%          [a,{<<"b">>,<<"desc">>}]
%%          [{a,<<"asc">>},{<<"b">>,<<"desc">>}]
%%          <<"a,b">>
%%          #{a => <<"asc">>, <<"b">> => <<"desc">>}
%%          <<"{\"a\":\"asc\",\"b\":\"desc\"}">>
%%          <<"{\"opts.a\":\"asc\",\"opts.b\":\"desc\"}">>
%%   <<"mask">>:
%%          <<"id,name,ext.lwt,ext.ct">>
%%          <<"id,name,ext">>
%%          [<<"id">>,<<"name">>,<<"ext.lwt">>]
%%          [id,name,<<"ext.lwt">>,opts,ext]
%%   <<"flat">>:
%%          true | false | 1 | 0 | <<"TrUe">> | <<"falSe">> | ...
%%   <<"countonly">>:
%%          true | false | 1 | 0 | <<"TrUe">> | <<"falSe">> | ...
%%   <<"offset">>:
%%          <<"10">> | 10 | undefined
%%   <<"limit">>:
%%          <<"10">> | 10 | undefined
%%   'composite_fields':
%%          [opts,ext]
%%   'fun_map_value_filter':
%%          function/1 : fun({K,V}) -> {K,V1} end
%%   'fun_map_item':
%%          function/1 : fun(Item) -> Item1 end
%%   'format':
%%          'map' | 'list'
%% ------------------------------------------------------------
-spec select(Collection:: list() | reference() | atom() | function(), Opts::list()) -> {ok,[Item::map() | proplist()]} | {ok,Count::integer()}. % also dict(), ets()
%% ------------------------------------------------------------
select(Collection, Opts) ->
    CountOnly = ?BU:get_by_key(<<"countonly">>, Opts, undefined),
    Filter = ?BU:get_by_key(<<"filter">>, Opts, []),
    Order = ?BU:get_by_key(<<"order">>, Opts, []),
    Offset = ?BU:get_by_key(<<"offset">>, Opts, undefined),
    Limit = ?BU:get_by_key(<<"limit">>, Opts, undefined),
    Mask = ?BU:get_by_key(<<"mask">>, Opts, <<>>),
    % filter + countonly
    case ?BU:to_bool(CountOnly) of
        true ->
            find_items(Collection, Filter, [countonly|Opts]);
        _ ->
            % filtering
            {ok,Items0} = find_items(Collection, Filter, Opts), % [#{Key::binary() => Value::term()}]
            % sorting
            Items1 = sort_items(Items0, Order),
            % offset
            Items2 = case Offset of
                         undefined -> Items1;
                         _ ->
                             case try ?BU:to_int(Offset) catch error:_ -> false end of
                                 false -> Items1;
                                 NF when is_integer(NF), NF > length(Items1) -> [];
                                 NF when is_integer(NF) -> lists:nthtail(NF, Items1);
                                 _ -> Items1
                             end end,
            % limit
            Items3 = case Limit of
                         undefined -> Items2;
                         _ ->
                             case try ?BU:to_int(Limit) catch error:_ -> false end of
                                 false -> Items2;
                                 NC when is_integer(NC), NC < length(Items2) -> element(1,lists:split(NC,Items2));
                                 _ -> Items2
                             end end,
            % masking + flatting + formatting
            Items4 = format_items(Items3, Mask, Opts),
            {ok,Items4}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------------------------
%% @private
%% Find items by filter
%% Collection could be list, ets, fold_function or dict. Contains entities in maps or proplist forms. Keys could be atoms or binaries.
%% Returns {ok,Count::integer} or {ok,[Item::map()]}, Elements' keys remain atoms or binaries.
%% ------------------------------------------------------------
-spec find_items(Collection:: proplist() | list() | reference() | atom() | function(), FilterOpts::map() | list(), Opts::list()) -> {ok,[Item::map()]} | {ok,Count::integer()}. % also dict(), ets()
%% ------------------------------------------------------------

% when list
find_items(Collection, FilterOpts, Opts) when is_list(Collection) ->
    FunFold = fun(Fun,Acc0) -> lists:foldl(map_acc_fun2(Fun), Acc0, Collection) end,
    do_find_items(FunFold, FilterOpts, Opts);
% when ets
find_items(Collection, FilterOpts, Opts) when is_reference(Collection); is_atom(Collection) ->
    case ets:info(Collection,size) of
        undefined -> throw({error,{invalid_collection, ets}});
        _ ->
            FunFold = fun(Fun,Acc0) -> ets:foldl(map_acc_fun2(Fun), Acc0, Collection) end,
            do_find_items(FunFold, FilterOpts, Opts)
    end;
% when function/2 (FunAcc, Acc0)
find_items(Collection, FilterOpts, Opts) when is_function(Collection,2) ->
    FunFold = fun(Fun,Acc0) -> Collection(map_acc_fun2(Fun), Acc0) end,
    do_find_items(FunFold, FilterOpts, Opts);
% when dict
find_items(Collection, FilterOpts, Opts) ->
    case catch dict:size(Collection) of
        {'EXIT',_} -> throw({error,{invalid_collection,dict}});
        _ ->
            FunFold = fun(Fun,Acc0) -> dict:fold(map_acc_fun3(Fun), Acc0, Collection) end,
            do_find_items(FunFold, FilterOpts, Opts)
    end.

%% @private
map_acc_fun2(Fun) ->
    fun({_,Item},Acc) when is_map(Item) -> Fun(Item,Acc);
       ({_,Item},Acc) when is_list(Item) -> Fun(?BU:json_to_map(Item),Acc);
       (Item,Acc) when is_map(Item) -> Fun(Item,Acc);
       (Item,Acc) when is_list(Item) -> Fun(?BU:json_to_map(Item),Acc)
    end.
map_acc_fun3(Fun) ->
    fun(_,Item,Acc) when is_map(Item) -> Fun(Item,Acc);
       (_,Item,Acc) when is_list(Item) -> Fun(?BU:json_to_map(Item),Acc)
    end.

%% @private
do_find_items(FunFold, FilterOpts, Opts) when is_map(FilterOpts) ->
    do_find_items(FunFold, maps:to_list(FilterOpts), Opts);
do_find_items(FunFold, FilterOpts, Opts) when is_binary(FilterOpts) ->
    FilterOpts1 = try jsx:decode(FilterOpts)
                  catch _:_ -> []
                  end,
    do_find_items(FunFold, FilterOpts1, Opts);
%% filter by expression
do_find_items(FunFold, [Op|_]=FilterOpts, Opts) when is_binary(Op) ->
    % make list by filter
    case ?ModFilterFun:build_filter_fun(FilterOpts) of
        Fcheck when is_function(Fcheck,1) ->
            Facc2 = fun(Item, Acc) when is_map(Item) ->
                        case Fcheck(Item) of
                            true when is_integer(Acc) -> Acc + 1;
                            true when is_list(Acc) -> [Item|Acc];
                            false -> Acc
                        end end,
            case lists:member(countonly,Opts) of
                true -> {ok,FunFold(Facc2,0)};
                false -> {ok,lists:reverse(FunFold(Facc2,[]))}
            end;
        {error,_} ->
            case lists:member(countonly,Opts) of
                true -> {ok,0};
                false -> {ok,[]}
            end
    end;
%% filter by masking functions
do_find_items(FunFold, FilterOpts, Opts) when is_list(FilterOpts), is_function(FunFold,2) ->
    FunMapValueFilter = ?BU:get_by_key('fun_map_value_filter', Opts, fun({K,V}) -> {K,V} end),
    % filter opts
    FilterOpts0 = ?BU:filter_props_list(FilterOpts),
    % make mapped filter by dotted fields
    {MapFlats,FilterOpts1} = lists:foldl(fun({K,V},{AccM,AccF}=Acc) ->
                                                case binary:split(?BU:to_binary(K),<<".">>) of
                                                    [K1,K2] -> {AccM#{K1 => (maps:get(K1,AccM,#{}))#{K2 => V}}, lists:delete({K,V},AccF)};
                                                    [_] -> Acc
                                                end;
                                            (Elt,{AccM,AccF}) -> {AccM,lists:delete(Elt,AccF)}
                                         end, {#{},FilterOpts0}, FilterOpts0),
    FilterOpts2 = FilterOpts1 ++ maps:to_list(MapFlats),
    % make compare functions
    FilterOpts3 = make_compare_functions(FilterOpts2),
    % make list by filter
    Fcheck = fun(Item) ->
                    F = fun({K,FCmp},true) when is_function(FCmp,1) ->
                                case get_by_key(K, Item) of
                                    undefined -> false; % means not match
                                    Value ->
                                        {_,Value1} = FunMapValueFilter({K,Value}),
                                        FCmp(Value1)
                                end;
                           (_,_) -> false
                        end,
                    lists:foldl(F, true, FilterOpts3)
             end,
    Facc2 = fun(Item, Acc) when is_map(Item) ->
                    case Fcheck(Item) of
                        true when is_integer(Acc) -> Acc + 1;
                        true when is_list(Acc) -> [Item|Acc];
                        false -> Acc
                    end end,
    case lists:member(countonly,Opts) of
        true -> {ok,FunFold(Facc2,0)};
        false -> {ok,lists:reverse(FunFold(Facc2,[]))}
    end.

%% ------------------------------------------------------------
%% @private
%% Sort items
%% ------------------------------------------------------------
sort_items(Items, SortOpts)
  when is_list(Items) andalso (is_list(SortOpts) orelse is_map(SortOpts) orelse is_binary(SortOpts)) ->
    SortOpts0 = case SortOpts of
                    _ when is_map(SortOpts) -> maps:to_list(SortOpts);
                    _ when is_binary(SortOpts) ->
                        try jsx:decode(SortOpts)
                        catch _:_ -> lists:filter(fun(<<" ">>) -> false; (_) -> true end, binary:split(SortOpts,[<<",">>, <<" ">>], [global]))
                        end;
                    _ when is_list(SortOpts) -> SortOpts
                end,
    case SortOpts0 of
        [] -> Items;
        _ ->
            Fk = fun(K) when is_atom(K) -> K;
                    (K) when is_binary(K) ->
                        case binary:split(K,<<".">>) of % first level only
                            [K1] -> K1;
                            Keys -> Keys
                        end end,
            SL = lists:filtermap(fun([{K,<<"asc">>}]) when is_binary(K); is_atom(K) -> {true, {Fk(K), true}};
                                    ([{K,<<"desc">>}]) when is_binary(K); is_atom(K) -> {true, {Fk(K), false}};
                                    ({K,<<"asc">>}) when is_binary(K); is_atom(K) -> {true, {Fk(K), true}};
                                    ({K,<<"desc">>}) when is_binary(K); is_atom(K) -> {true, {Fk(K), false}};
                                    (K) when is_binary(K); is_atom(K) -> {true, {Fk(K), true}};
                                    (_) -> false
                                 end, SortOpts0),
            Fsort = fun(A,B) ->
                            FS1 = fun FS1(_,_,true) -> true;
                                      FS1(_,_,false) -> false;
                                      FS1({[K],V},ItemsX,undefined) when is_boolean(V) -> FS1({K,V}, ItemsX, undefined);
                                      FS1({[K|KRest],V},{AI,BI},undefined) when is_boolean(V) ->
                                            case {get_by_key(K,AI),get_by_key(K,BI)} of
                                                {undefined,undefined} -> undefined;
                                                {_,undefined} -> V;
                                                {undefined,_} -> not V;
                                                {T1,T2} when (is_list(T1) orelse is_map(T1)) andalso (is_list(T2) orelse is_map(T2)) -> FS1({KRest,V},{T1,T2},undefined);
                                                _ -> not V
                                             end;
                                      FS1({K,V},{AI,BI},undefined) when is_boolean(V) ->
                                             case {get_by_key(K,AI), get_by_key(K,BI)} of
                                                 {undefined,undefined} -> undefined;
                                                 {_,undefined} -> V;
                                                 {undefined,_} -> not V;
                                                 {T1,T2} when T1==T2 -> undefined;
                                                 {T1,T2} when T1=<T2 -> V;
                                                 _ -> not V
                                             end end,
                            case lists:foldl(fun({K,V},Acc) -> FS1({K,V},{A,B},Acc) end, undefined, SL) of
                                undefined -> true;
                                R -> R
                            end end,
            lists:sort(Fsort, Items)
    end.

%% ------------------------------------------------------------
%% @private
%% format items and flatten composite fields
%% Return masking fields only;
%% Maps and proplists values could be flatten to first level by subkeys (key.subkey)
%% Could return map or proplist as result.
%% ------------------------------------------------------------
format_items(Items, Mask, Opts) ->
    FunMapItem = ?BU:get_by_key('fun_map_item', Opts, fun(Item) -> Item end),
    CompositeFieldsOpt = ?BU:get_by_key('composite_fields', Opts, auto), % auto | [opts,ext]
    Flat = ?BU:to_bool(?BU:get_by_key(<<"flat">>, Opts, false)), % <<"true">>
    Format = ?BU:get_by_key('format', Opts, 'map'),
    SelectFields = case Mask of
                       _ when is_binary(Mask) -> binary:split(Mask, <<",">>, [global,trim_all]);
                       _ when is_list(Mask) -> lists:map(fun(Field) -> ?BU:to_binary(Field) end, Mask)
                   end,
    lists:map(fun(Item) -> map_item(Item, FunMapItem, SelectFields, CompositeFieldsOpt, Flat, Format) end, Items).
    
%% @private
map_item(ItemData, FunMapItem, SelectFields, CompositeFieldsOpt, Flat, Format) ->
    ItemData1 = FunMapItem(ItemData),
    ItemData2 = ?BU:filter_jsonable(ItemData1),
    CompositeFields = get_composite_fields(CompositeFieldsOpt,ItemData2),
    % flatten
    {ok,ItemData3,SelectFields1} = apply_flatten(Flat,ItemData2,SelectFields,CompositeFields),
    % take only selected fields
    ItemData4 = case SelectFields1 of
                    [] -> ItemData3;
                    _L when is_list(_L) ->
                        maps:filter(fun(K,_V) -> lists:member(?BU:to_binary(K),SelectFields1) end, ItemData3)
                end,
    case Format of
        'list' -> ?BU:map_to_json(ItemData4);
        'map' -> ItemData4;
        _ -> ItemData4
    end.

%% @private
get_composite_fields(CompositeFieldsOpt,ItemData) ->
    case CompositeFieldsOpt of
        CF when is_list(CF) -> CF;
        CF when CF==auto; true ->
            maps:fold(fun(K,V,Acc) when is_map(V) -> [K|Acc];
                         (K,V,Acc) when is_list(V) -> case lists:all(fun({K1,_}) when is_atom(K1);is_binary(K1) -> true; (_) -> false end, V) of true -> [K|Acc]; false -> Acc end;
                         (_,_,Acc) -> Acc
                      end, [], ItemData)
    end.

%% @private
apply_flatten(_Flat,ItemData,SelectFields,CompositeFields) when CompositeFields==[] ->
    {ok,ItemData,SelectFields};

%% Extend selected opts. "mask" vs "flat" on composite fields (mask=opts & flat=true)
apply_flatten(Flat,ItemData,SelectFields,CompositeFields) when Flat==true, SelectFields==[]->
    ItemData1 = ?BU:flatten_one(CompositeFields, ItemData),
    {ok,ItemData1,SelectFields};
apply_flatten(Flat,ItemData,SelectFields,CompositeFields) when Flat==true ->
    ItemData1 = ?BU:flatten_one(CompositeFields, ItemData),
    CompositeFieldsB = lists:map(fun(X) -> ?BU:to_binary(X) end, CompositeFields),
    X2 = ordsets:intersection(ordsets:from_list(SelectFields), ordsets:from_list(CompositeFieldsB)),
    SelectFields1 = maps:fold(fun(K,_,Acc) when is_binary(K) ->
                                      [K0|_] = binary:split(K,<<".">>,[global]),
                                      case ordsets:is_element(K0,X2) of
                                          true -> [K|Acc];
                                          false -> Acc
                                      end;
                                 (_,_,Acc) -> Acc
                              end, SelectFields, ItemData1),
    {ok, ItemData1, SelectFields1};

%% Filter ItemData by selected. "mask" vs "flat" on composite fields (mask=opts.a,opts.b & flat=false)
apply_flatten(Flat,ItemData,SelectFields,_CompositeFields) when Flat/=true, SelectFields==[] ->
    {ok,ItemData,SelectFields};
apply_flatten(Flat,ItemData,SelectFields,CompositeFields) when Flat/=true ->
    CompositeFieldsB = lists:map(fun(X) -> ?BU:to_binary(X) end, CompositeFields),
    FldMap = lists:foldl(fun(Field,Acc) ->
                                    case binary:split(Field,<<".">>,[]) of
                                        [_] -> Acc;
                                        [K1,K2] ->
                                            case lists:member(K1,CompositeFieldsB) of
                                                true -> Acc#{K1 => ordsets:add_element(K2,maps:get(K1,Acc,[]))};
                                                _ -> Acc
                                            end end end, #{}, SelectFields),
    FldMap1 = maps:filter(fun(Field,_SubFields) -> not lists:member(Field,SelectFields) end, FldMap),
    ItemData1 = maps:fold(fun(Field,SubFields,Acc) ->
                                FK = case maps:is_key(Field,Acc) of true -> Field; false ->
                                     case maps:is_key(B=?BU:to_binary(Field),Acc) of true -> B; false ->
                                     case maps:is_key(A=?BU:to_atom_safe(Field),Acc) of true -> A; false -> Field
                                     end end end,
                                case get_by_key(FK,ItemData) of
                                    Val when is_map(Val) ->
                                        ValX = maps:filter(fun(K,_) -> lists:member(?BU:to_binary(K),SubFields) end, Val),
                                        Acc#{FK => ValX};
                                    Val when is_list(Val) ->
                                        ValX = lists:filter(fun({K,_}) -> lists:member(?BU:to_binary(K),SubFields) end, Val),
                                        Acc#{FK => ValX};
                                    _ -> Acc
                                end end, ItemData, FldMap1),
    SelectFields1 = maps:fold(fun(K,_,Acc) -> [K|Acc] end, SelectFields, FldMap1),
    {ok,ItemData1,SelectFields1}.

%% @private
get_by_key(K,{_,Item}) when is_map(Item); is_list(Item) -> get_by_key(K,Item);
get_by_key(K,Item) when is_map(Item); is_list(Item) ->
    case ?BU:get_by_key(K,Item,undefined) of
        undefined when is_atom(K) -> ?BU:get_by_key(?BU:to_binary(K),Item,undefined);
        undefined when is_binary(K) ->
            case catch ?BU:get_by_key(?BU:to_atom(K),Item,undefined) of
                {'EXIT',_} -> undefined; % catch when not_existing_atom
                T -> T
            end;
        T -> T
    end.

%% ------------------------------------------------------------
%% Compare functions
%% ------------------------------------------------------------
make_compare_functions(FilterOpts) ->
    lists:map(fun({K,Filter}) -> {K,build_compare_fun(Filter,0)} end, FilterOpts).

%% @private
%% return function that would match field value by filter value
build_compare_fun(Filter,Level) ->
    FNum = build_compare_fun_num(Filter),
    FBin = build_compare_fun_binary(Filter),
    FBool = build_compare_fun_bool(Filter),
    FMap = build_compare_fun_map(Filter,Level),
    FList = build_compare_fun_list(Filter,Level),
    fun(Value) when is_number(Value) -> FNum(Value);
       (Value) when is_binary(Value) -> FBin(Value);
       (Value) when is_boolean(Value) -> FBool(Value);
       (Value) when is_map(Value) -> FMap(Value);
       (Value) when is_list(Value) -> FList(Value);
       (_) -> fun_not_match()
    end.

%% @private
%% value is number (means integer or float)
build_compare_fun_num(Filter) when is_number(Filter) -> fun(Value) -> Value==Filter end;
build_compare_fun_num(Filter) when is_binary(Filter) ->
    Ors = binary:split(Filter, <<";">>, [global]),
    Ffrom = fun(<<>>) -> -4294967296;
               (From) -> ?BU:to_float(From)
            end,
    Fto = fun(<<>>) -> 4294967295;
             (To) -> ?BU:to_float(To)
          end,
    Frep = fun(B) -> re:replace(B,<<"\s\t\n\r">>,<<>>,[{return,binary},global]) end,
    Ors1 = try lists:map(fun(A) ->
                                 case binary:split(A, <<"~">>, [global]) of
                                     [X] -> Ffrom(Frep(X));
                                     [X,Y] -> {Ffrom(Frep(X)), Fto(Frep(Y))}
                                 end end, Ors)
           catch error:_ -> []
           end,
    fun(Value) -> lists:any(fun(X) when X==Value -> true;
                               ({X,Y}) when X=<Value, Y>=Value -> true;
                               (_) -> false
                            end, Ors1) end;
build_compare_fun_num(_) -> fun_not_match().

%% @private
%% value is binary (means string)
build_compare_fun_binary(Filter) when not is_binary(Filter) -> fun_not_match();
build_compare_fun_binary(<<"/reg/",Rest/bitstring>>) ->
    {Pattern,ReOpts} = case binary:split(Rest, [<<"/">>], []) of
                           [Pat] -> {Pat,[]};
                           [Opts,Pat] ->
                               ReOptsX = lists:foldl(fun($i, Acc) -> [caseless|Acc];
                                                        ($u, Acc) -> [unicode|Acc];
                                                        ($m, Acc) -> [multiline|Acc];
                                                        (_, Acc) -> Acc
                                                     end, [], ?BU:to_list(Opts)),
                               {Pat,ReOptsX}
                       end,
    case re:compile(Pattern,ReOpts) of
        {error,_} -> fun(_) -> false end; % means not match
        {ok,Re} ->
            fun(Value) ->
                case re:run(Value,Re) of
                    {match,_} -> true;
                    nomatch -> false
                end end end;
build_compare_fun_binary(Filter) ->
    F = fun($[,Acc) -> Acc ++ "[\\[]";
           ($],Acc) -> Acc ++ "[\\]]";
           ($\\,Acc) -> Acc ++ "[\\\\]";
           ($%,Acc) -> Acc ++ ".*";
           ($|,Acc) -> Acc ++ "|";
           (A,Acc) -> Acc ++ "[" ++ [A] ++"]"
        end,
    Mask = lists:foldl(F, [], ?BU:to_list(Filter)),
    case catch re:compile("^" ++ Mask ++ "$") of
        {'EXIT',_} -> fun(_) -> false end; % true
        {ok,Re} ->
             fun Fch(Value) when is_binary(Value) -> Fch(?BU:to_list(Value));
                 Fch(Value) when is_list(Value) ->
                       case catch re:run(Value,Re) of
                           {'EXIT',_} -> false; % means not match
                           nomatch -> false;
                           {match,_} -> true
                       end;
                 Fch(_) -> false
             end end.

%% @private
%% value is bool
build_compare_fun_bool(Filter) when is_boolean(Filter) -> fun(Value) -> Value==Filter end;
build_compare_fun_bool(Filter) when is_integer(Filter) -> FilterBool = ?BU:to_bool(Filter), fun(Value) -> Value==FilterBool end;
build_compare_fun_bool(Filter) when is_binary(Filter) -> FilterBool = ?BU:to_bool(Filter), fun(Value) -> Value==FilterBool end;
build_compare_fun_bool(_) -> fun_not_match().

%% @private
%% value is map, mean opts field
build_compare_fun_map(Filter,Level) when is_map(Filter) ->
    Funs = maps:map(fun(_K,SubFilter) -> build_compare_fun(SubFilter,Level+1) end, Filter),
    fun(Value) -> maps:fold(fun(_,_,false) -> false;
                               (K,FunFilter,true) ->
                                    case get_by_key(K,Value) of
                                        undefined -> false; % means not match
                                        SubValue -> FunFilter(SubValue)
                                    end end, true, Funs) end;
build_compare_fun_map(Filter,Level) when is_list(Filter) ->
    case ?BU:is_props_list(Filter) of
        false -> fun_not_match();
        true -> build_compare_fun_map(maps:from_list(Filter),Level)
    end;
build_compare_fun_map(_,_) -> fun_not_match().

%% @private
%% value is list, mean opts field or array
build_compare_fun_list(Filter,Level) when is_map(Filter) ->
    Funs = maps:map(fun(_K,SubFilter) -> build_compare_fun(SubFilter,Level+1) end, Filter),
    fun(Value) ->
        case ?BU:is_props_list(Value) of
            false -> fun_not_match();
            true -> maps:fold(fun(_,_,false) -> false;
                                 (K,FunFilter,true) ->
                                      case get_by_key(K,Value) of
                                          undefined -> false; % means not match
                                          SubValue -> FunFilter(SubValue)
                                      end end, true, Funs)
        end end;
build_compare_fun_list(Filter,Level) when is_list(Filter) ->
    case ?BU:is_props_list(Filter) of
        true -> build_compare_fun_list(maps:from_list(Filter),Level);
        false -> fun(Value) -> lists:all(fun(FV) -> lists:member(FV,Value) end, Filter) end
    end;
build_compare_fun_list(_,_) -> fun_not_match().

%% @private
fun_not_match() -> fun(_) -> false end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

find_items_test_() ->
    Filter0l = [],
    Filter0m = #{},
    Opts0 = [],
    List1a = [#{a=>1},#{a=>2,b=><<"abc">>}],
    List2a = [[{a,1}],[{a,2},{b,<<"abc">>}]],
    List1b = [#{a=>1,b=><<"def">>},#{a=>2,b=><<"abc">>}],
    List2b = [#{<<"a">>=>1,<<"b">>=><<"def">>},#{<<"a">>=>2,<<"b">>=><<"abc">>}],
    List1c = [{aaa,#{a=>1,b=><<"def">>}},{bbb,#{a=>2,b=><<"abc">>}}],
    List2c = [{aaa,[{<<"a">>,1},{<<"b">>,<<"def">>}]},{bbb,[{<<"a">>,2},{<<"b">>,<<"abc">>}]}],
    E1 = ets:new(e1,[public,set]), ets:insert(E1, {aaa,#{a=>1,b=><<"def">>}}), ets:insert(E1, {bbb,#{a=>2,b=><<"abc">>}}),
    E2 = ets:new(e2,[public,set]), ets:insert(E2, {aaa,[{a,1},{b,<<"def">>}]}), ets:insert(E2, {bbb,[{a,2},{b,<<"abc">>}]}),
    Dict1 = dict:store(aaa,#{a=>1,b=><<"def">>}, dict:store(bbb,[{a,2},{b,<<"abc">>}], dict:new())),
    Dict2 = dict:store(aaa,#{a=>1,b=><<"def">>}, dict:store(bbb,[{a,2},{b,<<"abc">>}], dict:new())),
    FoldFun = fun(Fun,Acc0) -> lists:foldl(Fun, Acc0, List1b) end,
    {"find items", [
        % simple, list
        ?_assertEqual({ok,[#{a=>1}]}, find_items([#{a => 1}], Filter0l, Opts0)),
        ?_assertEqual({ok,[#{a=>1},#{a=>2,b=><<"abc">>}]}, find_items(List1a, Filter0m, Opts0)),
        ?_assertEqual({ok,[#{a=>1},#{a=>2,b=><<"abc">>}]}, find_items(List1a, Filter0l, Opts0)),
        ?_assertEqual({ok,[#{a=>1},#{a=>2,b=><<"abc">>}]}, find_items(List2a, Filter0m, Opts0)),
        ?_assertEqual({ok,[#{a=>1},#{a=>2,b=><<"abc">>}]}, find_items(List2a, Filter0l, Opts0)),
        % list filters
        ?_assertEqual({ok,[]}, find_items(List1a, #{a=>3}, Opts0)),
        ?_assertEqual({ok,[]}, find_items(List2a, #{a=>3}, Opts0)),
        ?_assertEqual({ok,[]}, find_items(List1a, [{a,3}], Opts0)),
        ?_assertEqual({ok,[]}, find_items(List2a, [{a,3}], Opts0)),
        ?_assertEqual({ok,[#{a=>1}]}, find_items(List1a, #{a=>1}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(List1a, #{b=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(List2a, #{b=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(List1b, #{b=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{<<"a">>=>2,<<"b">>=><<"abc">>}]}, find_items(List2b, #{<<"b">>=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(List1c, #{b=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{<<"a">>=>2,<<"b">>=><<"abc">>}]}, find_items(List2c, #{<<"b">>=><<"abc">>}, Opts0)),
        % dict filters
        ?_assertEqual({ok,[]}, find_items(Dict2, #{a=>3}, Opts0)),
        ?_assertEqual({ok,[]}, find_items(Dict2, [{a,3}], Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(Dict1, #{a=>2}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(Dict1, [{a,2}], Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(Dict2, #{a=>2}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(Dict2, [{a,2}], Opts0)),
        % ets filters
        ?_assertEqual({error,{invalid_collection,ets}}, catch find_items(e1, [{a,3}], Opts0)),
        ?_assertEqual({ok,[]}, find_items(E1, [{a,3}], Opts0)),
        ?_assertEqual({ok,[]}, find_items(E1, #{a=>3}, Opts0)),
        ?_assertEqual({ok,[]}, find_items(E2, #{a=>3}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(E1, [{a,2}], Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(E1, #{a=>2}, Opts0)),
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(E2, [{a,2}], Opts0)),
        % fold function
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(FoldFun, [{a,2}], Opts0)),
        % countonly
        ?_assertEqual({ok,0}, find_items(E1, [{c,2}], [countonly])),
        ?_assertEqual({ok,1}, find_items(List1a, [{a,2}], [countonly])),
        ?_assertEqual({ok,0}, find_items([#{},#{},#{},#{}], [{a,2}], [countonly])),
        ?_assertEqual({ok,4}, find_items([#{},#{},#{},#{}], [], [countonly])),
        % filter formats
        ?_assertEqual({ok,[#{a=>2,b=><<"abc">>}]}, find_items(List1b, #{<<"b">>=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{<<"a">>=>2,<<"b">>=><<"abc">>}]}, find_items(List2b, #{b=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{<<"a">>=>2,<<"b">>=><<"abc">>}]}, find_items(List2b, #{b=><<"abc">>}, Opts0)),
        ?_assertEqual({ok,[#{<<"a">>=>2,<<"b">>=><<"abc">>}]}, find_items(List2b, [{b,<<"abc">>}], Opts0)),
        ?_assertEqual({ok,[#{<<"a">>=>2,<<"b">>=><<"abc">>}]}, find_items(List2b, <<"{\"b\":\"abc\"}">>, Opts0))
        ]}.

sort_items_test_() ->
    List0 = [#{a=>2,b=><<"def">>},
             #{a=>3,b=><<"ghi">>},
             #{a=>4,b=><<"jkl">>},
             #{a=>2,b=><<"mno">>},
             #{a=>1,b=><<"pqr">>},
             #{a=>1,b=><<"abc">>},
             #{a=>3}],
    ListA = [#{a=>1,b=><<"pqr">>},
             #{a=>1,b=><<"abc">>},
             #{a=>2,b=><<"def">>},
             #{a=>2,b=><<"mno">>},
             #{a=>3,b=><<"ghi">>},
             #{a=>3},
             #{a=>4,b=><<"jkl">>}],
    ListB = [#{a=>1,b=><<"abc">>},
             #{a=>1,b=><<"pqr">>},
             #{a=>2,b=><<"def">>},
             #{a=>2,b=><<"mno">>},
             #{a=>3,b=><<"ghi">>},
             #{a=>3},
             #{a=>4,b=><<"jkl">>}],
    ListC = [#{a=>1,b=><<"abc">>},
             #{a=>2,b=><<"def">>},
             #{a=>3,b=><<"ghi">>},
             #{a=>4,b=><<"jkl">>},
             #{a=>2,b=><<"mno">>},
             #{a=>1,b=><<"pqr">>},
             #{a=>3}],
    ListD = lists:reverse(ListC),
    %
    ListEa = [#{a=>1,<<"b">>=><<"abc">>},
              #{<<"a">>=>3,b=><<"ghi">>},
              #{a=>2,b=><<"def">>},
              #{a=>4,b=><<"jkl">>},
              #{a=>3},
              #{<<"a">>=>2,b=><<"mno">>},
              #{a=>1,<<"b">>=><<"pqr">>}],
    ListEb = [#{a=>1,<<"b">>=><<"abc">>},
              #{a=>2,b=><<"def">>},
              #{b=><<"ghi">>,<<"a">>=>3},
              #{a=>4,b=><<"jkl">>},
              #{b=><<"mno">>,<<"a">>=>2},
              #{a=>1,<<"b">>=><<"pqr">>},
              #{a=>3}],
    %
    ListFl = [#{a=>1,b=>#{x=>9,y=>#{z=><<"abc">>}}},
              #{a=>2,b=>#{x=>5,y=>#{z=><<"def">>}}},
              #{a=>3,b=>#{x=>8,y=>#{z=><<"xyz">>}}},
              #{a=>5},
              #{a=>4,b=>#{x=>6,y=>#{z=><<"ghi">>}}},
              #{a=>6,b=>#{x=>5}}],
    U = <<"zzzz">>,
    ListFla = lists:sort(fun(A,B) -> maps:get(a,A)=<maps:get(a,B) end, ListFl),
    ListFlb = lists:sort(fun(A,B) -> maps:get(b,A,U)=<maps:get(b,B,U) end, ListFl),
    ListFlx = lists:sort(fun(A,B) -> maps:get(x,maps:get(b,A,#{}),U)=<maps:get(x,maps:get(b,B,#{}),U) end, ListFl),
    {"sort items", [
        ?_assertEqual(List0, sort_items(List0, [])),
        ?_assertEqual(ListA, sort_items(List0, [a])),
        ?_assertEqual(ListB, sort_items(List0, [a,b])),
        ?_assertEqual(ListC, sort_items(List0, [b,a])),
        ?_assertEqual(ListC, sort_items(List0, [b])),
        ?_assertEqual(ListD, sort_items(List0, [{b,<<"desc">>}])),
        ?_assertEqual(ListD, sort_items(List0, [[{b,<<"desc">>}]])),
        ?_assertEqual(ListEb, sort_items(ListEa, [<<"b">>,a])),
        % sort formats
        ?_assertEqual(ListB, sort_items(List0, [a,b])),
        ?_assertEqual(ListB, sort_items(List0, <<"a,b">>)),
        ?_assertEqual(ListB, sort_items(List0, <<"{\"a\":\"asc\",\"b\":\"asc\"}">>)),
        ?_assertEqual(ListB, sort_items(List0, [{<<"a">>,<<"asc">>},{<<"b">>,<<"asc">>}])),
        ?_assertEqual(ListB, sort_items(List0, #{a => <<"asc">>,b => <<"asc">>})),
        % sort flatten
        ?_assertEqual(ListFla, sort_items(ListFl, [a])),
        ?_assertEqual(ListFlb, sort_items(ListFl, [b])),
        ?_assertEqual(ListFlx, sort_items(ListFl, [<<"b.x">>])),
        ?_assertMatch([#{a:=1},#{a:=2},#{a:=4},#{a:=3},_,_], sort_items(ListFl, [<<"b.y">>])),
        ?_assertMatch([_,_,_,_,_,_], sort_items(ListFl, [<<"b.y.z">>])),
        ?_assertEqual(ListFla, sort_items(ListFl, [<<"a">>,b])),
        ?_assertEqual(ListFlb, sort_items(ListFl, [<<"b">>,a])),
        ?_assertEqual(ListFlx, sort_items(ListFl, [<<"b.x">>,<<"a">>])),
        ?_assertMatch([#{a:=1},#{a:=2},#{a:=4},#{a:=3},_,_], sort_items(ListFl, [<<"b.y">>,<<"a">>])),
        ?_assertMatch([_,_,_,_,_,_], sort_items(ListFl, [<<"b.y.z">>,<<"a">>]))
        ]}.

format_items_test_() ->
    List1 = [#{<<"a">>=>1,
               <<"b">>=><<"abc">>,
               <<"c">>=>#{<<"x">>=>1, <<"y">>=>2, <<"z">>=>#{<<"m">> => <<"M">>, <<"n">>=><<"N">>}},
               <<"d">>=>4}],
    List2 = [#{<<"a">>=>1,
               b=><<"abc">>,
               <<"c">>=>#{x=>1, y=>2, z=>#{<<"m">> => <<"M">>, <<"n">>=>[{k,0}]}},
               <<"d">>=>4}],
    OptsFlatM = [{'format','map'},{<<"flat">>,true}],
    OptsFlatL = [{'format','list'},{<<"flat">>,true}],
    OptsDeepM = [{'format','map'},{<<"flat">>,false}],
    {"format items", [
        ?_assertEqual([#{<<"a">>=>1,<<"b">>=><<"abc">>,<<"c">>=>#{<<"x">>=>1,<<"y">>=>2,<<"z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>}},<<"d">>=>4}], format_items(List1, <<"">>, OptsDeepM)),
        ?_assertEqual([#{<<"a">>=>1,<<"b">>=><<"abc">>,<<"c.x">>=>1,<<"c.y">>=>2,<<"c.z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>},<<"d">>=>4}], format_items(List1, [], OptsFlatM)),
        ?_assertEqual([#{<<"a">>=>1}], format_items(List1, <<"a">>, OptsFlatM)),
        ?_assertEqual([#{<<"a">>=>1,<<"b">>=><<"abc">>}], format_items(List1, <<"a,b">>, OptsFlatM)),
        ?_assertEqual([[{<<"a">>,1},{<<"b">>,<<"abc">>}]], format_items(List1, <<"a,b">>, OptsFlatL)),
        ?_assertEqual([[{<<"a">>,1},{<<"b">>,<<"abc">>}]], format_items(List1, <<"b,a">>, OptsFlatL)),
        ?_assertEqual([#{<<"a">>=>1,<<"c">>=>#{<<"x">>=>1,<<"y">>=>2,<<"z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>}}}], format_items(List1, <<"a,c">>, OptsDeepM)),
        ?_assertEqual([#{<<"a">>=>1,<<"c">>=>#{<<"y">>=>2}}], format_items(List1, <<"a,c.y">>, OptsDeepM)),
        ?_assertEqual([#{<<"a">>=>1,<<"c">>=>#{<<"y">>=>2,<<"z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>}}}], format_items(List1, <<"c.y,c.z,a">>, OptsDeepM)),
        ?_assertEqual([#{<<"a">>=>1,<<"c">>=>#{<<"x">>=>1,<<"y">>=>2,<<"z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>}}}], format_items(List1, <<"c,c.y,c.z,a">>, OptsDeepM)),
        ?_assertEqual([#{<<"a">>=>1,<<"c.x">>=>1,<<"c.y">>=>2,<<"c.z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>}}], format_items(List1, <<"a,c">>, OptsFlatM)),
        ?_assertEqual([#{<<"a">>=>1,<<"c.x">>=>1,<<"c.y">>=>2}], format_items(List1, <<"c.y,c.x,a">>, OptsFlatM)),
        ?_assertEqual([#{<<"a">>=>1,<<"c.y">>=>2,<<"c.z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>}}], format_items(List1, <<"c.y,c.z,a">>, OptsFlatM)),
        ?_assertEqual([#{<<"a">>=>1,<<"c.x">>=>1,<<"c.y">>=>2,<<"c.z">>=>#{<<"m">>=><<"M">>,<<"n">>=><<"N">>}}], format_items(List1, <<"c,c.y,c.z,a">>, OptsFlatM)),
        % atoms/binaries mix
        ?_assertEqual([#{<<"c.x">>=>1,<<"c.z">>=>#{<<"m">>=><<"M">>,<<"n">>=>[{k,0}]}}], format_items(List2, <<"c.x,c.z">>, OptsFlatM)),
        ?_assertEqual([#{b=><<"abc">>}], format_items(List2, <<"b">>, OptsFlatM)),
        ?_assertEqual([#{b=><<"abc">>}], format_items(List2, <<"b">>, OptsDeepM)),
        ?_assertEqual([#{a=>1,<<"b.x">>=>1,<<"b.y">>=>2,<<"b.c">>=>3}], format_items([#{a=>1,b=>#{x=>1,y=>2,c=>3}}], <<"a,b">>, OptsFlatM)),
        ?_assertEqual([#{a=>1,<<"b.x">>=>1}], format_items([#{a=>1,b=>#{x=>1,y=>2,c=>3}}], <<"b.x,a">>, OptsFlatM)),
        ?_assertEqual([#{a=>1,b=>#{x=>1}}], format_items([#{a=>1,b=>#{x=>1,y=>2,c=>3}}], <<"b.x,a">>, OptsDeepM))
        ]}.

select_test_() ->
    List = [#{id=>5,a=>2,b=><<"def">>,c=>#{x=>1,y=>2,z=>#{<<"m">> => <<"M">>, <<"n">> => [{<<"l">>,0}]}}},
            #{id=>2,a=>3,b=><<"ghi">>},
            #{id=>1,a=>2,b=><<"def">>},
            #{id=>3,a=>2,b=><<"mno">>},
            #{id=>6,a=>1,b=><<"pqr">>},
            #{id=>4,a=>1,b=><<"abc">>},
            #{id=>7,a=>3}],
    List1 = [#{id=>5,a=>2,b=><<"def">>,c=>#{x=>1,y=>2,z=>#{<<"m">> => <<"M">>, <<"n">> => [{<<"l">>,0}]}}},
             #{id=>1,a=>2,b=><<"def">>}],
    List2 = [#{id=>1,a=>2,b=><<"def">>},
             #{id=>5,a=>2,b=><<"def">>,c=>#{x=>1,y=>2,z=>#{<<"m">> => <<"M">>, <<"n">> => [{<<"l">>,0}]}}}],
    Opts = [{<<"filter">>,[{<<"a">>,2},{<<"b">>,<<"def">>}]},
            {<<"mask">>,<<"c.x,c.z,b">>},
            {<<"order">>,[{<<"id">>,<<"asc">>}]},
            {<<"offset">>,1},
            {<<"limit">>,1},
            {<<"flat">>,true},
            {'format','map'}],
    {ok,Items1} = find_items(List, ?BU:get_by_key(<<"filter">>,Opts,un), []),
    Items2 = sort_items(Items1, ?BU:get_by_key(<<"order">>,Opts,un)),
    {"select", [
        ?_assertEqual({ok,List1}, find_items(List, ?BU:get_by_key(<<"filter">>,Opts,un), [])),
        ?_assertEqual(List2, sort_items(Items1, ?BU:get_by_key(<<"order">>,Opts))),
        ?_assertEqual([#{b=><<"def">>, <<"c.x">>=>1, <<"c.z">>=>#{<<"m">> => <<"M">>, <<"n">> => [{<<"l">>,0}]}}], format_items(lists:nthtail(1,Items2),?BU:get_by_key(<<"mask">>,Opts),Opts)),
        ?_assertEqual({ok,[[{b,<<"def">>}, {<<"c.x">>,1}, {<<"c.z">>,[{<<"m">>, <<"M">>}, {<<"n">>, [{<<"l">>,0}]}]}]]}, select(List, lists:keyreplace('format',1,Opts,{'format','list'})))
        ]}.

select_comparers_test_() ->
    List = [#{a=>1,<<"b">>=><<"abc">>},
            #{<<"a">>=>3,b=><<"ghi">>},
            #{a=>2,b=><<"def">>},
            #{a=>4,b=><<"jkl">>},
            #{a=>3},
            #{a=>true,b=><<"ZZZ">>},
            #{<<"a">>=>2,b=><<"mno">>},
            #{a=>1,<<"b">>=><<"pqr">>}],
    Sort = {<<"order">>,[{<<"a">>,<<"asc">>}]},
    {"select comparers (find item comparers)", [
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>},
                           #{a => 1,<<"b">> => <<"pqr">>},
                           #{a => true,b => <<"ZZZ">>}]}, select(List, [{<<"filter">>,[{<<"a">>,1}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>},
                           #{a => 1,<<"b">> => <<"pqr">>},
                           #{a => true,b => <<"ZZZ">>}]}, select(List, [{<<"filter">>,[{<<"a">>,<<"1">>}]},Sort])),
        ?_assertEqual({ok,[]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0">>}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>},
                           #{a => 1,<<"b">> => <<"pqr">>},
                           #{a => 2,b => <<"def">>},
                           #{b => <<"mno">>,<<"a">> => 2}]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0~2">>}]},Sort])),
        ?_assertEqual({ok,[]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0~1;3-6">>}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>},
                           #{a => 1,<<"b">> => <<"pqr">>},
                           #{b => <<"ghi">>,<<"a">> => 3},
                           #{a => 3},
                           #{a => 4,b => <<"jkl">>}]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0~1;3~6">>}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>}]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0~1;3~6">>},{<<"b">>,<<"abc">>}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>}]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0~1;3~6">>},{<<"b">>,<<"/reg/abc">>}]},Sort])),
        ?_assertEqual({ok,[]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0~1;3~6">>},{<<"b">>,<<"/reg/ABC">>}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>}]}, select(List, [{<<"filter">>,[{<<"a">>,<<"0~1;3~6">>},{<<"b">>,<<"/reg/i/ABC">>}]},Sort])),
        ?_assertEqual({ok,[#{a=>true,b=><<"ZZZ">>}]}, select(List, [{<<"filter">>,[{<<"a">>,true}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>}]}, select(List, [{<<"filter">>,[{<<"b">>,<<"a%">>}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>}]}, select(List, [{<<"filter">>,[{<<"b">>,<<"abc|fff">>}]},Sort])),
        ?_assertEqual({ok,[#{a => 1,<<"b">> => <<"abc">>}]}, select(List, [{<<"filter">>,[{<<"b">>,<<"fff|abc">>}]},Sort]))
        ]}.

select_opts_test_() ->
    ListA = [#{a=>1,b=><<"abc">>,c=>#{x=>3,y=><<"333">>,z=>#{ab=>5,ac=><<"AA">>}},d=>[<<"abc">>,<<"def">>]}],
    ListB = [#{a=>1,b=><<"abc">>,c=>[{x,3},{y,<<"333">>},{z,#{ab=>5,ac=><<"AA">>}}]}],
    ListC = [#{a=>1,b=><<"abc">>,c=>[<<"abc">>,<<"def">>,<<"geh">>],d=>#{e=>#{f=>1,g=>1},h=>3}}],
    {"select opts (find item by opts)", [
        ?_assertMatch({ok,[_]}, select(ListA, [{<<"filter">>,<<"{\"c\": {\"y\": \"333\"}}">>}])),
        ?_assertMatch({ok,[]}, select(ListA, [{<<"filter">>,<<"{\"c\": {\"y\": \"331\"}}">>}])),
        %
        ?_assertMatch({ok,[_]}, select(ListB, [{<<"filter">>,[{<<"c">>,[{<<"y">>, <<"/reg/3+">>}]}]}])),
        ?_assertMatch({ok,[]}, select(ListB, [{<<"filter">>,[{<<"c">>,[{<<"y">>, <<"/reg/1+">>}]}]}])),
        %
        ?_assertMatch({ok,[_]}, select(ListB, [{<<"filter">>,[{a,1},{<<"c">>,[{<<"y">>, <<"/reg/3+">>}]}]}])),
        ?_assertMatch({ok,[]}, select(ListB, [{<<"filter">>,[{a,0},{<<"c">>,[{<<"y">>, <<"/reg/3+">>}]}]}])),
        %
        ?_assertMatch({ok,[_]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/3+">>}]}])),
        ?_assertMatch({ok,[]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/1+">>}]}])),
        %
        ?_assertMatch({ok,[_]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/3+">>},{<<"c.x">>,3}]}])),
        ?_assertMatch({ok,[]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/3+">>},{<<"c.x">>,2}]}])),
        ?_assertMatch({ok,[]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/2+">>},{<<"c.x">>,3}]}])),
        %
        ?_assertMatch({ok,[_]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/3+">>},{c,#{x=>3}}]}])),
        ?_assertMatch({ok,[]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/2+">>},{c,#{x=>3}}]}])),
        ?_assertMatch({ok,[]}, select(ListB, [{<<"filter">>,[{<<"c.y">>,<<"/reg/3+">>},{c,#{x=>2}}]}])),
        %
        ?_assertMatch({ok,[_]}, select(ListA, [{<<"filter">>,[{<<"d">>,[<<"def">>]}]}])),
        ?_assertMatch({ok,[]}, select(ListA, [{<<"filter">>,[{<<"d">>,[<<"def1">>]}]}])),
        %
        ?_assertMatch({ok,[_]}, select(ListC, [{<<"filter">>,[{<<"d">>,#{e=>#{f=>1}}}]}])),
        ?_assertMatch({ok,[_]}, select(ListC, [{<<"filter">>,[{<<"d.e">>,#{f=>1}}] }])),
        ?_assertMatch({ok,[_]}, select(ListC, [{<<"filter">>,[{<<"d">>,#{e=>#{f=>1,g=>1}}}]}])),
        ?_assertMatch({ok,[]}, select(ListC, [{<<"filter">>,[{<<"d">>,#{e=>#{f=>1,g=>2}}}]}])),
        ?_assertMatch({ok,[]}, select(ListC, [{<<"filter">>,[{<<"d.e.f">>,1}] }])),
        ?_assertMatch({ok,[_]}, select(ListC, [{<<"filter">>,[{<<"d.e">>,#{<<"f">> => 1}}]}])),
        ?_assertMatch({ok,[_]}, select(ListC, [{<<"filter">>,[{<<"d.e">>,#{<<"f">> => 1}},{<<"d.h">>,3}]}])),
        ?_assertMatch({ok,[]}, select(ListC, [{<<"filter">>,[{<<"d.e">>,#{<<"f">> => 1}},{<<"d.h">>,2}]}]))
        ]}.
    
-endif.

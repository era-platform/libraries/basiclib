%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.11.2016.
%%% @doc Utils to urlencode

-module(basiclib_urlencode).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([encode/1,
         decode/1,
         decode_sip/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% tests
%%  decode(encode(<<"фыва"/utf8>>)).
%%  decode(encode("фыва")).

encode(undefined) -> undefined;
encode(S) when is_binary(S) -> ?BU:to_binary(encode(unicode:characters_to_list(S)));
encode(S) when is_list(S) ->
    escape_uri(S).

decode(undefined) -> undefined;
decode(S) when is_binary(S) -> unicode:characters_to_binary(decode(?BU:to_list(S)));
decode(S) when is_list(S) ->
    % erlang 20.2 "+" -> "+" instead of previous "+" -> " "
    S1 = replace(S,<<"+">>,<<"%20">>),
    ?BU:to_unicode_list(uri_string:percent_decode(S1)).

decode_sip(S) when is_binary(S) -> decode(replace(S,<<"+">>,<<"%2b">>));
decode_sip(S) when is_list(S) -> decode_sip(?BU:to_binary(S)).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
replace(S,A,B) when is_binary(S) -> binary:replace(S,b(A),b(B),[global]);
replace(S,A,B) when is_list(S) -> l(binary:replace(b(S),b(A),b(B),[global])).

%% @private
l(A) -> unicode:characters_to_list(A).
b(A) -> unicode:characters_to_binary(A).


% @private
escape_uri(S) when is_list(S) ->
    escape_uri(unicode:characters_to_binary(S));
escape_uri(<<C:8, Cs/binary>>) when C >= $a, C =< $z ->
    [C] ++ escape_uri(Cs);
escape_uri(<<C:8, Cs/binary>>) when C >= $A, C =< $Z ->
    [C] ++ escape_uri(Cs);
escape_uri(<<C:8, Cs/binary>>) when C >= $0, C =< $9 ->
    [C] ++ escape_uri(Cs);
escape_uri(<<C:8, Cs/binary>>) when C == $. ->
    [C] ++ escape_uri(Cs);
escape_uri(<<C:8, Cs/binary>>) when C == $- ->
    [C] ++ escape_uri(Cs);
escape_uri(<<C:8, Cs/binary>>) when C == $_ ->
    [C] ++ escape_uri(Cs);
escape_uri(<<C:8, Cs/binary>>) ->
    escape_byte(C) ++ escape_uri(Cs);
escape_uri(<<>>) ->
    "".

% @private
escape_byte(C) ->
  H = hex_octet(C),
  normalize(H).

% @private
%  Append 0 if length == 1
normalize(H) when length(H) == 1 -> "%0" ++ H;
normalize(H) -> "%" ++ H.

% @private
hex_octet(N) when N =< 9 -> [$0 + N];
hex_octet(N) when N > 15 ->
    hex_octet(N bsr 4) ++ hex_octet(N band 15);
hex_octet(N) -> [N - 10 + $a].


%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

f_test_() ->
    {"urlencode test",
     [?_assertEqual(<<"asdf">>, encode(<<"asdf">>)),
      ?_assertEqual(<<"as%20df">>, encode(<<"as df">>)),
      ?_assertEqual(<<"asdf%26%d1%84%d1%8b%d0%b2%d0%b0">>, encode(<<"asdf&фыва"/utf8>>)),
      ?_assertEqual(<<"%26%3c%3e%22a%d1%84f">>, encode(<<"&<>\"aфf"/utf8>>)),
      ?_assertEqual(undefined, decode(undefined)),
      ?_assertEqual(<<"asdf">>, decode(<<"asdf">>)),
      ?_assertEqual(<<"as df">>, decode(<<"as%20df">>)),
      ?_assertEqual(<<"qw ва"/utf8>>, decode(<<"qw ва"/utf8>>)),
      ?_assertEqual(<<" 79274123@domain.com">>, decode(<<"+79274123@domain.com">>)),
      ?_assertEqual(<<"+79274123@domain.com">>, decode_sip(<<"+79274123@domain.com">>)),
      ?_assertEqual(<<" 7927asваD@domain.com"/utf8>>, decode(<<"+7927asва%44@domain.com"/utf8>>)),
      ?_assertEqual(<<"+7927asваD@domain.com"/utf8>>, decode_sip(<<"+7927asва%44@domain.com"/utf8>>))
      ]}.

-endif.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 2017-Nov-17 9:20:21
%%% @doc A subscriber to the events of the erlang error logger

-module(basiclib_error_logger_srv).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>','Peter Bukashin <tbotc@yandex.ru>']).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3,
    send_developers_report/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(state, {
    eventpid,
    ref
}).

-define(HandlerId, r_error_logger).
-define(InitEvHanTout, 3000).
-define(ChSubscrTout, 10000).
-define(Profile, 'era_error_handler_httpc').

-define(ELEVENT, basiclib_error_logger_eventhandler).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).

%% ---
-spec send_developers_report(Body::binary()) -> ok.
%% ---
send_developers_report(Body) ->
    do_send_developers_report(Body).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    {Self,Ref} = {self(),make_ref()},
    State1 = #state{ref=Ref},
    erlang:send_after(0,Self,{init,Ref}),
    logger:set_primary_config(level, debug),
    logger:set_handler_config(default, level, critical),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast(_, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({init,Ref}, #state{ref=Ref}=State) ->
    Self = self(),
    init_httpc(),
    erlang:send_after(0,Self,{init_eventhandler,Ref}),
    {noreply,State};

%% ---
handle_info({init_eventhandler,Ref}, #state{ref=Ref}=State) ->
    Self = self(),
    case init_error_logger_eventhandler() of
        ok -> erlang:send_after(?ChSubscrTout,Self,{check_subscribe,Ref});
        {error,_} -> erlang:send_after(?InitEvHanTout,Self,{init_eventhandler,Ref})
    end,
    {noreply,State};

%% ---
handle_info({check_subscribe,Ref}, #state{ref=Ref}=State) ->
    Self = self(),
    case check_subscribe() of
        ok -> erlang:send_after(?ChSubscrTout,Self,{check_subscribe,Ref});
        {error,_} -> erlang:send_after(0,Self,{init_eventhandler,Ref})
    end,
    {noreply,State};

% ----
% other
handle_info(_, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------
%% DO INIT
%% -------------------------------
init_error_logger_eventhandler() ->
    ?OUT("ErrLogger. Start init_eventhandler"),
    delete_handler_if_exists(),
    HandlerConfig = #{}, % use default config - #{level => all, filter_default => log, filters => [], formatter => {logger_formatter, DefaultFormatterConfig}}
    case logger:add_handler(?HandlerId,?ELEVENT,HandlerConfig) of
        ok ->
            ?OUT("ErrLogger. Add handler success"),
            ok;
        {error,Err} ->
            ?OUT("ErrLogger. Add handler Error:~120p",[Err]),
            {error,<<"error_add_eventhandler">>}
    end.

%% @private
delete_handler_if_exists() ->
    case logger:get_handler_config(?HandlerId) of
        {ok,_} ->
            ?OUT("ErrLogger. Detect exists handler"),
            RemRes = logger:remove_handler(?HandlerId),
            ?OUT("ErrLogger. Delete exists handler result:~120p",[RemRes]),
            RemRes;
        {error,_} -> ok
    end.

%% ---
check_subscribe() ->
    case logger:get_handler_config(?HandlerId) of
        {ok,_} -> ok;
        {error,Err} ->
            ?OUT("ErrLogger. Error handler missed. Get config for (~120p) error:~120p",[?HandlerId,Err]),
            {error,<<"subscribe missed">>}
    end.

%% ---
init_httpc() ->
    SslStartRes = application:ensure_all_started(ssl),
    ?OUT("ErrLogger. SslStartRes: ~120p",[SslStartRes]),
    InetsStartRes = inets:start(),
    ?OUT("ErrLogger. InetsStartRes: ~120p",[InetsStartRes]),
    InetsStartProfRes = inets:start(httpc, [{profile,?Profile}]),
    ?OUT("ErrLogger. InetsStartProfRes: ~120p",[InetsStartProfRes]),
    HttpOpts = [{max_sessions,10000},
        {keep_alive_timeout,5000}],
    SetOptsRes = httpc:set_options(HttpOpts,?Profile),
    ?OUT("ErrLogger. SetOptsRes: ~120p",[SetOptsRes]),
    SetOptsRes.

%% ---
do_send_developers_report(Text) ->
    Url = "https://api.telegram.org/bot5385020502:AAGmRfOiKgKm_rTrIiFO3DBACTJFLgDuKrE/sendMessage",
    Method = post,
    Headers = [{"Content-Type","application/json; charset=utf-8"}],
    Body = jsx:encode(#{chat_id => <<"-1001712170673">>, text => Text}),
    Request = {Url,Headers,"application/json; charset=utf-8",Body},
    HTTPOptions = [{timeout,5000},{relaxed,true}],
    Options = [{body_format, binary},{sync, false}],
    httpc:request(Method, Request, HTTPOptions, Options, ?Profile).
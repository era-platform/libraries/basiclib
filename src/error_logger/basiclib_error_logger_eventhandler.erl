%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 2017-Nov-16 12:28:01
%%% @doc Formats and writes events to a file

-module(basiclib_error_logger_eventhandler).

-export([log/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ERLERR, error_logger).

-define(EL_EMER_LOGFILE, {error_logger,emergency}).
-define(EL_ALERT_LOGFILE, {error_logger,alert}).
-define(EL_CRIT_LOGFILE, {error_logger,critical}).
-define(EL_ERR_LOGFILE, {error_logger,err}).
-define(EL_WARN_LOGFILE, {error_logger,warn}).
-define(EL_INFO_LOGFILE, {error_logger,info}).
-define(EL_NOTC_LOGFILE, {error_logger,notice}).
-define(EL_DBG_LOGFILE, {error_logger,debug}).
-define(EL_OTH_LOGFILE, {error_logger,other}).

-define(EmLOG(Fmt,Args), ?LOG('$force', ?EL_EMER_LOGFILE, Fmt,Args)).
-define(ALOG(Fmt,Args), ?LOG('$force', ?EL_ALERT_LOGFILE, Fmt,Args)).
-define(CLOG(Fmt,Args), ?LOG('$force', ?EL_CRIT_LOGFILE, Fmt,Args)).
-define(ELOG(Fmt,Args), ?LOG('$error', ?EL_ERR_LOGFILE, Fmt,Args)).
-define(WLOG(Fmt,Args), ?LOG('$warning', ?EL_WARN_LOGFILE, Fmt,Args)).
-define(ILOG(Fmt,Args), ?DOLOG('$info', ?EL_INFO_LOGFILE, Fmt,Args)). % TODO: apply log level
-define(NLOG(Fmt,Args), ?DOLOG('$trace', ?EL_NOTC_LOGFILE, Fmt,Args)). % TODO: apply log level
-define(DLOG(Fmt,Args), ?DOLOG('$debug', ?EL_DBG_LOGFILE, Fmt,Args)). % TODO: apply log level
-define(OLOG(Fmt,Args), ?LOG('$info', ?EL_OTH_LOGFILE, Fmt,Args)).

%-define(DOLOG(LogLevel,LogFile,Fmt,Args), case application:get_env(?APP,LogFile,false) of {ok,true} -> ?LOG(LogLevel,LogFile,Fmt,Args); _ -> ok end). % TODO: apply log level
-define(DOLOG(LogLevel,LogFile,Fmt,Args), case ?CFG:get_env(LogFile,false) of true -> ?LOG(LogLevel,LogFile,Fmt,Args); _ -> ok end). % TODO: apply log level

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ---
-spec log(LogEvent::map(),Config::map()) -> ok.
%% ---
log(LogEvent,_Config) when is_map(LogEvent) ->
    try
        PrintData = prepare_event_to_print(LogEvent),
        print_event(PrintData),
        ok
    catch E:R:ST ->
        ?LOG('$crash', ?EL_OTH_LOGFILE, "error_logger catched: {~120tp, ~120tp}~n\tStack: ~120tp~n\tLogEvent: ~120tp", [E,R,ST,LogEvent])
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
prepare_event_to_print(LogEvent) ->
    [Level,Meta,Msg] = ?BU:maps_get([level,meta,msg],LogEvent),
    Pid = maps:get(pid,Meta),
    case Msg of
        {report, Report} ->
            {Format,Args} = logger:format_report(Report),
            {report,Level,Pid,Format,Args};
        {string, String} ->
            {string,Level,Pid,String};
        {Fmt, Args} when is_list(Args) ->
            {fmt_args,Level,Pid,Fmt,Args};
        _ ->
            LogEvent
    end.

%% ----
%% Print Events
%% ----
-spec print_event({EventLevel::emergency | alert | critical | error | warning | notice | info | debug,
    Pid::pid(), Format::io:format(),Args::[term()]}) -> ok.
%% ----
%% print raw report data
print_event({report, EventLevel, Pid, Format, Args}) ->
    Prefix = do_print_get_prefix(EventLevel),
    LogMacroFun = do_print_get_log_macro_f(EventLevel),
    Fmt = Prefix++Format++"~n",
    FmtArgs = [Pid|Args],
    erlang:apply(LogMacroFun,[Fmt,FmtArgs]);
%% print clear string
print_event({string, EventLevel, Pid, String}) ->
    Prefix = do_print_get_prefix(EventLevel),
    LogMacroFun = do_print_get_log_macro_f(EventLevel),
    Fmt = Prefix++"~ts~n",
    FmtArgs = [Pid,String],
    erlang:apply(LogMacroFun,[Fmt,FmtArgs]);
%% print by fmt-args
print_event({fmt_args, EventLevel, Pid, Format, Args}) ->
    Prefix = do_print_get_prefix(EventLevel),
    LogMacroFun = do_print_get_log_macro_f(EventLevel),
    Fmt = Prefix++Format++"~n",
    FmtArgs = [Pid|Args],
    erlang:apply(LogMacroFun,[Fmt,FmtArgs]);
%% other
print_event(Event) -> ?OLOG("Unknown Event:~n\t~120tp",[Event]).

%% @private
do_print_get_prefix(EventLevel) ->
    "="++?BU:to_list(EventLevel)++"=" ++ " at (~120tp)====~n".

%% @private
do_print_get_log_macro_f(emergency) -> fun(Fmt,Args) -> ?EmLOG(Fmt,Args) end;
do_print_get_log_macro_f(alert) -> fun(Fmt,Args) -> ?ALOG(Fmt,Args) end;
do_print_get_log_macro_f(critical) -> fun(Fmt,Args) -> ?CLOG(Fmt,Args) end;
do_print_get_log_macro_f(error) -> fun(Fmt,Args) -> ?ELOG(Fmt,Args) end;
do_print_get_log_macro_f(warning) -> fun(Fmt,Args) -> ?WLOG(Fmt,Args) end;
do_print_get_log_macro_f(notice) -> fun(Fmt,Args) -> ?NLOG(Fmt,Args) end;
do_print_get_log_macro_f(info) -> fun(Fmt,Args) -> ?ILOG(Fmt,Args) end;
do_print_get_log_macro_f(debug) -> fun(Fmt,Args) -> ?DLOG(Fmt,Args) end.

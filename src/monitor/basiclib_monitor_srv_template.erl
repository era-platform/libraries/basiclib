%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.12.2015
%%% @doc Monitor processes gen_server template.
%%%        Could be decorated and started by different regnames simultaneoulsly.
%%%        Uses erlang:monitor(process,Pid) to monitor process and guaranteed handle of 'DOWN' instead of state of calling process.
%%%        Filters only abnormal results (excepts Result==normal; Result==shutdown).
%%%        When destination process found down abnormally, executes registered set of functions.
%%%        Allows to append funs to linked process and drop funs from linked process (appended by keys)
%%%      Parameters:
%%%        ets - passed ets storage from supervisor (not to loose functions and monitoring state if gen_server is down)
%%%        funlog - function to write logs.

-module(basiclib_monitor_srv_template).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/2,
         %
         start_monitor/3, start_monitor/4,
         start_monitor_async/3, start_monitor_async/4,
         append_fun/3, append_fun/4,
         append_final_fun/3, append_final_fun/4,
         drop_fun/3,
         stop_monitor/2,
         stop_monitor_async/2]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(monitorstate, {
    ets,
    funlog
}).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%%
%% ------------------------------
start_link(M,StartOpts) ->
    Opts = case StartOpts of
               _ when is_map(StartOpts) -> maps:to_list(StartOpts);
               _ when is_list(StartOpts) -> StartOpts
           end,
    gen_server:start_link({local,M}, ?MODULE, Opts, []).

%% ------------------------------
%% Starts monitor of pid by third-party process
%%   Several functions could be applied to handle pid's termination.
%%   Function could have arity of 0 or 1. Then Reason would be passed.
%%   Name could be set to appear in log when applying functions.
%% ------------------------------

%% synchronously (guarantee monitoring is started before call returned)
start_monitor(M, Pid, Funs) when is_list(Funs) ->
    start_monitor(M, Pid, "", Funs).
start_monitor(M, Pid, Name, Funs) when is_list(Funs) ->
    gen_server:call(M, {start_monitor, Pid, Name, Funs}).

%% asynchronously
start_monitor_async(M, Pid, Funs) when is_list(Funs) ->
    start_monitor_async(M, Pid, "", Funs).
start_monitor_async(M, Pid, Name, Funs) when is_list(Funs) ->
    gen_server:cast(M, {start_monitor, Pid, Name, Funs}).

%% ------------------------------
%% Append fun partially and put it to default list of functions.
%%   Operation is asynchronous.
%%   Could be called for the same monitored process several times. Then appends fun to call list
%%   If process is not being monitored yet, then starts monitor automatically.
%%   Key could be passed to allow drop selected fun later.
%% ------------------------------
append_fun(M, Pid, Fun) when is_function(Fun) ->
    gen_server:cast(M, {append_fun, Pid, undefined, Fun}).

append_fun(M, Pid, Key, Fun) when is_function(Fun) ->
    gen_server:cast(M, {append_fun, Pid, Key, Fun}).

%% ------------------------------
%% Append fun partially and put it to final list of functions. Final list is being applied after other functions.
%%   Operation is asynchronous.
%%   Key could be passed to allow drop selected fun later.
%% ------------------------------
append_final_fun(M, Pid, Fun) when is_function(Fun) ->
    gen_server:cast(M, {append_final_fun, Pid, undefined, Fun}).

append_final_fun(M, Pid, Key, Fun) when is_function(Fun) ->
    gen_server:cast(M, {append_final_fun, Pid, Key, Fun}).

%% ------------------------------
%% Removes function by registered Key from both lists: default and final.
%%   Operation is synchronous to guarantee that funs wouldn't be applied if monitored process calles it by itself just before termination.
%%   Other functions added earlier by append_fun or start_monitor remain.
%%   If last function is removed, then monitor stops automatically.
%% ------------------------------
drop_fun(M, Pid, Key) ->
    gen_server:call(M, {drop_fun, Pid, Key}).

%% ------------------------------
%% Abort monitoring of PID totally
%%   All of registered functions would be removed automatically.
%% ------------------------------

%% Synchronously (guarantee not apply final functions after call returned)
stop_monitor(M, Pid) ->
    gen_server:call(M, {stop_monitor, Pid}).

%% Asynchronously
stop_monitor_async(M, Pid) ->
    gen_server:cast(M, {stop_monitor, Pid}).


%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Opts) ->
    ETS = ?BU:get_by_key(ets, Opts),
    FunLog = ?BU:get_by_key(funlog, Opts, undefined),
    State = #monitorstate{ets=ETS,
                          funlog=FunLog},
    restart_monitor(State),
    {ok, State}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

%% print state
handle_call({print}, _From, State) ->
    ?OUTC('$force',"Monitor. State: ~140tp~n", [State]),
    {reply, ok, State};

%% returns count of callid-dialog pairs linked
handle_call({count}, _From, State) ->
    #monitorstate{ets=ETS}=State,
    Size = ets:info(ETS, size),
    ?OUTC('$force',"Monitor. Count of items: ~140p~n", [Size]),
    {reply, Size, State};

%% attaching to process
handle_call({start_monitor, Pid, Name, Funs}, _From, State) ->
    do_start_monitor(Pid, Name, Funs, State),
    {reply, ok, State};

%% detaching from process
handle_call({stop_monitor, Pid}, _From, State) ->
    do_stop_monitor(Pid, State),
    {reply, ok, State};

%% detaching fun by key from process' fun list
handle_call({drop_fun, Pid, Key}, _From, State) ->
    Res = do_drop_fun(Pid, Key, State),
    {reply, Res, State};

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

%% attaching to process
handle_cast({start_monitor, Pid, Name, Funs}, State) ->
    do_start_monitor(Pid, Name, Funs, State),
    {noreply, State};

%% appending fun to process' fun list
handle_cast({append_fun, Pid, Key, Fun}, State) ->
    do_append_xfun(Pid, Key, Fun, funs, State),
    {noreply, State};

%% appending fun to process' fun list
handle_cast({append_final_fun, Pid, Key, Fun}, State) ->
    do_append_xfun(Pid, Key, Fun, final_funs, State),
    {noreply, State};

%%%% detaching fun by key from process' fun list
%%handle_cast({drop_fun, Pid, Key}, State) ->
%%    do_drop_fun(Pid, Key, State),
%%    {noreply, State};

%% detaching from process
handle_cast({stop_monitor, Pid}, State) ->
    do_stop_monitor(Pid, State),
    {noreply, State};

%% restart monitor
handle_cast({restart}, State) ->
    {stop, restart, State};

%% other
handle_cast(_, State) ->
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

%% when proc is down
handle_info({'DOWN', Ref, process, Pid, Result}, State) ->
    #monitorstate{ets=ETS}=State,
    ApplyF = fun(M) ->
                    Funs = maps:get(funs,M,[]),
                    FinalFuns = maps:get(final_funs,M,[]),
                    ets:delete(ETS, Pid),
                    exec_funs(Funs++FinalFuns, {Pid,Result}, State#monitorstate.funlog) end,
    case ets:lookup(ETS, Pid) of
        [] -> ok;
        [{Pid, #{}=M}] when Result==normal; Result==shutdown ->
            ApplyF(M);
        [{Pid, #{}=M}] ->
            Ref1 = maps:get(ref,M),
            Name = maps:get(name,M),
            funlog({"Found process ~p 'DOWN' abnormally (ref=~140p, ref1=~140p).~n\t\tName=~140p~n\t\tResult=~140p", [Pid, Ref, Ref1, Name, Result]}, State),
            ApplyF(M)
    end,
    {noreply, State};


%% other
handle_info(_, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

restart_monitor(#monitorstate{ets=ETS}=State) ->
    F = fun({Pid,#{}=Map},Acc) ->
        Funs = maps:get(funs,Map),
        try case ?BLrpc:pinfo(Pid, status) of
                % case erlang:process_info(Pid, status) of
                undefined ->
                    exec_funs(Funs, {Pid,restart_monitor}, State#monitorstate.funlog),
                    [Pid|Acc];
                _ ->
                    ets:insert(ETS, {Pid,Map#{ref => erlang:monitor(process, Pid)}}),
                    Acc
            end
        catch
            error:R ->
                funlog({"restart_monitor ~p error: ~p", [Pid, R]},State),
                exec_funs(Funs, {Pid,restart_monitor}, State#monitorstate.funlog),
                [Pid|Acc];
            exit:R ->
                funlog({"restart_monitor ~p caught error: ~p", [Pid, R]},State),
                exec_funs(Funs, {Pid,restart_monitor}, State#monitorstate.funlog),
                [Pid|Acc]
        end end,
    case ets:foldl(F, [], ETS) of
        [] -> ok;
        Pids -> lists:foreach(fun(Pid) -> ets:delete(ETS,Pid) end, Pids)
    end.

% --------------
exec_funs(Funs, {Pid,Result}, FunLog) ->
    try
        Fexec = fun(Fun) when is_function(Fun) ->
            try
                case erlang:fun_info(Fun,arity) of
                    {arity, 0} -> Fun(); % spawn
                    {arity, 1} -> Fun(Result); % spawn
                    {arity, N} -> funlog({"Monitor ~p fun error (Invalid function arity: ~p). Proc terminated: ~n\t\t~120p", [Pid, N, Result]}, FunLog);
                    _ -> ok
                end
            catch
                error:R -> funlog({"Monitor ~p fun error (~120p). Proc terminated: ~n\t\t~120p", [Pid, R, Result]}, FunLog);
                throw:R -> funlog({"Monitor ~p fun throw (~120p). Proc terminated: ~n\t\t~120p", [Pid, R, Result]}, FunLog);
                exit:R -> funlog({"Monitor ~p fun exit (~120p). Proc terminated: ~n\t\t~120p", [Pid, R, Result]}, FunLog)
            end end,
        F = fun(Fun, Acc) when is_function(Fun) -> Fexec(Fun), Acc+1;
            ({_K,Fun}, Acc) when is_function(Fun) -> Fexec(Fun), Acc+1;
            (T, Acc) -> funlog({"Monitor ~p error. Term idx=~p is neither function nor tuple ~120p. Proc terminated: ~n\t\t~120p", [Pid, Acc, T, Result]}, FunLog), Acc+1
            end,
        lists:foldr(F, 1, Funs)
    catch
        error:R -> funlog({"exec_funs ~p error: ~p ~n\t\t(~p)", [Pid, R, Funs]},FunLog);
        exit:R -> funlog({"exec_funs ~p caught error: ~p ~n\t\t(~p)", [Pid, R, Funs]},FunLog)
    end.

% --------------
do_start_monitor(Pid, Name, Funs, State) ->
    #monitorstate{ets=ETS}=State,
    ets:insert(ETS, {Pid, #{name => Name,
                            funs => lists:reverse(Funs),
                            final_funs => [],
                            ref => erlang:monitor(process, Pid)}}).

% --------------
do_append_xfun(Pid, Key, Fun, FunKey, State) ->
    #monitorstate{ets=ETS}=State,
    case ets:lookup(ETS, Pid) of
        [] ->
            DefMap = #{funs => [], final_funs => []},
            ets:insert(ETS, {Pid, DefMap#{name => "",
                                          FunKey => [{Key,Fun}],
                                          ref => erlang:monitor(process, Pid)}});
        [{_,#{}=Map}] ->
            Funs = maps:get(FunKey,Map),
            Item = case Key of undefined -> Fun; _ -> {Key,Fun} end,
            ets:insert(ETS, {Pid, Map#{FunKey:=[Item|Funs]}})
    end.

% --------------
do_drop_fun(Pid, Key, State) ->
    #monitorstate{ets=ETS}=State,
    case ets:lookup(ETS, Pid) of
        [] -> false;
        [{_,#{}=Map}] ->
            Funs = maps:get(funs, Map),
            case lists:keydelete(Key, 1, Funs) of
                Funs -> false;
                Funs1 ->
                    ets:insert(ETS, {Pid, Map#{funs:=Funs1}}),
                    ok
            end
    end.

% --------------
do_stop_monitor(Pid, State) ->
    #monitorstate{ets=ETS}=State,
    case ets:lookup(ETS, Pid) of
        [] -> ok;
        [{Pid,#{}=Map}] ->
            Ref = maps:get(ref,Map),
            Funs = maps:get(final_funs,Map),
            exec_funs(Funs, {Pid,normal}, State#monitorstate.funlog),
            erlang:demonitor(Ref)
    end,
    ets:delete(ETS, Pid).

% --------------
funlog({Fmt,Args},FunLog) when is_function(FunLog,2) -> FunLog(Fmt,Args);
funlog({Fmt,Args},#monitorstate{funlog=FunLog}) when is_function(FunLog,2) -> FunLog(Fmt,Args);
funlog({_Fmt,_Args},_State) -> ok.

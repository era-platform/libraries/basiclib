%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.12.2015
%%% @doc Monitor processes module+gen_server.
%%%         When destination process down, executes registered set of functions.
%%%        Allow to append funs to linked process and drop funs from linked process (appended by keys)

-module(basiclib_monitor_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1,
         %
         start_monitor/2, start_monitor/3,
         start_monitor_async/2, start_monitor_async/3,
         append_fun/2, append_fun/3,
         append_final_fun/2, append_final_fun/3,
         drop_fun/2,
         stop_monitor/1,
         stop_monitor_async/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Start functions
%% ====================================================================

start_link(Opts) -> exec(start_link,[Opts]).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Starts monitor of pid by third-party process
%%   Several functions could be applied to handle pid's termination.
%%   Function could have arity of 0 or 1. Then Reason would be passed.
%%   Name could be set to appear in log when applying functions.
%% ------------------------------

%% synchronously (guarantee monitoring is started before call returned)
start_monitor(Pid, Funs) when is_list(Funs) -> exec(start_monitor, [Pid, Funs]).
start_monitor(Pid, Name, Funs) when is_list(Funs) -> exec(start_monitor, [Pid, Name, Funs]).

%% asynchronously
start_monitor_async(Pid, Funs) -> exec(start_monitor_async, [Pid, Funs]).
start_monitor_async(Pid, Name, Funs) -> exec(start_monitor_async, [Pid, Name, Funs]).

%% ------------------------------
%% Append fun partially and put it to default list of functions.
%%   Operation is asynchronous.
%%   Could be called for the same monitored process several times. Then appends fun to call list
%%   If process is not being monitored yet, then starts monitor automatically.
%%   Key could be passed to allow drop selected fun later.
%% ------------------------------
append_fun(Pid, Fun) when is_function(Fun) -> exec(append_fun, [Pid, Fun]).
append_fun(Pid, Key, Fun) when is_function(Fun) -> exec(append_fun, [Pid, Key, Fun]).

%% ------------------------------
%% Append fun partially and put it to final list of functions. Final list is being applied after other functions.
%%   Operation is asynchronous.
%%   Key could be passed to allow drop selected fun later.
%% ------------------------------
append_final_fun(Pid, Fun) when is_function(Fun) -> exec(append_final_fun, [Pid, Fun]).
append_final_fun(Pid, Key, Fun) -> exec(append_final_fun, [Pid, Key, Fun]).

%% ------------------------------
%% Removes function by registered Key from both lists: default and final.
%%   Operation is synchronous to guarantee that funs wouldn't be applied if monitored process calles it by itself just before termination.
%%   Other functions added earlier by append_fun or start_monitor remain.
%%   If last function is removed, then monitor stops automatically.
%% ------------------------------
drop_fun(Pid, Key) -> exec(drop_fun, [Pid, Key]).

%% ------------------------------
%% Abort monitoring of PID totally
%%   All of registered functions would be removed automatically.
%% ------------------------------

%% Synchronously (guarantee not apply final functions after call returned)
stop_monitor(Pid) -> exec(stop_monitor, [Pid]).

%% Asynchronously
stop_monitor_async(Pid) -> exec(stop_monitor_async, [Pid]).

%% ====================================================================
%% Internal functions
%% ====================================================================

exec(Method, Args) -> erlang:apply(?BLmonitor_template,Method,[?MODULE|Args]).


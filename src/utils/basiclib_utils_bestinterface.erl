%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.06.2021
%%% @doc Return cached interfaces, routetable, bestinterface for address.
%%%      Note, that it use default route table (main) and doesn't reflect dynamic routing rules models.
%%%      Use for static only.

-module(basiclib_utils_bestinterface).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([getinterfaces/0,
         getroutetable/0,
         getbestinterface/1]).

-compile(export_all).

%% ====================================================================
%% Types
%% ====================================================================

-include("app.hrl").

-define(CacheTimeout, 60000).
-define(RefreshTimeout, 20000).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
getinterfaces() ->
    Key = 'ifaddrs',
    Fbuild = fun() -> ifaddrs() end,
    FunStore = fun(undefined) -> Fbuild(); (R) -> R end,
    FunReply = fun(_,R) -> R end,
    ?BLstore:func_t(Key, ?CacheTimeout, FunStore, FunReply).

%% ------------------------------
getroutetable() ->
    case os:type() of
        {unix,linux} -> getroutetable_linux();
        {win32,_} -> getroutetable_win()
    end.

%% ------------------------------
getbestinterface(AddrBin) when is_binary(AddrBin) ->
    getbestinterface(?BU:to_list(AddrBin));
getbestinterface(AddrString) when is_list(AddrString) ->
    {ok,Addr} = inet:parse_address(AddrString),
    getbestinterface(Addr);
getbestinterface({_,_,_,_}=Targ) ->
    case os:type() of
        {unix,linux} -> getbestinterface_linux_iproute(Targ);
        {win32,_} -> getbestinterface_win(Targ)
    end.
    
%% ---------------------------------------------------------------
%% Windows (cmd: "route print")
%% ---------------------------------------------------------------

%% @private
getbestinterface_win(Targ) ->
    lists:foldl(fun({Z,M,I}, undefined) ->
                        case match_route(Targ,Z,M) of
                            true -> I;
                            false -> undefined
                        end;
                   (_,Acc) -> Acc
                end, undefined, getroutetable_win()).

%% @private
getroutetable_win() ->
    Key = 'routetable_windows',
    Fbuild = fun() -> routetable_win() end,
    FunStore = fun(undefined) -> Fbuild(); (R) -> R end,
    FunReply = fun(_,R) -> R end,
    ?BLstore:func_t(Key, ?CacheTimeout, FunStore, FunReply).

%% @private
routetable_win() ->
    A = os:cmd("route print -4"),
    B = string:tokens(A, "==========================================================================="),
    C = lists:nth(3,B),
    D = string:tokens(C, "\r\n"),
    E = lists:filter(fun([32|_]) -> true; (_) -> false end, D),
    F = lists:map(fun(L) -> string:tokens(L," ") end, E),
    Fparse = fun(Addr) -> element(2,inet:parse_address(Addr)) end,
    G = lists:map(fun([Zone,Mask,_,Iface,_]) -> {Fparse(Zone),Fparse(Mask),Fparse(Iface)} end, F),
    lists:reverse(G).

%% -------------------------------------------------------
%% Linux (cmd: "ip route")
%% -------------------------------------------------------
%% Console response sample:
%%   default via 192.168.0.1 dev enp8s0 proto static metric 100
%%   169.254.0.0/16 dev enp8s0 scope link metric 1000
%%   172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 linkdown
%%   172.18.0.0/16 dev br-72e5ddc3f1ba proto kernel scope link src 172.18.0.1 linkdown
%%   192.168.0.0/24 dev enp8s0 proto kernel scope link src 192.168.0.112 metric 100
%% -------------------------------------------------------

getbestinterface_linux_iproute(Target) ->
    getbestinterface_linux_iproute(Target,getroutetable_linux(),undefined).

%% @private
getbestinterface_linux_iproute(Target,Routes,FilterDev) ->
    F = fun(Item, undefined) ->
            Fapply = fun() ->
                            case maps:get(src,Item,undefined) of
                                undefined ->
                                    Via = maps:get(via,Item),
                                    Dev = maps:get(dev,Item),
                                    {ok,GateIp} = inet:parse_address(?BU:to_list(Via)),
                                    getbestinterface_linux_iproute(GateIp,Routes,Dev);
                                Src -> Src
                            end end,
            Fcheckapply = fun(NetIp) ->
                                NetMask = maps:get(netmask,Item),
                                case match_route(Target,NetIp,NetMask) of
                                    false -> undefined;
                                    true -> Fapply()
                                end end,
            case maps:get(netip,Item) of
                'default' when FilterDev==undefined ->
                    Fapply();
                'default' ->
                    % gate over default - it's loop
                    undefined;
                NetIp when FilterDev==undefined ->
                    % check current route if satisfy by mask
                    Fcheckapply(NetIp);
                NetIp ->
                    case maps:get(dev,Item) of
                        FilterDev -> Fcheckapply(NetIp);
                        _ -> undefined
                    end
            end;
        (_, Acc) -> Acc
        end,
    lists:foldl(F, undefined, Routes).

%% @private
getroutetable_linux() ->
    Key = 'routetable_linux',
    FLazy = fun() -> routetable_linux() end,
    ?BLstore:lazy_t(Key, FLazy, {?CacheTimeout,?RefreshTimeout,2000}, {true,false}).

%% @private
routetable_linux() ->
    {0,Data} = ?BU:cmd_exec("ip route"),
    D1 = binary:split(Data, <<"\n">>, [global,trim_all]),
    D2 = lists:map(fun(I) -> binary:split(I, <<" ">>, [global,trim_all]) end, D1),
    F = fun(X) ->
            [X0|XRest]=X,
            {NetIp,NetMask} =
                case binary:split(X0,<<"/">>) of
                    [<<"default">>] -> {'default',make_mask(0)};
                    [BinIp,BinBits] ->
                        {ok,Addr} = inet:parse_address(?BU:to_list(BinIp)),
                        Mask = make_mask(?BU:to_int(BinBits)),
                        {Addr,Mask};
                    _ -> {undefined,undefined}
                end,
            case NetIp of
                undefined -> false;
                _ ->
                    F2 = fun(Item,{Code,Acc}) ->
                        case ?BU:to_list(Code) of
                            C when C=="via"; C=="dev"; C=="proto"; C=="scope" ->
                                {undefined, Acc#{?BU:to_atom_new(Code) => Item}};
                            C when C=="metric" ->
                                {undefined, Acc#{?BU:to_atom_new(Code) => ?BU:to_int(Item)}};
                            C when C=="src" ->
                                {ok,IPv4} = inet:parse_address(?BU:to_list(Item)),
                                {undefined, Acc#{?BU:to_atom_new(Code) => IPv4}};
                            _ -> {Item,Acc}
                        end end,
                    {_,Map} = lists:foldl(F2, {undefined,#{netip=>NetIp,netmask=>NetMask}}, XRest),
                    {true,Map}
            end end,
    sort_routes(lists:filtermap(F, D2)).

%% @private
make_mask(0) -> {0,0,0,0};
make_mask(8) -> {255,0,0,0};
make_mask(16) -> {255,255,0,0};
make_mask(24) -> {255,255,255,0};
make_mask(32) -> {255,255,255,255};
make_mask(MaskBitSize) ->
    F = fun F(Bits,Acc) when Bits>=8 -> F(Bits-8, Acc ++ [255]);
        F(Bits,Acc) when Bits>0 -> F(0, Acc ++ [256-trunc(math:pow(2,8-Bits))]);
        F(0,Acc) when length(Acc)<4 -> F(0, Acc ++ [0]);
        F(0,[A,B,C,D]) -> {A,B,C,D}
        end,
    F(MaskBitSize,[]).

%% @private
sort_routes(Routes) ->
    lists:sort(fun(ItemA, ItemB) ->
                    Fmetric = fun() ->
                                    case {maps:get(metric,ItemA,undefined), maps:get(metric,ItemB,undefined)} of
                                        {X,X} -> false;
                                        {X,Y} -> X =< Y
                                    end end,
                    case {maps:get(netip,ItemA), maps:get(netip,ItemB)} of
                        {'default','default'} -> Fmetric();
                        {'default',_} -> false;
                        {_,'default'} -> true;
                        _ ->
                            case {maps:get(netmask,ItemA,undefined), maps:get(netmask,ItemB,undefined)} of
                                {A,A} -> Fmetric();
                                {A,B} -> A >= B
                            end
                    end
               end, Routes).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------
%% Return true when NetIp and NetMask correspond to TargetIp
%% ----------------------------------
match_route(TargetIp, NetIp, NetMask)
    when tuple_size(TargetIp) =:= tuple_size(NetIp),
    tuple_size(TargetIp) =:= tuple_size(NetMask) ->
    lists:all(fun (A) -> A end,
        [element(I, TargetIp) band element(I, NetMask) =:= element(I, NetIp) band element(I, NetMask)
            || I <- lists:seq(1, tuple_size(TargetIp)) ]).

%% ----------------------------------
%% Return iface addrs
%% ----------------------------------
ifaddrs() ->
    {ok, IFData} = inet:getifaddrs(),
    lists:append([ifaddrs(IF, IFOpts) || {IF, IFOpts} <- IFData]).

%% @private
ifaddrs(IF, Opts) ->
    {_,IfAddrs} = lists:foldl(fun parse_opts/2, {undefined, []}, Opts),
    [{IF, IfAddrItem} || IfAddrItem <- IfAddrs].

%% @private
parse_opts({addr, Addr}, {undefined, IfAddrs}) ->
    {{addr, Addr}, IfAddrs};
parse_opts({netmask, Mask}, {{addr, Addr}, IfAddrs})
  when tuple_size(Mask) =:= tuple_size(Addr) ->
    {undefined, [{Addr, Mask} | IfAddrs]};
parse_opts(_, Acc) -> Acc.

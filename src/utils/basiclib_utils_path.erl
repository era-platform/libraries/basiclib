%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc File path operations

-module(basiclib_utils_path).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------
%% Path operations
%% ------------------------------------

get_filename(Path) ->
    lists:reverse(lists:takewhile(fun($/) -> false; (_) -> true end, lists:reverse(Path))).

get_directory(Path) ->
    lists:reverse(lists:dropwhile(fun($/) -> false; (_) -> true end, lists:reverse(Path))).

drop_last_path(Path) ->
    D = get_directory(Path),
    case length(D) of
        0 -> D;
        _ -> lists:droplast(D)
    end.

trim_left_slashes([]=Path) -> Path;
trim_left_slashes([$/|T]) -> trim_left_slashes(T);
trim_left_slashes([_|_]=Path) -> Path.

trim_right_slashes(Path) ->
    case length(Path) of
        0 -> Path;
        _ ->
            case lists:last(Path) of
                $/ -> trim_right_slashes(lists:droplast(Path));
                _ -> Path
            end
    end.

change_extension(Path, []) -> change_extension_1(Path,[]);
change_extension(Path, [$.|_]=Ext) -> change_extension_1(Path,Ext);
change_extension(Path, [_|_]=Ext) -> change_extension_1(Path,[$.|Ext]).
% @private
change_extension_1(Path, Ext) ->
    case filename:extension(Path) of
        [] -> Path ++ Ext;
        [$.|_]=E -> lists:foldl(fun(_,Acc) -> lists:droplast(Acc) end, Path, lists:seq(1,length(E))) ++ Ext
    end.

%% ------------------------------
%% drops last N path parts
%% ------------------------------

drop_last_subdir(Path, 0) -> Path;
drop_last_subdir(Path, N)
  when is_integer(N) ->
    drop_last_subdir(
      lists:droplast(
        lists:reverse(
          lists:dropwhile(fun($/) -> false; (_) -> true end, lists:reverse(Path)))), N-1).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Filepath helpers Test
%% ====================================================================

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

get_filename_test_() ->
    {"get_filename test",
     [
      ?_assertEqual("file", get_filename("/f1/f2/file")),
      ?_assertEqual("file", get_filename("file")),
      ?_assertEqual("", get_filename(""))
     ]}.

get_directory_test_() ->
    {"get_directory test",
     [
      ?_assertEqual("/f1/f2/", get_directory("/f1/f2/file")),
      ?_assertEqual("", get_directory("file")),
      ?_assertEqual("", get_directory(""))
     ]}.

%% trim_left_slashes
trim_left_slashes_test_() ->
    {"trim_left_slashes test",
     [
      ?_assertEqual("f1/f2//", trim_left_slashes("////f1/f2//")),
      ?_assertEqual("f1/f2", trim_left_slashes("f1/f2")),
      ?_assertEqual("", trim_left_slashes("/")),
      ?_assertEqual("", trim_left_slashes(""))
     ]}.


%% drop_last_path

drop_last_path_test_() ->
    {"drop_last_path test",
     [
      ?_assertEqual("/f1/f2", drop_last_path("/f1/f2/file")),
      ?_assertEqual("", drop_last_path("file")),
      ?_assertEqual("", drop_last_path(""))
     ]}.
trim_right_slashes_test_() ->
    {"trim_right_slashes test",
     [
      ?_assertEqual("/f1/f2", trim_right_slashes("/f1/f2//")),
      ?_assertEqual("/f1/f2", trim_right_slashes("/f1/f2")),
      ?_assertEqual("", trim_right_slashes("/")),
      ?_assertEqual("", trim_right_slashes(""))
     ]}.

%% change_extension

change_extension_test_() ->
    {"change_extension test",
     [
      ?_assertEqual("filename.ext", change_extension("filename", ".ext")),
      ?_assertEqual("filename.ext", change_extension("filename", "ext")),
      ?_assertEqual("filename.ext", change_extension("filename.old", ".ext")),
      ?_assertEqual("filename.ext", change_extension("filename.old", "ext")),
      ?_assertEqual("filename", change_extension("filename.old", "")),
      ?_assertEqual("filename", change_extension("filename", ""))
     ]}.

%% drop_last_subdir

drop_last_subdir_test_() ->
    {"drop_last_subdir test",
     [
      ?_assertEqual("/f1/f2/f3", drop_last_subdir("/f1/f2/f3", 0)),                 %% path returned without changes
      ?_assertEqual("/f1/f2", drop_last_subdir("/f1/f2/f3", 1)),                    %% path returned without last one dir
      ?_assertEqual("/f1/f2/f3", drop_last_subdir("/f1/f2/f3/", 1)),                %% behavior changing trap
      ?_assertException(error, function_clause, drop_last_subdir("/f1/f2/f3", 4))    %% function failed with exception! error:function_clause
     ]}.

-endif.

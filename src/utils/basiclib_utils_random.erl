%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Random values. Generation, sorting

-module(basiclib_utils_random).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Random
%% ------------------------------

random(Max) -> rand:uniform(Max).
%random(Max) when Max < 4294967296 -> erlang:phash2(make_ref(), 4294967296) rem Max;
%random(Max) -> erlang:phash2(make_ref(), 4294967296) * erlang:phash2(make_ref(), 4294967296) rem Max.
random(From,From) -> From;
random(From,To) -> From + random(To - From).

%% -----------------------------
%% Randomizes, Shuffles list by random
%% -----------------------------

randomize([]) -> [];
randomize([A]) -> [A];
randomize(List) when is_list(List) ->
   D = lists:map(fun(A) ->
                    {erlang:phash2(make_ref()), A}
                 end, List),
   {_, D1} = lists:unzip(lists:keysort(1, D)),
   D1.

shuffle([]) -> [];
shuffle([A]) -> [A];
shuffle(List) when is_list(List) ->
%% Determine the log n portion then randomize the list.
   randomize(round(math:log(length(List)) + 0.5), List).

randomize(1, List) ->
   randomize(List);
randomize(T, List) ->
   lists:foldl(fun(_E, Acc) ->
                  randomize(Acc)
               end, randomize(List), lists:seq(1, (T - 1))).

% randomize by seed (result same order of elements when same seed value)
randomize_seed(Seed,List) ->
    D = lists:map(fun(A) ->
        {erlang:phash2({Seed,A}), A}
                  end, List),
    {_, D1} = lists:unzip(lists:keysort(1, D)),
    D1.

%% return bytes count = RandomBytesCount * 4 / 3
random_b64(RandomBytesCount) when is_integer(RandomBytesCount), RandomBytesCount > 0 ->
    base64:encode(crypto:strong_rand_bytes(RandomBytesCount)).

%% return bytes
random_b62(SymbolCnt) when is_integer(SymbolCnt), SymbolCnt > 0 ->
    lists:flatten(
        lists:foldl(fun(_,Acc) ->
                        case random(0,62) of
                            X when X < 10 -> [string:chars(X+48, 1)|Acc];
                            X when X < 36 -> [string:chars(X-10+65, 1)|Acc];
                            X when X < 62 -> [string:chars(X-36+97, 1)|Acc]
                        end end, [], lists:seq(1,SymbolCnt))).


%% ====================================================================
%% Internal functions
%% ====================================================================


%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% ==================================

random_1_test_() ->
    {"random/1 test",
     [
      fun() ->
              Max = 100,
              lists:foreach(fun(_) ->
                                    R = ?MODULE:random(Max),
                                    ?assert(R >= 0),
                                    ?assert(R < Max)
                            end,
                            lists:seq(1,Max * 1000))
      end
     ]
    }.

%% ==================================

random_2_test_() ->
    {"random/2 test",
     [
      % when To == From there's a fallback
      ?_assertEqual(whatever, ?MODULE:random(whatever, whatever)),
      % when To = From + 1 the value is always From
      ?_assertEqual(5, ?MODULE:random(5, 6)),
      fun() ->
              Max = 100,
              lists:foreach(fun(_) ->
                                    ?assert(?MODULE:random(Max, Max+1) == Max)
                            end,
                            lists:seq(1,Max * 1000))
      end,
      % value must be within Min and Max range
      fun() ->
              Min = 50,
              Max = 100,
              lists:foreach(fun(_) ->
                                    R = ?MODULE:random(Min, Max),
                                    ?assert(R >= Min),
                                    ?assert(R < Max)
                            end,
                            lists:seq(1,(Max - Min) * 1000))
      end,
      % reverse Min and Max
      fun() ->
              Min = 50,
              Max = 100,
              lists:foreach(fun(_) ->
                                    R = ?MODULE:random(Min, Max),
                                    ?assert(R >= Min),
                                    ?assert(R < Max)
                            end,
                            lists:seq(1,(Max - Min) * 1000))
      end,
      % negative values
      fun() ->
              Min = -100,
              Max = -50,
              lists:foreach(fun(_) ->
                                    R = ?MODULE:random(Min, Max),
                                    ?assert(R >= Min),
                                    ?assert(R < Max)
                            end,
                            lists:seq(1,(Max - Min) * 1000))
      end
      % negative reverse Min and Max -- does not work, do not test
      %fun() ->
      %        Min = -50,
      %        Max = -100,
      %        lists:foreach(fun(_) ->
      %                              R = ?MODULE:random(Min, Max),
      %                              ?debugFmt("random(Min=~tp, Max=~tp) => ~tp", [Min, Max, R]),
      %                              ?assert(R >= Min),
      %                              ?assert(R < Max)
      %                      end,
      %                      lists:seq(1,(Min - Max) * 1000))
      %end
     ]
    }.

%% ==================================

randomize_test_() ->
    {"randomize/1 test",
     [
      fun() ->
              lists:foreach(fun(_) ->
              test_randomize_list(fun randomize/1, 20)
                            end,
                            lists:seq(1,100))
      end
     ]
    }.

%% ==================================

shuffle_test_() ->
    {"shuffle/1 test",
     [
      fun() ->
              lists:foreach(fun(_) ->
              test_randomize_list(fun shuffle/1, 5)
                            end,
                            lists:seq(1,100))
      end
     ]
    }.

%% ====================================================================
%% Internal Test functions
%% ====================================================================

test_randomize_list(RandomizeFunc, Limit) ->
    List = [a,b,c,d,e,f],
    %% at first it seemed randmized result must never equal original
    %lists:foreach(fun(_) ->
    %                      ?assertNotEqual(List, ?MODULE:randomize(List))
    %              end,
    %              lists:seq(1, 1000))
    %% but then it became clear this case actually must happen
    Count = lists:foldl(fun(_, Acc) ->
                                case RandomizeFunc(List) of
                                    List -> Acc + 1;
                                    _ -> Acc
                                end
                        end,
                        0,
                        lists:seq(1, 1000)),
    % 6 items in the list yield 720 permutations.
    % with perfect randrom it would mean exactly 1 case when done 720 iterations.
    % in reality, 0 to 2 cases for 1000 iterations is obviously a common case
    % but the edge case seems to be ~ 2.375 so not more than 3.
    % .. and yet I've seen values up to 5.
    %?debugFmt("randomize/shuffle same permutaitons count = ~tp vs limit = ~tp", [Count, Limit]),
    case Count =< Limit of
        false -> ?debugFmt("randomized list ended up same as original ~tp times out of max expected ~tp times", [Count, Limit]);
        _ -> ok
    end,
    ?assert(Count =< Limit).

-endif.

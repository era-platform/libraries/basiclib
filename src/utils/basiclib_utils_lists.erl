%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.09.2020
%%% @doc List/set operations

-module(basiclib_utils_lists).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------
%% Drop duplicates from list, store sequence
%% ----------------------------------
deduplicate(List) ->
    A = lists:zip(lists:seq(1,length(List)),List),
    B = lists:ukeysort(2,A),
    C = lists:keysort(1,B),
    element(2,lists:unzip(C)).

%% ----------------------------------
%% Drop duplicates from keylist, store sequence
%% ----------------------------------
keydeduplicate(Pos,List) ->
    F = fun(Item, {Acc,Dict}) ->
            Key = element(Pos,Item),
            case dict:is_key(Key,Dict) of
                true -> {Acc,Dict};
                false -> {[Item|Acc],dict:store(Key,1,Dict)}
            end end,
    lists:reverse(element(1,lists:foldl(F, {[],dict:new()}, List))).

%% ----------------------
%% Mix two lists
%% ----------------------
mix(List1, List2) ->
    lists:foldr(
        fun(Item1, Acc1) ->
            lists:foldr(
                fun(Item2, Acc2) ->
                    [{Item1,Item2} | Acc2]
                end, Acc1, List2)
        end, [], List1).

%% ----------------------
%% Intersect
%% ----------------------
intersection(List1, List2) ->
    ordsets:intersection(ordsets:from_list(List1),ordsets:from_list(List2)).

intersection_in_order(List1, List2) ->
    lists:foldr(fun(Node,Acc) ->
                    case lists:member(Node,List2) of
                        false -> Acc;
                        true -> [Node|Acc]
                    end
                end, [], List1).

%% ----------------------
position(Elt,List) ->
    case lists:foldl(fun(_,{ok,Acc}) -> {ok,Acc};
                        (E,Acc) when E==Elt -> {ok,Acc+1};
                        (_,Acc) -> Acc+1
                     end, 0, List) of
        {ok,Pos} -> Pos;
        _ -> -1
    end.

%% ----------------------
%% Analyse lists of items for changes.
%% ----------------------
-spec get_changes(AllCache,ActiveCache,All,Active) -> Result when
    AllCache :: [Item],     % Total items in cache
    ActiveCache :: [Item],  % Active items in cache
    All :: [Item],          % Total items now
    Active :: [Item],       % Active items now
    Result :: [{Item,Mode}],
    Item :: term(),
    Mode :: 'na_na' |    % was inactive -> now still inactive
            'na_a'  |    % was inactive -> now active
            'na_no' |    % was inactive -> now absent
            'a_na'  |    % was active -> now inactive
            'a_a'   |    % was active -> now still active
            'a_no'  |    % was active -> now absent
            'no_na' |    % absent -> now inactive
            'no_a'.      % absent -> now active
%% ----------------------
get_changes(AllCache,ActiveCache,All,Active) ->
    {C1,C2,N1,N2} = {lists:usort(AllCache),lists:usort(ActiveCache),lists:usort(All),lists:usort(Active)},
    % subsets
    C3 = ordsets:subtract(C1,C2),  % deactivated in cache
    N3 = ordsets:subtract(N1,N2),  % deactivated now
    New = ordsets:subtract(N1,C1), % added
    Del = ordsets:subtract(C1,N1), % deleted
    Act = ordsets:subtract(N2,C2), % activated (both added and cached)
    Dea = ordsets:subtract(C2,N2), % deactivated (both deleted and deaactivated)
    % lists
    A1 = ordsets:intersection(N3,C3),  % 'na_na'
    A2 = ordsets:intersection(N2,C3),  % 'na_a'
    A4 = ordsets:intersection(N3,C2),  % 'a_na'
    A5 = ordsets:intersection(N2,C2),  % 'a_a'
    A6 = ordsets:intersection(Del,Dea), % 'a_no'
    A3 = ordsets:subtract(Del,A6),  % 'na_no'
    A8 = ordsets:intersection(New,Act), % 'no_a'
    A7 = ordsets:subtract(New,A8),  % 'no_na'
    % map lists to make [{Gate,Type}]
    lists:foldl(fun({L,T}, Acc) ->
        lists:map(fun(I) -> {I,T} end, L) ++ Acc end,
                  [], [{A1,'na_na'},
                       {A2,'na_a'},
                       {A3,'na_no'},
                       {A4,'a_na'},
                       {A5,'a_a'},
                       {A6,'a_no'},
                       {A7,'no_na'},
                       {A8,'no_a'}]).

%% ----------------------
%% Partition 2 lists by modified state
%% ----------------------
juxtapose(OldList, NewList, FunId) when is_function(FunId,1) ->
    OldIds = [FunId(Item) || Item <- OldList],
    NewIds = [FunId(Item) || Item <- NewList],
    DeletedIds = OldIds -- NewIds,
    AddedIds = NewIds -- OldIds,
    {Deleted,OldRemain} = lists:partition(fun(Item) -> lists:member(FunId(Item), DeletedIds) end, OldList),
    {Added,NewRemain} = lists:partition(fun(Item) -> lists:member(FunId(Item), AddedIds) end, NewList),
    Modified = NewRemain -- OldRemain,
    NonModified = NewRemain -- Modified,
    {ok,Added,Deleted,NonModified,Modified}.

%% ----------------------
%% Partition 2 lists by modified state
%% ----------------------
juxtapose_ex(OldList, NewList, FunId) ->
    Old = [{FunId(Item),Item} || Item <- OldList],
    New = [{FunId(Item),Item} || Item <- NewList],
    OldIds = lists:map(fun({K,_}) -> K end, Old),
    NewIds = lists:map(fun({K,_}) -> K end, New),
    DeletedIds = OldIds -- NewIds,
    AddedIds = NewIds -- OldIds,
    {Deleted,OldRemain} = lists:partition(fun({Id,_Item}) -> lists:member(Id,DeletedIds) end, Old),
    {Added,NewRemain} = lists:partition(fun({Id,_Item}) -> lists:member(Id,AddedIds) end, New),
    Modified = NewRemain -- OldRemain,
    NonModified = NewRemain -- Modified,
    ModifiedMix = lists:map(fun({Id,NewItem}) -> {_,OldItem} = lists:keyfind(Id,1,Old), {OldItem,NewItem} end, Modified),
    X = fun(List) -> lists:map(fun({_,Item}) -> Item end, List) end,
    {ok,X(Added),X(Deleted),X(NonModified),ModifiedMix}.

%% ----------------------
%% Modify list of objects by nesting
%% ----------------------
nest_items(Items,AdapterMap) ->
    EmptyId = maps:get(emptyid,AdapterMap),
    FunName = maps:get(fun_name,AdapterMap),
    FunId = maps:get(fun_id,AdapterMap),
    FunParent = maps:get(fun_parent,AdapterMap),
    FunProps = maps:get(fun_properties,AdapterMap),
    FunPropName = maps:get(fun_propname,AdapterMap),
    FunSetProps = maps:get(fun_set_properties,AdapterMap),
    FunLogWarn = maps:get(fun_logwarning,AdapterMap,fun(_) -> ok end),
    % nesting
    ItemsHash = lists:map(fun(Item) -> {FunId(Item),FunParent(Item),Item,[]} end, Items),
    {Top,Nested} = lists:partition(fun({_Id,ParentId,_Item,_NestedProps}) -> ParentId==EmptyId end, ItemsHash),
    F = fun F(Ready,Remain) ->
                Res = lists:foldl(fun({Id,ParentId,Item,_}=Elt,{ReadyAcc,RemainAcc}) ->
                                        case lists:keyfind(ParentId,1,Ready) of
                                            false -> {ReadyAcc,[Elt|RemainAcc]};
                                            {_,_,ParItem,NestedProps} ->
                                                {[{Id,ParentId,Item,[FunProps(ParItem)|NestedProps]}|ReadyAcc], RemainAcc}
                                        end
                                  end, {[],[]}, Remain),
                case Res of
                    {ReadyX,[]} ->
                        % final
                        Ready++lists:reverse(ReadyX);
                    {[],RemainX} ->
                        % some nesting errors
                        case lists:partition(fun({_,ParentId,_,_}) -> not lists:keymember(ParentId,1,RemainX) end, lists:reverse(RemainX)) of
                            {[],RemainY} when is_function(FunLogWarn,1) ->
                                % remain internal cycling
                                FunLogWarn(?BU:str("WARNING! Found nesting cycle. Skipped: ~n\t~ts",
                                                   [?BU:join_binary([FunName(Item) || {_,_,Item,_} <- RemainY], <<", ">>)])),
                                Ready;
                            {ReadyY,RemainY} when is_function(FunLogWarn,1) ->
                                % found broken nesting from unknown id
                                FunLogWarn(?BU:str("WARNING! Found broken nesting from unknown id: ~ts. Skipped nested: ~ts",
                                                   [?BU:join_binary([?BU:to_binary(FunName(Item)) || {_,_,Item,_} <- ReadyY], <<", ">>),
                                                    ?BU:join_binary([?BU:to_binary(FunName(Item)) || {_,_,Item,_} <- RemainY], <<", ">>)])),
                                Ready;
                            _ ->
                                Ready
                        end;
                    {ReadyX,RemainX} ->
                        % next iteration
                        F(Ready++lists:reverse(ReadyX),lists:reverse(RemainX))
                end
        end,
    Ready = F(Top,Nested),
    lists:map(fun({_,_,Item,NestedProps}) ->
                    Props = FunProps(Item),
                    NestSeq = lists:foldl(fun(NProps,Acc) -> [NProps|Acc] end, [Props], NestedProps),
                    NestSeq1 = [[{FunPropName(P),P} || P <- NProps] || NProps <- NestSeq],
                    AllProps = lists:foldl(fun(NProps,Acc) ->
                                                lists:foldl(fun({PName,P},Acc1) ->
                                                                lists:keystore(PName,1,Acc1,{PName,P})
                                                            end, Acc, NProps)
                                           end, [], NestSeq1),
                    FunSetProps(Item,[P || {_,P} <- AllProps])
              end, Ready).

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

deduplicate_test_() ->
    {"deduplicate test",
     [?_assertEqual(["a","asdf","xcv","3t3","zbsd","255"], deduplicate(["a","asdf","xcv","a","3t3","zbsd","xcv","a","255"])),
      ?_assertEqual(["a","b","c","d"], deduplicate(["a","b","c","d"])),
      ?_assertEqual(["d","c","b","a"], deduplicate(["d","c","b","a"])),
      ?_assertEqual([], deduplicate([]))
     ]}.

keydeduplicate_test_() ->
    {"keydeduplicate test",
     [?_assertEqual([{"a",1},{"b",2},{"c",4}], keydeduplicate(1,[{"a",1},{"b",2},{"a",3},{"c",4},{"b",1}])),
      ?_assertEqual([{"d",10},{"b",2},{"a",3},{"c",4}], keydeduplicate(1,[{"d",10},{"b",2},{"a",3},{"a",3},{"c",4},{"b",1}])),
      ?_assertEqual([{10,"d"},{2,"b"},{3,"a"},{3,"c"}], keydeduplicate(2,[{10,"d"},{2,"b"},{3,"a"},{3,"c"},{4,"a"}])),
      ?_assertEqual([], keydeduplicate(1,[]))
     ]}.

position_test_() ->
    {"position test",
     [?_assertEqual(1, position(a,[a,b,c])),
      ?_assertEqual(3, position(c,[a,b,c])),
      ?_assertEqual(-1, position(d,[a,b,c])),
      ?_assertEqual(-1, position(d,[]))
     ]}.

juxtapose_test_() ->
    {"juxtapose test",
     [?_assertEqual({ok,[#{id=>1,n=>a}],[],[],[]}, juxtapose([],[#{id=>1,n=>a}],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[#{id=>1,n=>a}],[],[]}, juxtapose([#{id=>1,n=>a}],[],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[],[#{id=>1,n=>a}],[]}, juxtapose([#{id=>1,n=>a}],[#{id=>1,n=>a}],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[],[],[#{id=>1,n=>b}]}, juxtapose([#{id=>1,n=>a}],[#{id=>1,n=>b}],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[],[],[{1,b}]}, juxtapose([{1,a}],[{1,b}],fun({Id,_}) -> Id end)),
      ?_assertEqual({ok,[d],[a],[c,b],[]}, juxtapose([a,b,c],[d,c,b],fun(Id) -> Id end))
     ]}.

juxtapose_ex_test_() ->
    {"juxtapose_ex test",
     [?_assertEqual({ok,[#{id=>1,n=>a}],[],[],[]}, juxtapose_ex([],[#{id=>1,n=>a}],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[#{id=>1,n=>a}],[],[]}, juxtapose_ex([#{id=>1,n=>a}],[],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[],[#{id=>1,n=>a}],[]}, juxtapose_ex([#{id=>1,n=>a}],[#{id=>1,n=>a}],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[],[],[{#{id=>1,n=>a},#{id=>1,n=>b}}]}, juxtapose_ex([#{id=>1,n=>a}],[#{id=>1,n=>b}],fun(Item) -> maps:get(id,Item) end)),
      ?_assertEqual({ok,[],[],[],[{{1,a},{1,b}}]}, juxtapose_ex([{1,a}],[{1,b}],fun({Id,_}) -> Id end)),
      ?_assertEqual({ok,[d],[a],[c,b],[]}, juxtapose_ex([a,b,c],[d,c,b],fun(Id) -> Id end))
     ]}.

nest_items_test_() ->
    EmptyId = 0,
    AM = #{emptyid => EmptyId,
           fun_name => fun(Item) -> maps:get('cn',Item) end,
           fun_id => fun(Item) -> maps:get('id',Item) end,
           fun_parent => fun(Item) -> maps:get('par',Item,EmptyId) end,
           fun_properties => fun(Item) -> maps:get('props',Item) end,
           fun_set_properties => fun(Item,Props) -> Item#{props => Props} end,
           fun_propname => fun(Prop) -> maps:get('n',Prop) end},
    %
    Dat1 = [],
    Res1 = [],
    %
    Dat2 = [#{id=>1,cn=>1,props=>[]}],
    Res2 = [#{id=>1,cn=>1,props=>[]}],
    %
    Dat3 = [#{id=>1,cn=>1,par=>1,props=>[]}],
    Res3 = [],
    %
    Dat4 = [#{id=>1,cn=>1,par=>2,props=>[]}],
    Res4 = [],
    %
    Dat5 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12}]},
            #{id=>2,cn=>2,props=>[#{n=>p21},#{n=>p22}]}],
    Res5 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12}]},
            #{id=>2,cn=>2,props=>[#{n=>p21},#{n=>p22}]}],
    %
    Dat6 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p21},#{n=>p22}]}],
    Res6 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p11},#{n=>p12},#{n=>p21},#{n=>p22}]}],
    %
    Dat7 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p21},#{n=>p22},#{n=>p12,x=>2}]}],
    Res7 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22}]}],
    %
    Dat8 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p21},#{n=>p22},#{n=>p12,x=>2}]},
            #{id=>3,cn=>3,par=>2,props=>[#{n=>p31}]}],
    Res8 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22}]},
            #{id=>3,cn=>3,par=>2,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22},#{n=>p31}]}],
    %
    Dat9 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p21},#{n=>p22},#{n=>p12,x=>2}]},
            #{id=>3,cn=>3,par=>2,props=>[#{n=>p31}]},
            #{id=>4,cn=>4,props=>[#{n=>p41}]}],
    Res9 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
            #{id=>4,cn=>4,props=>[#{n=>p41}]},
            #{id=>2,cn=>2,par=>1,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22}]},
            #{id=>3,cn=>3,par=>2,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22},#{n=>p31}]}],
    %
    Dat10 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
             #{id=>2,cn=>2,par=>1,props=>[#{n=>p21},#{n=>p22},#{n=>p12,x=>2}]},
             #{id=>3,cn=>3,par=>2,props=>[#{n=>p31}]},
             #{id=>4,cn=>4,par=>5,props=>[#{n=>p41}]},
             #{id=>5,cn=>5,par=>4,props=>[#{n=>p51}]}],
    Res10 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
             #{id=>2,cn=>2,par=>1,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22}]},
             #{id=>3,cn=>3,par=>2,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22},#{n=>p31}]}],
    %
    Dat11 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
             #{id=>2,cn=>2,par=>1,props=>[#{n=>p21},#{n=>p22},#{n=>p12,x=>2}]},
             #{id=>3,cn=>3,par=>2,props=>[#{n=>p31}]},
             #{id=>4,cn=>4,par=>5,props=>[#{n=>p41}]},
             #{id=>5,cn=>5,par=>1,props=>[#{n=>p51}]}],
    Res11 = [#{id=>1,cn=>1,props=>[#{n=>p11},#{n=>p12,x=>1}]},
             #{id=>2,cn=>2,par=>1,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22}]},
             #{id=>5,cn=>5,par=>1,props=>[#{n=>p11},#{n=>p12,x=>1},#{n=>p51}]},
             #{id=>3,cn=>3,par=>2,props=>[#{n=>p11},#{n=>p12,x=>2},#{n=>p21},#{n=>p22},#{n=>p31}]},
             #{id=>4,cn=>4,par=>5,props=>[#{n=>p11},#{n=>p12,x=>1},#{n=>p51},#{n=>p41}]}],
    %
    {"nest_items test",
        [?_assertEqual(Res1, nest_items(Dat1,AM)),
            ?_assertEqual(Res2, nest_items(Dat2,AM)),
            ?_assertEqual(Res3, nest_items(Dat3,AM)),
            ?_assertEqual(Res4, nest_items(Dat4,AM)),
            ?_assertEqual(Res5, nest_items(Dat5,AM)),
            ?_assertEqual(Res6, nest_items(Dat6,AM)),
            ?_assertEqual(Res7, nest_items(Dat7,AM)),
            ?_assertEqual(Res8, nest_items(Dat8,AM)),
            ?_assertEqual(Res9, nest_items(Dat9,AM)),
            ?_assertEqual(Res10, nest_items(Dat10,AM)),
            ?_assertEqual(Res11, nest_items(Dat11,AM))
        ]}.

-endif.

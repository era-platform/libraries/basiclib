%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Timers management, waiting, cyclic starts

-module(basiclib_utils_timer).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Timer operations
%% ------------------------------

%% cancels timer if it is reference
cancel_timer(undefined) -> ok;
cancel_timer(Ref) when is_reference(Ref) -> erlang:cancel_timer(Ref);
cancel_timer(_) -> ok.

%% ------------------------------
%% return next pause in growing list (from 0)
%% ex. Starting=10, Levels=5 => [10, 20, 40, 80, 160, 320, 320, 320, ....]
%% ex. Starting=1000, Levels=3 => [1000, 2000, 4000, 8000, 8000, 8000, ...]
%% ------------------------------
growing_pause(Retry,Levels,Starting)
  when is_integer(Retry), is_integer(Levels), is_integer(Starting),
       Retry>=0, Levels>0, Starting>0 ->
    erlang:round(math:pow(2,min(Retry,Levels))*Starting).

%% -------------------------
%% blocks process, wait for Fun to return true by periodical execute
%%   till timeout expire
%% -------------------------
-spec wait(function(),integer(),integer()) -> boolean().
%% -------------------------
wait(Fun,PeriodMs,TimeoutMs) ->
    wait_expired(Fun,0,PeriodMs,?BU:timestamp()+TimeoutMs).
%% @private
wait_expired(Fun,IterNo,PeriodMs,ExpireTS) when is_function(Fun,0); is_function(Fun,1) ->
    TS = ?BU:timestamp(),
    case apply_fun(Fun,IterNo) of
        true -> true;
        false when TS<ExpireTS ->
            timer:sleep(PeriodMs),
            wait_expired(Fun,IterNo+1,PeriodMs,ExpireTS);
        false -> false
    end.
%% @private
apply_fun(Fun,_IterNo) when is_function(Fun,0) -> Fun();
apply_fun(Fun,IterNo) when is_function(Fun,1) -> Fun(IterNo).

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

growing_pause_test_() ->
    {"growing pause",
       [?_assertEqual(10, growing_pause(0,5,10)),
      ?_assertEqual(10, growing_pause(0,5,10)),
      ?_assertEqual(20, growing_pause(1,5,10)),
      ?_assertEqual(320, growing_pause(5,5,10)),
      ?_assertEqual(320, growing_pause(8,5,10)),
      ?_assertEqual(4000, growing_pause(2,3,1000)),
      ?_assertEqual(8000, growing_pause(10,3,1000))
      ]}.

wait_expired_test_() ->
    F = fun(3) -> true; (_) -> false end,
    {"wait_expired",
       [?_assertMatch({_,true}, case timer:tc(fun() -> wait(F,100,500) end) of {T,true} when T>300000,T<500000 -> {T,true}; _ -> error end),
      ?_assertMatch({_,false}, case timer:tc(fun() -> wait(F,100,200) end) of {T,false} when T>200000 -> {T,false}; _ -> error end)
      ]}.

-endif.


%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Operations with files on disk

-module(basiclib_utils_disk).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% File copy
%% ------------------------------
file_copy(Src,Dst) ->
    ?BLfilelib:ensure_dir(Dst),
    case ?BLfile:copy(Src,Dst) of
        {ok,R} ->
            {ok,FInfo} = ?BLfile:read_file_info(Src),
            ?BLfile:write_file_info(Dst, FInfo),
            {ok,R};
        T -> T
    end.

%% ------------------------------
%% File md5 pretty binary string
%% ------------------------------
-spec file_md5_pretty(FilePath::file:name_all()) -> {ok,MD5_PRETTY::string()} | {error,Reason::file:posix()|atom()}.
%% ------------------------------
file_md5_pretty(FilePath) ->
    case file_md5(FilePath) of
        {error,_}=Err -> Err;
        {ok,MD5} ->
            MD5Hex = [begin if N < 10 -> 48 + N; true -> 87 + N end end || <<N:4>> <= MD5],
            {ok, string:to_upper(MD5Hex)}
    end.

%% ------------------------------
%% File md5 raw bytes
%% ------------------------------
-define(MD5_READ_CHUNK_SIZE, 256*1024).
-spec file_md5(FilePath::file:name_all()) -> {ok,RawMD5::binary()} | {error,Reason::file:posix()|atom()}.
%% ------------------------------
file_md5(FilePath) ->
    case ?BLfile:open(FilePath, [read, raw, binary]) of
        {ok, IODevice} ->
            F = fun F(ContextAcc) ->
                        case ?BLfile:read(IODevice, ?MD5_READ_CHUNK_SIZE) of
                            {ok, Data} ->
                                NewContext = crypto:hash_update(ContextAcc, Data),
                                F(NewContext); % recurse
                            eof -> {md5, crypto:hash_final(ContextAcc)};
                            {error,_}=RErr -> RErr
                        end end,
            Res = case F(crypto:hash_init(md5)) of
                      {md5,MD5} -> {ok,MD5};
                      {error,_}=Err -> Err
                  end,
            ?BLfile:close(IODevice),
            Res;
        {error,_}=Err -> Err
    end.

%% ------------------------------
%% File age in seconds
%% ------------------------------
-spec file_age(FilePath::file:name_all()) -> {ok,Seconds::integer()} | {error,Reason::file:posix()|badarg}.
%% ------------------------------
file_age(FilePath) ->
    case ?BLfile:read_file_info(FilePath, [{time,universal}]) of
        {ok,#file_info{mtime=MTime}} ->
            Now = calendar:now_to_universal_time(os:timestamp()),
            MTimeSec = calendar:datetime_to_gregorian_seconds(MTime),
            NowSec = calendar:datetime_to_gregorian_seconds(Now),
            {ok, NowSec - MTimeSec};
        Err -> Err
    end.

%% --------------------------------------
%% File combined information: size, mtime, md5 in a map.
%% Opts can be used to customize the output:
%% - {skip_hasha,true} to avoid reading the file content, 'hasha' key will be missing in result.
%% --------------------------------------
-spec file_info_hasha(FilePath::binary(), Opts::map()) -> {ok,map()} | {error,Reason::atom()}.
%% --------------------------------------
file_info_hasha(FilePath, Opts) ->
    case ?BLfile:read_file_info(FilePath, [{time,universal}]) of
        {ok,#file_info{mtime=MTime,size=Size}} ->
            Info = #{size => Size,
                     mtime => ?BU:strdatetime3339(MTime)},
            case maps:get(skip_hasha, Opts, false) of
                true -> {ok, Info};
                false ->
                    {ok,MD5Pretty} = file_md5_pretty(FilePath),
                    MD5StrHash = [$m,$d,$5,$; | MD5Pretty],
                    {ok,Info#{hasha => ?BU:to_binary(MD5StrHash)}}
            end;
        ErrT -> ErrT
    end.

%% @public - default empty Opts.
file_info_hasha(FilePath) -> file_info_hasha(FilePath, #{}).

%% ------------------------------
%% File read starting From bytes before file end.
%% ------------------------------

file_read_endpart(FilePath,From) ->
    ?BLopts:set_group_leader(),
    {ok,IoDevice} = file:open(FilePath,[read,binary]),
    Size = filelib:file_size(FilePath),
    Res = file:pread(IoDevice,{bof,From},Size-From),
    file:close(IoDevice),
    Res.

%% ------------------------------------
%% Directory operations
%% ------------------------------------

directory_delete(Dir) ->
    Paths = ?BLfilelib:wildcard(?BU:to_unicode_list(Dir) ++ "/**"),
    {Dirs, Files} = lists:partition(fun ?BLfilelib:is_dir/1, Paths),
    ok = lists:foreach(fun ?BLfile:delete/1, Files),
    Sorted = lists:reverse(lists:sort(Dirs)),
    ok = lists:foreach(fun ?BLfile:del_dir/1, Sorted),
    ?BLfile:del_dir(Dir).

directory_delete_empties(Dir) ->
    Paths = ?BLfilelib:wildcard(Dir ++ "/**"),
    Dirs = lists:filter(fun ?BLfilelib:is_dir/1, Paths),
    Sorted = lists:reverse(lists:sort(Dirs)),
    lists:foreach(fun ?BLfile:del_dir/1, Sorted).

directory_copy(From,To) ->
    ?BLfilelib:ensure_dir(?BU:to_unicode_list(To)),
    case os:type() of
        {unix,_} -> os:cmd(?BU:str("cp -rT ~s ~s",[From,To]));
        {win32} -> os:cmd(?BU:str("Xcopy /E /I /Y ~s ~s",[From,To]))
    end.

%% -------------------------------------
%% Disk space where directory is located
%% -------------------------------------

-spec get_disk_space(Dir::string() | atom()) -> {DiskName::string(), TotalKB::integer(), UsedPercent::integer()} | undefined.

get_disk_space(cwd) ->
    {ok,CWD} = ?BLfile:get_cwd(),
    get_disk_space(CWD);
get_disk_space(source) ->
    {file,Dir} = code:is_loaded(?MODULE),
    get_disk_space(Dir);
get_disk_space(log) ->
    %get_disk_space(cwd),
    LogDir = ?BLlog:get_basic_log_dir(),
    get_disk_space(LogDir);
get_disk_space(Dir) when is_list(Dir) ->
    Funix = fun Funix(Dir1) ->
                case ?BLfilelib:is_dir(Dir1) of
                    false -> Funix(filename:dirname(Dir1));
                    true ->
                        case ?BU:cmd_exec(?BU:str("df -lP ~ts | tail -1",[Dir1])) of
                            {0,Data} ->
                                Tokens = string:tokens(?BU:to_list(Data)," \n"),
                                Size = lists:nth(2,Tokens),
                                [Perc] = string:tokens(lists:nth(5,Tokens),"%"),
                                Mount = lists:nth(6,Tokens),
                                {Mount,?BU:to_int(Size),?BU:to_int(Perc)};
                            {_,_} -> 0
                        end end end,
    Fwin = fun(_Dir1) ->
                DD = disksup:get_disk_data(),
                F = fun({X,_,_}=Itm, {_,_Len}=Acc) when length(X)>_Len ->
                    Pref = ?BU:to_list(binary:replace(?BU:to_binary(X), <<"\\">>, <<"/">>, [global])),
                    case lists:prefix(?BU:lowercase(Pref),?BU:lowercase(Dir)) of
                        true -> {Itm,length(Pref)};
                        false -> Acc
                    end;
                    (_,Acc) -> Acc
                    end,
                element(1,lists:foldl(F, {undefined,0}, DD))
           end,
    Fdef = Fwin,
    try
        F = fun() ->
            try case os:type() of
                    {unix,_} -> Funix(Dir);
                    {win32,_} -> Fwin(Dir)
                end
            catch _:_ -> Fdef()
            end end,
        Key = {disk_space,Dir},
        ?BLstore:lazy_t(Key, F, {20000,10000,2000}, {true,true})
    catch _:_ -> Fdef()
    end.


%% -------------------------------------
%% Disks space info
%% -------------------------------------

-spec get_disk_data() -> {DiskName::string(), TotalKB::integer(), UsedPercent::integer()} | undefined.

get_disk_data() ->
    Funix = fun() ->
        case ?BU:cmd_exec("df -al") of
            {0,Data} ->
                Strs = case binary:split(Data,<<"\n">>,[global]) of
                           Strs0 when length(Strs0) > 2 -> lists:reverse(lists:nthtail(1,lists:reverse(lists:nthtail(1,Strs0))));
                           _ -> []
                       end,
                lists:foldr(fun(Str,Acc) ->
                    Tokens = string:tokens(?BU:to_list(Str)," \n"),
                    Size = lists:nth(2,Tokens),
                    [Perc] = string:tokens(lists:nth(5,Tokens),"%"),
                    Mount = lists:nth(6,Tokens),
                    case {?BU:to_int(Size,un),?BU:to_int(Perc,un)} of
                        {un,_} -> Acc;
                        {_,un} -> Acc;
                        {SizeI,PercI} -> [{Mount,SizeI,PercI} | Acc]
                    end end, [], Strs);
            {_,_} -> []
        end end,
    Fwin = fun() -> disksup:get_disk_data() end,
    Fdef = Fwin,
    try
        case os:type() of
            {unix,_} -> Funix();
            {win32,_} -> Fwin()
        end
    catch _:_ -> Fdef()
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

file_copy_test_() ->
    {"file copy",
     [fun() ->
       file:write_file("testfile1",<<"abc">>),
       file_copy("testfile1","testfile2"),
       {ok, Data} = file:read_file("testfile2"),
       Res = ?assertEqual(<<"abc">>,Data),
       file:delete("testfile1"),
       file:delete("testfile2"),
       Res
      end]}.

directory_delete_test_() ->
    {"directory delete",
     [fun() ->
       file:make_dir("testdir"),
       file:write_file("testdir/.dot-testfile3",<<"abcdef">>),
       file:write_file("testdir/testfile3",<<"abc">>),
       directory_delete("testdir"),
       {Result,_} = file:read_file("testdir/testfile3"),
       ?assertEqual(error,Result)
     end]}.

directory_delete_empties_test_() ->
    {"directory delete empties",
     [fun() ->
       file:make_dir("testdir"),
       file:write_file("testdir/testfile4",<<"abc">>),
       directory_delete_empties("testdir"),
       {Result,_} = file:read_file("testdir/testfile4"),
       TestResult = ?assertEqual(ok,Result),
       file:delete("testdir/testfile4"),
       file:del_dir("testdir"),
       TestResult
      end,
      fun() ->
       file:make_dir("testdir1"),
       file:make_dir("testdir1/testdir2"),
       directory_delete_empties("testdir1"),
       Result = file:write_file("testdir1/testdir2/testfile5",<<"abc">>),
       TestResult = ?assertEqual({error,enoent},Result),
       file:del_dir("testdir1"),
       TestResult
         end]}.


-endif.




%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Generate and verify date and time

-module(basiclib_utils_time).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(BS, bitstring).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Local time for log
%% ------------------------------
timestamp() ->
    timestamp(os:timestamp()).

timestamp(TimestampMs) when is_integer(TimestampMs), TimestampMs > 1000000000000 -> TimestampMs;
timestamp({Mega,Sec,Micro}) ->
    (Mega*1000000+Sec)*1000 + Micro div 1000.

%% ------------------------------
timestamp_mks() ->
    timestamp_mks(os:timestamp()).

timestamp_mks(TimestampMks) when is_integer(TimestampMks), TimestampMks > 1000000000000000 -> TimestampMks;
timestamp_mks(TimestampMs) when is_integer(TimestampMs), TimestampMs > 1000000000000 -> TimestampMs;
timestamp_mks({Mega,Sec,Micro}) ->
    (Mega * 1000000 + Sec) * 1000000 + Micro.

%% ------------------------------
erl_timestamp(ErlTimestampMs) when is_integer(ErlTimestampMs), ErlTimestampMs < 1000000000000000 ->
    ErlangSystemTime = ErlTimestampMs*1000,
    erl_timestamp(ErlangSystemTime);
erl_timestamp(ErlTimestampMks) when is_integer(ErlTimestampMks) ->
    MegaSecs = ErlTimestampMks div 1000000000000,
    Secs = ErlTimestampMks div 1000000 - MegaSecs*1000000,
    MicroSecs = ErlTimestampMks rem 1000000,
    {MegaSecs, Secs, MicroSecs}.

%% ------------------------------
localdatetime_ms() ->
    localdatetime_ms(os:timestamp()).

localdatetime_ms({_, _, Micro} = TimeStamp) ->
    {Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(TimeStamp),
    {Date, {Hours, Minutes, Seconds}, Micro div 1000 rem 1000};
localdatetime_ms(TimeStamp) when is_integer(TimeStamp) ->
    timestamp_to_datetime_ms(TimeStamp).

timestamp_to_datetime_ms(TimeStamp) ->
    {D,T} = timestamp_to_datetime(TimeStamp),
    {D,T,TimeStamp rem 1000}.

timestamp_to_datetime(TimeStamp) when is_integer(TimeStamp) ->
    {_,_} = calendar:gregorian_seconds_to_datetime(TimeStamp div 1000 + calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}})).

%% ------------------------------
localtime_ms() ->
    localtime_ms(os:timestamp()).

localtime_ms(TimeStamp) ->
    {_, _, Micro} = TimeStamp,
    {_Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(TimeStamp),
    {Hours, Minutes, Seconds, Micro div 1000 rem 1000}.

%% ------------------------------
localtime() ->
    calendar:local_time().

%% ------------------------------
localdate() ->
    localdate(os:timestamp()).

localdate(TimeStamp) ->
    {{Y,M,D}, _Time} = calendar:now_to_local_time(TimeStamp),
    {Y,M,D}.

%% ------------------------------
%% Time & pid for OUT log
%% ------------------------------
log_date_pid(Pid) ->
    {_, _, Micro}=TimeStamp = os:timestamp(),
    {_Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(TimeStamp),
    {H, M, S, Ms} = {Hours, Minutes, Seconds, Micro div 1000 rem 1000},
    ?BU:str("~2..0B:~2..0B:~2..0B.~3..0B\t~p \t",[H,M,S,Ms,Pid]).

%% ------------------------------
%% datetime/timestamp conversion
%% ------------------------------
datetime_to_timestamp({{Y,M,D},{H,Mi,S}}=DT) when is_integer(Y),is_integer(M),is_integer(D),is_integer(H),is_integer(Mi),is_integer(S) ->
    % 62167219200 == calendar:datetime_to_gregorian_seconds({{1970, 1, 1}, {0, 0, 0}})
    Secs = calendar:datetime_to_gregorian_seconds(DT) - 62167219200,
    {Secs div 1000000, Secs rem 1000000, 0}.

%% ------------------------------
%% UTC time
%% ------------------------------
utcdatetime_ms() ->
    utcdatetime_ms(os:timestamp()).

%% ------------------------------
utcdatetime_ms(TimeStamp) ->
    {_, _, Micro} = TimeStamp,
    {Date, {Hours, Minutes, Seconds}} = calendar:now_to_universal_time(TimeStamp),
    {Date, {Hours, Minutes, Seconds}, Micro div 1000 rem 1000}.

%% ------------------------------
utcdiff() ->
    {D,{H,M,S}} = calendar:time_difference(calendar:universal_time(), calendar:local_time()),
    {H + 24 * D, M, S}.

%% ------------------------------
timezone() ->
    {D,{H,M,S}} = calendar:time_difference(calendar:universal_time(), calendar:local_time()),
    {H + 24 * D, M, S}.

timezonesec() ->
    {D,{H,M,S}} = calendar:time_difference(calendar:universal_time(), calendar:local_time()),
    86400 * D + 3600 * H + 60* M + S.

%% ------------------------------
current_gregsecond() ->
    calendar:datetime_to_gregorian_seconds(erlang:universaltime()).

current_gregsecond(UTC) ->
    calendar:datetime_to_gregorian_seconds(UTC).

%% ------------------------------
gregsecond({{_,_,_},{_,_,_}}=Time) ->
    calendar:datetime_to_gregorian_seconds(Time);

gregsecond({{_,_,_}=D,{_,_,_}=T,_}) ->
    calendar:datetime_to_gregorian_seconds({D,T});

gregsecond({_,_,_}=TS) ->
    calendar:datetime_to_gregorian_seconds(calendar:now_to_datetime(TS)).

%% ------------------------------
strtime({{_,_,_},{H,M,S},_}) -> ?BU:strbin("~2..0B:~2..0B:~2..0B",[H,M,S]);
strtime({{_,_,_},{H,M,S}}) -> ?BU:strbin("~2..0B:~2..0B:~2..0B",[H,M,S]);
strtime({H,M,S}) -> ?BU:strbin("~2..0B:~2..0B:~2..0B",[H,M,S]).

%% ------------------------------
strdate({{Y,M,D},{_,_,_},_}) -> ?BU:strbin("~4..0B-~2..0B-~2..0B",[Y,M,D]);
strdate({{Y,M,D},{_,_,_}}) -> ?BU:strbin("~4..0B-~2..0B-~2..0B",[Y,M,D]);
strdate({Y,M,D}) -> ?BU:strbin("~4..0B-~2..0B-~2..0B",[Y,M,D]).

%% ------------------------------
-spec strdatetime(DtateTime :: calendar:datetime()) -> binary().
%% ------------------------------
strdatetime({{Y,Mo,D},{H,Mi,S}}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B", [Y,Mo,D,H,Mi,S]);
strdatetime({{Y,Mo,D},{H,Mi,S},0}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B", [Y,Mo,D,H,Mi,S]);
strdatetime({{Y,Mo,D},{H,Mi,S},Ms}) ->
    %Ms = MsX div 10,
    ?BU:strbin("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B.~3..0B", [Y,Mo,D,H,Mi,S,Ms]).

%% ------------------------------
-spec strdatetime3339(DtateTime :: {{Y,Mo,D},{H,Mi,S}} | {{Y,Mo,D},{H,Mi,S},Ms} | binary()) -> binary()
        when Y::integer(), Mo::integer(), D::integer(), H::integer(), Mi::integer(), S::integer(), Ms::integer().
%% ------------------------------
strdatetime3339() ->
    strdatetime3339(calendar:universal_time()).

strdatetime3339({{Y,Mo,D},{H,Mi,S}}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0BZ", [Y,Mo,D,H,Mi,S]);
strdatetime3339({{Y,Mo,D},{H,Mi,S},0}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0BZ", [Y,Mo,D,H,Mi,S]);
strdatetime3339({{Y,Mo,D},{H,Mi,S},Ms}) ->
    %Ms = MsX div 10,
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B.~3..0BZ", [Y,Mo,D,H,Mi,S,Ms]);
strdatetime3339(DT) when is_binary(DT) ->
    strdatetime3339(?BU:parse_datetime_utc(DT)).

%% ------------------------------
-spec strdatetime3339(DtateTime :: calendar:datetime(), {OP :: [$+ | $-], Tzh :: integer(), Tzm :: integer()}) -> binary().
%% ------------------------------
strdatetime3339({{Y,Mo,D},{H,Mi,S}},{"+",Hz,Mz}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B+~2..0B:~2..0B", [Y,Mo,D,H,Mi,S,Hz,Mz]);
strdatetime3339({{Y,Mo,D},{H,Mi,S}},{"-",Hz,Mz}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B-~2..0B:~2..0B", [Y,Mo,D,H,Mi,S,Hz,Mz]);

strdatetime3339({{Y,Mo,D},{H,Mi,S},0},{"+",Hz,Mz}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B+~2..0B:~2..0B", [Y,Mo,D,H,Mi,S,Hz,Mz]);
strdatetime3339({{Y,Mo,D},{H,Mi,S},Ms},{"+",Hz,Mz}) ->
    %Ms = MsX div 10,
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B.~3..0B+~2..0B:~2..0B", [Y,Mo,D,H,Mi,S,Ms,Hz,Mz]);
strdatetime3339({{Y,Mo,D},{H,Mi,S},0},{"-",Hz,Mz}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B-~2..0B:~2..0B", [Y,Mo,D,H,Mi,S,Hz,Mz]);
strdatetime3339({{Y,Mo,D},{H,Mi,S},Ms},{"-",Hz,Mz}) ->
    %Ms = MsX div 10,
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B.~3..0B-~2..0B:~2..0B", [Y,Mo,D,H,Mi,S,Ms,Hz,Mz]).

%% ------------------------------
utcstrdatetime() ->
    {{Y, Mo, D}, {H, Mi, S}, _} = utcdatetime_ms(),
    ?BU:str("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B", [Y, Mo, D, H, Mi, S]).

%% ------------------------------
utcbindatetime({MegaSecs,Secs,MicroSecs}=Ts) when is_integer(MegaSecs), MegaSecs>=0, is_integer(Secs), Secs>=0, is_integer(MicroSecs), MicroSecs>=0 ->
    {{Y,M,D},{H,Mi,S}} = calendar:now_to_universal_time(Ts),
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0BZ", [Y,M,D,H,Mi,S]).

%% ------------------------------
localbindatetime({MegaSecs,Secs,MicroSecs}=Ts) when is_integer(MegaSecs), MegaSecs>=0, is_integer(Secs), Secs>=0, is_integer(MicroSecs), MicroSecs>=0 ->
    {{Y,M,D},{H,Mi,S}} = calendar:now_to_local_time(Ts),
    ?BU:strbin("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B", [Y,M,D,H,Mi,S]).

%% ------------------------------
%% 2016-04-28 12:02:58
%% ------------------------------
check_utcdatetime(<<_:32/?BS,"-",_:16/?BS,"-",_:16/?BS," ",_:16/?BS,":",_:16/?BS,":",_:16/?BS>>=DT) ->
    try
        {{Y,M,D}, {_H,_Mi,_S}=T, 0} = ?BU:parse_datetime(DT),
        case calendar:valid_date(Y, M, D) of
            true -> is_time_valid(T);
            false -> false
        end
    catch _:_ -> false
    end;
%%
check_utcdatetime({{Y,M,D}, {_H,_Mi,_S}=T}) ->
    try
        case calendar:valid_date(Y, M, D) of
            true -> is_time_valid(T);
            false -> false
        end
    catch _:_ -> false
    end;
%%
check_utcdatetime({{Y,M,D}, {_H,_Mi,_S}=T,Ms}) ->
    try case calendar:valid_date(Y, M, D) of
            true ->
                case is_time_valid(T) of
                    true -> Ms =< 99;
                    false -> false
                end;
            false -> false
        end
    catch _:_ -> false
    end;
%%
check_utcdatetime(DT) ->
    try case ?BU:parse_datetime(DT) of
            {{1970,1,1},{0,0,0},0} -> false; % unless the time was really 1970/01/01 00:00:00, eh? ;)
            {{Y,M,D}, {_H,_Mi,_S}=T, 0} ->
                case calendar:valid_date(Y, M, D) of
                    true -> is_time_valid(T);
                    false -> false
                end
        end
    catch _:_ -> false
    end.

%% ------------------------------
is_time_valid({H,M,S}) when is_integer(H), is_integer(M), is_integer(S) ->
    case H >= 0 andalso H =< 23 of
        true -> case M >= 0 andalso M =< 59 of
                    true -> case S >= 0 andalso S =< 59 of
                                true -> true;
                                _ -> false
                            end;
                    _ -> false
                end;
        _ -> false
    end;
%%
is_time_valid(_) -> false.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

f_test_() ->
    {"utils_time test",
     [?_assertEqual(1513937513914, timestamp({1513,937513,914000})),
      ?_assertEqual({1513,937513,914000}, erl_timestamp(1513937513914)),
      ?_assertMatch(I when (is_integer(I) andalso I < 100 andalso I > -100), timestamp()-timestamp()),
      ?_assertMatch({H,M,S,Ms} when is_integer(H) and is_integer(M) and is_integer(S) and is_integer(Ms), localtime_ms()),
      ?_assertEqual({2017,12,22}, localdate({1513,937513,914000})),
      ?_assertEqual({{2017,12,22},{10,11,53},914}, utcdatetime_ms({1513,937513,914000})),
      ?_assertEqual(true, check_utcdatetime(<<"2016-04-28 12:02:58">>))
     ]}.

strdatetime_test_() ->
    {"srtdatetime test",
     [?_assertEqual(<<"2016-04-28 12:02:58">>, strdatetime({{2016,4,28},{12,2,58}})),
      ?_assertEqual(<<"2016-04-28 12:02:58.054">>, strdatetime({{2016,4,28},{12,2,58}, 54})),
      ?_assertEqual(<<"2016-04-28 12:02:58">>, strdatetime({{2016,4,28},{12,2,58}, 0})),
      ?_assertEqual(<<"2016-04-28T12:02:58Z">>, strdatetime3339({{2016,4,28},{12,2,58}})),
      ?_assertEqual(<<"2016-04-28T12:02:58.054Z">>, strdatetime3339({{2016,4,28},{12,2,58},54})),
      ?_assertEqual(<<"2016-04-28T12:02:58.***Z">>, strdatetime3339({{2016,4,28},{12,2,58},73739556385})),
      ?_assertEqual(<<"2016-04-28T12:02:58Z">>, strdatetime3339({{2016,4,28},{12,2,58},0})),
      ?_assertEqual(<<"2016-04-28T12:02:58+02:00">>, strdatetime3339({{2016,4,28},{12,2,58}}, {"+", 2, 0})),
      ?_assertEqual(<<"2016-04-28T12:02:58+02:00">>, strdatetime3339({{2016,4,28},{12,2,58}, 0}, {"+", 2, 0})),
      ?_assertEqual(<<"2016-04-28T12:02:58.054-03:30">>, strdatetime3339({{2016,4,28},{12,2,58},54}, {"-", 3, 30}))
     ]}.

is_time_valid_test_() ->
    {"is_time_valid test",
     [
      ?_assertEqual(true, ?MODULE:is_time_valid({0,0,0})),
      ?_assertEqual(false, ?MODULE:is_time_valid({-1,0,0})),
      ?_assertEqual(false, ?MODULE:is_time_valid({0,-1,0})),
      ?_assertEqual(false, ?MODULE:is_time_valid({0,0,-1})),
      ?_assertEqual(true, ?MODULE:is_time_valid({23,59,59})),
      ?_assertEqual(false, ?MODULE:is_time_valid({24,0,0})),
      ?_assertEqual(false, ?MODULE:is_time_valid({23,60,50})),
      ?_assertEqual(false, ?MODULE:is_time_valid({0,60,0})),
      ?_assertEqual(false, ?MODULE:is_time_valid({23,59,60})),
      ?_assertEqual(false, ?MODULE:is_time_valid({0,0,60})),
      ?_assertEqual(true, ?MODULE:is_time_valid({12,34,56}))
     ]
    }.

-endif.

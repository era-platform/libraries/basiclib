%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 2.05.2019
%%% @doc Parsing and generation of values, containing SrvIdx of current node

-module(basiclib_utils_srvidx).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([parse_srvidx/1,
         parse_textcode_to_index/1,
         gen_srvidx_value/2,
         get_current_srv_textcode/0]).

%%% ====================================================================
%%% Define
%%% ====================================================================

-include("app.hrl").

%%% ====================================================================
%%% API functions
%%% ====================================================================

%% ---------------------------------------------------
%% Extract numeric SrvIdx from DialogId
%% ---------------------------------------------------
parse_srvidx(<<"-",SrvIdxEnc:3/binary,"-",_/binary>>) -> parse_textcode_to_index(SrvIdxEnc);
parse_srvidx(<<_:1/binary,"-",SrvIdxEnc:3/binary,"-",_/binary>>) -> parse_textcode_to_index(SrvIdxEnc);
parse_srvidx(<<_:2/binary,"-",SrvIdxEnc:3/binary,"-",_/binary>>) -> parse_textcode_to_index(SrvIdxEnc);
parse_srvidx(<<_:3/binary,"-",SrvIdxEnc:3/binary,"-",_/binary>>) -> parse_textcode_to_index(SrvIdxEnc);
parse_srvidx(<<_:4/binary,"-",SrvIdxEnc:3/binary,"-",_/binary>>) -> parse_textcode_to_index(SrvIdxEnc);
parse_srvidx(<<_:5/binary,"-",SrvIdxEnc:3/binary,"-",_/binary>>) -> parse_textcode_to_index(SrvIdxEnc).

%% ---------------------------------------------------
%% Make numeric SrvIdx by TextKey
%% ---------------------------------------------------
parse_textcode_to_index(TextCode) when is_binary(TextCode) ->
    parse_textcode(TextCode, 0).

%% @private
parse_textcode(<<>>, Acc) -> Acc;
parse_textcode(<<B:1/binary, Rest/binary>>, Acc) ->
    parse_textcode(Rest, parse_textcode(?BU:to_list(B)) + Acc * 62).

%% ---------------------------------------------------
%% Build srvidx value
%% ---------------------------------------------------
gen_srvidx_value(Prefix, Postfix) ->
    TextCode = get_current_srv_textcode(),
    <<Prefix/binary,"-",TextCode/binary,"-",Postfix/binary>>.

%% ----------------------------------------------------------
%% Builds 3-sym string code by current B2BUA server index.
%% Supported srvidx up to 238328
%% ----------------------------------------------------------
get_current_srv_textcode() ->
    build_textcode_by_index(?CFG:node_index(), 3).

%% @private
build_textcode_by_index(Idx, Size) when is_integer(Idx), is_integer(Size) ->
    String = ?BU:to_list(build_textcode(Idx, <<>>)),
    ?BU:to_binary(enlarge_textcode(String, Size - length(String))).

%% @private
enlarge_textcode(List, N) when N =< 0 -> List;
enlarge_textcode(List, Size) ->
    enlarge_textcode(["0"|List], Size-1).

%% @private
build_textcode(0, Acc) -> Acc;
build_textcode(Idx, Acc) ->
    C = ?BU:to_binary(make_textcode(Idx rem 62)),
    build_textcode(Idx div 62, <<C/bitstring, Acc/bitstring>>).

%%% ====================================================================
%%% Internal functions
%%% ====================================================================

%% @private
make_textcode(Idx) when Idx < 10 -> [48 + Idx];
make_textcode(Idx) when Idx < 36 -> [65 + Idx - 10];
make_textcode(Idx) when Idx < 62 -> [97 + Idx - 36].

%% @private
parse_textcode(Code) when Code >= 48, Code < 48+10 -> Code - 48;
parse_textcode(Code) when Code >= 65, Code < 65+26 -> Code - 65 + 10;
parse_textcode(Code) when Code >= 97, Code < 97+26 -> Code - 97 + 36.



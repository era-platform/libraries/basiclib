%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Parsing and time transformations

-module(basiclib_utils_parsedt).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(BS, bitstring).
-define(BI, binary).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Parse datetime
%% ------------------------------
%% ---
%% datetime sql
%% ---
% 2016-04-28
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS>>) -> {{i(Y),i(M),i(D)}, {0,0,0}, 0};
% 12:02
parse_datetime(<<H:16/?BS,":",Mi:16/?BS>>) -> {{0,0,0}, {i(H),i(Mi),0}, 0};
% 12:02:58
parse_datetime(<<H:16/?BS,":",Mi:16/?BS,":",S:16/?BS>>) -> {{0,0,0}, {i(H),i(Mi),i(S)}, 0};
% 12:02:58.25
parse_datetime(<<H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS>>) -> {{0,0,0}, {i(H),i(Mi),i(S)}, i(Ms)*10};
% 12:02:58.250
parse_datetime(<<H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS>>) -> {{0,0,0}, {i(H),i(Mi),i(S)}, i(Ms)};
% 2016-04-28 12:02
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),0}, 0};
% 2016-04-28 12:02:58
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
% 2016-04-28 12:02:58.2
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:8/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*100};
% 2016-04-28 12:02:58.25
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*10};
% 2016-04-28 12:02:58.256
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)};


%% ---
%% datetime postgresql
%% ---
% 2016-04-28 12:02:58-02
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"-",_ZH:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
% 2016-04-28 12:02:58+02
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"+",_ZH:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
% 2016-04-28 12:02:58-02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"-",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
% 2016-04-28 12:02:58+02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"+",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};


%% ---
%% datetime rfc 3339
%% ---
% 2016-04-28T12:02:58Z
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"Z">>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
% 2016-04-28T12:02:58-02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"-",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
% 2016-04-28T12:02:58+02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"+",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
%
% 2016-04-28T12:02:58.2Z
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:8/?BS,"Z">>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*100};
% 2016-04-28T12:02:58.2-02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:8/?BS,"-",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*100};
% 2016-04-28T12:02:58.2+02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:8/?BS,"+",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*100};
%
% 2016-04-28T12:02:58.25Z
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS,"Z">>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*10};
% 2016-04-28T12:02:58.25-02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS,"-",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*10};
% 2016-04-28T12:02:58.25+02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS,"+",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*10};
%
% 2016-04-28T12:02:58.253Z
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS,"Z">>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)};
% 2016-04-28T12:02:58.253-02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS,"-",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)};
% 2016-04-28T12:02:58.253+02:00
parse_datetime(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS,"+",_ZH:16/?BS,":",_ZM:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)};


%% ---
%% datetime rus
%% ---
% 28.04.2016
parse_datetime(<<D:16/?BS,".",M:16/?BS,".",Y:32/?BS>>) -> {{i(Y),i(M),i(D)}, {0,0,0}, 0};
% 12:02:58
parse_datetime(<<H:16/?BS,":",Mi:16/?BS,":",S:16/?BS>>) -> {{0,0,0}, {i(H),i(Mi),i(S)}, 0};
% 28.04.2016 12:02:58
parse_datetime(<<D:16/?BS,".",M:16/?BS,".",Y:32/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS>>) -> {{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0};
%% Feb 23 05:23:34 2033 GMT
parse_datetime(<<Ms:24/?BS," ",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS," ",Y:32/?BS," GMT">>) -> {{i(Y),month_to_int(?BU:to_lower(Ms)),i(D)}, {i(H),i(Mi),i(S)}, 0};
% Fri, 16 Dec 2016 07:29:36 GMT
% 16 Dec 2016 07:29:36 GMT
parse_datetime(B) when is_binary(B) ->
    case httpd_util:convert_request_date(?BU:to_list(B)) of
        {D,T} -> {D,T,0};
        bad_date ->
            case re:run(B, <<"((?<weekday>[A-Za-z]{3}), |)(?<d>\\d{1,2}) (?<m>[A-Za-z]{3}) (?<y>\\d{4}) (?<h>\\d{1,2}):(?<mi>\\d{2}):(?<s>\\d{2})( (?<tz>.{2,5})|)">>,
                        [{capture,["weekday","d","m","y","h","mi","s","tz"],binary}])
            of
                {match, Vals} ->
                    F = fun F([<<>>,D,M,Y,H,Mi,S,TZ]) -> F([<<"Tue">>,D,M,Y,H,Mi,S,TZ]);
                            F([W,D,M,Y,H,Mi,S,TZ]) when size(D)==1 -> F([W,<<"0",D/binary>>,M,Y,H,Mi,S,TZ]);
                            F([W,D,M,Y,H,Mi,S,TZ]) when size(H)==1 -> F([W,D,M,Y,<<"0",H/binary>>,Mi,S,TZ]);
                            F([W,D,M,Y,H,Mi,S,<<>>]) when size(H)==1 -> F([W,D,M,Y,H,Mi,S,<<"GMT">>]);
                            F(Values) -> Values
                        end,
                    case F(Vals) of
                        Vals -> {{1970,1,1}, {0,0,0}, 0};
                        [W,D,M,Y,H,Mi,S,_TZ] ->
                            parse_datetime(<<W/binary,", ",D/binary," ",M/binary," ",Y/binary," ",H/binary,":",Mi/binary,":",S/binary>>)
                    end;
                nomatch ->
                    case ?BU:to_int(B,-1) of
                        -1 -> {{1970,1,1}, {0,0,0}, 0};
                        I -> parse_datetime(I)
                    end end end;

%% ---
%% other
%% ---
parse_datetime(D) when is_list(D) -> parse_datetime(?BU:to_binary(D));
parse_datetime(I) when is_integer(I), I > 315569519999 -> ticktolocal(I); % 315569519999 - gregorian seconds = 9999 year, 1980-01-01T13:11:59
%parse_datetime(I) when is_integer(I), I =< 315569519999 -> {D,T} = calendar:gregorian_seconds_to_datetime(I), to_datetime({D,T,0});
parse_datetime({{_,_,_}=D,{_,_,_},_}=DT) -> case calendar:valid_date(D) of true -> DT; false -> parse_datetime(undefined) end;
parse_datetime({{_,_,_}=D,{_,_,_}=T}) -> case calendar:valid_date(D) of true -> {D,T,0}; false -> parse_datetime(undefined) end;
parse_datetime({_,_,_}=D) -> case calendar:valid_date(D) of true -> {D,{0,0,0},0}; false -> parse_datetime(undefined) end;
parse_datetime(_) -> {{1970,1,1}, {0,0,0}, 0}.

%% ----------------------------
%% Parse datetime in timezone mode (convert and return utc time)
%% ----------------------------

%% -----------
%% helper function for parsing default as utc time
% 2016-04-28 12:02:58 -> 2016-04-28T12:02:58Z
parse_datetime_utc(<<_:10/binary," ",_:8/binary>>=V) -> parse_datetime(V);
parse_datetime_utc(<<_:10/binary," ",_:8/binary,".",_:1/binary>>=V) -> parse_datetime(V);
parse_datetime_utc(<<_:10/binary," ",_:8/binary,".",_:2/binary>>=V) -> parse_datetime(V);
parse_datetime_utc(<<_:10/binary," ",_:8/binary,".",_:3/binary>>=V) -> parse_datetime(V);
%
% 2016-04-28 12:02:58-02
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"-",ZH:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0}, {"-",i(ZH),0});
% 2016-04-28 12:02:58+02
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"+",ZH:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0}, {"+",i(ZH),0});
% 2016-04-28 12:02:58-02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"-",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0}, {"-",i(ZH),i(ZM)});
% 2016-04-28 12:02:58+02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS," ",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"+",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0}, {"+",i(ZH),i(ZM)});
%
% 2016-04-28T12:02:58Z
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"Z">>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0}, {"+",0,0});
% 2016-04-28T12:02:58-02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"-",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0}, {"-",i(ZH),i(ZM)});
% 2016-04-28T12:02:58+02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,"+",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, 0}, {"+",i(ZH),i(ZM)});
%
% 2016-04-28T12:02:58.2Z
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:8/?BS,"Z">>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*100}, {"+",0,0});
% 2016-04-28T12:02:58.2-02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:8/?BS,"-",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*100}, {"-",i(ZH),i(ZM)});
% 2016-04-28T12:02:58.2+02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:8/?BS,"+",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*100}, {"+",i(ZH),i(ZM)});
%
% 2016-04-28T12:02:58.25Z
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS,"Z">>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*10}, {"+",0,0});
% 2016-04-28T12:02:58.25-02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS,"-",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*10}, {"-",i(ZH),i(ZM)});
% 2016-04-28T12:02:58.25+02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:16/?BS,"+",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)*10}, {"+",i(ZH),i(ZM)});
%
% 2016-04-28T12:02:58.253Z
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS,"Z">>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)}, {"+",0,0});
% 2016-04-28T12:02:58.253-02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS,"-",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)}, {"-",i(ZH),i(ZM)});
% 2016-04-28T12:02:58.253+02:00
parse_datetime_utc(<<Y:32/?BS,"-",M:16/?BS,"-",D:16/?BS,"T",H:16/?BS,":",Mi:16/?BS,":",S:16/?BS,".",Ms:24/?BS,"+",ZH:16/?BS,":",ZM:16/?BS>>) -> convert_to_utc({{i(Y),i(M),i(D)}, {i(H),i(Mi),i(S)}, i(Ms)}, {"+",i(ZH),i(ZM)});
% Fri, 16 Dec 2016 07:29:36 GMT
% 16 Dec 2016 07:29:36 +0300
parse_datetime_utc(B) when is_binary(B) ->
    case re:run(B, <<"((?<weekday>[A-Za-z]{3}), |)(?<d>\\d{1,2}) (?<m>[A-Za-z]{3}) (?<y>\\d{4}) (?<h>\\d{1,2}):(?<mi>\\d{2}):(?<s>\\d{2})( (?<tz>.{2,5})|)">>,
                [{capture,["weekday","d","m","y","h","mi","s","tz"],binary}])
    of
        {match, Vals} ->
            F = fun F([<<>>,D,M,Y,H,Mi,S,TZ]) -> F([<<"Tue">>,D,M,Y,H,Mi,S,TZ]);
                    F([W,D,M,Y,H,Mi,S,TZ]) when size(D)==1 -> F([W,<<"0",D/binary>>,M,Y,H,Mi,S,TZ]);
                    F([W,D,M,Y,H,Mi,S,TZ]) when size(H)==1 -> F([W,D,M,Y,<<"0",H/binary>>,Mi,S,TZ]);
                    F([W,D,M,Y,H,Mi,S,<<>>]) when size(H)==1 -> F([W,D,M,Y,H,Mi,S,<<"GMT">>]);
                    F(Values) -> Values
                end,
            case F(Vals) of
                [W,D,M,Y,H,Mi,S,TZ] ->
                    case httpd_util:convert_request_date(?BU:to_list(<<W/binary,", ",D/binary," ",M/binary," ",Y/binary," ",H/binary,":",Mi/binary,":",S/binary>>)) of
                        {Date,Time} ->
                            Ms = 0,
                            case TZ of
                                <<"+",ZH:2/binary,ZM:2/binary>> -> convert_to_utc({Date,Time,Ms},{"+",i(ZH),i(ZM)});
                                <<"-",ZH:2/binary,ZM:2/binary>> -> convert_to_utc({Date,Time,Ms},{"-",i(ZH),i(ZM)});
                                <<"GMT">> -> {Date,Time,Ms};
                                <<>> -> {Date,Time,Ms}
                            end;
                        R -> R
                    end
            end;
        nomatch ->
            parse_datetime_utc_x(B)
    end.

% other
parse_datetime_utc_x(X) ->
    case parse_datetime(X) of
        {D,T,Ms} -> convert_to_utc({D,T,Ms})%;
        %undefined -> exit(<<"Invalid datetime">>) % dialyzer says this case will never match.
    end.

%% ------------------------------
%% Parse time
%% ------------------------------

% 12:02
parse_time(<<H:2/?BI,":",Mi:2/?BI>>) -> {i(H),i(Mi),0};
% 12:02:58
parse_time(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI>>) -> {i(H),i(Mi),i(S)};

% 12:02Z
parse_time(<<H:2/?BI,":",Mi:2/?BI,"Z">>) -> {i(H),i(Mi),0};
% 12:02:58Z
parse_time(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"Z">>) -> {i(H),i(Mi),i(S)};
% 12:02-02:00
parse_time(<<H:2/?BI,":",Mi:2/?BI,"-",_ZH:2/?BI,":",_ZM:2/?BI>>) -> {i(H),i(Mi),0};
% 12:02:58-02:00
parse_time(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"-",_ZH:2/?BI,":",_ZM:2/?BI>>) -> {i(H),i(Mi),i(S)};
% 12:02+02:00
parse_time(<<H:2/?BI,":",Mi:2/?BI,"+",_ZH:2/?BI,":",_ZM:2/?BI>>) -> {i(H),i(Mi),0};
% 12:02:58+02:00
parse_time(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"+",_ZH:2/?BI,":",_ZM:2/?BI>>) -> {i(H),i(Mi),i(S)}.

%% ----------------------------
%% Parse time in timezone mode (convert and return utc time)
%% ----------------------------

%% 12:02
%parse_time_utc(<<H:2/?BI,":",Mi:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),0}, 0}, {"+",0,0}));
%% 12:02:58
%parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,S:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),i(S)}, 0}, {"+",0,0}));
% 12:02Z
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,"Z">>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),0}, 0}, {"+",0,0}));
% 12:02:58Z
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"Z">>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),i(S)}, 0}, {"+",0,0}));
%
% 12:02-02
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,"-",ZH:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),0}, 0}, {"-",i(ZH),0}));
% 12:02-02:00
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,"-",ZH:2/?BI,":",ZM:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),0}, 0}, {"-",i(ZH),i(ZM)}));
% 12:02:58-02
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"-",ZH:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),i(S)}, 0}, {"-",i(ZH),0}));
% 12:02:58-02:00
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"-",ZH:2/?BI,":",ZM:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),i(S)}, 0}, {"-",i(ZH),i(ZM)}));
% 12:02+02
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,"+",ZH:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),0}, 0}, {"+",i(ZH),0}));
% 12:02+02:00
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,"+",ZH:2/?BI,":",ZM:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),0}, 0}, {"+",i(ZH),i(ZM)}));
% 12:02:58+02
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"+",ZH:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),i(S)}, 0}, {"+",i(ZH),0}));
% 12:02:58+02:00
parse_time_utc(<<H:2/?BI,":",Mi:2/?BI,":",S:2/?BI,"+",ZH:2/?BI,":",ZM:2/?BI>>) -> element(2,convert_to_utc({{2018,1,1}, {i(H),i(Mi),i(S)}, 0}, {"+",i(ZH),i(ZM)})).

%% ----------------------------
%% Convert datetime to utc
%% ----------------------------
convert_to_utc({D,T,Ms}) ->
    case calendar:local_time_to_universal_time_dst({D,T}) of
        [] -> {D,T,Ms};
        [{D1,T1}] -> {D1,T1,Ms};
        [{D1,T1},_] -> {D1,T1,Ms}
    end.
convert_to_utc({D,T,Ms}, {"+",ZH,ZM}) ->
    {D1,T1} = calendar:gregorian_seconds_to_datetime(calendar:datetime_to_gregorian_seconds({D,T}) - ZH*3600 - ZM*60), {D1,T1,Ms};
convert_to_utc({D,T,Ms}, {"-",ZH,ZM}) ->
    {D1,T1} = calendar:gregorian_seconds_to_datetime(calendar:datetime_to_gregorian_seconds({D,T}) + ZH*3600 + ZM*60), {D1,T1,Ms}.

%% ----------------------------
ticktolocal(TS) ->
    XMs = TS rem 1000,
    XS = TS div 1000 rem 1000000,
    XD = TS div 1000000000,
    {D,T} = calendar:now_to_local_time({XD,XS,XMs * 1000}),
    {D,T,XMs}.

%% ----------------------------
ticktoutc(TS) ->
    XMs = TS rem 1000,
    XS = TS div 1000 rem 1000000,
    XD = TS div 1000000000,
    {D,T} = calendar:now_to_universal_time({XD,XS,XMs * 1000}),
    {D,T,XMs}.

%% ----------------------------
to_dt(B) when is_binary(B) -> to_dt(parse_datetime(B));
to_dt(S) when is_list(S) -> to_dt(parse_datetime(?BU:to_binary(S)));
to_dt(A) when is_atom(A) -> to_dt(parse_datetime(?BU:to_binary(A)));
to_dt({{_,_,_}=D,{_,_,_}=T}) -> {D,T};
to_dt({{_,_,_}=D,{_,_,_}=T,_Ms}) -> {D,T}.

%% ====================================================================
%% Internal functions
%% ====================================================================

i(X) -> ?BU:i(X).

%% ----------------------------
month_to_int(<<"jan">>) -> 1;
month_to_int(<<"january">>) -> 1;
month_to_int(<<"feb">>) -> 2;
month_to_int(<<"febuary">>) -> 2;
month_to_int(<<"mar">>) -> 3;
month_to_int(<<"march">>) -> 3;
month_to_int(<<"apr">>) -> 4;
month_to_int(<<"april">>) -> 4;
month_to_int(<<"may">>) -> 5;
month_to_int(<<"jun">>) -> 6;
month_to_int(<<"june">>) -> 6;
month_to_int(<<"jul">>) -> 7;
month_to_int(<<"july">>) -> 7;
month_to_int(<<"aug">>) -> 8;
month_to_int(<<"august">>) -> 8;
month_to_int(<<"sep">>) -> 9;
month_to_int(<<"sept">>) -> 9;
month_to_int(<<"september">>) -> 9;
month_to_int(<<"oct">>) -> 10;
month_to_int(<<"october">>) -> 10;
month_to_int(<<"nov">>) -> 11;
month_to_int(<<"november">>) -> 11;
month_to_int(<<"dec">>) -> 12;
month_to_int(<<"december">>) -> 12.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

-define(DEFAULT_DT, {{1970,1,1}, {0,0,0}, 0}).

%% ==================================

parse_datetime_test_() ->
    {"parse_datetime/1 test",
     [
      fun() ->
          A = ?MODULE:parse_datetime(<<"67:89:71.8">>),
          ?debugFmt("A = ~tp", [A])
      end,
    %% ---
    %% datetime sql
    %% ---
      %%%% date only
      ?_assertEqual({{1234,56,78}, {0,0,0}, 0}, ?MODULE:parse_datetime(<<"1234-56-78">>)),
      %%%% time only
      ?_assertEqual({{0,0,0}, {34,67,89},   0}, ?MODULE:parse_datetime(<<"34:67:89">>)),
      %%%% time only with one-digit seconds fraction (invalid)
      ?_assertEqual(?DEFAULT_DT,                ?MODULE:parse_datetime(<<"67:89:71.8">>)),
      %%%% time only with two-digit seconds fraction
      %?_assertEqual({{0,0,0}, {67,89,71},  30}, ?MODULE:parse_datetime(<<"67:89:71.03">>)), % XXX fails because 4-digit hour is expected
      ?_assertEqual({{1970,1,1},{0,0,0},0}, ?MODULE:parse_datetime(<<"4367:89:71.03">>)),
      %?_assertEqual({{0,0,0}, {17,98,76}, 950}, ?MODULE:parse_datetime(<<"17:98:76.95">>)), % XXX fails because 4-digit hour is expected
      ?_assertEqual({{1970,1,1},{0,0,0},0}, ?MODULE:parse_datetime(<<"8217:98:76.95">>)),
      %%%% time only with three-digit seconds fraction
      %?_assertEqual({{0,0,0}, {67,89,71},   8}, ?MODULE:parse_datetime(<<"67:89:71.008">>)), % XXX fails because 4-digit hour is expected
      ?_assertEqual({{1970,1,1},{0,0,0},0}, ?MODULE:parse_datetime(<<"4367:89:71.008">>)),
      %?_assertEqual({{0,0,0}, {67,89,71},  37}, ?MODULE:parse_datetime(<<"67:89:71.037">>)), % XXX fails because 4-digit hour is expected
      ?_assertEqual({{1970,1,1},{0,0,0},0}, ?MODULE:parse_datetime(<<"4367:89:71.037">>)),
      %?_assertEqual({{0,0,0}, {17,98,76}, 185}, ?MODULE:parse_datetime(<<"17:98:76.185">>)) % XXX fails because 4-digit hour is expected
      ?_assertEqual({{1970,1,1},{0,0,0},0}, ?MODULE:parse_datetime(<<"8217:98:76.185">>)),
      %%%% date and time
      ?_assertEqual({{1234,56,78}, {34,67,89},   0}, ?MODULE:parse_datetime(<<"1234-56-78 34:67:89">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},  800},?MODULE:parse_datetime(<<"1234-56-78 67:89:71.8">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},  30}, ?MODULE:parse_datetime(<<"1234-56-78 67:89:71.03">>)),
      ?_assertEqual({{1234,56,78}, {17,98,76}, 950}, ?MODULE:parse_datetime(<<"1234-56-78 17:98:76.95">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},   8}, ?MODULE:parse_datetime(<<"1234-56-78 67:89:71.008">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},  37}, ?MODULE:parse_datetime(<<"1234-56-78 67:89:71.037">>)),
      ?_assertEqual({{1234,56,78}, {17,98,76}, 185}, ?MODULE:parse_datetime(<<"1234-56-78 17:98:76.185">>)),
      ?_assertEqual(?DEFAULT_DT,                     ?MODULE:parse_datetime(<<"1234-56-78 17:98:76.1859">>)),
    %% ---
    %% datetime rfc 3339
    %% ---
      ?_assertEqual({{1234,56,78}, {34,67,89},   0}, ?MODULE:parse_datetime(<<"1234-56-78T34:67:89Z">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},   0}, ?MODULE:parse_datetime(<<"1234-56-78T67:89:71-99:88">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},   0}, ?MODULE:parse_datetime(<<"1234-56-78T67:89:71+77:66">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},  800},?MODULE:parse_datetime(<<"1234-56-78T67:89:71.8Z">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},  30}, ?MODULE:parse_datetime(<<"1234-56-78T67:89:71.03Z">>)),
      ?_assertEqual({{1234,56,78}, {17,98,76}, 950}, ?MODULE:parse_datetime(<<"1234-56-78T17:98:76.95Z">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},   8}, ?MODULE:parse_datetime(<<"1234-56-78T67:89:71.008Z">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},  37}, ?MODULE:parse_datetime(<<"1234-56-78T67:89:71.037Z">>)),
      ?_assertEqual({{1234,56,78}, {17,98,76}, 185}, ?MODULE:parse_datetime(<<"1234-56-78T17:98:76.185Z">>)),
      ?_assertEqual(?DEFAULT_DT,                     ?MODULE:parse_datetime(<<"1234-56-78T17:98:76.1859Z">>)),
      ?_assertEqual({{1234,56,78}, {67,89,71},  30}, ?MODULE:parse_datetime(<<"1234-56-78T67:89:71.03-55:44">>)),
      ?_assertEqual({{1234,56,78}, {17,98,76}, 950}, ?MODULE:parse_datetime(<<"1234-56-78T17:98:76.95+33:22">>)),
    %% ---
    %% datetime rus
    %% ---
      ?_assertEqual({{4321,65,87}, {0,0,0}, 0}, ?MODULE:parse_datetime(<<"87.65.4321">>)),
      ?_assertEqual({{0,0,0}, {87,65,93}, 0},   ?MODULE:parse_datetime(<<"87:65:93">>)),
      ?_assertEqual({{4321,65,87}, {87,65,93}, 0}, ?MODULE:parse_datetime(<<"87.65.4321 87:65:93">>)),
    %% ---
    %% http request datetime format
    %% ---
      ?_assertEqual({{2016,12,16}, {7,29,36}, 0}, ?MODULE:parse_datetime(<<"Fri, 16 Dec 2016 07:29:36 GMT">>)),
    %% ---
    %% other (than binary)
    %% ---
      ?_assertEqual({{2012,12,15}, {13,45,26}, 0}, ?MODULE:parse_datetime("Sat, 15 Dec 2012 13:45:26 MSK")),
      ?_assertEqual(?DEFAULT_DT,                   ?MODULE:parse_datetime(315569519999))%,
      %?_assertEqual({{1980,1,1}, {13,12,5}, 36},   ?MODULE:parse_datetime(315569525036)), % depends on test machine time zone
      %?_assertEqual({{2018,3,2}, {12,56,53}, 8},   ?MODULE:parse_datetime(1519984613008)) % depends on test machine time zone
     ]}.

parse_datetime_utc_test_() ->
    {"parse_datetime_utc/1 test",
     [
    %% ---
    %% datetime sql
    %% ---
      %%%% date only
      %?_assertEqual({{4321,10,24}, {21,0,0}, 0}, ?MODULE:parse_datetime_utc(<<"4321-10-25">>))%, % depends on test machine time zone
      % TODO finish like parse_datetime_test_()
     ]}.

parse_time_test_() ->
    {"parse_time/1 test",
     [
      %%%% time without seconds (UTC implied)
      ?_assertEqual({34,67,0}, ?MODULE:parse_time(<<"34:67">>)),
      %%%% time (UTC implied)
      ?_assertEqual({34,67,89}, ?MODULE:parse_time(<<"34:67:89">>)),

      %%%% time without seconds (explicit UTC)
      ?_assertEqual({34,67,0}, ?MODULE:parse_time(<<"34:67Z">>)),
      %%%% time (explicit UTC)
      ?_assertEqual({34,67,89}, ?MODULE:parse_time(<<"34:67:89Z">>)),

      %%%% time without seconds (negative offset)
      ?_assertEqual({34,67,0}, ?MODULE:parse_time(<<"34:67-98:76">>)),
      %%%% time (negative offset)
      ?_assertEqual({34,67,89}, ?MODULE:parse_time(<<"34:67:89-54:32">>)),

      %%%% time without seconds (positive offset)
      ?_assertEqual({34,67,0}, ?MODULE:parse_time(<<"34:67+98:76">>)),
      %%%% time (positive offset)
      ?_assertEqual({34,67,89}, ?MODULE:parse_time(<<"34:67:89+54:32">>))
     ]}.

parse_time_utc_test_() ->
    {"parse_time_utc/1 test",
     [
      %%%% time without seconds
      ?_assertEqual({12,47,0}, ?MODULE:parse_time_utc(<<"12:47Z">>)),
      %%%% time
      ?_assertEqual({12,47,35}, ?MODULE:parse_time_utc(<<"12:47:35Z">>)),

      %%%% time without seconds (negative offset)
      ?_assertEqual({21,46,0}, ?MODULE:parse_time_utc(<<"14:31-07:15">>)),
      %%%% time (negative offset)
      ?_assertEqual({22,6,25}, ?MODULE:parse_time_utc(<<"14:31:25-07:35">>)),

      %%%% time without seconds (positive offset)
      ?_assertEqual({11,19,0}, ?MODULE:parse_time_utc(<<"14:31+03:12">>)),
      %%%% time (positive offset)
      ?_assertEqual({10,3,25}, ?MODULE:parse_time_utc(<<"14:31:25+04:28">>))
     ]}.

convert_to_utc_test_() ->
    {"convert_to_utc/1 test",
     [
      %?_assertEqual({{2018,1,1},{8,12,13},5}, ?MODULE:convert_to_utc({{2018,1,1},{11,12,13},5})), % XXX this test will only pass on GMT+3 machine
      ?_assertEqual({{2018,1,1},{11,12,13},5}, ?MODULE:convert_to_utc({{2018,1,1},{11,12,13},5},{"+",0,0})),
      ?_assertEqual({{2018,1,1},{10,11,13},5}, ?MODULE:convert_to_utc({{2018,1,1},{11,12,13},5},{"+",1,1})),
      ?_assertEqual({{2018,1,1},{11,12,13},5}, ?MODULE:convert_to_utc({{2018,1,1},{11,12,13},5},{"-",0,0})),
      ?_assertEqual({{2018,1,1},{13,14,13},5}, ?MODULE:convert_to_utc({{2018,1,1},{11,12,13},5},{"-",2,2}))
     ]}.

ticktolocal_test_() ->
    {"ticktolocal_utc/1 test",
     [
      %?_assertEqual({{2009,2,14},{2,31,30},123}, ?MODULE:ticktolocal(1234567890123)) % XXX this test will only pass on GMT+3 machine
     ]}.

ticktoutc_test_() ->
    {"ticktoutc_utc/1 test",
     [
      ?_assertEqual({{2009,2,13},{23,31,30},123}, ?MODULE:ticktoutc(1234567890123))
     ]}.

to_dt_test_() ->
    {"to_dt/1 test",
     [
      ?_assertEqual({{2018,4,17},{9,47,0}}, to_dt(<<"2018-04-17 09:47:00">>)),
      ?_assertEqual({{2018,4,17},{9,47,0}}, to_dt("2018-04-17 09:47:00")),
      ?_assertEqual({{2018,4,17},{9,47,0}}, to_dt({{2018,4,17},{9,47,0},142})),
      ?_assertEqual({{2018,4,17},{9,47,0}}, to_dt({{2018,4,17},{9,47,0}})),
      ?_assertEqual({{1970,1,1},{0,0,0}}, to_dt(undefined))
     ]}.

%% ====================================================================
%% Internal test functions
%% ====================================================================

-endif.

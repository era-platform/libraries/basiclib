%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Operations on sets. Sets, ordsets, lists.

-module(basiclib_utils_set).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Sets
%% ------------------------------
get_intersect(L1,L2) ->
    ordsets:intersection(ordsets:from_list(L1),ordsets:from_list(L2)).

% split by portions
portions(Cnt,List) ->
    F = fun F(List1,Acc) ->
           {A,B} = lists:split(Cnt,List1),
           if length(B) >= Cnt -> F(B,[A|Acc]);
              B == [] -> [A|Acc];
              true -> [B,A|Acc]
           end end,
    if length(List) > Cnt -> lists:reverse(F(List,[]));
       true -> [List]
    end.

% replace nth element of list by
replace_nth(N,List,Elt) when is_integer(N), is_list(List) ->
    {A,B} = lists:split(N,List), lists:append(lists:droplast(A), [Elt|B]).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

get_intersect_test_() ->
    {"get intersect",
       [?_assertEqual([{1,"a"},{3,"e"}], get_intersect([{2,"c"},{1,"a"},{3,"e"},{1,"b"},{2,"d"},{4,"f"}], [{3,"e"},{1,"a"},{2,"h"}])),
      ?_assertEqual([{1,"a"},{3,"c"}], get_intersect([{2,"b"},{1,"a"},{3,"c"},{1,"a"},{2,"e"},{4,"e"}], [{3,"e"},{1,"a"},{1,"a"},{3,"c"},{2,"h"}])),
      ?_assertEqual("c", get_intersect("cba", "cfs")),
      ?_assertEqual([abc], get_intersect(["abc",123,{123,"abc"},{1,a},{2,"e"},abc,[abc,123],abc,321,{abc,123}], ["cfs",{2,"c"},{1,"a"},{123,abc},abc]))
      ]}.

portions_test_() ->
    {"portions",
       [?_assertEqual(["abc","def","ghi"], portions(3,"abcdefghi")),
      ?_assertEqual(["abcd","efgh","i"], portions(4,"abcdefghi")),
      ?_assertEqual(["abcde","fghi"], portions(5,"abcdefghi")),
      ?_assertEqual(["a","b","c","d","e","f","g","h","i"], portions(1,"abcdefghi")),
      ?_assertEqual([["abc",123,{123,"abc"}],[{1,a},{2,"e"},abc],[[abc,123],abc,321],[{abc,123}]], portions(3,["abc",123,{123,"abc"},{1,a},{2,"e"},abc,[abc,123],abc,321,{abc,123}]))
      ]}.

replace_nth_test_() ->
    {"replace nth",
       [?_assertEqual("abCdefghi", replace_nth(3,"abcdefghi",$C)),
      ?_assertEqual(["abc","C","ghi"], replace_nth(2,["abc","def","ghi"],"C")),
      ?_assertEqual(["abc",123,{123,"abc"},{1,a},{2,"e"},abc,"C",abc,321,{abc,123}], replace_nth(7,["abc",123,{123,"abc"},{1,a},{2,"e"},abc,[abc,123],abc,321,{abc,123}],"C"))
      ]}.

-endif.


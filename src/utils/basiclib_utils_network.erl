%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Addresses and names in a network

-module(basiclib_utils_network).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------
%% Network Functions
%% ------------------------------------
is_local_ipaddr({_,_,_,_}=IpAddr) when is_tuple(IpAddr) ->
    case IpAddr of
        %% @@TODO re-check RFC's
        {0,_,_,_} -> true;
        {10,_,_,_} -> true;
        {127,_,_,_} -> true;
        {169,254,_,_} -> true;
        {172,16,_,_} -> true;
        {172,17,_,_} -> true;
        {172,18,_,_} -> true;
        {172,19,_,_} -> true;
        {172,20,_,_} -> true;
        {172,21,_,_} -> true;
        {172,22,_,_} -> true;
        {172,23,_,_} -> true;
        {172,24,_,_} -> true;
        {172,25,_,_} -> true;
        {172,26,_,_} -> true;
        {172,27,_,_} -> true;
        {172,28,_,_} -> true;
        {172,29,_,_} -> true;
        {172,30,_,_} -> true;
        {172,31,_,_} -> true;
        {192,168,_,_} -> true;
        _ -> false
    end;
is_local_ipaddr(Addr) ->
    case inet:parse_address(?BU:to_list(Addr)) of
        {ok, IpAddr} ->
            is_local_ipaddr(IpAddr);
        {error, _} ->
            false
    end.

% return ip-addr of node
node_addr() -> node_addr(node()).
node_addr(Node) when is_atom(Node); is_binary(Node); is_list(Node) ->
    [_,Addr] = binary:split(?BU:to_binary(Node), <<"@">>, []),
    Addr.

% return name of node (up to @)
node_name() -> node_name(node()).
node_name(Node) when is_atom(Node); is_binary(Node); is_list(Node) ->
    [Name,_] = binary:split(?BU:to_binary(Node), <<"@">>, []),
    Name.

% is node open (not hidden and connect_all=true)
is_open_node() ->
    case init:get_argument(hidden) of
        {ok, _} -> false;
        error ->
            case init:get_argument(connect_all) of
                {ok, [["false"]]} -> false;
                _ -> true
            end end.

% return true if <<"ipv4:port">> or <<"ipv4">> (or <<"ipv6">>?)
is_addr_or_addrport(Value) when is_binary(Value) ->
    case binary:split(Value,<<":">>,[global]) of
        [<<>>] -> false;
        [BAddr] ->
            case inet:parse_address(?BU:to_list(BAddr)) of
                {ok,_} -> true;
                _ -> false
            end;
        [BAddr,BPort] ->
            case inet:parse_address(?BU:to_list(BAddr)) of
                {ok,_} ->
                    try ?BU:to_int(BPort) of
                        P when P>0, P<65536 -> true;
                        _ -> false
                    catch _:_ -> false
                    end;
                _ -> false
            end end.

%% ---
%% Returns inet:ip_address() for specified host name or ip address as string.
%% ---
-spec get_host_addr(Host::binary() | list()) -> {ok,ResolvedAddr::inet:ip_address()} | {error,Reason:: inet:posix() | inet_res:res_error()}.
%% ---
get_host_addr(Host) when is_binary(Host) -> get_host_addr(?BU:to_list(Host));
get_host_addr(Host) when is_list(Host) ->
    [RemoteIp1|_] = string:tokens(Host,":"),
    case inet:parse_address(RemoteIp1) of
        {ok,_RemoteIp2}=Ok -> Ok;
        {error,einval} ->
            ResolveKey = {'resolve_cache',RemoteIp1},
            case ?BLstore:find_t(ResolveKey) of
                {_,{error,_}=Err} -> Err;
                {_,RemoteIp2} -> {ok,RemoteIp2};
                false ->
                    case inet:getaddrs(RemoteIp1, inet) of
                        {ok,[RemoteIp2|_]} ->
                            ?BLstore:store_t(ResolveKey,RemoteIp2,600000), % 10 min
                            {ok,RemoteIp2};
                        {error,nxdomain}=Err ->
                            ?BLstore:store_t(ResolveKey,Err,600000), % 10 min
                            Err;
                        {error,_}=Err -> Err
                    end
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Network helpers Test
%% ====================================================================

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

is_local_ipaddr_test_() ->
    {"is_local_ipaddr test",
     [
      ?_assertEqual(true, is_local_ipaddr(<<"192.168.0.10">>)),
      ?_assertEqual(true, is_local_ipaddr("192.168.0.10")),
      ?_assertEqual(true, is_local_ipaddr({192,168,0,10})),
      ?_assertEqual(true, is_local_ipaddr({10,168,0,10})),
      ?_assertEqual(true, is_local_ipaddr({127,0,0,1})),
      ?_assertEqual(false, is_local_ipaddr([192,168,0,10])),
      ?_assertEqual(false, is_local_ipaddr(<<>>)),
      ?_assertEqual(false, is_local_ipaddr("")),
      ?_assertEqual(false, is_local_ipaddr({212,53,40,40})),
      ?_assertEqual(false, is_local_ipaddr([199,178,12,10]))
     ]}.

node_addr_test_() ->
    {"node_addr test",
     [
      ?_assertEqual(<<"nohost">>, node_addr('nonode@nohost')),
      ?_assertEqual(<<>>, node_addr('nonode@')),
      ?_assertEqual(<<"nohost">>, node_addr('@nohost')),
      ?_assertEqual(<<>>, node_addr('@'))
     ]}.

node_name_test_() ->
    {"node_name test",
     [
      ?_assertEqual(<<"nonode">>, node_name('nonode@nohost')),
      ?_assertEqual(<<"nonode">>, node_name('nonode@')),
      ?_assertEqual(<<>>, node_name('@nohost')),
      ?_assertEqual(<<>>, node_name('@'))
     ]}.

is_addr_or_addrport_test_() ->
    {"is_local_ipaddr test",
     [
      ?_assertEqual(true, is_addr_or_addrport(<<"192.168.0.10">>)),
      ?_assertEqual(true, is_addr_or_addrport(<<"192.168.0.10:5060">>)),
      ?_assertMatch({'EXIT',_}, catch is_addr_or_addrport(<<"2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d">>)),
      ?_assertEqual(false, is_addr_or_addrport(<<"212.53.40.40:67536">>)),
      ?_assertEqual(false, is_addr_or_addrport(<<".192.168.0">>)),
      ?_assertEqual(false, is_addr_or_addrport(<<"321.543.23.16">>))
     ]}.

-endif.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Data type conversions

-module(basiclib_utils_transform).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Transform types
%% ------------------------------

% no unicode transform!!!
to_binary(Item) when is_binary(Item) ->    Item;
to_binary(Item) when is_list(Item) -> case catch list_to_binary(Item) of {'EXIT',_} -> unicode:characters_to_binary(Item); T -> T end;
to_binary(Item) when is_atom(Item) -> list_to_binary(atom_to_list(Item));
to_binary(Item) when is_integer(Item) -> list_to_binary(integer_to_list(Item));
to_binary(Item) when is_float(Item) -> list_to_binary(float_to_list(Item,[{decimals,6},compact])).

% UNICODE TRANSFORM
to_unicode_binary(Item) -> to_binary(to_unicode_list(Item)).
to_unicode_binary(Item,InEnc) -> to_binary(to_unicode_list(Item,InEnc)).

ub(A) when is_binary(A) -> A;
ub(A) when is_list(A) -> ?BU:to_unicode_binary(A).

% no unicode transform!!!
to_list(Item) when is_tuple(Item) -> erlang:tuple_to_list(Item);
to_list(Item) when is_list(Item) ->    Item;
to_list(Item) when is_binary(Item) -> binary_to_list(Item); % some modules use cyrillic names, so simple transformation from list to binary is wrong
to_list(Item) when is_atom(Item) -> atom_to_list(Item);
to_list(Item) when is_integer(Item) -> integer_to_list(Item);
to_list(Item) when is_float(Item) -> float_to_list(Item,[{decimals,6},compact]);
to_list(Item) when is_pid(Item) -> pid_to_list(Item).

% UNICODE TRANSFORM
to_unicode_list(Item) -> unicode:characters_to_list(?BU:to_binary(Item)).
to_unicode_list(Item,InEnc) -> unicode:characters_to_list(?BU:to_binary(Item),InEnc).

any_to_str(Term) -> ?BU:str("~tp",[Term]).

to_atom(Item) when is_atom(Item) -> Item;
to_atom(Item) when is_list(Item) -> list_to_existing_atom(Item);
to_atom(Item) when is_binary(Item) -> erlang:binary_to_existing_atom(Item, latin1).

to_atom_new(Item) when is_atom(Item) -> Item;
to_atom_new(Item) when is_list(Item) -> erlang:list_to_atom(Item);
to_atom_new(Item) when is_binary(Item) -> erlang:binary_to_atom(Item, latin1).

to_atom_safe(Item) when is_atom(Item) -> Item;
to_atom_safe(Item) when is_list(Item) -> try list_to_existing_atom(Item) catch _:_ -> Item end;
to_atom_safe(Item) when is_binary(Item) -> try erlang:binary_to_existing_atom(Item, latin1) catch _:_ -> Item end.

to_integer(Item) -> to_int(Item).
to_int(<<"true">>) -> 1;
to_int(<<"false">>) -> 0;
to_int(<<"TRUE">>) -> 1;
to_int(<<"FALSE">>) -> 0;
to_int([]) -> 0;
to_int(Item) when is_integer(Item) -> Item;
to_int(Item) when is_binary(Item) -> binary_to_integer(string:trim(Item));
to_int(Item) when is_list(Item) -> list_to_integer(string:trim(Item));
to_int(Item) when is_float(Item) -> trunc(Item); % !
to_int(true) -> 1;
to_int(false) -> 0.

to_int(X,Def) -> try to_int(X) catch _:_ -> Def end.

str_to_int(Str0) when is_binary(Str0) ->
    str_to_int(to_list(Str0));
str_to_int(Str0) ->
    Str = string:strip(Str0, both),
    {Val,_} = string:to_integer(Str),
    Val.

to_float(Item) when is_float(Item) -> Item;
to_float(Item) when is_integer(Item) -> Item * 1.0;
to_float(Item) when is_binary(Item) -> Item1 = string:trim(Item), try erlang:binary_to_float(Item1) catch error:_ -> to_int(Item1) * 1.0 end;
to_float(Item) when is_list(Item) -> Item1 = string:trim(Item), try erlang:list_to_float(Item1) catch error:_ -> to_int(Item1) * 1.0 end;
to_float(true) -> 1.0;
to_float(false) -> 0.0.
to_float(X,Def) -> try to_float(X) catch _:_ -> Def end.

as_number(X,_) when is_integer(X) -> X;
as_number(X,_) when is_float(X) -> X;
as_number(X,_) when is_number(X) -> X;
as_number(X,Default) when is_binary(X) ->
    try erlang:binary_to_integer(X)
    catch error:_ ->
        try erlang:binary_to_float(X)
        catch error:_ -> Default
        end end;
as_number(_,Default) -> Default.


to_bool(false) -> false;
to_bool(true) -> true;
to_bool(0) -> false;
to_bool(Item) when is_integer(Item) -> true;
to_bool(0.0) -> false;
to_bool(Item) when is_float(Item) -> true;
to_bool(undefined) -> false;
to_bool(null) -> false;
to_bool(Item) when is_atom(Item) -> true;
to_bool(Item) when is_binary(Item) -> to_bool(string:to_lower(to_list(string:trim(Item))));
to_bool("t") -> true;
to_bool("f") -> false;
to_bool("true") -> true;
to_bool("false") -> false;
to_bool("TRUE") -> true;
to_bool("FALSE") -> false;
to_bool("1") -> true;
to_bool("0") -> false;
to_bool(Item) when is_list(Item) -> try to_bool(to_int(string:trim(Item))) catch error:_ -> false end.

to_bool(X,Def) -> try to_bool(X) catch _:_ -> Def end.

to_guid(<<_:64/bitstring, "-", _:32/bitstring, "-", _:32/bitstring, "-", _:32/bitstring, "-", _:96/bitstring>> = Item) when is_binary(Item) -> check_guid(Item);
to_guid(<<A:64/bitstring,B1:32/bitstring,B2:32/bitstring,B3:32/bitstring,C:96/bitstring>> = Item) when is_binary(Item) -> check_guid(<<A/bitstring,"-",B1/bitstring,"-",B2/bitstring,"-",B3/bitstring,"-",C/bitstring>>);
to_guid(Item) when is_list(Item) andalso length(Item)==32 orelse length(Item)==36 -> to_guid(to_binary(Item));
to_guid(_) -> ?BLuuid:empty().

%@private
check_guid(Item) when is_binary(Item) ->
    L = string:to_lower(to_list(Item)),
    case lists:all(fun(A) when A>=$0, A=<$9 -> true;
                      (A) when A>=$a, A=<$f -> true;
                      ($-) -> true;
                      (_) -> false end, L)
      of
        true -> to_binary(L);
        false -> throw({error,"invalid guid symbols"})
    end.

luid() -> ?BLuuid:luid().
emptyid() -> ?BLuuid:empty().
newid() -> ?BLuuid:uuid_4122().
newid_v3(Data) -> ?BLuuid:new_v3(Data).
uuid_srvidx() -> ?BLuuid:uuid_srvidx().
parse_uuid_srvidx(X) -> ?BLuuid:parse_uuid_srvidx(X).

% base64 to/from base64url encoding (which has '-' and '_' characters in place of '+' and '/' in classic base64).
to_base64url(Base64) when is_binary(Base64) ->
    binary:replace(
        binary:replace(
            binary:replace(
                Base64,
                <<"+">>, <<"-">>, [global]),
            <<"/">>, <<"_">>, [global]),
        <<"=">>,<<>>,[global]).
from_base64url(Base64) when is_binary(Base64) ->
    B = binary:replace(binary:replace(Base64, <<"-">>, <<"+">>, [global]), <<"_">>, <<"/">>, [global]),
    case size(B) rem 4 of
        0 -> B;
        3 -> <<B/binary,"=">>;
        2 -> <<B/binary,"==">>
    end.

% base64 without '==' appended by spaces at the end
to_base64_ex(V) when is_binary(V) ->
    F = fun F(B) ->
            case size(B) rem 3 of
                T when T == 0 -> B;
                _ -> F(<<B/binary, " ">>)
            end end,
    base64:encode(F(V)).

%% ------------------------------------
is_int_val(Item) when is_binary(Item) ->
    try erlang:is_integer(erlang:binary_to_integer(string:trim(Item)))
    catch _:_ -> false
    end;
is_int_val(Item) when is_integer(Item) -> true;
is_int_val(Item) when is_list(Item) ->
    try erlang:is_integer(erlang:list_to_integer(string:trim(Item)))
    catch _:_ -> false
    end;
is_int_val(_Item) -> false.

is_pos_int(Val) -> is_int_val(Val) andalso i(Val) >= 0.

is_guid(Item) when is_binary(Item) ->
    L = string:to_lower(to_list(Item)),
    case lists:all(fun(A) when A>=$0, A=<$9 -> true;
                      (A) when A>=$a, A=<$f -> true;
                      ($-) -> true;
                      (_) -> false end, L)
      of
        true when length(L)==36 -> {true,to_binary(L)};
        _ -> false
    end.

i(A) -> ?BU:to_int(A).

pid_to_bin(Pid) when is_pid(Pid) ->
    L = to_binary(erlang:pid_to_list(Pid)),
    R1 = binary:replace(binary:replace(L,<<"<">>,<<"{">>),<<">">>,<<"}">>),
    binary:replace(R1,<<".">>,<<",">>,[global]).

bin_to_pid(Pid) when is_binary(Pid) ->
    Pb1 = binary:replace(binary:replace(Pid,<<"{">>,<<"<">>),<<"}">>,<<">">>),
    Pb2 = to_list(binary:replace(Pb1,<<",">>,<<".">>,[global])),
    erlang:list_to_pid(Pb2).

%% --------------------------
-spec trunc(float()|integer(), integer()) -> float().
%% --------------------------
trunc(F, N) ->
    Prec = math:pow(10, N),
    trunc(F*Prec)/Prec.

%% --------------------------
-spec is_socketid(Data::binary()) -> {true,SocketId::term()} | false.
%% --------------------------
is_socketid(Data) ->
    case catch erlang:binary_to_term(base64:decode(Data)) of
        {'EXIT',_} -> false;
        Term when element(1,Term)==websock -> {true,Term};
        _ -> false
    end.

%% --------------------------
'if'(true,TrueVal,_) -> TrueVal;
'if'(false,_,FalseVal) -> FalseVal.

unempty(Value,DefaultValue) -> unempty(Value,undefined,DefaultValue).

unempty(Value,FNullValue,DefaultValue) when is_function(FNullValue,1) -> case FNullValue(Value) of true -> DefaultValue; _ -> Value end;
unempty(Value,NullValue,DefaultValue) when Value==NullValue -> DefaultValue;
unempty(Value,_,_) -> Value.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

to_binary_test_() ->
    {"transform to binary",
     [?_assertEqual(<<>>, to_binary(<<>>)),
      ?_assertEqual(<<>>, to_binary("")),
      ?_assertEqual(<<>>, to_binary('')),
      ?_assertEqual(<<>>, to_binary([])),
      ?_assertEqual(<<"abc">>, to_binary(<<"abc">>)),
      ?_assertEqual(<<"abc">>, to_binary("abc")),
      ?_assertEqual(<<"abc">>, to_binary(["a","b","c"])),
      ?_assertEqual(<<"абв"/utf8>>, to_binary("абв")),
      ?_assertEqual(<<"abc">>, to_binary('abc')),
      ?_assertEqual(<<"abc">>, to_binary(abc)),
      ?_assertEqual(<<"123">>, to_binary(123)),
      ?_assertEqual(<<"0">>, to_binary(0)),
      ?_assertEqual(<<"-123">>, to_binary(-123)),
      ?_assertEqual(<<"123.0">>, to_binary(123.0)),
      ?_assertEqual(<<"123.01">>, to_binary(123.01)),
      ?_assertEqual(<<"123.001">>, to_binary(123.001)),
      ?_assertEqual(<<"123.000001">>, to_binary(123.000001)),
      ?_assertEqual(<<"123.0">>, to_binary(123.0000001)),
      ?_assertEqual(<<"123.123457">>, to_binary(123.1234567)),
      ?_assertEqual(<<"123.123457">>, to_binary(123.1234565)),
      ?_assertEqual(<<"123.123456">>, to_binary(123.1234564)),
      ?_assertEqual(<<"123.0">>, to_binary(123.0000000001)),
      ?_assertEqual(<<"-123.001">>, to_binary(-123.001))
      ]}.

to_list_test_() ->
    {"transform to list",
     [?_assertEqual("", to_list("")),
      ?_assertEqual([], to_list("")),
      ?_assertEqual("", to_list([])),
      ?_assertEqual([], to_list([])),
      ?_assertEqual("123", to_list("123")),
      ?_assertEqual("123", to_list([49,50,51])),
      ?_assertNotEqual("123", to_list(["1","2","3"])),
      ?_assertNotMatch("123", to_list(["1","2","3"])),
      ?_assertEqual([49,50,51], to_list("123")),
      ?_assertNotEqual(["1","2","3"], to_list("123")),
      ?_assertNotMatch(["1","2","3"], to_list("123")),
      ?_assertEqual(["1","2","3"], to_list(["1","2","3"])),
      ?_assertEqual(["1","2"], to_list({"1","2"})),
      ?_assertEqual(["1",{"2","3"}], to_list({"1",{"2","3"}})),
      ?_assertEqual("abc", to_list(<<"abc">>)),
      ?_assertEqual([97,98,99], to_list(<<"abc">>)),
      ?_assertNotEqual(["a","b","c"], to_list(<<"abc">>)),
      ?_assertEqual("Ð°Ð±Ð²", to_list(<<"абв"/utf8>>)),
      ?_assertEqual([208,176,208,177,208,178], to_list(<<"абв"/utf8>>)),
      ?_assertNotEqual(["а","б","в"], to_list(<<"абв"/utf8>>)),
      ?_assertEqual("123", to_list(<<"123">>)),
      ?_assertEqual("abc", to_list(abc)),
      ?_assertEqual([97,98,99], to_list(abc)),
      ?_assertNotEqual(["a","b","c"], to_list(abc)),
      ?_assertEqual("123", to_list(123)),
      ?_assertEqual([49,50,51], to_list(123)),
      ?_assertNotEqual(["1","2","3"], to_list(123)),
      ?_assertEqual("0", to_list(0)),
      ?_assertEqual([48], to_list(0)),
      ?_assertNotEqual(["0"], to_list(0)),
      ?_assertEqual("-123", to_list(-123)),
      ?_assertEqual([45,49,50,51], to_list(-123)),
      ?_assertNotEqual(["-","1","2","3"], to_list(-123)),
      ?_assertEqual("123.0", to_list(123.0)),
      ?_assertEqual([49,50,51,46,48], to_list(123.0)),
      ?_assertNotEqual(["1","2","3",".","0"], to_list(123.0)),
      ?_assertEqual("123.01", to_list(123.01)),
      ?_assertEqual([49,50,51,46,48,49], to_list(123.01)),
      ?_assertNotEqual(["1","2","3",".","0","1"], to_list(123.01)),
      ?_assertEqual("123.001", to_list(123.001)),
      ?_assertEqual([49,50,51,46,48,48,49], to_list(123.001)),
      ?_assertNotEqual(["1","2","3",".","0","0","1"], to_list(123.001)),
      ?_assertEqual("123.000001", to_list(123.000001)),
      ?_assertEqual([49,50,51,46,48,48,48,48,48,49], to_list(123.000001)),
      ?_assertNotEqual(["1","2","3",".","0","0","0","0","0","1"], to_list(123.000001)),
      ?_assertEqual("123.123457", to_list(123.1234567)),
      ?_assertEqual([49,50,51,46,49,50,51,52,53,55], to_list(123.1234567)),
      ?_assertNotEqual(["1","2","3",".","1","2","3","4","5","7"], to_list(123.1234567)),
      ?_assertEqual("123.123456", to_list(123.1234564)),
      ?_assertEqual([49,50,51,46,49,50,51,52,53,54], to_list(123.1234564)),
      ?_assertNotEqual(["1","2","3",".","1","2","3","4","5","6"], to_list(123.1234564)),
      ?_assertEqual("123.0", to_list(123.00000001)),
      ?_assertEqual([49,50,51,46,48], to_list(123.00000001)),
      ?_assertNotEqual(["1","2","3",".","0"], to_list(123.00000001)),
      ?_assertEqual("123.0", to_list(123.0000000001)),
      ?_assertEqual([49,50,51,46,48], to_list(123.0000000001)),
      ?_assertNotEqual(["1","2","3",".","0"], to_list(123.0000000001)),
      ?_assertEqual("-123.001", to_list(-123.001)),
      ?_assertEqual([45,49,50,51,46,48,48,49], to_list(-123.001)),
      ?_assertNotEqual(["-","1","2","3",".","0","0","1"], to_list(-123.001)),
      ?_assertEqual("<0.0.0>", to_list(c:pid(0,0,0))),
      ?_assertEqual([60,48,46,48,46,48,62], to_list(c:pid(0,0,0))),
      ?_assertNotEqual(["<","0",".","0",".","0",">"], to_list(c:pid(0,0,0)))
      ]}.

to_int_test_() ->
    {"transform to int",
     [?_assertEqual(0, to_int(0)),
      ?_assertEqual(0, to_int("")),
      ?_assertEqual(0, to_int([])),
      ?_assertEqual(0, to_int("0")),
      ?_assertEqual(0, to_int(<<"0">>)),
      ?_assertEqual(123, to_int("123")),
      ?_assertEqual(-123, to_int("-123")),
      ?_assertException(error, badarg, to_int("123.0")),
      ?_assertEqual(123, to_int(<<"123">>)),
      ?_assertEqual(-123, to_int(<<"-123">>)),
      ?_assertException(error, badarg, to_int(<<"123.0">>)),
      ?_assertEqual(123, to_int(123.0)),
      ?_assertEqual(-123, to_int(-123.0)),
      ?_assertEqual(123, to_int(123.01)),
      ?_assertEqual(123, to_int(123.001)),
      ?_assertEqual(123, to_int(123.000001)),
      ?_assertEqual(123, to_int(123.0000001)),
      ?_assertEqual(1, to_int(true)),
      ?_assertEqual(0, to_int(false)),
      ?_assertEqual(1, to_int(<<"true">>)),
      ?_assertEqual(0, to_int(<<"false">>)),
      ?_assertEqual(1, to_int(<<"TRUE">>)),
      ?_assertEqual(0, to_int(<<"FALSE">>)),
      ?_assertEqual(123, to_int(<<"123">>,0)),
      ?_assertEqual(123, to_int("123",0)),
      ?_assertEqual(123, to_int(123,0)),
      ?_assertEqual(123, to_int(123.01,0)),
      ?_assertEqual(0, to_int(<<"abc">>,0)),
      ?_assertEqual(0, to_int("abc",0)),
      ?_assertEqual(0, to_int(abc,0))
      ]}.

to_integer_test_() ->
    {"transform to integer",
     [?_assertEqual(0, to_integer(0)),
      ?_assertEqual(0, to_integer("")),
      ?_assertEqual(0, to_integer([])),
      ?_assertEqual(0, to_integer("0")),
      ?_assertEqual(0, to_integer(<<"0">>)),
      ?_assertEqual(123, to_integer("123")),
      ?_assertEqual(-123, to_integer("-123")),
      ?_assertException(error, badarg, to_integer("123.0")),
      ?_assertEqual(123, to_integer(<<"123">>)),
      ?_assertEqual(-123, to_integer(<<"-123">>)),
      ?_assertException(error, badarg, to_integer(<<"123.0">>)),
      ?_assertEqual(123, to_integer(123.0)),
      ?_assertEqual(-123, to_integer(-123.0)),
      ?_assertEqual(123, to_integer(123.01)),
      ?_assertEqual(123, to_integer(123.001)),
      ?_assertEqual(123, to_integer(123.000001)),
      ?_assertEqual(123, to_integer(123.0000001)),
      ?_assertEqual(1, to_integer(true)),
      ?_assertEqual(0, to_integer(false)),
      ?_assertEqual(1, to_integer(<<"true">>)),
      ?_assertEqual(0, to_integer(<<"false">>)),
      ?_assertEqual(1, to_integer(<<"TRUE">>)),
      ?_assertEqual(0, to_integer(<<"FALSE">>))
      ]}.

i_test_() ->
    {"transform i",
     [?_assertEqual(0, i(0)),
      ?_assertEqual(0, i("")),
      ?_assertEqual(0, i([])),
      ?_assertEqual(0, i("0")),
      ?_assertEqual(0, i(<<"0">>)),
      ?_assertEqual(123, i("123")),
      ?_assertEqual(-123, i("-123")),
      ?_assertException(error, badarg, i("123.0")),
      ?_assertEqual(123, i(<<"123">>)),
      ?_assertEqual(-123, i(<<"-123">>)),
      ?_assertException(error, badarg, i(<<"123.0">>)),
      ?_assertEqual(123, i(123.0)),
      ?_assertEqual(-123, i(-123.0)),
      ?_assertEqual(123, i(123.01)),
      ?_assertEqual(123, i(123.001)),
      ?_assertEqual(123, i(123.000001)),
      ?_assertEqual(123, i(123.0000001)),
      ?_assertEqual(1, i(true)),
      ?_assertEqual(0, i(false)),
      ?_assertEqual(1, i(<<"true">>)),
      ?_assertEqual(0, i(<<"false">>)),
      ?_assertEqual(1, i(<<"TRUE">>)),
      ?_assertEqual(0, i(<<"FALSE">>))
      ]}.

str_to_int_test_() ->
    {"transform str to int",
     [?_assertEqual(0, str_to_int("0")),
      ?_assertEqual(0, str_to_int(<<"0">>)),
      ?_assertEqual(123, str_to_int("123")),
      ?_assertEqual(-123, str_to_int("-123")),
      ?_assertEqual(123, str_to_int("123.0")),
      ?_assertEqual(-123, str_to_int("-123.0")),
      ?_assertEqual(123, str_to_int(<<"123">>)),
      ?_assertEqual(-123, str_to_int(<<"-123">>)),
      ?_assertEqual(123, str_to_int(<<"123.0">>)),
      ?_assertEqual(-123, str_to_int(<<"-123.0">>))
      ]}.


to_float_test_() ->
    {"transform to float",
     [?_assertException(error, badarg, to_float(<<>>)),
      ?_assertEqual(0.0, to_float("")),
      ?_assertException(error, function_clause, to_float('')),
      ?_assertEqual(0.0, to_float([])),
      ?_assertEqual(1.0, to_float(true)),
      ?_assertEqual(0.0, to_float(false)),
      ?_assertEqual(123.0, to_float(123.0)),
      ?_assertEqual(123.123456789, to_float(123.123456789)),
      ?_assertEqual(123.0, to_float(123)),
      ?_assertEqual(0.0, to_float(0)),
      ?_assertEqual(0.0, to_float(<<"0">>)),
      ?_assertEqual(0.123, to_float(<<"0.123">>)),
      ?_assertEqual(123.123456789, to_float(<<"123.123456789">>)),
      ?_assertEqual(-123.123, to_float(<<"-123.123">>)),
      ?_assertEqual(0.0, to_float("0")),
      ?_assertEqual(0.123, to_float("0.123")),
      ?_assertEqual(123.123456789, to_float("123.123456789")),
      ?_assertEqual(-123.123, to_float("-123.123"))
      ]}.

to_bool_test_() ->
    {"transform to bool",
     [?_assertEqual(false, to_bool(false)),
      ?_assertEqual(false, to_bool(0)),
      ?_assertEqual(false, to_bool(0.0)),
      ?_assertEqual(false, to_bool(undefined)),
      ?_assertEqual(false, to_bool("f")),
      ?_assertEqual(false, to_bool("false")),
      ?_assertEqual(false, to_bool("FALSE")),
      ?_assertEqual(false, to_bool(<<"f">>)),
      ?_assertEqual(false, to_bool(<<"false">>)),
      ?_assertEqual(false, to_bool(<<"FALSE">>)),
      ?_assertEqual(false, to_bool("0")),
      ?_assertEqual(false, to_bool(<<"0">>)),
      ?_assertEqual(false, to_bool(<<"abc">>)),
      ?_assertEqual(false, to_bool("abc")),
      ?_assertEqual(false, to_bool(<<"0.0123">>)),
      ?_assertEqual(false, to_bool("0.123")),
      ?_assertEqual(false, to_bool(<<"123.0123">>)),
      ?_assertEqual(false, to_bool("123.123")),
      ?_assertEqual(true, to_bool(true)),
      ?_assertEqual(true, to_bool(1)),
      ?_assertEqual(true, to_bool(0.1)),
      ?_assertEqual(true, to_bool(1.1)),
      ?_assertEqual(true, to_bool(abc)),
      ?_assertEqual(true, to_bool("t")),
      ?_assertEqual(true, to_bool("true")),
      ?_assertEqual(true, to_bool("TRUE")),
      ?_assertEqual(true, to_bool("1")),
      ?_assertEqual(true, to_bool(<<"t">>)),
      ?_assertEqual(true, to_bool(<<"true">>)),
      ?_assertEqual(true, to_bool(<<"TRUE">>)),
      ?_assertEqual(true, to_bool(<<"1">>)),
      ?_assertEqual(true, to_bool(<<"123">>)),
      ?_assertEqual(true, to_bool("123"))
      ]}.

to_unicode_list_test_() ->
    {"transform to unicode list",
     [?_assertEqual("abc", to_unicode_list("abc")),
      ?_assertEqual("abc", to_unicode_list("abc",utf8)),
      ?_assertEqual("abc/utf8", to_unicode_list("abc/utf8",utf8)),
      ?_assertEqual("абв", to_unicode_list("абв")),
       ?_assertEqual("абв/utf8", to_unicode_list("абв/utf8")),
      ?_assertEqual("абв", to_unicode_list("абв",utf8)),
      ?_assertEqual("abc", to_unicode_list(<<"abc">>)),
      ?_assertEqual("abc", to_unicode_list(<<"abc">>,utf8)),
      ?_assertEqual("abc", to_unicode_list(<<"abc"/utf8>>,utf8)),
      ?_assertNotEqual("абв", to_unicode_list(<<"абв">>)),
       ?_assertEqual("абв", to_unicode_list(<<"абв"/utf8>>)),
      ?_assertNotEqual("абв", to_unicode_list(<<"абв">>,utf8)),
      ?_assertEqual("abc", to_unicode_list(abc)),
      ?_assertEqual("abc", to_unicode_list(abc,utf8))
      ]}.

to_unicode_binary_test_() ->
    {"transform to unicode binary",
     [?_assertEqual(<<"abc">>, to_unicode_binary("abc")),
      ?_assertEqual(<<"abc">>, to_unicode_binary("abc",utf8)),
      ?_assertEqual(<<"abc/utf8">>, to_unicode_binary("abc/utf8",utf8)),
      ?_assertEqual(<<"абв"/utf8>>, to_unicode_binary("абв")),
       ?_assertEqual(<<"абв/utf8"/utf8>>, to_unicode_binary("абв/utf8")),
      ?_assertEqual(<<"абв"/utf8>>, to_unicode_binary("абв",utf8)),
      ?_assertEqual(<<"abc">>, to_unicode_binary(<<"abc">>)),
      ?_assertEqual(<<"abc">>, to_unicode_binary(<<"abc">>,utf8)),
      ?_assertEqual(<<"abc">>, to_unicode_binary(<<"abc"/utf8>>,utf8)),
      ?_assertEqual(<<"абв">>, to_unicode_binary(<<"абв">>)),
       ?_assertEqual(<<"абв"/utf8>>, to_unicode_binary(<<"абв"/utf8>>)),
      ?_assertEqual(<<"абв">>, to_unicode_binary(<<"абв">>,utf8)),
      ?_assertEqual(<<"abc">>, to_unicode_binary(abc)),
      ?_assertEqual(<<"abc">>, to_unicode_binary(abc,utf8))
      ]}.

any_to_str_test_() ->
    {"transform any to str",
     [?_assertEqual("\"abc\"", any_to_str("abc")),
      ?_assertEqual("<<\"abc\">>", any_to_str(<<"abc">>)),
      ?_assertEqual("abc", any_to_str(abc)),
      ?_assertEqual("123", any_to_str(123)),
      ?_assertEqual("{\"abc\",\"def\"}", any_to_str({"abc","def"})),
      ?_assertEqual("{\"abc\",[\"def\"]}", any_to_str({"abc",["def"]})),
      ?_assertEqual("{\"abc\",[\"def\",\"ghi\"]}", any_to_str({"abc",["def", "ghi"]})),
      ?_assertEqual("{123,[abc,\"def\"]}", any_to_str({123,[abc, "def"]})),
      ?_assertEqual("[{123,abc},[{def,\"ghi\"},{jkl,<<\"mno\">>}]]", any_to_str([{123,abc},[{def,"ghi"},{jkl,<<"mno">>}]]))
      ]}.

to_atom_test_() ->
    {"transform to atom",
     [?_assertEqual(abc, to_atom(abc)),
      ?_assertEqual(abc, to_atom("abc")),
      ?_assertEqual(abc, to_atom(<<"abc">>))
      ]}.

to_atom_new_test_() ->
    {"transform to atom new",
     [?_assertEqual(abc_new_1, to_atom_new(abc_new_1)),
      ?_assertEqual(abc_new_2, to_atom_new("abc_new_2")),
      ?_assertEqual(abc_new_3, to_atom_new(<<"abc_new_3">>))
      ]}.

emptyid_test_() ->
    {"emptyid",
     [?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, emptyid())
      ]}.

to_guid_test_() ->
    {"transform to guid",
     [?_assertEqual(<<"12345678-12ab-34cd-56ef-abcdef123456">>, to_guid(<<"12345678-12AB-34CD-56EF-ABCDEF123456">>)),
      ?_assertEqual(<<"12345678-12ab-34cd-56ef-abcdef123456">>, to_guid(<<"1234567812AB34CD56EFABCDEF123456">>)),
      ?_assertEqual(<<"12345678-12ab-34cd-56ef-abcdef123456">>, to_guid("12345678-12AB-34CD-56EF-ABCDEF123456")),
      ?_assertEqual(<<"12345678-12ab-34cd-56ef-abcdef123456">>, to_guid("1234567812AB34CD56EFABCDEF123456")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("1234567812AB34CD56EFABCDEF1234567")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid(<<"1234567812AB34CD56EFABCDEF1234567">>)),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("123456781-12AB-34CD-56EF-ABCDEF123456")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("12345678-12AB1-34CD-56EF-ABCDEF123456")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("12345678-12AB-34CD1-56EF-ABCDEF123456")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("12345678-12AB-34CD-56EF1-ABCDEF123456")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("12345678-12AB-34CD-56EF-ABCDEF1234561")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid(<<>>)),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid([])),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid(abc)),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid("test")),
      ?_assertEqual(<<"00000000-0000-0000-0000-000000000000">>, to_guid(<<"test">>))
      ]}.

check_guid_test_() ->
    {"check guid",
     [?_assertEqual(<<"12345678-12ab-34cd-56ef-abcdef123456">>, check_guid(<<"12345678-12AB-34CD-56EF-ABCDEF123456">>)),
      ?_assertException(throw, {error,"invalid guid symbols"}, check_guid(<<"12345678 12AB 34CD 56EF ABCDEF123456">>)),
      ?_assertException(throw, {error,"invalid guid symbols"}, check_guid(<<"12345678:12AB:34CD:56EF:ABCDEF123456">>)),
      ?_assertEqual(<<"123456789abcdef">>, check_guid(<<"123456789abcdef">>)),
      ?_assertException(throw, {error,"invalid guid symbols"}, check_guid(<<"а"/utf8>>)),
      ?_assertException(throw, {error,"invalid guid symbols"}, check_guid(<<"j">>)),
      ?_assertException(throw, {error,"invalid guid symbols"}, check_guid(<<"+">>)),
      ?_assertException(throw, {error,"invalid guid symbols"}, check_guid(<<"test">>))
      ]}.

newid_test_() ->
    {"newid",
     [?_assertEqual({match,[[{0,36}]]}, re:run(newid(),<<"[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}">>,[global]))
      ]}.

newid_v3_test_() ->
    {"newid_v3",
     [?_assertEqual({match,[[{0,36}]]}, re:run(newid_v3(<<"a">>),<<"[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}">>,[global])),
      ?_assertEqual(true, newid_v3(<<"a">>) == newid_v3(<<"a">>))
      ]}.

is_int_val_test_() ->
    {"is int val",
     [?_assertEqual(true, is_int_val(0)),
      ?_assertEqual(true, is_int_val(123)),
      ?_assertEqual(true, is_int_val(-123)),
      ?_assertEqual(true, is_int_val(<<"0">>)),
      ?_assertEqual(true, is_int_val(<<"123">>)),
      ?_assertEqual(true, is_int_val(<<"-123">>)),
      ?_assertEqual(true, is_int_val("0")),
      ?_assertEqual(true, is_int_val("123")),
      ?_assertEqual(true, is_int_val("-123")),
      ?_assertEqual(false, is_int_val(123.01)),
      ?_assertEqual(false, is_int_val("123.01")),
      ?_assertEqual(false, is_int_val(<<"123.01">>)),
      ?_assertEqual(false, is_int_val(-123.01)),
      ?_assertEqual(false, is_int_val("-123.01")),
      ?_assertEqual(false, is_int_val(<<"-123.01">>)),
      ?_assertEqual(false, is_int_val("abc")),
      ?_assertEqual(false, is_int_val(<<"abc">>)),
      ?_assertEqual(false, is_int_val(abc)),
      ?_assertEqual(false, is_int_val("123abc")),
      ?_assertEqual(false, is_int_val(<<"123abc">>)),
      ?_assertEqual(false, is_int_val("abc123")),
      ?_assertEqual(false, is_int_val(<<"abc123">>))
      ]}.

is_guid_test_() ->
    {"is guid",
       [?_assert(case is_guid(<<"12345678-12AB-34CD-56EF-ABCDEF123456">>) of {true,_} -> true; _ -> false end),
      ?_assertNot(case is_guid(<<"12345678 12AB 34CD 56EF ABCDEF123456">>) of {true,_} -> true; _ -> false end),
      ?_assertNot(case is_guid(<<"12345678:12AB:34CD:56EF:ABCDEF123456">>) of {true,_} -> true; _ -> false end),
      ?_assertNot(case is_guid(<<"123456789abcdef">>) of {true,_} -> true; _ -> false end),
      ?_assertNot(case is_guid(<<"12345678-12AB-34CD-56EF-АBCDEF123456"/utf8>>) of {true,_} -> true; _ -> false end),
      ?_assertNot(case is_guid(<<"test">>) of {true,_} -> true; _ -> false end)
      ]}.

pid_to_bin_test_() ->
    Pid = erlang:list_to_pid("<0.1.0>"),
    {"pid_to_bin",
     [?_assertEqual(<<"{0,1,0}">>,pid_to_bin(Pid))
      ]}.

bin_to_pid_test_() ->
    Pid = erlang:list_to_pid("<0.1.0>"),
    {"bin_to_pid",
     [?_assertEqual(Pid,bin_to_pid(<<"{0,1,0}">>))
      ]}.

trunc_test_() ->
    {"trunc",
     [?_assertEqual(0.0, trunc(0,5)),
      ?_assertEqual(123.12, trunc(123.123,2)),
      ?_assertEqual(123.123, trunc(123.123,3)),
      ?_assertEqual(123.123, trunc(123.123,4)),
      ?_assertEqual(123.1234, trunc(123.123456789,4)),
      ?_assertEqual(-123.1234, trunc(-123.123456789,4)),
      ?_assertEqual(123.1234567890, trunc(123.1234567890123456789,10)),
      ?_assertEqual(123.12345678901, trunc(123.1234567890123456789,11)),
      ?_assertEqual(123.0, trunc(123,4))
      ]}.

unempty_test_() ->
    {"unempty",
     [?_assertEqual(b, unempty(a,a,b)),
      ?_assertEqual(a, unempty(a,b,b)),
      ?_assertEqual(1, unempty(1,undefined,2)),
      ?_assertEqual(2, unempty(undefined,undefined,2)),
      ?_assertEqual(6, unempty(6,fun(X) when X==1;X==2;X==3;X==4;X==5 -> true; (_) -> false end,2)),
      ?_assertEqual(2, unempty(5,fun(X) when X==1;X==2;X==3;X==4;X==5 -> true; (_) -> false end,2))
     ]}.

-endif.

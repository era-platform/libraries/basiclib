%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.02.2019
%%% @doc Semantic version parser.

-module(basiclib_utils_semver).
-author(['Anton Makarov <anton@mastermak.ru>','Peter Bukashin <tbotc@yandex.ru>']).

-export([parse_semver/1]).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%% Return semantic version parsed from binary string as {Major,Minor,Patch,PreRelease}.
%% --------------------------------------
-spec parse_semver(SemVer::binary()) -> {ok,{Major::binary(),Minor::binary(),Patch::binary(),PreRelease::binary()}} |
                                        {error,Reason::term()}.
%% --------------------------------------
parse_semver(SemVer) when is_binary(SemVer) ->
    <<_:1/binary,VsnX/binary>> = SemVer,
    XInt = binary:at(SemVer,0),
    VsnBin = case XInt >= $0 andalso XInt =< $9 of
                 true -> SemVer;
                 false -> VsnX % eat 'v' prefix (actually, any non-digit (!)).
             end,
    Query = <<"^([0-9]+)\\.([0-9]+)\\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\\.[0-9A-Za-z-]+)*))?(?:\\+[0-9A-Za-z-]+)?$">>,
    case re:run(VsnBin,Query,[global,dotall]) of
        nomatch -> {error,<<"bad_vsn">>};
        {match,Matches} ->
            FData = fun({_Start,_Cnt}=PosLen,Acc) -> Acc++[binary:part(VsnBin,PosLen)] end,
            {ok,lists:foldl(FData,[],lists:flatten(Matches))}
    end.

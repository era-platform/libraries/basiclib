%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Checking and Executing Functions

-module(basiclib_utils_fun).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% executes function with 0 args
%% ------------------------------
exec(F) when is_function(F) -> F().

exec(F,Args) when is_function(F) -> erlang:apply(F,Args).

%% XXX what's wrong with erlang:spawn/1 that already does exactly the same ?
spawn_fun(F) when is_function(F) ->
    spawn(?MODULE, exec, [F]).

%% ------------------------------
%% checks if function exported
%% ------------------------------
function_exported(M,F,Arity) ->
    case erlang:function_exported(M, F, Arity) of
        true -> true;
        false ->
            case code:is_loaded(M) of
                false -> code:ensure_loaded(M);
                _ -> ok
            end,
            erlang:function_exported(M, F, Arity)
    end.

%% ------------------------------
%% Sends message to on current site
%% ------------------------------
send_to_pid(Pid,Msg) -> Pid ! Msg.

%% ------------------------------
%% Monitor / demonitor pid from other node,site through unknown channel managed by using module
%%   Some transport function could be passed as MFA, ex.
%%      NotifyTo = {{CurSite,node()},Self},
%%      MFA_here = {?MODULE, event, [Self, Msg]},
%%      MFA_there = [{timer,sleep,[1000]},
%%                   {?CROSS,call_node,[{?CFG:get_current_site(),node()},MFA_here,undefined,5000]}]
%% ------------------------------
monitor_pid(Pid,From,MFAs) when is_list(MFAs) ->
    ?BLopts:set_group_leader(),
    MonRef = make_ref(),
    FClear = fun(_Reason) -> lists:foreach(fun({M,F,A}) -> erlang:apply(M,F,A) end, MFAs) end,
    ?BLmonitor:append_fun(Pid,From,FClear),
    MonRef.
%%
demonitor_pid(Pid,{{_,_}=_Dest,_Pid}=From) ->
    ?BLmonitor:drop_fun(Pid,From).

%% ------------------------------------
%% Executes function limited by timer.
%% If timeout then asynchronously stops node
%% ------------------------------------
exec_fun_timered(F,Timeout) when is_function(F), is_integer(Timeout) ->
    exec_fun_timered(F,fun() -> ?BU:stop_node(0) end,Timeout).

%% ------------------------------------
%% Executes function limited by timer.
%% If timeout then asyncronously execute stop_fun
%% ------------------------------------
exec_fun_timered(F,FStop,Timeout) when is_function(F), is_function(FStop), is_integer(Timeout) ->
    Ref = make_ref(),
    Pid = spawn(fun() ->
                    receive {ok,Ref} -> ok
                    after Timeout -> ?OUT('$warning', "exec_fun_timered fstop by ~p", [F]), FStop()
                    end end),
    R = F(),
    Pid ! {ok,Ref},
    R.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% ==================================

exec_test_() ->
    {"exec/1 test",
     [?_assertEqual(8 - 2, ?MODULE:exec(fun() -> 2 + 2 * 2 end))]
    }.

%% ==================================

spawn_fun_test_() ->
    {"spawn_fun/1 test",
     fun() ->
         euf_ets_init(eft_counter),
         Fspawn = fun FS() ->
                      receive
                          {stop} -> ok
                      after 100 ->
                          euf_ets_increment(eft_counter),
                          FS()
                      end
                  end,
         Pid = ?MODULE:spawn_fun(Fspawn),
         ?assertNotEqual(Pid, self()),
         timer:sleep(200),
         Counter = euf_ets_lookup(eft_counter),
         ?assert(Counter > 0),
         Pid ! {stop}
     end
    }.

%% ==================================

exec_fun_timered_fstop_called_test_() ->
    {"exec_fun_timered/3 test fstop is called",
     fun() ->
         TestRunner = self(),
         euf_ets_init(eft_counter),
         euf_ets_init(eft_stop),
         FTimered = fun FT() ->
                        receive
                            {fstop} -> ok % return
                        after 100 ->
                            euf_ets_increment(eft_counter),
                            FT()
                        end
                    end,
         FStop = fun() ->
                     euf_ets_increment(eft_stop),
                     TestRunner ! {fstop}
                 end,
         Timeout = 1000,
         ?MODULE:exec_fun_timered(FTimered, FStop, Timeout),
         Counter = euf_ets_lookup(eft_counter),
         Stop = euf_ets_lookup(eft_stop),
         ?assert(Counter > 5 andalso Counter < 11),
         ?assert(Stop == 1)
     end}.

exec_fun_timered_fstop_not_called_test_() ->
    {"exec_fun_timered/3 test fstop is not called",
     fun() ->
         TestRunner = self(),
         euf_ets_insert({eft_counter, 5}),
         euf_ets_init(eft_stop),
         FTimered = fun FT() ->
                        receive
                            {fstop} -> ok % return by stop
                        after 100 ->
                            case euf_ets_decrement(eft_counter) of
                                0 -> ok; % return by counter
                                _ -> FT()
                            end
                        end
                    end,
         FStop = fun() ->
                     euf_ets_increment(eft_stop),
                     TestRunner ! {fstop}
                 end,
         Timeout = 1000,
         ?MODULE:exec_fun_timered(FTimered, FStop, Timeout),
         Counter = euf_ets_lookup(eft_counter),
         Stop = euf_ets_lookup(eft_stop),
         ?assert(Counter == 0), % finished countdown
         ?assert(Stop == 0) % and stop was never called
     end
    }.

%% ==================================
%% Internal test functions
%% ==================================

-define(BUF_ETS_TABLE, basiclib_utils_fun).

euf_ets_insert(Tuple) ->
    case ets:info(?BUF_ETS_TABLE) of undefined -> ?BUF_ETS_TABLE = ets:new(?BUF_ETS_TABLE, [set, public, named_table]); _ -> ok end,
    true = ets:insert(?BUF_ETS_TABLE, Tuple).

euf_ets_lookup(Key) ->
    case ets:lookup(?BUF_ETS_TABLE, Key) of
        [] -> throw({error,"Key not found in EUF_ETS_TABLE"});
        [{Key,Value}] -> Value
    end.

euf_ets_init(Key) ->
    euf_ets_insert({Key, 0}).

euf_ets_update_with(Key, Fun) ->
    Value = Fun(euf_ets_lookup(Key)),
    euf_ets_insert({Key, Value}),
    Value.

euf_ets_increment(Key) ->
    euf_ets_update_with(Key, fun(Value) -> Value + 1 end).

euf_ets_decrement(Key) ->
    euf_ets_update_with(Key, fun(Value) -> Value - 1 end).

-endif.

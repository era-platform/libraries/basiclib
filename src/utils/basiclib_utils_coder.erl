%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Functions that implement the hashing algorithms

-module(basiclib_utils_coder).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% MD5
%% ------------------------------

md5(X) when is_list(X); is_binary(X); is_atom(X) ->
    F=fun([]) -> [$0,$0];
         ([A]) -> [$0,A];
         ([A,B]) -> [A,B]
      end,
    ?BU:to_binary(lists:foldl(fun(A,Acc) -> Acc ++ F(integer_to_list(A,16)) end, [], ?BU:to_list(crypto:hash(md5,?BU:to_binary(X))))).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% ==================================

md5_test_() ->
    {"md5/1 test",
     [
      % same value using different types
      ?_assertEqual(<<"900150983CD24FB0D6963F7D28E17F72">>, ?MODULE:md5("abc")),
      ?_assertEqual(<<"900150983CD24FB0D6963F7D28E17F72">>, ?MODULE:md5(<<"abc">>)),
      ?_assertEqual(<<"900150983CD24FB0D6963F7D28E17F72">>, ?MODULE:md5(abc)),
      % some other values
      ?_assertEqual(<<"B45CFFE084DD3D20D928BEE85E7B0F21">>, ?MODULE:md5("string")),
      ?_assertEqual(<<"E31F1273FA25B378C4B6B8AB7CBA87D1">>, ?MODULE:md5(<<"bitstring">>)),
      ?_assertEqual(<<"3E10F8C809242D3A0F94C18E7ADDB866">>, ?MODULE:md5(atom)),
      % some other stranger values
      ?_assertEqual(<<"93B885ADFE0DA089CDF634904FD59F71">>, ?MODULE:md5(<<0>>)),
      ?_assertEqual(<<"C4103F122D27677C9DB144CAE1394A66">>, ?MODULE:md5(<<0,0>>)),
      ?_assertEqual(<<"693E9AF84D3DFCC71E640E005BDC5E2E">>, ?MODULE:md5(<<0,0,0>>)),
      ?_assertEqual(<<"55A54008AD1BA589AA210D2629C1DF41">>, ?MODULE:md5(<<1>>)),
      ?_assertEqual(<<"37B59AFD592725F9305E484A5D7F5168">>, ?MODULE:md5(<<0,1,2,3>>)),
      ?_assertEqual(<<"E55364919FA622BE72642C4A25FE83E3">>, ?MODULE:md5(<<0,1,2,3,0>>)),
      ?_assertEqual(<<"8597D4E7E65352A302B63E07BC01A7DA">>, ?MODULE:md5(<<255,255,255>>))
     ]
    }.

-endif.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Operations with json data representation in erlang

-module(basiclib_utils_mapjson).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% json <=> map transform
%% ------------------------------

%%
map_to_json(Map) ->
    case maps:to_list(Map) of
        [] -> [{}];
        List ->
            lists:map(fun({K,V}) when is_map(V) -> {K,map_to_json(V)};
                          (X) -> X
                        end, List)
    end.

%%
json_to_map([{}]) -> #{};
json_to_map([]) -> [];
json_to_map(J) when is_list(J) ->
    case is_props_list(J) of
        false ->
            lists:map(fun(E) when is_list(E) -> json_to_map(E);
                         (X) -> X
                      end, J);
        true ->
            V1 = lists:map(fun({K,V}) when is_list(V) -> {K,json_to_map(V)};
                               (X) -> X
                           end, J),
            maps:from_list(V1)
    end.

%% checks if Value is prop-list
is_props_list(List) when is_list(List) ->
    lists:all(fun({K,_}) when is_atom(K); is_binary(K) -> true; (_) -> false end, List);
is_props_list(_) -> false.

%% make new list, containing only props elements ({K,V})
filter_props_list(Map) when is_map(Map) -> filter_props_list(maps:to_list(Map));
filter_props_list(List) when is_list(List) ->
    lists:filter(fun({K,_}) when is_atom(K); is_binary(K) -> true; (_) -> false end, List).

%% make new list, containing only props elements ({K,V}) and make ({K,true}) for single keys in list
make_props_list(Map) when is_map(Map) -> make_props_list(maps:to_list(Map));
make_props_list(List) when is_list(List) ->
    lists:filtermap(fun({K,_}=Elt) when is_atom(K); is_binary(K) -> {true,Elt};
                       (K) when is_atom(K); is_binary(K) -> {true,{K,true}};
                       (_) -> false end, List).

%% merges m2 to m1, prefer m1 values.
merge_maps(M1, M2, 1) when is_map(M1), is_map(M2) -> maps:merge(M2, M1); % sequence differs to lists:ukeymerge !!!
merge_maps(M1, M2, Level) when is_map(M1), is_map(M2), is_integer(Level), Level>0 ->
    maps:fold(fun(K,V1,Acc) when is_map(V1) ->
                      case maps:get(K,Acc,undefined) of
                          undefined -> Acc#{K=>V1};
                            V2 when is_map(V2) -> Acc#{K:=merge_maps(V1,V2,Level-1)};
                          _ -> Acc#{K:=V1}
                      end;
                 (K,V,Acc) -> Acc#{K=>V}
              end, M2, M1).

%% ----------------------
%% Make sure and reformat given data to make it json-encodable.
%% ----------------------
-spec ensure_jsonable(A::term()) -> B::term().
%% ----------------------
ensure_jsonable(A) when is_atom(A) -> A;
ensure_jsonable(A) when is_binary(A) -> A;
ensure_jsonable(A) when is_number(A) -> A;
ensure_jsonable(A) when is_reference(A); is_port(A); is_pid(A); is_function(A) -> ?BU:strbin("~tp",[A]);
ensure_jsonable(A) when is_tuple(A) -> ensure_jsonable_deep(A);
ensure_jsonable(A) when is_list(A) -> ensure_jsonable_deep(A);
ensure_jsonable(A) when is_map(A) -> ensure_jsonable_deep(A).

%% @private
ensure_jsonable_deep(A) when is_tuple(A) -> ensure_jsonable_deep([<<"!tuple!">> | tuple_to_list(A)]);
ensure_jsonable_deep(A) when is_list(A) -> case ?BU:is_props_list(A) of
                                               true ->
                                                   lists:foldr(fun({K,V}, Acc) when is_atom(K); is_binary(K) ->
                                                                   [{K, ensure_jsonable(V)} | Acc];
                                                                  ({K,V}, Acc) ->
                                                                   [{ensure_jsonable_key(K), ensure_jsonable(V)} | Acc]
                                                               end, [], A);
                                               false ->
                                                   [ensure_jsonable(X) || X <- A]
                                           end;
ensure_jsonable_deep(A) when is_map(A) -> maps:fold(fun(K,V,Acc) when is_atom(K); is_binary(K) ->
                                                        Acc#{K => ensure_jsonable(V)};
                                                       (K,V,Acc) ->
                                                        Acc#{ensure_jsonable_key(K) => ensure_jsonable(V)}
                                                    end, #{}, A).

%% @private
ensure_jsonable_key(K) ->
    try ?BU:to_binary(K)
    catch error:function_clause -> ?BU:strbin("~tp", [K])
    end.


%% --------------------------------------
%% Returns value ready to jsx:encode, filters inappropriate values
%% --------------------------------------
filter_jsonable(Data) -> fjx(Data).

%% @private
fjx(undefined) -> undefined;
fjx(Data) when is_binary(Data); is_number(Data); is_atom(Data) -> Data;
fjx([{}]=Data) -> Data;
fjx(Data) when is_list(Data) ->
    IsPropList = lists:all(fun({K,_}) when is_atom(K); is_binary(K) -> true; (_) -> false end, Data),
    case lists:filtermap(fun({K,V}) when IsPropList==true -> case fjx(V) of undefined -> false; X -> {true,{K,X}} end;
                            (V) -> case fjx(V) of undefined -> false; X -> {true,X} end
                         end, Data)
    of
        R when length(R) == length(Data) -> R;
        _ -> undefined
    end;
fjx(Data) when is_map(Data) ->
    maps:fold(fun(Key,Value,Acc) ->
                    case fjx(Value) of
                        undefined -> Acc;
                        R -> Acc#{Key => R}
                    end end, #{}, Data);
fjx(_) -> undefined.

%% --------------------------------------
%% --------------------------------------
encode_to_json(Data) -> jsx:encode(filter_jsonable(Data)).

%% --------------------------------------
%% --------------------------------------
map_diff(M1,M2) ->
    maps:fold(fun(K,V,Acc) -> case maps:get(K,M2,undefined) of V -> Acc; V1 -> Acc#{K => {V,V1}} end end, #{}, M1).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% ==================================

map_to_json_test_() ->
    {"map_to_json/1 test",
     [
      ?_assertEqual([{}], ?MODULE:map_to_json(#{})),
      ?_assertEqual([{<<"false">>, false}, {<<"float">>, 1.2345}, {<<"int">>, 1024}, {<<"list">>, [1,2,3]},
                     {<<"null">>, null}, {<<"str">>, <<"ing">>}, {<<"true">>, true}],
                    ?MODULE:map_to_json(#{
                       <<"str">> => <<"ing">>, <<"int">> => 1024, <<"float">> => 1.2345, <<"list">> => [1,2,3],
                       <<"true">> => true, <<"false">> => false, <<"null">> => null})),
      ?_assertEqual([{<<"nested">>, [{<<"deep">>, [{<<"ly">>, [{}]}]}]}],
                    ?MODULE:map_to_json(#{
                       <<"nested">> => #{<<"deep">> => #{<<"ly">> => #{}}}}))
     ]
    }.

%% ==================================

json_to_map_test_() ->
    {"json_to_map/1 test",
     [
      ?_assertEqual(#{}, ?MODULE:json_to_map([{}])),
      ?_assertEqual([], ?MODULE:json_to_map([])),
      ?_assertEqual(#{<<"str">> => <<"ing">>, <<"int">> => 1024, <<"float">> => 1.2345, <<"list">> => [1,2,3],
                      <<"true">> => true, <<"false">> => false, <<"null">> => null},
                    ?MODULE:json_to_map([{<<"false">>, false}, {<<"float">>, 1.2345}, {<<"int">>, 1024}, {<<"list">>, [1,2,3]},
                                         {<<"null">>, null}, {<<"str">>, <<"ing">>}, {<<"true">>, true}])),
      ?_assertEqual(#{<<"nested">> => #{<<"deep">> => #{<<"ly">> => #{}}}},
                    ?MODULE:json_to_map([{<<"nested">>, [{<<"deep">>, [{<<"ly">>, [{}]}]}]}])),
      ?_assertEqual([#{key => val}, #{second => <<"map">>}], ?MODULE:json_to_map([[{key,val}],[{second,<<"map">>}]]))
     ]
    }.

%% ==================================

is_props_list_test_() ->
    {"is_props_list/1 test",
     [
      ?_assert(?MODULE:is_props_list([])),
      ?_assertNot(?MODULE:is_props_list([{}])),
      ?_assert(?MODULE:is_props_list([{a,b}])),
      ?_assertNot(?MODULE:is_props_list([{a,b},{1,2}])), % because key 1 is neither binary nor atom
      ?_assertNot(?MODULE:is_props_list([{a,b},{1,2},false])),
      ?_assertNot(?MODULE:is_props_list([{a,b},null,{1,2}])),
      ?_assertNot(?MODULE:is_props_list([{a,b},{1,2,3}])),
      ?_assertNot(?MODULE:is_props_list([{a,b},{1},{c,d}]))
     ]
    }.

%% ==================================

merge_maps_test_() ->
    {"merge_maps/1 test",
     [
      % level 1
      ?_assertEqual(#{a => 1, b => 2}, ?MODULE:merge_maps(#{a => 1}, #{b => 2}, 1)),
      ?_assertEqual(#{a => 1, c => 4}, ?MODULE:merge_maps(#{a => 1}, #{a => 3, c => 4}, 1)),
      % level 2
      ?_assertEqual(#{a => #{a => 1}, b => #{b => 2}}, ?MODULE:merge_maps(#{a => #{a => 1}}, #{b => #{b => 2}}, 2)),
      ?_assertEqual(#{a => #{a => 1, b => 2}}, ?MODULE:merge_maps(#{a => #{a => 1}}, #{a => #{b => 2}}, 2)),
      ?_assertEqual(#{a => #{a => 1, c => 4}}, ?MODULE:merge_maps(#{a => #{a => 1}}, #{a => #{a => 3, c => 4}}, 2)),
      % level 3
      ?_assertEqual(#{a => #{a => #{a => 1}}, b => #{b => #{b => 2}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{b => #{b => #{b => 2}}}, 3)),
      ?_assertEqual(#{a => #{a => #{a => 1}, b => #{b => 2}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{b => #{b => 2}}}, 3)),
      ?_assertEqual(#{a => #{a => #{a => 1, b => 2}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{a => #{b => 2}}}, 3)),
      ?_assertEqual(#{a => #{a => #{a => 1, c => 4}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{a => #{a => 3, c => 4}}}, 3)),
      % level 2 with level 3 cases
      ?_assertEqual(#{a => #{a => #{a => 1}}, b => #{b => #{b => 2}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{b => #{b => #{b => 2}}}, 2)),
      ?_assertEqual(#{a => #{a => #{a => 1}, b => #{b => 2}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{b => #{b => 2}}}, 2)),
      ?_assertEqual(#{a => #{a => #{a => 1}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{a => #{b => 2}}}, 2)),
      % level 1 with level3 cases
      ?_assertEqual(#{a => #{a => #{a => 1}}, b => #{b => #{b => 2}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{b => #{b => #{b => 2}}}, 1)),
      ?_assertEqual(#{a => #{a => #{a => 1}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{b => #{b => 2}}}, 1)),
      ?_assertEqual(#{a => #{a => #{a => 1}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{a => #{b => 2}}}, 1)),
      ?_assertEqual(#{a => #{a => #{a => 1}}}, ?MODULE:merge_maps(#{a => #{a => #{a => 1}}}, #{a => #{a => #{a => 3, c => 4}}}, 1))
     ]
    }.

-endif.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.04.2018.
%%% @doc Converting and validating database connection strings

-module(basiclib_utils_dbconnstr).
-author(['Anton Makarov <anton@mastermak.ru>','Peter Bukashin <tbotc@yandex.ru>']).

-export([parse_dbconnstr/1,
         parse_dbconnstr/2,
         parse_dbconnstrs/1,
         parse_dbconn_any/1]).

-export([validate_dbconnstr/1,
         validate_dbconnstrs/1,
         validate_dbconn_proplist/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(DefPgPort, 5432).

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------
%% dbconnstr parse
%% -------------------------------------

%% --------
-spec parse_dbconnstr(ConnStr :: binary()) -> ConnPL :: [{K :: atom(), V :: string() | integer()}] | undefined.
parse_dbconnstr(ConnStrOrDict) ->
    parse_dbconn_string(ConnStrOrDict).

-spec parse_dbconnstr(ConnStr :: binary(), Database :: asis | string() | binary()) -> ConnPL :: [{K :: atom(), V :: binary() | integer()}] | undefined.
parse_dbconnstr(ConnStrOrDict, Database) ->
    parse_dbconn_string(ConnStrOrDict, Database).

%% --------
-spec parse_dbconnstrs(ConnStr :: binary() | [binary()]) -> ConnPLs :: [ [{K :: atom(), V :: binary() | integer()}] | undefined ].
parse_dbconnstrs([XConnStr|_]=ConnStrList) when is_binary(XConnStr) ->
    [parse_dbconnstr(ConnStr) || ConnStr <- ConnStrList];
parse_dbconnstrs(ConnStr) when is_binary(ConnStr) ->
    parse_dbconnstrs(binary:split(ConnStr, <<";">>, [global])).

%% --------
-spec parse_dbconn_any(ConnStr :: binary() | [{K::binary(),V::binary()|integer()}]) -> ConnPL :: [{K :: atom(), V :: string() | integer()}].
parse_dbconn_any(ConnStr) when is_binary(ConnStr) ->
    parse_dbconnstr(ConnStr);
parse_dbconn_any([{K,_}|_]=ConnPL) when is_binary(K) orelse is_atom(K) -> % XXX is it ?
    parse_dbconndict(ConnPL);
parse_dbconn_any([ConnStr|_]=ConnStrList) when is_binary(ConnStr) ->
    parse_dbconnstrs(ConnStrList);
parse_dbconn_any([]) -> [].

%% --------
-spec validate_dbconnstr(ConnStr :: binary()) -> ConnPL :: {ok, [{K :: atom(), V :: string() | integer()}]} | {error, Reason :: binary()}.
validate_dbconnstr(ConnStrOrDict) ->
    validate_dbconn_string(ConnStrOrDict).

%% --------
-spec validate_dbconnstrs(ConnStr :: binary()) -> ConnPL :: {ok, [ [{K :: atom(), V :: string() | integer()}] ]} | {error, Reason :: binary()}.
validate_dbconnstrs(ConnStrOrDict) ->
    validate_dbconn_strings(ConnStrOrDict).

%% ====================================================================
%% Internal functions
%% ====================================================================

parse_dbconn_string(ConnStr) when is_binary(ConnStr) ->
    parse_dbconn_string(ConnStr, asis);
parse_dbconn_string([{K,V}|_]=ConnDict) when is_binary(K) andalso (is_binary(V) orelse is_integer(V)) ->
    parse_dbconn_string(ConnDict, asis).

parse_dbconn_string(ConnStr, Database) when is_binary(ConnStr) ->
    V1 = binary:split(ConnStr, <<",">>,[global]),
    try lists:map(fun(Item) ->
                          try binary:split(Item, <<":">>,[global]) of
                              [K,V] ->
                                  AtomKey = ?BU:to_atom_new(K),
                                  PropValue =
                                      case AtomKey of
                                          port -> ?BU:to_int(V);
                                          database ->
                                              case Database of
                                                  asis -> ?BU:to_list(V);
                                                  _ -> ?BU:to_list(Database)
                                              end;
                                          _ -> ?BU:to_list(V)
                                      end,
                                  {AtomKey,PropValue};
                              _ -> throw(undefined)
                          catch
                              _:_ -> throw(undefined)
                          end
                  end,V1) of
        X -> X
    catch
        _:_ -> undefined
    end.

parse_dbconndict([H|_]=DbParams) when is_tuple(H) ->
    FunToTargetType = fun({Key,Value}) ->
                              case Key of
                                  <<"port">> -> {?BU:to_atom_new(Key),?BU:str_to_int(?BU:to_list(Value))};
                                  _Oth -> {?BU:to_atom_new(Key),?BU:to_list(Value)}
                              end
                      end,
    NewDbProp0 = lists:map(FunToTargetType,DbParams),
    NewDbProp =
        case lists:keymember(port, 1, NewDbProp0) of
            false -> NewDbProp0++[{port,?DefPgPort}];
            true -> NewDbProp0
        end,
    NewDbProp.

validate_dbconn_string(I) when is_binary(I) ->
    V1 = binary:split(I, <<",">>,[global]),
    V2 = lists:foldl(fun(_,{error,_}=Err) -> Err;
                        (K,Acc) ->
                             case catch binary:split(K, <<":">>,[global]) of
                                 %{'EXIT',_} -> error;
                                 [Kx,Vx] -> [{Kx,Vx}|Acc];
                                 _ -> {error, <<"separator ':' not found in '",K/binary,"'">>}
                             end end, [], V1),
    case V2 of
        {error,_}=Err -> Err;
        [_|_] -> validate_dbconn_proplist(V2)
    end.

validate_dbconn_strings(CS) when is_binary(CS) ->
    ConnStrs = binary:split(CS,<<";">>),
    lists:foldl(fun(_,{error,_}=Err) -> Err;
                   (ConnStr,{ok,Conns}) ->
                    case validate_dbconn_string(ConnStr) of
                        {ok,ConnPL} -> {ok,Conns++[ConnPL]};
                        {error,_}=Err -> Err
                    end
                end,
                {ok,[]},
                ConnStrs).

validate_dbconn_proplist([A,_|_]=V2) when is_tuple(A) ->
    CheckPort = fun(Port) ->
                    case ?BU:is_int_val(Port) of
                        true ->
                            IntPort = ?BU:to_int(Port),
                            case IntPort >= 0 andalso IntPort =< 65535 of
                                true -> ok;
                                false -> {error, <<"port value must be between 0 and 65535">>}
                            end;
                        false -> {error, <<"port value must be integer">>}
                    end
                end,
    ParamsToCheck = [<<"host">>,{<<"port">>,CheckPort},<<"login">>,<<"pwd">>,<<"database">>],
    lists:foldl(fun(_,{error,_}=Err) -> Err;
                   (CheckParam,{ok,_}=Acc) ->
                    ParamKey = case CheckParam of {P,PF} when is_function(PF) -> P; _ -> CheckParam end,
                    case lists:keyfind(ParamKey,1,V2) of
                        {_,ParamVal} ->
                            case CheckParam of
                                {_,Fun} when is_function(Fun) ->
                                case Fun(ParamVal) of
                                    ok -> Acc;
                                    {error,_}=Err -> Err
                                end;
                            _ -> Acc
                        end;
                    false -> {error, <<"missing '",CheckParam/binary,":' key">>}
                end
            end,
            {ok,V2},
            ParamsToCheck).

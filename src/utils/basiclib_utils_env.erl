%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.02.2021
%%% @doc Application environment variables

-module(basiclib_utils_env).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

get_env(App,Key) ->
    application:get_env(App, envkey(Key)).

get_env(App,Key,{fn,FunDefault}) ->
    case application:get_env(App, envkey(Key)) of
        undefined -> FunDefault();
        {ok,Value} -> Value
    end;
get_env(App,Key,Default) ->
    case application:get_env(App, envkey(Key)) of
        undefined -> Default;
        {ok,Value} -> Value
    end.

get_all_env(App) ->
    application:get_all_env(App).

set_env(App,Key,Value) ->
    application:set_env(App, envkey(Key), Value).

unset_env(App,Key) ->
    application:unset_env(App, envkey(Key)).

update_env(App,Opts) ->
    KeySpec = {app_updated_keys},
    PreviousKeys = get_env(App,KeySpec,[]),
    NewKeys = lists:map(fun({Key,_Value}) -> Key end, Opts),
    lists:foreach(fun(Key) -> unset_env(App,Key) end, PreviousKeys -- NewKeys),
    lists:foreach(fun({Key,Value}) -> set_env(App,Key,Value) end, Opts),
    set_env(App,KeySpec,NewKeys).

%% @private
envkey(Key) -> Key. % {app_option, Key}

%% ====================================================================
%% Internal functions
%% ====================================================================
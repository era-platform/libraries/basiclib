%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.10.2015
%%% @doc Environment utils and services facade.

-module(basiclib_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(BUcmd, basiclib_utils_cmd).
-define(BUcoder, basiclib_utils_coder).
-define(BUcompress, basiclib_utils_compress).
-define(BUdbstr, basiclib_utils_dbconnstr).
-define(BUdisk, basiclib_utils_disk).
-define(BUdomname, basiclib_utils_domainname).
-define(BUfun, basiclib_utils_fun).
-define(BUlists, basiclib_utils_lists).
-define(BUmapjson, basiclib_utils_mapjson).
-define(BUnetw, basiclib_utils_network).
-define(BUparsedt, basiclib_utils_parsedt).
-define(BUparsextract, basiclib_utils_parsextract).
-define(BUpath, basiclib_utils_path).
-define(BUprocess, basiclib_utils_process).
-define(BUrandom, basiclib_utils_random).
-define(BUrec, basiclib_utils_record).
-define(BUsemver, basiclib_utils_semver).
-define(BUset, basiclib_utils_set).
-define(BUsrvidx, basiclib_utils_srvidx).
-define(BUstring, basiclib_utils_string).
-define(BUtime, basiclib_utils_time).
-define(BUtimer, basiclib_utils_timer).
-define(BUtrans, basiclib_utils_transform).
-define(BUurl, basiclib_urlencode).
-define(BUwav, basiclib_wav).
-define(BUbestiface, basiclib_utils_bestinterface).
-define(BUenv, basiclib_utils_env).

-define(BS, bitstring).

%% ====================================================================
%% API Facade functions
%% ===================================================================

%% ------------------------------
%% Function methods
%% ------------------------------

exec(F) -> ?BUfun:exec(F).
exec(F,Args) -> ?BUfun:exec(F,Args).
spawn_fun(F) -> ?BUfun:spawn_fun(F).
function_exported(M,F,Arity) -> ?BUfun:function_exported(M,F,Arity).

send_to_pid(Pid,Msg) -> ?BUfun:send_to_pid(Pid,Msg).
monitor_pid(Pid,From,MFAs) -> ?BUfun:monitor_pid(Pid,From,MFAs).
demonitor_pid(Pid,From) -> ?BUfun:demonitor_pid(Pid,From).

exec_fun_timered(F,Timeout) -> ?BUfun:exec_fun_timered(F,Timeout).
exec_fun_timered(F,FStop,Timeout) -> ?BUfun:exec_fun_timered(F,FStop,Timeout).

%% ------------------------------
%% Transform methods
%% ------------------------------

to_binary(X) -> ?BUtrans:to_binary(X).
to_unicode_binary(X) -> ?BUtrans:to_unicode_binary(X).
ub(X) -> ?BUtrans:ub(X).
to_list(X) -> ?BUtrans:to_list(X).
to_unicode_list(X) -> ?BUtrans:to_unicode_list(X).
any_to_str(X) -> ?BUtrans:any_to_str(X).
to_int(X) -> ?BUtrans:to_int(X).
to_int(X,Default) -> ?BUtrans:to_int(X,Default).
to_integer(X) -> ?BUtrans:to_integer(X).
str_to_int(X) -> ?BUtrans:str_to_int(X).
to_float(X) -> ?BUtrans:to_float(X).
to_float(X,Def) -> ?BUtrans:to_float(X,Def).
as_number(X,Default) -> ?BUtrans:as_number(X,Default).
to_bool(X) -> ?BUtrans:to_bool(X).
to_bool(X,Def) -> ?BUtrans:to_bool(X,Def).
to_atom(X) -> ?BUtrans:to_atom(X).
to_atom_new(X) -> ?BUtrans:to_atom_new(X).
to_atom_safe(X) -> ?BUtrans:to_atom_safe(X).
to_guid(X) -> ?BUtrans:to_guid(X).
to_base64url(X) -> ?BUtrans:to_base64url(X).
from_base64url(X) -> ?BUtrans:from_base64url(X).
to_base64_ex(X) -> ?BUtrans:to_base64_ex(X).

pid_to_bin(X) -> ?BUtrans:pid_to_bin(X).
bin_to_pid(X) -> ?BUtrans:bin_to_pid(X).

luid() -> ?BUtrans:luid().
check_guid(X) -> ?BUtrans:check_guid(X).
emptyid() -> ?BUtrans:emptyid().
newid() -> ?BUtrans:newid().
newid_v3(X) -> ?BUtrans:newid_v3(X).
uuid_srvidx() -> ?BUtrans:uuid_srvidx().
parse_uuid_srvidx(X) -> ?BUtrans:parse_uuid_srvidx(X).
is_int_val(X) -> ?BUtrans:is_int_val(X).
is_pos_int(X) -> ?BUtrans:is_pos_int(X).
is_guid(X) -> ?BUtrans:is_guid(X).
i(X) -> ?BUtrans:i(X).
trunc(F,N) -> ?BUtrans:trunc(F,N).
is_socketid(X) -> ?BUtrans:is_socketid(X).

urlencode(S) -> ?BUurl:encode(S).
urldecode(S) -> ?BUurl:decode(S).

'if'(Eq,TrueValue,FalseValue) -> ?BUtrans:'if'(Eq,TrueValue,FalseValue).
unempty(Value,DefaultValue) -> ?BUtrans:unempty(Value,DefaultValue).
unempty(Value,NullValue,DefaultValue) -> ?BUtrans:unempty(Value,NullValue,DefaultValue).

%% ------------------------------
%% Extract, parse, update opts
%% ------------------------------

extract_required_props(Keys, Opts) -> ?BUparsextract:extract_required_props(Keys, Opts).
extract_optional_props(Keys, Opts) -> ?BUparsextract:extract_optional_props(Keys, Opts).
extract_optional_default(KeyVals, Opts) -> ?BUparsextract:extract_optional_default(KeyVals, Opts).
extract_optional_default(KeyVals, Opts, Defaults) -> ?BUparsextract:extract_optional_default(KeyVals, Opts, Defaults).
lists_get(Keys, List) -> ?BUparsextract:lists_get(Keys,List).
parse(X, Data) -> ?BUparsextract:parse(X, Data).
parse_map(X, Data) -> ?BUparsextract:parse_map(X, Data).
parse_map(X, Data, Default) -> ?BUparsextract:parse_map(X, Data, Default).
delete_keys(List, Keys) -> ?BUparsextract:delete_keys(List, Keys).
get_by_key(Key, List) -> ?BUparsextract:get_by_key(Key, List).
get_by_key(Key, List, Default) -> ?BUparsextract:get_by_key(Key, List, Default).
maps_get_default(KeyVals, Map) -> ?BUparsextract:maps_get_default(KeyVals, Map).
maps_get_default(Keys, Map, Defaults) -> ?BUparsextract:maps_get_default(Keys, Map, Defaults).
maps_get(Keys, Map) -> ?BUparsextract:maps_get(Keys, Map).
parse_update(Keys, Data, Oper) -> ?BUparsextract:parse_update(Keys, Data, Oper).
parse_required_opts(Keys, Opts) -> ?BUparsextract:parse_required_opts(Keys, Opts).
get_first_defined(List) -> ?BUparsextract:get_first_defined(List).
delist(X) -> ?BUparsextract:delist(X).
delist(X, Acc) -> ?BUparsextract:delist(X, Acc).
flatten_one(X, Y) -> ?BUparsextract:flatten_one(X, Y).
fatten_one(X, Y) -> ?BUparsextract:fatten_one(X, Y).
parse_error_reason(Err) -> ?BUparsextract:parse_error_reason(Err).

%% ------------------------------
%% Parse datetime
%% ------------------------------

parse_datetime(X) -> ?BUparsedt:parse_datetime(X).
parse_datetime_utc(X) -> ?BUparsedt:parse_datetime_utc(X).
parse_time(X) -> ?BUparsedt:parse_time(X).
parse_time_utc(X) -> ?BUparsedt:parse_time_utc(X).
convert_to_utc(DT) -> ?BUparsedt:convert_to_utc(DT).
ticktolocal(TS) -> ?BUparsedt:ticktolocal(TS).
ticktoutc(TS) -> ?BUparsedt:ticktoutc(TS).
to_dt(T) -> ?BUparsedt:to_dt(T).

%% ------------------------------
%% Local time for log
%% ------------------------------
timestamp() -> ?BUtime:timestamp().
timestamp(Ts) -> ?BUtime:timestamp(Ts).
timestamp_mks() -> ?BUtime:timestamp_mks().
timestamp_mks(Ts) -> ?BUtime:timestamp_mks(Ts).
erl_timestamp(Ts) -> ?BUtime:erl_timestamp(Ts).
localdatetime_ms() -> ?BUtime:localdatetime_ms().
localdatetime_ms(Ts) -> ?BUtime:localdatetime_ms(Ts).
timestamp_to_datetime_ms(Ts) -> ?BUtime:timestamp_to_datetime_ms(Ts).
timestamp_to_datetime(Ts) -> ?BUtime:timestamp_to_datetime(Ts).
localtime_ms() -> ?BUtime:localtime_ms().
localtime_ms(Ts) -> ?BUtime:localtime_ms(Ts).
localtime() -> ?BUtime:localtime().
localdate() -> ?BUtime:localdate().
localdate(Ts) -> ?BUtime:localdate(Ts).
log_date_pid(Pid) -> ?BUtime:log_date_pid(Pid).

datetime_to_timestamp(DT) -> ?BUtime:datetime_to_timestamp(DT).

%% ------------------------------
%% datetime to to string binary
%% ------------------------------
strdatetime(Datetime) -> ?BUtime:strdatetime(Datetime).
strdatetime3339() -> ?BUtime:strdatetime3339().
strdatetime3339(Datetime) -> ?BUtime:strdatetime3339(Datetime).
strdatetime3339(Datetime, Tz) -> ?BUtime:strdatetime3339(Datetime, Tz).
strtime(Time) -> ?BUtime:strtime(Time).
strdate(Date) -> ?BUtime:strdate(Date).

%% ------------------------------
%% UTC time
%% ------------------------------
utcdatetime_ms() -> ?BUtime:utcdatetime_ms().
utcdatetime_ms(Ts) -> ?BUtime:utcdatetime_ms(Ts).
utcdiff() -> ?BUtime:utcdiff().
timezone() -> ?BUtime:timezone().
timezonesec() -> ?BUtime:timezonesec().
current_gregsecond() -> ?BUtime:current_gregsecond().
current_gregsecond(UTC) -> ?BUtime:current_gregsecond(UTC).
gregsecond(Time) -> ?BUtime:gregsecond(Time).
utcstrdatetime() -> ?BUtime:utcstrdatetime().
utcbindatetime(Ts) -> ?BUtime:utcbindatetime(Ts).
localbindatetime(Ts) -> ?BUtime:localbindatetime(Ts).
check_utcdatetime(DT) -> ?BUtime:check_utcdatetime(DT).
is_time_valid(T) -> ?BUtime:is_time_valid(T).

%% ------------------------------------
%% Process Functions
%% ------------------------------------
restart_node() -> ?BUprocess:restart_node().
node_start_ts() -> ?BUprocess:node_start_ts().
stop_node(Delay) -> ?BUprocess:stop_node(Delay).
stop_node() -> ?BUprocess:stop_node().
kill_sysproc() -> ?BUprocess:kill_sysproc().

%% -----------------------------
%% Randomizes, Shuffles list by random
%% -----------------------------

random(Max) -> ?BUrandom:random(Max).
random(From,To) -> ?BUrandom:random(From,To).
random_b64(RandomBytesCount) -> ?BUrandom:random_b64(RandomBytesCount).
random_b62(RandomBytesCount) -> ?BUrandom:random_b62(RandomBytesCount).

randomize(X) -> ?BUrandom:randomize(X).
shuffle(X) -> ?BUrandom:shuffle(X).
randomize(Times, List) -> ?BUrandom:randomize(Times,List).
randomize_seed(Seed, List) -> ?BUrandom:randomize_seed(Seed,List).

%% ------------------------------
%% Sets
%% ------------------------------
get_intersect(L1,L2) -> ?BUset:get_intersect(L1,L2).
portions(Cnt,List) -> ?BUset:portions(Cnt,List).
replace_nth(N,List,Elt) -> ?BUset:replace_nth(N,List,Elt).

%% ------------------------------
%% SrvIdx parser
%% ------------------------------
parse_srvidx(Value) -> ?BUsrvidx:parse_srvidx(Value).
parse_textcode_to_index(TextCode) -> ?BUsrvidx:parse_textcode_to_index(TextCode).

%% ------------------------------
%% lists operations
%% ------------------------------
deduplicate(List) -> ?BUlists:deduplicate(List).
keydeduplicate(Pos,List) -> ?BUlists:keydeduplicate(Pos,List).
mix(List1, List2) -> ?BUlists:mix(List1,List2).
intersection(List1, List2) -> ?BUlists:intersection(List1,List2).
intersection_in_order(List1, List2) -> ?BUlists:intersection_in_order(List1,List2).
get_changes(AllCache,ActiveCache,AllNow,ActiveNow) -> ?BUlists:get_changes(AllCache,ActiveCache,AllNow,ActiveNow).
position(Elt,List) -> ?BUlists:position(Elt,List).
juxtapose(Old,New,Fun) -> ?BUlists:juxtapose(Old,New,Fun).
juxtapose_ex(Old,New,Fun) -> ?BUlists:juxtapose_ex(Old,New,Fun).
nest_items(Items,AdapterMap) -> ?BUlists:nest_items(Items,AdapterMap).

%% ------------------------------
%% json <=> map transform
%% ------------------------------

map_to_json(Map) -> ?BUmapjson:map_to_json(Map).
json_to_map(J) -> ?BUmapjson:json_to_map(J).

is_props_list(List) -> ?BUmapjson:is_props_list(List).
filter_props_list(List) -> ?BUmapjson:filter_props_list(List).
make_props_list(List) -> ?BUmapjson:make_props_list(List).

merge_maps(M1, M2, Level) -> ?BUmapjson:merge_maps(M1, M2, Level).
ensure_jsonable(A) -> ?BUmapjson:ensure_jsonable(A).
filter_jsonable(A) -> ?BUmapjson:filter_jsonable(A).
encode_to_json(A) -> ?BUmapjson:encode_to_json(A).

map_diff(M1,M2) -> ?BUmapjson:map_diff(M1,M2).

%% ------------------------------
%% Build binary
%% ------------------------------

join_binary(List) -> ?BUstring:join_binary(List).
join_binary(List,Separator) -> ?BUstring:join_binary(List,Separator).
join_binary_quoted(List,QuoteSymbol,Separator) -> ?BUstring:join_binary_quoted(List,QuoteSymbol,Separator).

%% ------------------------------
%% Build string by format
%% ------------------------------

str(Fmt,Args) -> ?BUstring:str(Fmt,Args).
strbin(Fmt,Args) -> ?BUstring:strbin(Fmt,Args).

%% ------------------------------
%% Reverse of string and binary
%% ------------------------------

reverse(L) -> ?BUstring:reverse(L).

%% ------------------------------
%% Build regex pattern that matches one of allowed strings in the list
%% ------------------------------

strings_to_re_alt(Strings) -> ?BUstring:strings_to_re_alt(Strings).
string_to_re_escape(String) -> ?BUstring:string_to_re_escape(String).

%% ------------------------------
%% Change case of letters
%% ------------------------------

uppercase(Str) -> ?BUstring:uppercase(Str).
lowercase(Str) -> ?BUstring:lowercase(Str).
to_upper(Str) -> ?BUstring:to_upper(Str).
to_lower(Str) -> ?BUstring:to_lower(Str).

%% ------------------------------
%% Quoted strings
%% ------------------------------

quote(Str) -> ?BUstring:quote(Str).
squote(Str) -> ?BUstring:squote(Str).
unquote(Str) -> ?BUstring:unquote(Str).
ensure_quoted(Str) -> ?BUstring:ensure_quoted(Str).
ensure_unquoted(Str) -> ?BUstring:ensure_unquoted(Str).

%% ------------------------------
%% Words from list
%% ------------------------------

first_word(V) -> ?BUstring:first_word(V).
second_word(V) -> ?BUstring:second_word(V).
first_word(V, Div) -> ?BUstring:first_word(V, Div).
second_word(V, Div) -> ?BUstring:second_word(V, Div).

%% ------------------------------
%% Convert character encoding
%% ------------------------------

%% <<"utf-8">>, <<"koi8-r">>, <<"cp1251">>
iconvert(FromEnc, ToEnc, Data) -> ?BUstring:iconvert(FromEnc, ToEnc, Data).

%% ------------------------------
%% BIN to HEX, HEX to BIN
%% ------------------------------

bin_to_hexstr(Bin) -> ?BUstring:bin_to_hexstr(Bin).
bin_to_hexlstr(Bin) -> ?BUstring:bin_to_hexlstr(Bin).
hexstr_to_bin(S) -> ?BUstring:hexstr_to_bin(S).

%% ------------------------------
%% Regexp search in binary
%% ------------------------------

regexp_change_binary(ReQuery,BinData,Fs) -> ?BUstring:regexp_change_binary(ReQuery,BinData,Fs).

%% ------------------------------
%% Execute function on unquoted part of binary
%% ------------------------------
-spec map_unquoted(BinData::binary(), FData::fun()) -> ResBin::binary().
%% ------------------------------
map_unquoted(BinData, FData) -> ?BUstring:map_unquoted(BinData, FData).

%% ------------------------------
%% Translit string
%% ------------------------------
str_translit(Str) -> ?BUstring:str_translit(Str).

%% ------------------------------
%% Split
%% ------------------------------
split_by_comma(Str) -> ?BUstring:split_by_comma(Str).

%% ------------------------------
%% Domain name operations
%% ------------------------------

%% extracts parent domain (removes sub till first dot)
extract_parent_domain(Domain) -> ?BUdomname:extract_parent_domain(Domain).
check_issubdomain(SubDomain, Domains) -> ?BUdomname:check_issubdomain(SubDomain, Domains).
get_subdomain(SubDomain, Domain) -> ?BUdomname:get_subdomain(SubDomain, Domain).

%% ------------------------------
%% MD5
%% ------------------------------

md5(X) -> ?BUcoder:md5(X).

%% ------------------------------------
%% Path operations
%% ------------------------------------

get_filename(Path) -> ?BUpath:get_filename(Path).
get_directory(Path) -> ?BUpath:get_directory(Path).
drop_last_path(Path) -> ?BUpath:drop_last_path(Path).
trim_right_slashes(Path) -> ?BUpath:trim_right_slashes(Path).
trim_left_slashes(Path) -> ?BUpath:trim_left_slashes(Path).
change_extension(Path, Ext) -> ?BUpath:change_extension(Path, Ext).

%% ------------------------------
%% drops last N path parts
%% ------------------------------

drop_last_subdir(Path, N) -> ?BUpath:drop_last_subdir(Path, N).

%% ------------------------------
%% Timer operations
%% ------------------------------

cancel_timer(T) -> ?BUtimer:cancel_timer(T).
growing_pause(Retry,Levels,Starting) -> ?BUtimer:growing_pause(Retry,Levels,Starting).
wait(Fun,PeriodMs,TimeoutMs) -> ?BUtimer:wait(Fun,PeriodMs,TimeoutMs).

%% ------------------------------------
%% File operations
%% ------------------------------------

file_age(FilePath) -> ?BUdisk:file_age(FilePath).
file_copy(Src,Dst) -> ?BUdisk:file_copy(Src,Dst).
file_info_hasha(FilePath) -> ?BUdisk:file_info_hasha(FilePath).
file_info_hasha(FilePath, Opts) -> ?BUdisk:file_info_hasha(FilePath, Opts).
file_md5(FilePath) -> ?BUdisk:file_md5(FilePath).
file_md5_pretty(FilePath) -> ?BUdisk:file_md5_pretty(FilePath).
file_read_endpart(FilePath,From) -> ?BUdisk:file_read_endpart(FilePath,From).

%% ------------------------------------
%% Directory operations
%% ------------------------------------

directory_delete(Dir) -> ?BUdisk:directory_delete(Dir).
directory_delete_empties(Dir) -> ?BUdisk:directory_delete_empties(Dir).

directory_copy(From,To) -> ?BUdisk:directory_copy(From,To).

%% -------------------------------------
%% Disk space where directory is located
%% -------------------------------------

get_disk_space(X) -> ?BUdisk:get_disk_space(X).
get_disk_data() -> ?BUdisk:get_disk_data().

%% ------------------------------------
%% Network Functions
%% ------------------------------------

is_local_ipaddr(IpAddr) -> ?BUnetw:is_local_ipaddr(IpAddr).
node_addr() -> ?BUnetw:node_addr().
node_addr(Node) -> ?BUnetw:node_addr(Node).
node_name() -> ?BUnetw:node_name().
node_name(Node) -> ?BUnetw:node_name(Node).
is_open_node() -> ?BUnetw:is_open_node().
is_addr_or_addrport(Value) -> ?BUnetw:is_addr_or_addrport(Value).
get_host_addr(Host) -> ?BUnetw:get_host_addr(Host).

getinterfaces() ->          ?BUbestiface:getinterfaces().
getroutetable() ->          ?BUbestiface:getroutetable().
getbestinterface(Targ) ->   ?BUbestiface:getbestinterface(Targ).

%% -------------------------------------
%% cmd operations
%% -------------------------------------
os_cmd(Command) -> ?BUcmd:os_cmd(Command).
cmd_exec(Command) -> ?BUcmd:cmd_exec(Command).
cmd_exec(Ref, Command) -> ?BUcmd:cmd_exec(Ref, Command).
cmd_exec_args(Command,Args) -> ?BUcmd:cmd_exec_args(Command,Args).
cmd_exec_args(Command,Args,ExtraOpts) -> ?BUcmd:cmd_exec_args(Command,Args,ExtraOpts).

%% ------------------------------------
%% Compress Functions
%% ------------------------------------

compress_data(X) -> ?BUcompress:compress_data(X).
uncompress_data(X) -> ?BUcompress:uncompress_data(X).
uncompress_data_to_term(X) -> ?BUcompress:uncompress_data_to_term(X).
pack3(Data) -> ?BUcompress:pack3(Data).
unpack3(Data) -> ?BUcompress:unpack3(Data).

%% ------------------------------------
%% DbString parse functions
%% ------------------------------------

parse_dbconnstr(ConnStr) -> ?BUdbstr:parse_dbconnstr(ConnStr).
parse_dbconnstr(ConnStr,DB) -> ?BUdbstr:parse_dbconnstr(ConnStr,DB).
parse_dbconnstrs(ConnStrList) -> ?BUdbstr:parse_dbconnstrs(ConnStrList).
parse_dbconn_any(ConnStrOrDict) -> ?BUdbstr:parse_dbconn_any(ConnStrOrDict).

validate_dbconnstr(ConnStr) -> ?BUdbstr:validate_dbconnstr(ConnStr).
validate_dbconnstrs(ConnStrList) -> ?BUdbstr:validate_dbconnstrs(ConnStrList).
validate_dbconn_proplist(ConnPL) -> ?BUdbstr:validate_dbconn_proplist(ConnPL).

%% ------------------------------
%% Record
%% ------------------------------
get_record_field(Fld, Record, RecordFields) ->
    ?BUrec:get_record_field(Fld, Record, RecordFields).

set_record_field(Fld, Record, RecordFields, Value) ->
    ?BUrec:set_record_field(Fld, Record, RecordFields, Value).

%% ------------------------------
%% SemVer
%% ------------------------------
parse_semver(SemVer) -> ?BUsemver:parse_semver(SemVer).

%% ------------------------------
%% Wav functions
%% ------------------------------
get_wav_header(IsFloatingPoint,ChannelCount,BitDepth,SampleRate,TotalSamples) ->
    ?BUwav:get_wav_header(IsFloatingPoint,ChannelCount,BitDepth,SampleRate,TotalSamples).

%% ------------------------------
%% Env variables of apps
%% ------------------------------
get_env(App,Key) -> ?BUenv:get_env(App,Key).
get_env(App,Key,Default) -> ?BUenv:get_env(App,Key,Default).
get_all_env(App) -> ?BUenv:get_all_env(App).
set_env(App,Key,Value) -> ?BUenv:set_env(App,Key,Value).
unset_env(App,Key) -> ?BUenv:unset_env(App,Key).
update_env(App,Opts) -> ?BUenv:update_env(App,Opts).

%% ====================================================================
%% API functions
%% ===================================================================

%% -------------------------------------
%% Pid operations
%% -------------------------------------

is_registered(Name) -> whereis(Name) /= undefined.

%% ------------------------------------
%% Version operations
%% ------------------------------------

parse_version(V) ->
    lists:map(fun(T) -> case catch ?BU:to_int(T) of
                            I when is_integer(I) -> I;
                            {'EXIT',_} -> T
                        end end, string:tokens(?BU:to_list(V), ".")).

%% ------------------------------------
%% Spawned retry functions
%% ------------------------------------

%% runs spawned fun, to handle exception and exit reason, and return value in time
run_spawned_error(F, _OwnerName) when is_function(F) ->
    Self = self(),
    RefMsg = make_ref(),
    Pid = ?BU:spawn_fun(fun() ->
                                Reply = F(),
                                Self ! {reply, RefMsg, Reply}
                        end),
    RefMon = erlang:monitor(process, Pid),
    receive
        {'reply', RefMsg, Reply} -> Reply;
        {'DOWN', RefMon, process, Pid, _Result} ->
            ?OUT('$error',"Process ~p got exception: ~n\t~120tp", [_OwnerName,_Result]),
            {error, {exception, <<"DC catched error">>}}
    after
        10000 ->
            ?OUT('$error',"Process ~p error, spawned test timeout", [_OwnerName]),
            {error, {exception, <<"DC catched error">>}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

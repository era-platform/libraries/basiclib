%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc System processes. Getting information, managing

-module(basiclib_utils_process).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------
%% Process Functions
%% ------------------------------------
restart_node() ->
    ?BLopts:set_group_leader(),
    ?OUT('$force',"Restart node (cookie)...",[]),
    NewCook = ?BU:to_atom_new(?BU:newid()),
    erlang:set_cookie(node(),NewCook),
    init:stop().

node_start_ts() ->
    NowTS = ?BU:timestamp(),
    case application:get_env(?APP,start_utc) of
        undefined ->
            application:set_env(?APP,start_utc,NowTS),
            {ok,NowTS,NowTS};
        {ok,TS} -> {ok,TS,NowTS}
    end.

% Stops current node
stop_node(Delay) when is_integer(Delay), Delay>0 ->
    timer:sleep(Delay),
    stop_node();
stop_node(_) -> stop_node().
stop_node() ->
    ?OUT('$force',"Stop node...",[]),
    init:stop(),
    timer:sleep(1000),
    kill_sysproc().

% Kills current system process
kill_sysproc() ->
    SysPid = os:getpid(),
    CmdName = case os:type() of
                  {unix, _} -> "kill ";
                  {win32, _} -> "taskkill /pid "
              end,
    ?BU:os_cmd(CmdName ++ ?BU:to_list(SysPid)).

%% ====================================================================
%% Internal functions
%% ====================================================================




%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Parsing and retrieving data from complex structures

-module(basiclib_utils_parsextract).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(FLAT_ONE_LEVEL, {0,1}).
-define(FLAT_INFINITY, {1,0}).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------
%% extracts required props from opts by list. if not found then return error.
%% ---------------------------------------
extract_required_props(Keys, Opts) ->
    extract_required_props(Keys, Opts, []).

extract_required_props([], _Opts, Acc) -> lists:reverse(Acc);
extract_required_props([Key|Rest], Opts, Acc) when is_list(Opts) ->
    case lists:keyfind(Key, 1, Opts) of
        false when is_binary(Key);is_list(Key);is_atom(Key);is_integer(Key) ->
            BKey = ?BU:to_binary(Key),
            {error, {invalid_params, ?BU:strbin("Key '~s' not found", [BKey])}};
        false ->
            B = ?BU:to_binary(length(Acc)+1),
            {error, {invalid_params, <<"Key #", B/bitstring, " not found">>}};
        {_, Value} -> extract_required_props(Rest, Opts, [Value|Acc])
    end;
extract_required_props([Key|Rest], Opts, Acc) when is_map(Opts) ->
    case catch maps:get(Key, Opts) of
        {'EXIT',_} when is_binary(Key);is_list(Key);is_atom(Key);is_integer(Key) ->
            BKey = ?BU:to_binary(Key),
            {error, {invalid_params, ?BU:strbin("Key '~s' not found", [BKey])}};
        {'EXIT',_} ->
            B = ?BU:to_binary(length(Acc)+1),
            {error, {invalid_params, <<"Key #", B/bitstring, " not found">>}};
        Value -> extract_required_props(Rest, Opts, [Value|Acc])
    end.

%% ---------------------------------------
%% extract required props from proplist. if not found then return error
%% ---------------------------------------
lists_get(Keys, List) when is_list(Keys), is_list(List) ->
    extract_required_props(Keys, List).

%% ---------------------------------------
%% extracts optional props from opts by list. if not found then insert 'undefined' atom.
%% ---------------------------------------
extract_optional_props(Keys, Opts) ->
    extract_optional_props(Keys, Opts, []).

extract_optional_props([], _Opts, Acc) -> lists:reverse(Acc);
extract_optional_props([Key|Rest], Opts, Acc) when is_list(Opts) ->
    case lists:keyfind(Key, 1, Opts) of
        false -> extract_optional_props(Rest, Opts, [undefined|Acc]);
        {_, Value} -> extract_optional_props(Rest, Opts, [Value|Acc])
    end;
extract_optional_props([Key|Rest], Opts, Acc) when is_map(Opts) ->
    case catch maps:get(Key, Opts) of
        {'EXIT',_} -> extract_optional_props(Rest, Opts, [undefined|Acc]);
        Value -> extract_optional_props(Rest, Opts, [Value|Acc])
    end.

%% ---------------------------------------
%% extracts optional props from opts by list. if not found then insert default value.
%% ---------------------------------------
extract_optional_default(KeyVals, Opts) ->
    extract_optional_default_acc(KeyVals, Opts, []).

extract_optional_default(Keys, Opts, Defaults) ->
    extract_optional_default_acc(lists:zip(Keys, Defaults), Opts, []).

extract_optional_default_acc([], _Opts, Acc) -> lists:reverse(Acc);
extract_optional_default_acc([{Key,Def}|Rest], Opts, Acc) when is_list(Opts) ->
    case lists:keyfind(Key, 1, Opts) of
        false -> extract_optional_default_acc(Rest, Opts, [Def|Acc]);
        {_, Value} -> extract_optional_default_acc(Rest, Opts, [Value|Acc])
    end;
extract_optional_default_acc([{Key,Def}|Rest], Opts, Acc) when is_map(Opts) ->
    extract_optional_default_acc(Rest, Opts, [maps:get(Key, Opts, Def)|Acc]).

%% ---------------------------------------
-spec(parse(SearchList::list(),ListToSearch::list()) -> Result::any()).
%% ---------------------------------------
parse([], Data) -> Data;
parse(X, {Data}) when is_list(Data) -> parse(X, Data);
parse([Key|Tail], Data1) when is_list(Data1) ->
    Data = lists:flatten(Data1),
    {UT, UR} = case lists:keyfind(Key, 1, Data) of
                   {_, R} -> {Tail,R};
                   _A ->{[],[]}
               end,
    Res1 = case erlang:is_list(UR) of
               true -> lists:flatten(UR);
               false -> UR
           end,
    parse(UT, Res1).

%% ---------------------------------------
-spec parse_map(SearchList::list(),MapToSearch::map()) -> Result::term() | not_found.
%% ---------------------------------------
parse_map(SearchList, Map) ->
    parse_map(SearchList, Map, not_found).

%% ---------------------------------------
-spec parse_map(SearchList::list(), MapToSearch::map(), Default::term()) -> Result::term().
%% ---------------------------------------
parse_map([], Data, _) -> Data;
parse_map([Key|T], Data, Default) when is_map(Data) ->
    case catch maps:get(Key,Data) of
        {'EXIT',_} -> parse_map([],Default,Default);
        Result when T==[] -> parse_map([],Result,Default);
        NData when is_map(NData) -> parse_map(T,NData,Default);
        _ -> parse_map([],Default,Default)
    end;
parse_map(_,_,Default) -> parse_map([],Default,Default).

%% ---------------------------------------
%% delete options from list by keys
%% ---------------------------------------
delete_keys(List, Keys) ->
    lists:foldl(fun(K,Acc1) -> lists:keydelete(K,1,Acc1) end, List, Keys).

%% ---------------------------------------
%% get value from opts list
%% ---------------------------------------
get_by_key(Key, List, Default) when is_list(List) ->
    case lists:keyfind(Key, 1, List) of
        false -> Default;
        {_,V} -> V
    end;
get_by_key(Key, Map, Default) when is_map(Map) ->
    maps:get(Key, Map, Default).
%% ---
%% ---
get_by_key(Key, List) when is_list(List) ->
    case lists:keyfind(Key, 1, List) of
        false ->
            throw({error, {invalid, ?BU:strbin("Key not found in list: ~p",[Key])}});
        {_,V} -> V
    end;
get_by_key(Key, Map) when is_map(Map) ->
    case catch maps:get(Key, Map) of
        {'EXIT',_} ->
            throw({error, {invalid, ?BU:strbin("Key not found in map: ~p",[Key])}});
        V -> V
    end.

%% ---------------------------------------
%% extracts optional props from map. if not found then insert default value.
%% ---------------------------------------
maps_get_default(KeyVals, Map) ->
    extract_map_default_acc(KeyVals, Map, []).

maps_get_default(Keys, Map, Defaults) ->
    extract_map_default_acc(lists:zip(Keys, Defaults), Map, []).

extract_map_default_acc([], _Map, Acc) -> lists:reverse(Acc);
extract_map_default_acc([{Key,Def}|Rest], Map, Acc) ->
    Value = maps:get(Key, Map, Def),
    extract_map_default_acc(Rest, Map, [Value|Acc]).

%% ---------------------------------------
%% extracts required props from map. if not found then return error.
%% ---------------------------------------
maps_get(Keys, Map) ->
    maps_get(Keys, Map, []).

maps_get([], _Map, Acc) -> lists:reverse(Acc);
maps_get([Key|Rest], Map, Acc) ->
    try maps:get(Key, Map) of
        Value ->
            maps_get(Rest, Map, [Value|Acc])
    catch
        _:_ ->
            BKey = ?BU:to_binary(Key),
            {error, {invalid_params, <<"Key '", BKey/bitstring, "' not found">>}}
    end.

%% ---------------------------------------
%% deep modify proplist by path
%% ---------------------------------------
parse_update(Keys, Data, Oper) when is_list(Data) ->
    case catch do_parse_update(Keys, Data, Oper) of
        {error,_}=Err -> Err;
        Res -> Res
    end.

%% @private
do_parse_update([], Data, _Oper) -> Data;
do_parse_update([Key], Data, {modify,NewValue}) ->
    case lists:keyfind(Key, 1, Data) of
        {_, _Res} -> lists:keyreplace(Key,1,Data,{Key,NewValue});
        _A -> throw({error,'not_found'})
    end;
do_parse_update([Key], Data, {addl,AddList}) when is_list(AddList)->
    case lists:keyfind(Key, 1, Data) of
        {_, Res} when is_list(Res) ->
            NewRes = AddList++Res,
            lists:keyreplace(Key,1,Data,{Key,NewRes});
        _A -> throw({error,'not_found'})
    end;
do_parse_update([Key], Data, {addr,AddList}) when is_list(AddList)->
    case lists:keyfind(Key, 1, Data) of
        {_, Res} when is_list(Res) ->
            NewRes = Res++AddList,
            lists:keyreplace(Key,1,Data,{Key,NewRes});
        _A -> throw({error,'not_found'})
    end;
do_parse_update([Key], Data, {replacek,OldK,NewK}) ->
    case lists:keyfind(Key, 1, Data) of
        {_, Res} when is_list(Res) ->
            case lists:keytake(OldK,1,Res) of
                {value,{OldK,V},Data2} -> [{Key,lists:keystore(NewK,1,Data2,{NewK,V})}];
                false -> throw({error,'not_found'})
            end;
        _B -> throw({error,'not_found'})
    end;
do_parse_update([Key], Data, {del_el,ElemForDel}) ->
    case lists:keyfind(Key, 1, Data) of
        {_, Res} when is_list(Res) ->
            case lists:member(ElemForDel,Res) of
                true -> lists:keystore(Key,1,Data,{Key,Res--[ElemForDel]});
                false -> throw({error,'not_found'})
            end;
        _B -> throw({error,'not_found'})
    end;
do_parse_update([Key], Data, {del,KeyForDel}) ->
    case lists:keyfind(Key, 1, Data) of
        {_, Res} when is_list(Res) ->
            case lists:keytake(KeyForDel,1,Res) of
                {value,_,Data2} -> lists:keystore(Key,1,Data,{Key,Data2});
                false -> throw({error,'not_found'})
            end;
        _B -> throw({error,'not_found'})
    end;
do_parse_update([_Key], _Data1, _) ->
    throw({error,'invalid_operation'});
%% Index from 0 to X
do_parse_update([Index|T], Data, Oper) when is_list(Data), is_integer(Index) ->
    case Index < length(Data) of
        true ->
            {Pref,[Res|Suf]} = lists:split(Index, Data),
            UpData = do_parse_update(T,Res,Oper),
            Pref ++ [UpData | Suf];
        false ->
            throw({error,'not_found'})
    end;
do_parse_update([Key|T], Data, Oper) when is_list(Data) ->
    case lists:keyfind(Key, 1, Data) of
        {_, Res} ->
            UpData = {Key,do_parse_update(T,Res,Oper)},
            lists:keyreplace(Key,1,Data,UpData);
        _A -> throw({error,'not_found'})
    end.


%% ---------------------------------------
-spec(parse_required_opts(KeysToExtract::list(),List::list() | map()) -> ExtractedVarList::list()).
%% ---------------------------------------
parse_required_opts(Keys, Opts) when is_map(Opts) ->
    parse_required_opts(Keys, Opts, []);
parse_required_opts(Keys, [Opts]) when is_list(Opts) -> parse_required_opts(Keys, Opts);
parse_required_opts(Keys, Opts) ->
    parse_required_opts(Keys, Opts, []).

parse_required_opts([], _, Vals) -> lists:reverse(Vals);
parse_required_opts([Key|Rest], Opts, Vals) when is_map(Opts) ->
    Val = maps:get(Key, Opts),
    parse_required_opts(Rest, Opts, [Val|Vals]);
parse_required_opts([Key|Rest], Opts, Vals) when is_list(Opts) ->
    {_,Val} = lists:keyfind(Key, 1, Opts),
    parse_required_opts(Rest, Opts, [Val|Vals]).

%% ---------------------------------------
get_first_defined(List) when is_list(List) ->
    case lists:dropwhile(fun(A) -> A==undefined end, List) of
        [Value|_] -> Value;
        [] -> undefined
    end.

%% ---
delist(B) when is_binary(B) -> B;
delist(I) when is_integer(I) -> I;
delist([Tuple]) when is_tuple(Tuple) -> delist(Tuple);
delist(List) when is_list(List) -> delist(List, []);
delist({A1}) -> {delist(A1)};
delist({A1,[A2]}) when is_binary(A1) orelse is_atom(A1) andalso is_tuple(A2) -> {delist(A1),[delist(A2)]}; % new
delist({A1,A2}) -> {delist(A1),delist(A2)};
delist({A1,A2,A3}) -> {delist(A1),delist(A2),delist(A3)};
delist({A1,A2,A3,A4}) -> {delist(A1),delist(A2),delist(A3),delist(A4)};
delist(Tuple) when is_tuple(Tuple) -> [Tuple];
delist(T) -> T.
%
delist([], Acc) -> lists:reverse(Acc);
delist([X|Rest], Acc) -> delist(Rest, [delist(X)|Acc]).

%% ---------------------------------------
%% Reformate from flat fields to fatten on one level.
%% ---------------------------------------
-spec fatten_one(Fields::[atom()], Item::[tuple()]|map()) -> UpItem::[tuple()]|map().
%% ---------------------------------------
fatten_one(_Fields, [{}]=Item) -> Item;
fatten_one(Fields, Item) when is_list(Item) ->
    FieldsBin = lists:map(fun ?BU:to_binary/1, Fields),
    F = fun({K, V}=Element, {A1, B1}) ->
                IFieldB = ?BU:to_binary(K),
                Split = binary:split(IFieldB, <<".">>),
                [First|_] = Split,
                case lists:member(First, FieldsBin) of
                    true ->
                        [NewElement] = fatten_zip([V|lists:reverse(Split)]),
                        {fatten_append(NewElement, A1), B1};
                    false -> {A1, [Element|B1]}
                end
        end,
    {A,B} = lists:foldl(F, {#{},[]}, Item),
    B ++ maps:to_list(A);
fatten_one(Fields, Item) when is_map(Item) ->
    UpItem = fatten_one(Fields, maps:to_list(Item)),
    maps:from_list(UpItem).

%% @private
fatten_zip([Result]) -> Result;
fatten_zip([V,K|Rest]) when is_binary(K) ->
    fatten_zip([[{K, V}]|Rest]).

%% @private
fatten_append({K,V}, Acc) ->
    maps:update_with(K, fun(VI) -> VI ++ V end, V, Acc).

%% ---------------------------------------
%% [{<<"opts">>,[{<<"comment">>, [{<<"one">>, <<"test1">>}] -> [{<<"opts.comment.one">>, <<"test1">>}]
%% ---------------------------------------
-spec flatten(Fields::[atom()], Item::[tuple()]|map()) -> UpItem::[tuple()]|map().
%% ---------------------------------------
flatten(Fields, Item) -> flatten_level(Fields, Item, ?FLAT_INFINITY).

%% ---------------------------------------
%% [{<<"opts">>,[{<<"comment">>, [{<<"one">>, <<"test1">>}] -> [{<<"opts.comment">>, [{<<"one">>, <<"test1">>}]}]
%% ---------------------------------------
-spec flatten_one(Fields::[atom()], Item::[tuple()]|map()) -> UpItem::[tuple()]|map().
%% ---------------------------------------
flatten_one(Fields, Item) -> flatten_level(Fields, Item, ?FLAT_ONE_LEVEL).

%% ---------------------------------------
-spec flatten_level(Fields::[atom()], Item::[tuple()]|map(), Level::{integer(),integer()}) -> UpItem::[tuple()]|map().
%% ---------------------------------------
flatten_level(Fields, Item, Level) when is_list(Item) ->
    F = fun(Field, Acc) -> case keytake(Field, Acc) of false -> Acc; {K, V, Rest} -> Rest ++ flatten_field(V, K, Level) end end,
    lists:foldl(F, Item, Fields);
flatten_level(Fields, Item, Level) when is_map(Item) ->
    UpItem = flatten_level(Fields, maps:to_list(Item),Level),
    maps:from_list(UpItem).

%% @private
keytake(FieldAtom, Item) when is_atom(FieldAtom) ->
    Field = ?BU:to_binary(FieldAtom),
    case lists:keytake(FieldAtom, 1, Item) of
        false -> keytake(Field,Item);
        {value, {_, V}, Rest} -> {Field, V, Rest}
    end;
keytake(FieldBin, Item) when is_binary(FieldBin) ->
    case lists:keytake(FieldBin, 1, Item) of
        false -> false;
        {value, {_, V}, Rest} -> {FieldBin, V, Rest}
    end.

%% @private
flatten_field(OE, UpKey, _) when is_atom(OE) -> [{UpKey, OE}];
flatten_field(OE, UpKey, _) when is_integer(OE) -> [{UpKey, OE}];
flatten_field(OE, UpKey, _) when is_binary(OE) -> [{UpKey, OE}];
flatten_field(OE, UpKey, {C, C}) -> [{UpKey, OE}];
flatten_field(OE, UpKey, {Level, Need}=C) when is_list(OE) ->
    F = fun(Map, Acc) when is_map(Map) -> Acc ++ flatten_field(maps:to_list(Map),UpKey,{Level+1,Need});
           ({K, V}, Acc) -> flatten_field_acc(UpKey, K, V, Acc, C) end,
    lists:foldl(F, [], OE);
flatten_field(OE, UpKey, C) when is_map(OE) ->
    maps:fold(fun(K, V, Acc) -> flatten_field_acc(UpKey, K, V, Acc, C) end, [], OE).

%% @private
flatten_field_acc(Kl, Kr, V, Acc, {Level, Need}) ->
    Key = <<(?BU:to_binary(Kl))/binary, $., (?BU:to_binary(Kr))/binary>>,
    Acc ++ flatten_field(V, Key, {Level+1, Need}).

%% ---------------------------------------
parse_error_reason({error,{error,_}=Err}) -> parse_error_reason(Err);
parse_error_reason({error,{Atom,Reason}}) when is_atom(Atom) andalso (is_binary(Reason) orelse is_atom(Reason) orelse is_list(Reason)) -> ?BU:strbin("~ts. ~ts",[Atom,string(Reason)]);
parse_error_reason({error,Reason}) when is_binary(Reason); is_atom(Reason); is_list(Reason) -> ?BU:to_binary(string(Reason));
parse_error_reason({error,Reason}) -> ?BU:strbin("~120tp", [Reason]).

%% @private
string(Reason) when is_binary(Reason); is_atom(Reason) -> Reason;
string(Reason) when is_list(Reason) ->
    case lists:all(fun(X) -> is_integer(X) end, Reason) of
        true -> ?BU:to_binary(Reason);
        _ -> ?BU:strbin("~120tp", [Reason])
    end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% ==================================

extract_required_props_test_() ->
    {"extract_required_props/2 test",
     [?_assertEqual([a,1,"str",<<"bin">>,40.25], ?MODULE:extract_required_props([ak,bk,"ck",<<"dk">>,8], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}])),
      ?_assertMatch({error,_}, ?MODULE:extract_required_props([zk], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}]))
     ]}.

%% ==================================

lists_get_test_() ->
    {"lists_get/2 test",
     [?_assertEqual([a,1,"str",<<"bin">>,40.25], ?MODULE:lists_get([ak,bk,"ck",<<"dk">>,8], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}])),
      ?_assertMatch({error,_}, ?MODULE:lists_get([zk], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}]))
     ]}.

%% ==================================

extract_optional_props_test_() ->
    {"extract_optional_props/2 test",
     [?_assertEqual([a,1,"str",<<"bin">>,40.25], ?MODULE:extract_optional_props([ak,bk,"ck",<<"dk">>,8], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}])),
      ?_assertEqual([<<"bin">>,undefined,40.25], ?MODULE:extract_optional_props([<<"dk">>,zk,8], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}]))
     ]}.

%% ==================================

extract_optional_default_2_test_() ->
    {"extract_optional_default/2 test",
     [?_assertEqual([a,1,"str",<<"bin">>,40.25], ?MODULE:extract_optional_default([{ak,adef},{bk,bdef},{"ck",cdef},{<<"dk">>,ddef},{8,def8}], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}])),
      ?_assertEqual([<<"bin">>,zdef,40.25], ?MODULE:extract_optional_default([{<<"dk">>,ddef},{zk,zdef},{8,def8}], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}]))
     ]}.

%% ==================================

extract_optional_default_3_test_() ->
    {"extract_optional_default/3 test",
     [?_assertEqual([a,1,"str",<<"bin">>,40.25], ?MODULE:extract_optional_default([ak,bk,"ck",<<"dk">>,8], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}], [adef,bdef,cdef,ddef,def8])),
      ?_assertEqual([<<"bin">>,zdef,40.25], ?MODULE:extract_optional_default([<<"dk">>,zk,8], [{ak,a},{bk,1},{"ck","str"},{<<"dk">>,<<"bin">>},{8,40.25}], [ddef,zdef,def8]))
     ]}.

%% ==================================

parse_test_() ->
    {"parse/2 test",
     [?_assertEqual(data, ?MODULE:parse([], data)),
      ?_assertEqual(data, ?MODULE:parse([key],  [{key,data}] )),
      ?_assertEqual(data, ?MODULE:parse([key], {[{key,data}]})),
      ?_assertEqual([],   ?MODULE:parse([key], [{another,data}])),
      ?_assertEqual(data, ?MODULE:parse([key,deep,path,into,proplists], [{key,[{deep,[{path,[{into,[{proplists,data}]}]}]}]}]))
     ]}.

%% ==================================

parse_map_test_() ->
    {"parse_map/2 test",
     [?_assertEqual(data,      ?MODULE:parse_map([], data)),
      ?_assertEqual(data,      ?MODULE:parse_map([key],  #{key => data} )),
      ?_assertEqual(not_found, ?MODULE:parse_map([key], {#{key => data}})),
      ?_assertEqual(not_found, ?MODULE:parse_map([key], #{another => data})),
      ?_assertEqual(data,      ?MODULE:parse_map([key,deep,path,into,maps], #{key => #{deep => #{path => #{into => #{maps => data}}}}}))
     ]}.

%% ==================================

delete_keys_test_() ->
    {"delete_keys/2 test",
     [?_assertEqual([{another,val}],            ?MODULE:delete_keys([{another,val}], [key])),
      ?_assertEqual([{another,val}],            ?MODULE:delete_keys([{key,data},{another,val}], [key])),
      ?_assertEqual([{another,val},{key,more}], ?MODULE:delete_keys([{key,data},{another,val},{key,more}], [key])),
      ?_assertEqual([{key,more}],               ?MODULE:delete_keys([{key,data},{another,val},{key,more}], [key,another])),
      ?_assertEqual([{a,1}], ?MODULE:delete_keys([{key,data},{another,val},{key,more},{a,1}], [key,another,key])),
      ?_assertEqual([{a,1}], ?MODULE:delete_keys([{key,data},{another,val},{key,more},{a,1}], [key,key,another])),
      ?_assertEqual([],      ?MODULE:delete_keys([{key,data},{another,val},{key,more},{a,1}], [key,a,key,another])),
      ?_assertEqual([],      ?MODULE:delete_keys([{key,data},{another,val},{key,more},{a,1}], [a,key,a,key,another]))
     ]}.

%% ==================================

get_by_key_3_test_() ->
    {"get_by_key/3 test",
     [?_assertEqual(val,     ?MODULE:get_by_key(key,     [{key,val}], default)),
      ?_assertEqual(default, ?MODULE:get_by_key(another, [{key,val}], default))
     ]}.

%% ==================================

get_by_key_2_test_() ->
    {"get_by_key/2 test",
     [?_assertEqual(val,                  ?MODULE:get_by_key(key,     [{key,val}])),
      ?_assertException(throw, {error,_}, ?MODULE:get_by_key(another, [{key,val}]))
     ]}.

%% ==================================

maps_get_default_2_test_() ->
    {"maps_get_default/2 test",
     [?_assertEqual([val],     ?MODULE:maps_get_default([{key,default}],     #{key => val})),
      ?_assertEqual([default], ?MODULE:maps_get_default([{another,default}], #{key => val}))
     ]}.

%% ==================================

maps_get_default_3_test_() ->
    {"maps_get_default/3 test",
     [?_assertEqual([val],       ?MODULE:maps_get_default([key],       #{key => val}, [default])),
      ?_assertEqual([default],   ?MODULE:maps_get_default([another],   #{key => val}, [default])),
      ?_assertEqual([val,b],     ?MODULE:maps_get_default([key,a],     #{key => val, a => b}, [default,c])),
      ?_assertEqual([default,b], ?MODULE:maps_get_default([another,a], #{key => val, a => b}, [default,c]))
     ]}.

%% ==================================

maps_get_test_() ->
    {"maps_get/2 test",
     [?_assertEqual([val],     ?MODULE:maps_get([key],       #{key => val})),
      ?_assertMatch({error,_}, ?MODULE:maps_get([another],   #{key => val})),
      ?_assertEqual([val,b],   ?MODULE:maps_get([key,a],     #{key => val, a => b})),
      ?_assertMatch({error,_}, ?MODULE:maps_get([another,a], #{key => val, a => b}))
     ]}.

%% ==================================

parse_update_test_() ->
    {"parse_update/3 test",
     [?_assertEqual([{a,[[{b,[0,[{c,newval}],2]}],[a1]]}],
                    ?MODULE:parse_update([a,0,b,1,c], [{a,[[{b,[0,[{c,oldval}],2]}],[a1]]}], {modify,newval})),
      %
      ?_assertEqual([{a,[[{b,[0,[{c,[first,2,list]}],2]}],[a1]]}],
                    ?MODULE:parse_update([a,0,b,1,c], [{a,[[{b,[0,[{c,[list]}],2]}],[a1]]}], {addl,[first,2]})),
      %
      ?_assertEqual([{a,[[{b,[0,[{c,[list,7,last]}],2]}],[a1]]}],
                    ?MODULE:parse_update([a,0,b,1,c], [{a,[[{b,[0,[{c,[list]}],2]}],[a1]]}], {addr,[7,last]})),
      %
      ?_assertEqual([{a,[[{b,[0,[{c,[{e,f},{newk,val}]}],2]}],[a1]]}],
                    ?MODULE:parse_update([a,0,b,1,c], [{a,[[{b,[0,[{c,[{oldk,val},{e,f}]}],2]}],[a1]]}], {replacek,oldk,newk})),
      %
      ?_assertEqual([{a,[[{b,[0,[{c,[{oldk,val}]}],2]}],[a1]]}],
                    ?MODULE:parse_update([a,0,b,1,c], [{a,[[{b,[0,[{c,[{oldk,val},{e,f}]}],2]}],[a1]]}], {del_el,{e,f}})),
      %
      ?_assertEqual([{a,[[{b,[0,[{c,[{e,f}]}],2]}],[a1]]}],
                    ?MODULE:parse_update([a,0,b,1,c], [{a,[[{b,[0,[{c,[{oldk,val},{e,f}]}],2]}],[a1]]}], {del,oldk})),
      %
      ?_assertMatch({error,'invalid_operation'},
                    ?MODULE:parse_update([a,0,b,1,c], [{a,[[{b,[0,[{c,[{oldk,val},{e,f}]}],2]}],[a1]]}], {invalid_op,oldk}))
     ]}.

%% ==================================

parse_required_opts_test_() ->
    {"parse_required_opts/2 test",
     [?_assertEqual([val,opt],    ?MODULE:parse_required_opts([key,req], [{key,val},{req,opt}])),
      ?_assertError({badmatch,_}, ?MODULE:parse_required_opts([key,req], [{key,val},{a,b}]))
     ]}.

%% ==================================

get_first_defined_test_() ->
    {"get_first_defined/1 test",
     [?_assertEqual(undefined, ?MODULE:get_first_defined([])),
      ?_assertEqual(undefined, ?MODULE:get_first_defined([undefined,undefined])),
      ?_assertEqual(1, ?MODULE:get_first_defined([1,2,3])),
      ?_assertEqual(1, ?MODULE:get_first_defined([undefined,undefined,1,2,undefined]))
     ]}.

%% ==================================

fatten_test_() ->
    {"fatten_one/2 test",
     [?_assertEqual([{<<"opts">>,[{<<"comment">>, <<"test">>}]}], ?MODULE:fatten_one([opts], [{'opts.comment', <<"test">>}])),
      ?_assertEqual([{<<"opts">>,[{<<"comment">>, <<"test">>}]}], ?MODULE:fatten_one([opts], [{<<"opts.comment">>, <<"test">>}])),
      ?_assertEqual(#{<<"opts">> => [{<<"comment">>, <<"test">>}]}, ?MODULE:fatten_one([opts], #{'opts.comment' => <<"test">>})),
      ?_assertEqual(#{<<"opts">> => [{<<"comment">>, <<"test">>}]}, ?MODULE:fatten_one([opts], #{<<"opts.comment">> => <<"test">>})),
      ?_assertEqual([{<<"name">>, <<"name">>},{<<"id">>, <<"123">>},{<<"opts">>,[{<<"comment">>, <<"test">>}]}],
                    ?MODULE:fatten_one([opts], [{<<"opts.comment">>, <<"test">>},{<<"id">>, <<"123">>},{<<"name">>, <<"name">>}])),
      ?_assertEqual(#{<<"opts">> => [{<<"comment">>, <<"test">>}],<<"id">> => <<"123">>,<<"name">> => <<"name">>},
                    ?MODULE:fatten_one([opts], #{'opts.comment' => <<"test">>,<<"id">> => <<"123">>,<<"name">> => <<"name">>})),
      ?_assertEqual([{<<"name">>, <<"name">>},{<<"id">>, <<"123">>},{<<"ext">>, [{<<"title">>, <<"title">>}]},{<<"opts">>,[{<<"comment">>, <<"test">>}]}],
                    ?MODULE:fatten_one([opts,ext], [{<<"opts.comment">>, <<"test">>},{<<"id">>, <<"123">>},{<<"name">>, <<"name">>},{<<"ext.title">>, <<"title">>}])),
      ?_assertEqual(#{id => <<"123">>,name => <<"name">>,<<"ext">> => [{<<"title">>, <<"title">>}],<<"opts">> => [{<<"comment">>, <<"test">>}]},
                    ?MODULE:fatten_one([opts,ext], #{'opts.comment' => <<"test">>,id => <<"123">>,name => <<"name">>,<<"ext.title">> => <<"title">>})),
      ?_assertEqual(#{id => <<"123">>,<<"ext">> => [{<<"title">>,<<"title1">>}],<<"opts">> => [{<<"comment1">>, <<"test1">>},{<<"comment2">>, <<"test2">>}]},
                    ?MODULE:fatten_one([opts,ext], #{'opts.comment1' => <<"test1">>,'opts.comment2' => <<"test2">>,id => <<"123">>,<<"ext.title">> => <<"title1">>})),
      ?_assertEqual([{<<"id">>, <<"123">>},{<<"opts">>,[{<<"comment">>, <<"test">>},{<<"comment2">>, <<"name2">>}]}],
                    ?MODULE:fatten_one([opts], [{<<"opts.comment">>, <<"test">>},{<<"id">>, <<"123">>},{<<"opts.comment2">>, <<"name2">>}])),
      ?_assertEqual([{<<"id">>, <<"123">>},{<<"opts">>,[{<<"comment">>, <<"test">>},{<<"comment2">>, <<"name2">>}]}],
                    ?MODULE:fatten_one([opts], [{<<"opts.comment">>, <<"test">>},{<<"id">>, <<"123">>},{<<"opts">>,[{<<"comment2">>, <<"name2">>}]}])),
      ?_assertEqual([{<<"ext">>,[{<<"title">>, <<"titleflat">>}]},{<<"id">>, <<"123">>},{<<"opts">>,[{<<"comment">>, <<"test">>}]}],
                    ?MODULE:fatten_one([opts], [{<<"opts.comment">>, <<"test">>},{<<"id">>, <<"123">>},{<<"ext">>,[{<<"title">>, <<"titleflat">>}]}])),
      ?_assertEqual([{<<"id">>, <<"123">>},{<<"opts">>,[{<<"comment.one">>, <<"onet">>},{<<"comment.two">>, <<"twot">>}]}],
                    ?MODULE:fatten_one([opts], [{<<"opts.comment.one">>, <<"onet">>},{<<"id">>, <<"123">>},{<<"opts.comment.two">>, <<"twot">>}]))
     ]}.

%% ==================================

flatten_field_test_() ->
    {"flatten_field/3 test",
     [?_assertEqual([{<<"opts.comment.one">>, <<"test1">>}], ?MODULE:flatten_field([{<<"comment">>, [{<<"one">>, <<"test1">>}]}], <<"opts">>, ?FLAT_INFINITY)),
      ?_assertEqual([{<<"opts.comment.one.two.three">>, <<"test2">>}],
                    ?MODULE:flatten_field([{<<"comment">>, [{<<"one">>, [{<<"two">>, [{<<"three">>, <<"test2">>}]}]}]}], <<"opts">>, ?FLAT_INFINITY)),
      ?_assertEqual([{<<"opts.comment">>, [{<<"one">>, [{<<"two">>, [{<<"three">>, <<"test3">>}]}]}]}],
                    ?MODULE:flatten_field([{<<"comment">>, [{<<"one">>, [{<<"two">>, [{<<"three">>, <<"test3">>}]}]}]}], <<"opts">>, ?FLAT_ONE_LEVEL)),
      ?_assertEqual([{<<"opts.comment.one">>, [{<<"two">>, [{<<"three">>, <<"test4">>}]}]}],
                    ?MODULE:flatten_field([{<<"comment">>, [{<<"one">>, [{<<"two">>, [{<<"three">>, <<"test4">>}]}]}]}], <<"opts">>, {0,2})),
      ?_assertEqual([{<<"opts.comment.one">>, #{<<"two">> => #{<<"three">> => <<"test5">>}}}],
                    ?MODULE:flatten_field(#{<<"comment">> => #{<<"one">> => #{<<"two">> => #{<<"three">> => <<"test5">>}}}}, <<"opts">>, {0,2})),
      ?_assertEqual([{<<"opts.comment.one">>, [{<<"two">>, #{<<"three">> => <<"test6">>}}]}],
                    ?MODULE:flatten_field(#{<<"comment">> => #{<<"one">> => [{<<"two">>, #{<<"three">> => <<"test6">>}}]}}, <<"opts">>, {0,2})),
      ?_assertEqual([{<<"opts.comment.one.two.three">>, <<"test7">>}],
                    ?MODULE:flatten_field(#{<<"comment">> => #{<<"one">> => [{<<"two">>, #{<<"three">> => <<"test7">>}}]}}, <<"opts">>, ?FLAT_INFINITY))
     ]}.

flatten_test_() ->
    {"flatten/2 test",
     [?_assertEqual([{<<"opts.comment">>, <<"test">>}], ?MODULE:flatten([opts], [{opts,[{<<"comment">>, <<"test">>}]}])),
      ?_assertEqual([{<<"opts.comment">>, <<"test">>}], ?MODULE:flatten([opts], [{<<"opts">>,[{<<"comment">>, <<"test">>}]}])),
      ?_assertEqual([{<<"opts.comment">>, <<"test">>}], ?MODULE:flatten([opts], [{opts,#{<<"comment">> => <<"test">>}}])),
      ?_assertEqual([{<<"opts.comment">>, <<"test">>}], ?MODULE:flatten([opts], [{<<"opts">>,#{<<"comment">> => <<"test">>}}])),
      ?_assertEqual(#{<<"opts.comment">> => <<"test">>}, ?MODULE:flatten([opts], #{opts => [{<<"comment">>, <<"test">>}]})),
      ?_assertEqual(#{<<"opts.comment">> => <<"test">>}, ?MODULE:flatten([opts], #{<<"opts">> => [{<<"comment">>, <<"test">>}]})),
      ?_assertEqual([{<<"id">>, <<"123">>},{<<"name">>, <<"name">>},{<<"opts.comment">>, <<"test">>}],
                    ?MODULE:flatten([opts], [{<<"opts">>,[{<<"comment">>, <<"test">>}]},{<<"id">>, <<"123">>},{<<"name">>, <<"name">>}])),
      ?_assertEqual(#{<<"opts.comment">> => <<"test">>,<<"id">> => <<"123">>,<<"name">> => <<"name">>},
                    ?MODULE:flatten([opts], #{opts => #{<<"comment">> => <<"test">>},<<"id">> => <<"123">>,<<"name">> => <<"name">>})),
      ?_assertEqual(#{<<"opts.comment">> => <<"test">>,id => <<"123">>,name => <<"name">>},
                    ?MODULE:flatten([opts], #{opts => #{<<"comment">> => <<"test">>},id => <<"123">>,name => <<"name">>})),
      ?_assertEqual([{<<"id">>, <<"123">>},{<<"name">>, <<"name">>},{<<"opts.comment">>, <<"test">>},{<<"ext.title">>, <<"title">>}],
                    ?MODULE:flatten([opts,ext], [{<<"opts">>,[{<<"comment">>, <<"test">>}]},{<<"id">>, <<"123">>},{<<"name">>, <<"name">>},{<<"ext">>,[{<<"title">>, <<"title">>}]}])),
      ?_assertEqual(#{id => <<"123">>,name => <<"name">>,<<"ext.title">> => <<"title">>,<<"opts.comment">> => <<"test">>},
                    ?MODULE:flatten([opts,ext], #{opts => #{<<"comment">> => <<"test">>},id => <<"123">>,name => <<"name">>,ext => [{<<"title">>, <<"title">>}]})),
      ?_assertEqual(#{id => <<"123">>,<<"ext.title">> => <<"title">>,<<"opts.comment">> => <<"test">>,<<"opts.comment2">> => <<"test2">>},
                    ?MODULE:flatten([opts,ext], #{opts => [{<<"comment">>, <<"test">>},{<<"comment2">>, <<"test2">>}],id => <<"123">>,ext => [{<<"title">>, <<"title">>}]})),
      ?_assertEqual([{<<"id">>, <<"123">>},{<<"opts.comment">>, <<"test">>},{<<"opts.comment2">>, <<"test2">>},{<<"ext.title">>, <<"title">>}],
                    ?MODULE:flatten([opts,ext], [{<<"opts">>,[{<<"comment">>, <<"test">>},{<<"comment2">>, <<"test2">>}]},{<<"id">>, <<"123">>},{<<"ext">>,[{<<"title">>, <<"title">>}]}])),
      ?_assertEqual([{<<"id">>, <<"123">>},{<<"ext.title">>, <<"title">>},{<<"opts.comment.one">>, <<"test1">>},{<<"opts.comment2">>, <<"test2">>}],
                    ?MODULE:flatten([opts,ext], [{<<"opts">>,[{<<"comment">>, [{<<"one">>, <<"test1">>}]},{<<"comment2">>, <<"test2">>}]},{<<"id">>, <<"123">>},{<<"ext.title">>, <<"title">>}]))
     ]}.

flatten_one_test_() ->
    {"flatten_one/2 test",
     [?_assertEqual([{<<"id">>, <<"123">>},{<<"ext.title">>, <<"title">>},{<<"opts.comment">>,[{<<"one">>, <<"test1">>}]},{<<"opts.comment2">>, <<"test2">>}],
                    ?MODULE:flatten_one([opts,ext], [{<<"opts">>,[{<<"comment">>, [{<<"one">>, <<"test1">>}]},{<<"comment2">>, <<"test2">>}]},{<<"id">>, <<"123">>},{<<"ext.title">>, <<"title">>}])),
      ?_assertEqual([{<<"dotted.key.as.is">>, <<"dkai">>},{<<"opts.single">>,<<"single">>},{<<"opts.one.dot">>,<<"onedot">>},{<<"opts.two.dot.s">>, 22}, {<<"opts.deeper.two.dots">>, [{<<"more.dots">>,<<"foo">>}]}],
                    ?MODULE:flatten_one([opts,ext], [{<<"dotted.key.as.is">>, <<"dkai">>},{<<"opts">>,[{<<"single">>, <<"single">>},{<<"one.dot">>, <<"onedot">>},{<<"two.dot.s">>, 22},{<<"deeper.two.dots">>, [{<<"more.dots">>,<<"foo">>}]}]}]))
     ]}.


-endif.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.06.2020.
%%% @doc Record dynamic interaction

-module(basiclib_utils_record).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% RecordFields :: record_info(fields, RecordName)
get_record_field(Fld, Record, RecordFields) ->
    get_value(Fld, Record, RecordFields).

set_record_field(Fld, Record, RecordFields, Value) ->
    set_value(Fld, Record, RecordFields, Value).

%% ====================================================================
%% Internal functions
%% ====================================================================

get_value(Fld, Rec, RecordFields) -> element(field_index(Fld, RecordFields), Rec).
set_value(Fld, Rec, RecordFields, Value) -> setelement(field_index(Fld, RecordFields), Rec, Value).

field_index(Fld, RecordFields) -> index(Fld, RecordFields, 2).

index(M, [M|_], I) -> I;
index(M, [_|T], I) -> index(M, T, I+1).
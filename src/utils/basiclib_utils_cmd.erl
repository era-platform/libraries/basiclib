%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Functions for running external processes in the OS

-module(basiclib_utils_cmd).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([os_cmd/1,
         cmd_exec/1,
         cmd_exec/2,
         cmd_exec_args/2,
         cmd_exec_args/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------
%% Wrapper for os:cmd() that is supposed to fix some problem (which?), unsuccessfully.
%% Is left here for future implementation of the wrapper.
%% -------------------------------------
-spec os_cmd(Command::string()) -> string().
%% -------------------------------------
os_cmd(Command)  ->
    os:cmd(Command).

%% -------------------------------------
%% Execute external program that is found by searching PATH.
%% The first space-separated token of the command is considered as the name of the executable.
%% -------------------------------------
-spec cmd_exec(Command::string() | binary()) -> {ExitCode::integer(), Output::binary()}.
%% -------------------------------------
cmd_exec(Command) ->
    cmd_exec(erlang:make_ref(),Command).

%% -------------------------------------
%% Execute external program that is found by searching PATH.
%% The first space-separated token of the command is considered as the name of the executable.
%% Ref is used directly in receive loop to provide cancellation feature.
%% If {Ref,cancel} message is received, the running os process is killed.
%% -------------------------------------
-spec cmd_exec(Ref::term(), Command::string() | binary()) -> {ExitCode::integer(), Output::binary()}.
%% -------------------------------------
cmd_exec(Ref, Command) ->
    Port = open_port({spawn, Command}, [stream, eof, hide, exit_status, binary]),
    cmd_get_data(Ref, Port, <<>>).

%% -------------------------------------
%% Execute external program directly by complete path to executable. PATH is NOT searched. Use os:find_executable() as needed.
%% Arguments must be passed separately in a list, so each argument (and executable name) can have spaces.
%% -------------------------------------
-spec cmd_exec_args(Command::string() | binary(), Args::[string()|binary()]) -> {ExitCode::integer(), Output::binary()}.
%% -------------------------------------
cmd_exec_args(Command, Args) ->
    cmd_exec_args(Command, Args, []).

%% -------------------------------------
%% Execute external program directly by complete path to executable. PATH is NOT searched. Use os:find_executable() as needed.
%% Arguments must be passed separately in a list, so each argument (and executable name) can have spaces.
%% Accepts ExtraOpts that directly go into erlang:open_port(), like [{cd,Dir}] or other options.
%% NOTE: some options change message format (like [{packet,N}]), which is not supported.
%% -------------------------------------
-spec cmd_exec_args(Command::string() | binary(), Args::[string()|binary()], ExtraOpts::list()) -> {ExitCode::integer(), Output::binary()}.
%% --------
cmd_exec_args(Command, Args, ExtraOpts) ->
    Port = open_port({spawn_executable, Command}, [{args,Args}, stream, eof, hide, exit_status, binary] ++ ExtraOpts),
    cmd_get_data(erlang:make_ref(), Port, <<>>).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------
cmd_get_data(Ref, Port, Sofar) ->
    receive
        {Ref, cancel} ->
            PortInfo = erlang:port_info(Port),
            cmd_port_close(Port),
            cmd_port_kill(PortInfo);
        {Port, {data, Bytes}} ->
            cmd_get_data(Ref, Port, <<Sofar/binary, Bytes/binary>>);
        {Port, eof} ->
            cmd_port_close(Port),
            cmd_port_status(Port,Sofar)
    end.

%% --------
cmd_port_close(Port) ->
    Port ! {self(), close},
    receive
        {Port, closed} -> true
    end,
    receive
        {'EXIT',  Port,  _} -> ok
    after 1 ->              % force context switch
            ok
    end.

%% --------
cmd_port_status(Port, Sofar) ->
    ExitCode =
        receive
            {Port, {exit_status, Code}} -> Code
        end,
    {ExitCode, Sofar}.

%% --------
cmd_port_kill(PortInfo) ->
    case lists:keyfind(os_pid, 1, PortInfo) of
        false -> ok;
        {_,OsPid} ->
            CmdName = case os:type() of
                          {unix, _} -> "kill ";
                          {win32, _} -> "taskkill /pid "
                      end,
            cmd_exec(CmdName ++ ?BU:to_list(OsPid)),
            ok
    end.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Operations on the symbolic names of the subdomains

-module(basiclib_utils_domainname).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Domain name operations
%% ------------------------------

%% extracts parent domain (removes sub till first dot)
extract_parent_domain(Domain) when is_binary(Domain) ->
    case lists:dropwhile(fun($.) -> false; (_) -> true end, ?BU:to_list(Domain)) of
        [] -> <<>>;
        [_|PD] -> ?BU:to_binary(PD)
    end.

%% check subdomain name (if X is any-level subdomain of any of Domains in list)
check_issubdomain(_, []) -> false;
check_issubdomain(Domain, [Domain|_]) -> true;
check_issubdomain(Domain, [A|Rest]) ->
    case lists:suffix([$.|?BU:to_list(A)], ?BU:to_list(Domain)) of
        true -> true;
        false -> check_issubdomain(Domain, Rest)
    end.

%% get subdomain ("a.test.okteller.ru", "okteller.ru" -> "a.test")
get_subdomain(SubDomain, Domain) ->
    case check_issubdomain(SubDomain, [Domain]) of
        false -> error;
        true when SubDomain==Domain -> "";
        true ->
            case lists:nthtail(length(Domain) + 1, lists:reverse(SubDomain)) of
                [] -> error;
                R -> lists:reverse(R)
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================


%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

%% domainnames

extract_parent_domain_test_() ->
    {"extract_parent_domain test",
     [
      ?_assertEqual(<<>>, extract_parent_domain(<<>>)),
      ?_assertEqual(<<"">>, extract_parent_domain(<<".">>)),
      ?_assertEqual(<<"">>, extract_parent_domain(<<"Sub.">>)),
      ?_assertEqual(<<"Parent">>, extract_parent_domain(<<"Sub.Parent">>))
     ]}.

check_issubdomain_test_() ->
    {"check_issubdomain test",
     [
      ?_assertEqual(true, check_issubdomain(<<"a.b.domain.com">>, [<<"domain.com">>, <<"yandex.ru">>])),
      ?_assertEqual(false, check_issubdomain(<<"a.b.domain">>, [<<"domain.com">>, <<"yandex.ru">>])),
      ?_assertEqual(false, check_issubdomain(<<"a.b">>, [<<"domain.com">>, <<"yandex.ru">>])),
      ?_assertEqual(false, check_issubdomain(<<"a.b">>, [])),
      ?_assertEqual(false, check_issubdomain(<<"">>, []))
     ]}.

get_subdomain_test_() ->
    {"get_subdomain test",
     [
      ?_assertEqual("a.test", get_subdomain("a.test.era-platform.ru", "era-platform.ru")),
      ?_assertEqual(error, get_subdomain("a.test.era.ru", "era-platform.ru")),
      ?_assertEqual(error, get_subdomain("", "sub")),
      ?_assertEqual(error, get_subdomain("a.test.era-platform.ru", "")),
      ?_assertEqual(error, get_subdomain("sub.domain", ".sub"))
     ]}.

-endif.

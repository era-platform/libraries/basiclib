%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Working with strings as text

-module(basiclib_utils_string).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Build binary
%% ------------------------------

join_binary(List) ->
    lists:foldl(fun(A,Acc) -> BA = ?BU:to_binary(A), <<Acc/bitstring, BA/bitstring>> end, <<>>, List).

join_binary(List,Separator) ->
    ?BU:to_binary(string:join(lists:map(fun(A) -> ?BU:to_list(A) end, List), ?BU:to_list(Separator))).

join_binary_quoted(List,QuoteSymbol,Separator) when is_integer(QuoteSymbol) ->
    Q = [QuoteSymbol],
    ?BU:to_binary(string:join(lists:map(fun(A) -> Q ++ ?BU:to_list(A) ++ Q end, List), ?BU:to_list(Separator)));
join_binary_quoted(List,QuoteSymbol,Separator) when is_binary(QuoteSymbol) ->
    Q = ?BU:to_list(QuoteSymbol),
    ?BU:to_binary(string:join(lists:map(fun(A) -> Q ++ ?BU:to_list(A) ++ Q end, List), ?BU:to_list(Separator))).

%% ------------------------------
%% Build string by format
%% ------------------------------

str(Fmt,Args) -> lists:flatten(io_lib:format(Fmt,Args)).

strbin(Fmt,Args) -> ?BU:to_binary(str(Fmt,Args)).

%% ------------------------------
%% Reverse of string and binary
%% ------------------------------

reverse(L) when is_list(L) -> lists:reverse(L);
reverse(B) when is_binary(B) -> ?BU:to_binary(reverse(?BU:to_list(B))).

%% ------------------------------
%% Build regex pattern that matches one of allowed strings in the list
%% ------------------------------

strings_to_re_alt(Strings) when is_list(Strings) ->
    lists:flatten(["(", lists:join("|", [string_to_re_escape(String) || String <- Strings]), ")"]).

string_to_re_escape(String) when is_list(String) ->
    ["\\Q", String, "\\E"].

%% ------------------------------
%% Change case of letters
%% ------------------------------

uppercase(Str) when is_binary(Str) -> ?BU:to_binary(string:uppercase(?BU:to_unicode_list(Str)));
uppercase(Str) when is_list(Str) -> string:uppercase(Str).

lowercase(Str) when is_binary(Str) -> ?BU:to_binary(string:lowercase(?BU:to_unicode_list(Str)));
lowercase(Str) when is_list(Str) -> string:lowercase(Str).

to_upper(Str) when is_binary(Str) -> ?BU:to_binary(string:to_upper(?BU:to_list(Str)));
to_upper(Str) when is_list(Str) -> string:to_upper(Str).

to_lower(Str) when is_binary(Str) -> ?BU:to_binary(string:to_lower(?BU:to_list(Str)));
to_lower(Str) when is_list(Str) -> string:to_lower(Str).

%% ------------------------------
%% Quoted strings
%% ------------------------------

quote(Str) when is_binary(Str) -> <<"\"",(Str)/binary,"\"">>;
quote(Str) when is_list(Str) -> lists:append([[$"],Str,[$"]]).

squote(Str) when is_list(Str) -> [$' | lists:foldr(fun(I,Acc) when I==$' -> [$',$'|Acc];
                                                      (I,Acc) -> [I|Acc] end, [$'], Str) ].

unquote(Str) when is_binary(Str) -> ?BU:to_unicode_binary(unquote(?BU:to_unicode_list(Str)));
unquote([$"|Rest1]=Str) -> case lists:reverse(Rest1) of [$"|Rest2] -> lists:reverse(Rest2); _ -> Str end;
unquote(Str) when is_list(Str) -> Str.

ensure_quoted(Str) when is_binary(Str) -> ?BU:to_unicode_binary(ensure_quoted(?BU:to_unicode_list(Str)));
ensure_quoted([$"|Rest]=Str) when is_list(Str) -> case lists:reverse(Rest) of [$"|_] -> Str; _ -> quote(Str) end;
ensure_quoted(Str) when is_list(Str) -> quote(Str).

ensure_unquoted(Str) when is_binary(Str); is_list(Str) -> string:trim(Str, both, [32,"\r\n",$\r,$\n,$\t,$"]).

%% ------------------------------
%% Words from list
%% ------------------------------

first_word(V) when is_list(V) ->
    Fwrd = fun(32) -> false; (_) -> true end,
    lists:takewhile(Fwrd, V).

second_word(V) when is_list(V) ->
    Fwrd = fun(32) -> false; (_) -> true end,
    Fdiv = fun(32) -> true; (_) -> false end,
    lists:takewhile(Fwrd, lists:dropwhile(Fdiv, lists:dropwhile(Fwrd, V))).

first_word(V, Div) when is_list(V) ->
    Fwrd = fun(32) -> false; (X) when X==Div -> false; (_) -> true end,
    lists:takewhile(Fwrd, V).

second_word(V, Div) when is_list(V) ->
    Fwrd = fun(32) -> false; (X) when X==Div -> false; (_) -> true end,
    Fdiv = fun(32) -> true; (X) when X==Div -> true; (_) -> false end,
    lists:takewhile(Fwrd, lists:dropwhile(Fdiv, lists:dropwhile(Fwrd, V))).


%% ------------------------------
%% Split
%% ------------------------------
split_by_comma(Str) when is_binary(Str) ->
    lists:filter(fun(<<>>) -> false;
                    (_) -> true
                 end, re:split(Str,<<"\s*,[\s,]*\s*">>));
split_by_comma(Str) when is_list(Str) ->
    lists:filtermap(fun(<<>>) -> false;
                       (X) -> {true,?BU:to_unicode_list(X)}
                    end, re:split(?BU:to_unicode_binary(Str),<<"\s*,[\s,]*\s*">>)).

%% ------------------------------
%% Convert character encoding
%% ------------------------------

%% <<"utf-8">>, <<"koi8-r">>, <<"cp1251">>
-spec iconvert(FromEnc::iodata(), ToEnc::iodata(), Data::iodata()) -> {ok, binary()} | {error, atom()}.

iconvert(From, To, Data) when is_binary(Data) ->
    iconverl:conv(To, From, Data); % note the reverse To, From args order
iconvert(From, To, Data) ->
    iconvert(From, To, ?BU:to_binary(Data)).

%% ------------------------------
%% BIN to HEX, HEX to BIN
%% ------------------------------

bin_to_hexstr(Bin) ->
    lists:flatten([io_lib:format("~2.16.0B", [X]) || X <- binary_to_list(Bin)]).

bin_to_hexlstr(Bin) ->
    lists:flatten([io_lib:format("~2.16.0b", [X]) || X <- binary_to_list(Bin)]).

%
hexstr_to_bin(S) ->
    hexstr_to_bin(S, []).
hexstr_to_bin([], Acc) ->
    list_to_binary(lists:reverse(Acc));
hexstr_to_bin([X,Y|T], Acc) ->
    {ok, [V], []} = io_lib:fread("~16u", [X,Y]),
    hexstr_to_bin(T, [V | Acc]).

%% ------------------------------
%% Поиск по regexp в binary и замена на указанные значения
%% ------------------------------
%% Функция предназначена для поиска по регулярному выражению с одной группой
%% _FChangeData=fun(Data)-> ModifiedData end. - функция для изменения данных, передается найденный фрагмент binary, должна вернуть измененную binary
%% _FReconstructData=fun(ChangedData,{PartBegin,PLen},{DataBegin,DLen},WorkTemplateData) end - функция для востановления структуры, передаются:
%%      измененные данные (ChangedData); индекс начала найденного фрагмента по регулярному выражению (PartBegin) и его длина (PLen);
%%      индекс найденной группы в регулярном выражении (DataBegin) и его длина (DLen);
%%      оставшаяся для обработки часть входной binary (WorkTemplateData) - после каждого прохода отрезается часть с найденным фрагментом
%%
%% @EXAMPLE
%% Пример: замена содержимого CDATA в xml
%% ReQuery = "<!\\[CDATA\\[(.*)\\]\\]>",
%% FChangeData = fun(Data) -> <<"new_val_",Data/binary>> end,
%% FReconstructData = fun(ChangedData,{PartBegin,PLen},{DataBegin,DLen},WorkTemplateData) ->
%%        [binary:part(WorkTemplateData,{PartBegin,DataBegin-PartBegin}),ChangedData,binary:part(WorkTemplateData,{DataBegin+DLen,(PartBegin+PLen)-(DataBegin+DLen)})]
%%                    end,
%% regexp_change_binary(ReQuery,TemplateData,{FChangeData,FReconstructData}).
%% @EXAMPLE
%% ------------------------------
-spec regexp_change_binary(ReQuery::list(), BinData::binary(), {_FChangeData::fun(),_FReconstructData::fun()}) -> [binary()].
%% ------------------------------
regexp_change_binary(ReQuery,BinData,{_FChangeData,_FReconstructData}=Fs) ->
    case re:run(BinData,ReQuery,[global,dotall]) of
        nomatch -> [BinData];
        {match,Matches} -> prepare_analyze_data(Matches,BinData,Fs)
    end.

%% ---
%% TODO change name
prepare_analyze_data(Matches,TemplateData,WorkFuns) ->
    F = fun([{_,_},{_,0}]) -> false;
           ([{_,_},{_,_}]=Part) -> {true,Part}
        end,
    MatchesForWork = lists:reverse(lists:filtermap(F,Matches)),
    analyze_binary_data(MatchesForWork,TemplateData,WorkFuns,[]).

%% ---
analyze_binary_data([],HeadTemplateData,_WorkFuns,Res) ->
    [HeadTemplateData|Res];
analyze_binary_data([[{PartBegin,PLen},{DataBegin,DLen}=DPart]|T],TemplateData,{FWork,FNewData}=WorkFuns,Res) ->
    BinData = binary:part(TemplateData,DPart),
    ChangedData = FWork(BinData),
    NewData = FNewData(ChangedData,{PartBegin,PLen},{DataBegin,DLen},TemplateData),
    TailSize = erlang:size(TemplateData)-(PartBegin+PLen),
    TailBegin = PartBegin+PLen,
    TailData = binary:part(TemplateData,{TailBegin,TailSize}),
    NTemplateData = binary:part(TemplateData,{0,PartBegin}),
    NRes = [NewData,TailData|Res],
    analyze_binary_data(T,NTemplateData,WorkFuns,NRes).

%% ------------------------------
-spec map_unquoted(BinData::binary(),FChangeData::fun()) -> Result::binary().
%% ------------------------------
map_unquoted(BinData,FChangeData) ->
    ReSplitByQuoteQuery = <<"(?<!\\\\)(?:\\\\\\\\)*(?P<quote>[\"'])(?P<value>.*?)(?<!\\\\)(?:\\\\\\\\)*((?P=quote))">>,
    Parts = re:split(BinData,ReSplitByQuoteQuery),
    F = fun(V,{Accx,working}) when V== <<"\"">>; V== <<"'">> -> {[V|Accx],skipping};
           (V,{Accx,skipping}) when V== <<"\"">>; V== <<"'">> -> {[V|Accx],working};
           (V,{Accx,skipping}) -> {[V|Accx],skipping};
           (V,{Accx,working}) -> {[FChangeData(V)|Accx],working}
        end,
    {ResParts,_} = lists:foldr(F,{[],working},Parts),
    ?BU:join_binary(ResParts).

%% ------------------------------
%% Translit iodata
%% ------------------------------

str_translit(V) ->
    VList = unicode:characters_to_list(V),
    VT = lists:flatten([char_translit(C) || C <- VList]),
    unicode:characters_to_binary(VT).

%% ---
char_translit($А) -> "A";
char_translit($Б) -> "B";
char_translit($В) -> "V";
char_translit($Г) -> "G";
char_translit($Д) -> "D";
char_translit($Е) -> "E";
char_translit($Ё) -> "E";
char_translit($Ж) -> "ZH";
char_translit($З) -> "Z";
char_translit($И) -> "I";
char_translit($Й) -> "Y";
char_translit($К) -> "K";
char_translit($Л) -> "L";
char_translit($М) -> "M";
char_translit($Н) -> "N";
char_translit($О) -> "O";
char_translit($П) -> "P";
char_translit($Р) -> "R";
char_translit($С) -> "S";
char_translit($Т) -> "T";
char_translit($У) -> "U";
char_translit($Ф) -> "F";
char_translit($Х) -> "KH";
char_translit($Ц) -> "TS";
char_translit($Ч) -> "CH";
char_translit($Ш) -> "SH";
char_translit($Щ) -> "SCH";
char_translit($Ъ) -> "''";
char_translit($Ы) -> "Y";
char_translit($Ь) -> "'";
char_translit($Э) -> "E";
char_translit($Ю) -> "YU";
char_translit($Я) -> "YA";
char_translit($а) -> "a";
char_translit($б) -> "b";
char_translit($в) -> "v";
char_translit($г) -> "g";
char_translit($д) -> "d";
char_translit($е) -> "e";
char_translit($ё) -> "e";
char_translit($ж) -> "zh";
char_translit($з) -> "z";
char_translit($и) -> "i";
char_translit($й) -> "y";
char_translit($к) -> "k";
char_translit($л) -> "l";
char_translit($м) -> "m";
char_translit($н) -> "n";
char_translit($о) -> "o";
char_translit($п) -> "p";
char_translit($р) -> "r";
char_translit($с) -> "s";
char_translit($т) -> "t";
char_translit($у) -> "u";
char_translit($ф) -> "f";
char_translit($х) -> "kh";
char_translit($ц) -> "ts";
char_translit($ч) -> "ch";
char_translit($ш) -> "sh";
char_translit($щ) -> "sch";
char_translit($ъ) -> "''";
char_translit($ы) -> "y";
char_translit($ь) -> "'";
char_translit($э) -> "e";
char_translit($ю) -> "yu";
char_translit($я) -> "ya";
char_translit(A) when A < 128-> A;
char_translit(_) -> "?".


%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================================
%% String helpers Test
%% ====================================================================

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

%join_binary/1/2,
join_binary_test_() ->
    {"join_binary test",
     [
      ?_assertEqual(<<"102">>, join_binary([1,2], 0)),
      ?_assertEqual(<<"1|2|3">>, join_binary([1,2,3], "|")),
      ?_assertEqual(<<"">>, join_binary("", "")),
      ?_assertEqual(<<"">>, join_binary("", "2"))
     ]}.
str_test_() ->
    {"str test",
     [
      ?_assertEqual("atom:asdf", str("atom:~p", [asdf])),
      ?_assertException(error, badarg, str("atom:~p", [])),
      ?_assertEqual(<<"10,atom,list,binary">>, strbin("~p,~s,~s,~s",[10,atom,"list",<<"binary">>]))
     ]}.
%reverse(list,binary)
reverse_test_() ->
    {"reverse test",
     [
      ?_assertEqual("FEDCBA", reverse("ABCDEF")),
      ?_assertEqual(<<"54321">>, reverse(<<"12345">>)),
      ?_assertEqual("", reverse("")),
      ?_assertEqual(<<"">>, reverse(<<"">>))
     ]}.
strings_to_re_alt_test_() ->
    {"strings_to_re_alt test",
     [
      ?_assertEqual("(\\Q123\\E)", strings_to_re_alt(["123"])),
      ?_assertEqual("(\\Q1\\E|\\Q2\\E|\\Q3\\E)", strings_to_re_alt(["1", "2", "3"])),
      ?_assertEqual("()", strings_to_re_alt([])),
      ?_assertEqual("(\\Q\\E)", strings_to_re_alt([""])),
      ?_assertEqual("(\\Q\\E|\\Q\\E)", strings_to_re_alt(["",""]))
     ]}.
string_to_re_escape_test_() ->
    {"string_to_re_escape test",
     [
      ?_assertEqual(["\\Q","123", "\\E"], string_to_re_escape("123")),
      ?_assertEqual(["\\Q", "1", "\\E"], string_to_re_escape("1")),
      ?_assertEqual(["\\Q", [], "\\E"], string_to_re_escape(""))
     ]}.
% uppearcase (unicode)
uppercase_test_() ->
    {"uppercase test",
     [
      ?_assertEqual("ABC", uppercase("abc")),
      ?_assertEqual("ABC", uppercase("ABC")),
      ?_assertEqual("АБВ", uppercase("абв")),
      ?_assertEqual("", uppercase(""))
     ]}.
%lowercase (unicode)
lowercase_test_() ->
    {"lowercase test",
     [
      ?_assertEqual("abc", lowercase("abc")),
      ?_assertEqual("abc", lowercase("ABC")),
      ?_assertEqual("абв", lowercase("АБВ")),
      ?_assertEqual("", lowercase(""))
     ]}.
to_upper_test_() ->
    {"to_upper test",
     [
      ?_assertEqual("ABC", to_upper("abc")),
      ?_assertEqual("ABC", to_upper("ABC")),
      ?_assertEqual("абв", to_upper("абв")),
      ?_assertEqual("", to_upper(""))
     ]}.
to_lower_test_() ->
    {"lowercase test",
     [
      ?_assertEqual("abc", to_lower("abc")),
      ?_assertEqual("abc", to_lower("ABC")),
      ?_assertEqual("АБВ", to_lower("АБВ")),
      ?_assertEqual("", to_lower(""))
     ]}.
% quote
quote_test_() ->
    {"quote test",
     [
      ?_assertEqual("\"abc\"", quote("abc")),
      ?_assertEqual(<<"\"abc\"">>, quote(<<"abc">>)),
      ?_assertEqual("\"\"abc\"\"", quote("\"abc\"")),
      ?_assertEqual(<<"\"\"abc\"\"">>, quote(<<"\"abc\"">>)),
      ?_assertEqual(<<"\"фыва\""/utf8>>, quote(<<"фыва"/utf8>>)),
      ?_assertEqual(<<"\"\"">>, quote(<<>>))
     ]}.
% squote
squote_test_() ->
    {"quote test",
     [
      ?_assertEqual("\'abc\'", squote("abc")),
      ?_assertEqual("\'ab\'\'cd\'", squote("ab\'cd")),
      ?_assertEqual("\'ab\"cd\'", squote("ab\"cd")),
      ?_assertEqual("\'\'\'abc\'\'\'", squote("\'abc\'")),
      ?_assertEqual("\'\"abc\"\'", squote("\"abc\"")),
      ?_assertEqual("\'фыва\'", squote("фыва")),
      ?_assertEqual("\'фы\'\'ва\'", squote("фы\'ва")),
      ?_assertEqual("\'фы\"ва\'", squote("фы\"ва")),
      ?_assertEqual("\'\'\'\'", squote("\'")),
      ?_assertEqual("\'\"\'", squote("\"")),
      ?_assertEqual("\'\'", squote(""))
     ]}.
% unquote
unquote_test_() ->
    {"unquote test",
     [
      ?_assertEqual("abc", unquote("\"abc\"")),
      ?_assertEqual("abc", unquote("abc")),
      ?_assertEqual("abc\"", unquote("abc\"")),
      ?_assertEqual(" \"abc\"", unquote(" \"abc\"")),
      ?_assertEqual(<<" \"abc">>, unquote(<<"\" \"abc\"">>)),
      ?_assertEqual(<<"фыва"/utf8>>, unquote(<<"фыва"/utf8>>)),
      ?_assertEqual(<<"\"фыва\""/utf8>>, unquote(<<"\"\"фыва\"\""/utf8>>))
     ]}.
% ensure_quoted
ensure_quoted_test_() ->
    {"ensure_quoted test",
     [
      ?_assertEqual("\"abc\"", ensure_quoted("abc")),
      ?_assertEqual(<<"\"abc\"">>, ensure_quoted(<<"abc">>)),
      ?_assertEqual("\"abc\"", ensure_quoted("\"abc\"")),
      ?_assertEqual(<<"\"abc\"">>, ensure_quoted(<<"\"abc\"">>)),
      ?_assertEqual(<<"\"фыва\""/utf8>>, ensure_quoted(<<"фыва"/utf8>>)),
      ?_assertEqual(<<"\"фыва\"\""/utf8>>, ensure_quoted(<<"\"фыва\"\""/utf8>>)),
      ?_assertEqual(<<"\"\"">>, ensure_quoted(<<>>))
     ]}.
% ensure_unquoted
ensure_unquoted_test_() ->
    {"ensure_unquoted test",
     [
      ?_assertEqual("abc", ensure_unquoted("\"abc\"")),
      ?_assertEqual("abc", ensure_unquoted("abc ")),
      ?_assertEqual("abc", ensure_unquoted("abc\"")),
      ?_assertEqual("abc", ensure_unquoted(" \"abc\"")),
      ?_assertEqual("abc", ensure_unquoted("\" \"abc\"\r\n")),
      ?_assertEqual(<<"фыва"/utf8>>, ensure_unquoted(<<"фыва"/utf8>>)),
      ?_assertEqual(<<"фыва"/utf8>>, ensure_unquoted(<<"\" \" \"фыва\"  "/utf8>>)),
      ?_assertEqual(<<"фыва"/utf8>>, ensure_unquoted(<<"\" \" \"фыва\"  "/utf8>>)),
      ?_assertEqual(<<>>, ensure_unquoted(<<>>)),
      ?_assertEqual("", ensure_unquoted(""))
     ]}.

%first_word/1/2
first_word_test_() ->
    {"first_word test",
     [
      ?_assertEqual("first", first_word("first second")),
      ?_assertEqual("", first_word(" second")),
      ?_assertEqual("", first_word("")),
      ?_assertEqual("first", first_word("first|second", $|)),
      ?_assertEqual("", first_word(" second", $|)),
      ?_assertEqual("", first_word("|second", $|)),
      ?_assertEqual("", first_word("", $|))
     ]}.
%second_word/1/2
second_word_test_() ->
    {"second_word test",
     [
      ?_assertEqual("second", second_word("first second")),
      ?_assertEqual("second", second_word(" second")),
      ?_assertEqual("", second_word("first ")),
      ?_assertEqual("", second_word(" ")),
      ?_assertEqual("", second_word("")),
      ?_assertEqual("second", second_word("first|second", $|)),
      ?_assertEqual("second", second_word(" second", $|)),
      ?_assertEqual("second", second_word("|second", $|)),
      ?_assertEqual("", second_word("first ", $|)),
      ?_assertEqual("", second_word("", $|))
     ]}.
%split_by_comma/1
split_by_comma_test_() ->
    {"split_by_comma test",
     [
      ?_assertEqual([], split_by_comma(<<>>)),
      ?_assertEqual([<<"a">>], split_by_comma(<<"a">>)),
      ?_assertEqual([<<"a">>,<<"a">>,<<"abc">>], split_by_comma(<<"a,a,abc">>)),
      ?_assertEqual([<<"a">>,<<"b">>,<<"abc def">>], split_by_comma(<<",a,b,  ,,abc def,,">>)),
      ?_assertEqual([<<"фыва"/utf8>>,<<"zxcv">>], split_by_comma(<<"фыва, zxcv"/utf8>>)),
      ?_assertEqual([], split_by_comma("")),
      ?_assertEqual(["a"], split_by_comma("a")),
      ?_assertEqual(["a","a","abc"], split_by_comma("a,a,abc")),
      ?_assertEqual(["a","b","abc def"], split_by_comma(",a,b,  ,,abc def,,")),
      ?_assertEqual(["фыва","zxcv"], split_by_comma("фыва, zxcv"))
     ]}.
%iconvert/3 (binary,list)
iconvert_test_() ->
    {"iconvert test",
     [
      ?_assertEqual({ok, <<31,4,64,4,56,4,50,4,53,4,66,4,32,0,28,4,56,4,64,4,33,0>>}, iconvert(<<"utf-8">>, <<"utf-16le">>, <<"Привет Мир!"/utf8>>)),
      ?_assertEqual({ok, <<"Привет Мир!"/utf8>>}, iconvert(<<"utf-16le">>, <<"utf-8">>, <<31,4,64,4,56,4,50,4,53,4,66,4,32,0,28,4,56,4,64,4,33,0>>)),
      ?_assertEqual({ok, <<4,31,4,64,4,56,4,50,4,53,4,66,0,32,4,28,4,56,4,64,0,33>>}, iconvert(<<"utf-8">>, <<"utf-16be">>, <<"Привет Мир!"/utf8>>)),
      ?_assertEqual({ok, <<143,224,168,162,165,226,32,140,168,224,33>>},              iconvert(<<"utf-8">>, <<"cp866">>, <<"Привет Мир!"/utf8>>)),
      ?_assertEqual({ok, <<207,240,232,226,229,242,32,204,232,240,33>>},              iconvert(<<"utf-8">>, <<"cp1251">>, <<"Привет Мир!"/utf8>>))

     ]}.
bin_to_hexstr_test_() ->
    {"bin_to_hexstr test",
     [
      ?_assertEqual("313233", bin_to_hexstr(<<"123">>)),
      ?_assertEqual("", bin_to_hexstr(<<>>)),
      ?_assertEqual("EA3E493DE702", bin_to_hexstr(<<234,62,73,61,231,2>>)),
      ?_assertEqual("D09FD180D0B8D0B2D0B5D18220D0BCD0B8D18021", bin_to_hexstr(<<"Привет мир!"/utf8>>))
     ]}.
bin_to_hexlstr_test_() ->
    {"bin_to_hexlstr test",
     [
      ?_assertEqual("313233", bin_to_hexlstr(<<"123">>)),
      ?_assertEqual("", bin_to_hexlstr(<<>>)),
      ?_assertEqual("ea3e493de702", bin_to_hexlstr(<<234,62,73,61,231,2>>)),
      ?_assertEqual("d09fd180d0b8d0b2d0b5d18220d0bcd0b8d18021", bin_to_hexlstr(<<"Привет мир!"/utf8>>))
     ]}.
hexstr_to_bin_test_() ->
    {"hexstr_to_bin test",
     [
      ?_assertEqual(<<"123">>, hexstr_to_bin("313233")),
      ?_assertEqual(<<>>, hexstr_to_bin("")),
      ?_assertEqual(<<234,62,73,61,231,2>>, hexstr_to_bin("ea3e493de702")),
      ?_assertEqual(<<"Привет мир!"/utf8>>, hexstr_to_bin("d09fd180d0b8d0b2d0b5d18220d0bcd0b8d18021"))
     ]}.

regexp_change_binary_test_() ->

    Fun = fun() ->
                  TemplateData = <<"<xml><![CDATA[.new_val world123..]]><new_val/><world123/></xml>">>,
                  ReQuery = "<!\\[CDATA\\[(.*)\\]\\]>",
                  FChangeData = fun(Data) -> <<"new_val_",Data/binary>> end,
                  FReconstructData = fun(ChangedData,{PartBegin,PLen},{DataBegin,DLen},WorkTemplateData) ->
                                             [binary:part(WorkTemplateData,{PartBegin,DataBegin-PartBegin}),ChangedData,binary:part(WorkTemplateData,{DataBegin+DLen,(PartBegin+PLen)-(DataBegin+DLen)})]
                                     end,
                  regexp_change_binary(ReQuery,TemplateData,{FChangeData,FReconstructData})
          end,

    {"regexp_change_binary test",
     [
      ?_assertEqual([<<"<xml>">>, [<<"<![CDATA[">>,<<"new_val_.new_val world123..">>, <<"]]>">>], <<"<new_val/><world123/></xml>">>], Fun())
     ]}.

str_translit_test_() ->
    {"str_translit test",
     [
      ?_assertEqual(<<"123.,/\\|;:^&?-=+$#@!AaBbEeYAyaLl''''''EeYyYyFfYUyuZHzh CHchTStsKHkhSHshSCHsch?">>, str_translit("123.,/\\|;:^&?-=+$#@!АаБбЁёЯяЛлЬьЪъЭэЙйЫыФфЮюЖж ЧчЦцХхШшЩщ"++[169]))
     ]}.

map_unquoted_test_() ->
    FReplace1 = fun(V) -> re:replace(V,<<"(?<=[^@\\w\\d])@@name\\b">>,<<"test_name">>,[global]) end,
    FReplace2 = fun(V) -> re:replace(V,<<"(?<=[^@\\w\\d])@name\\b">>,<<"name_value">>,[global]) end,
    {"map_unquoted test",
     [
      ?_assertEqual(<<"not_changed">>, map_unquoted(<<"not_changed">>,fun(Val) -> Val end)),
      ?_assertEqual(<<"test \\ name_value \\ xxx">>, map_unquoted(<<"test \\ @name \\ xxx">>,FReplace2)),
      ?_assertEqual(<<"\"@@var\" name=test_name mail=\"x@domain.ru\";">>, map_unquoted(<<"\"@@var\" name=@@name mail=\"x@domain.ru\";">>, FReplace1)),
      ?_assertEqual(<<"foo \"bar\" team \"x\"\ni'll think about it name_value @@name\nname_value+\"@name\"\nname@name\n'foo' \"bar\" \"text with @name interleawed 'quotes\" '\"">>,
          map_unquoted(<<"foo \"bar\" team \"x\"\ni'll think about it @name @@name\n@name+\"@name\"\nname@name\n'foo' \"bar\" \"text with @name interleawed 'quotes\" '\"">>, FReplace2))
     ]}.

-endif.

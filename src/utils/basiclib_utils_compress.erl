%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2017.
%%% @doc Functions for data compression and recovery

-module(basiclib_utils_compress).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------
-spec compress_data(Data::term()|binary()) -> Result::binary().
%% ------------------------------------
compress_data(Data) when is_binary(Data)==false ->
    DataBin = erlang:term_to_binary(Data),
    compress_data(DataBin);
compress_data(Data) when is_binary(Data) ->
    Z = zlib:open(),
    Zip = zlib:compress(Data),
    zlib:close(Z),
    Zip.

%% ------------------------------------
-spec uncompress_data_to_term(Data::binary()) -> Result::term().
%% ------------------------------------
uncompress_data_to_term(Data) when is_binary(Data)->
    Res = uncompress_data(Data),
    erlang:binary_to_term(Res).

%% ------------------------------------
-spec uncompress_data(Data::binary()) -> Result::binary().
%% ------------------------------------
uncompress_data(Data) when is_binary(Data)->
    Z = zlib:open(),
    UnZip = zlib:uncompress(Data),
    zlib:close(Z),
    UnZip.

%% ------------------------------------
%% For proplists or map
%% ------------------------------------
-spec pack3(Data::list()|map()) -> {ok,Result::binary()} | {error,Reason::binary()}.
%% ------------------------------------
pack3(Data) when is_map(Data); is_list(Data) ->
    case catch jsx:encode(Data) of
        {'EXIT',_} -> {error,<<"value must be proplist or map">>};
        V ->
            Compressed = compress_data(V),
            {ok,base64:encode(Compressed)}
    end.

%% ------------------------------------
-spec unpack3(Data::binary()) -> {ok,Result::map()} | {error,Reason::binary()}.
%% ------------------------------------
unpack3(Data) when is_binary(Data) ->
    try
        Decoded = base64:decode(Data),
        Uncompressed = uncompress_data(Decoded),
        {ok,jsx:decode(Uncompressed,[return_maps])}
    catch
        _:_ -> {error,<<"bad data">>}
    end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% ------------------------------------
compress_data_test_() ->
    {"compress_data/1 test",
     [
      ?_assertEqual(<<120,156,107,206,102,96,43,46,41,202,204,75,7,0,18,81,3,140>>, ?MODULE:compress_data("string")),
      ?_assertEqual(<<120,156,75,202,44,41,46,41,202,204,75,7,0,19,43,3,215>>, ?MODULE:compress_data(<<"bitstring">>)),
      ?_assertEqual(<<120,156,107,78,97,96,73,44,201,207,5,0,11,27,2,157>>, ?MODULE:compress_data(atom)),
      ?_assertEqual(<<120,156,21,140,187,13,128,48,12,68,77,248,8,182,96,19,58,246,112,132,
                      80,18,18,59,2,83,80,51,5,108,198,54,196,215,248,249,164,123,183,0,64,
                      181,64,157,48,71,69,103,22,232,243,206,57,250,67,180,105,180,169,61,
                      9,130,82,131,194,169,28,203,28,245,111,215,200,40,243,244,61,154,215,
                      153,84,54,157,245,84,132,234,54,88,161,10,214,147,54,24,136,101,44,
                      68,87,8,63,143,66,33,232>>,
                    ?MODULE:compress_data(#{map => [{proplist,[{int,0},{atom,bool},{float,0.4},{<<"binmap">>,#{1=>2,'fun'=>"not funny"}}]}]})),
      fun() ->
              StructWithFunc1 = #{map => [{proplist,[{int,0},{atom,bool},{float,0.4},{<<"binmap">>,#{1=>2,'fun'=>fun(A)->[A,A] end}}]}]},
              StructWithFunc2 = #{map => [{proplist,[{int,0},{atom,bool},{float,0.4},{<<"binmap">>,#{1=>2,'fun'=>fun(A)->[A,A] end}}]}]},
              Comp1 = ?MODULE:compress_data(StructWithFunc1),
              Comp2 = ?MODULE:compress_data(StructWithFunc2),
              ?assertNotEqual(Comp1, Comp2) % functions produce different results in erlang:term_to_binary()
      end,
      ?_assertEqual(<<120,156,99,0,0,0,1,0,1>>, ?MODULE:compress_data(<<0>>)),
      ?_assertEqual(<<120,156,99,96,0,0,0,2,0,1>>, ?MODULE:compress_data(<<0,0>>)),
      ?_assertEqual(<<120,156,99,96,96,0,0,0,3,0,1>>, ?MODULE:compress_data(<<0,0,0>>)),
      ?_assertEqual(<<120,156,99,4,0,0,2,0,2>>, ?MODULE:compress_data(<<1>>)),
      ?_assertEqual(<<120,156,99,96,100,98,6,0,0,14,0,7>>, ?MODULE:compress_data(<<0,1,2,3>>)),
      ?_assertEqual(<<120,156,99,96,100,98,102,0,0,0,21,0,7>>, ?MODULE:compress_data(<<0,1,2,3,0>>)),
      ?_assertEqual(<<120,156,251,255,255,63,0,5,253,2,254>>, ?MODULE:compress_data(<<255,255,255>>))
     ]
    }.

%% ------------------------------------
uncompress_data_test_() ->
    {"uncopress_data/1 test",
     [
      ?_assertEqual("string", ?MODULE:uncompress_data_to_term(<<120,156,107,206,102,96,43,46,41,202,204,75,7,0,18,81,3,140>>)),
      ?_assertEqual(<<"bitstring">>, ?MODULE:uncompress_data(<<120,156,75,202,44,41,46,41,202,204,75,7,0,19,43,3,215>>)),
      ?_assertEqual(atom, ?MODULE:uncompress_data_to_term(<<120,156,107,78,97,96,73,44,201,207,5,0,11,27,2,157>>)),
      ?_assertEqual(#{map => [{proplist,[{int,0},{atom,bool},{float,0.4},{<<"binmap">>,#{1=>2,'fun'=>"not funny"}}]}]},
                    ?MODULE:uncompress_data_to_term(<<120,156,21,140,187,13,128,48,12,68,77,248,8,182,96,19,58,246,112,132,
                                              80,18,18,59,2,83,80,51,5,108,198,54,196,215,248,249,164,123,183,0,64,
                                              181,64,157,48,71,69,103,22,232,243,206,57,250,67,180,105,180,169,61,
                                              9,130,82,131,194,169,28,203,28,245,111,215,200,40,243,244,61,154,215,
                                              153,84,54,157,245,84,132,234,54,88,161,10,214,147,54,24,136,101,44,
                                              68,87,8,63,143,66,33,232>>)),
      ?_assertEqual(<<0>>, ?MODULE:uncompress_data(<<120,156,99,0,0,0,1,0,1>>)),
      ?_assertEqual(<<0,0>>, ?MODULE:uncompress_data(<<120,156,99,96,0,0,0,2,0,1>>)),
      ?_assertEqual(<<0,0,0>>, ?MODULE:uncompress_data(<<120,156,99,96,96,0,0,0,3,0,1>>)),
      ?_assertEqual(<<1>>, ?MODULE:uncompress_data(<<120,156,99,4,0,0,2,0,2>>)),
      ?_assertEqual(<<0,1,2,3>>, ?MODULE:uncompress_data(<<120,156,99,96,100,98,6,0,0,14,0,7>>)),
      ?_assertEqual(<<0,1,2,3,0>>, ?MODULE:uncompress_data(<<120,156,99,96,100,98,102,0,0,0,21,0,7>>)),
      ?_assertEqual(<<255,255,255>>, ?MODULE:uncompress_data(<<120,156,251,255,255,63,0,5,253,2,254>>))
     ]
    }.

%% ------------------------------------
pack3_test_() ->
    {"pack3/1 test",
     [
      ?_assertEqual({ok,<<"eJyLNjQ01TE0NANiEx1DAxDbAEgbxwIAQkMFFg==">>}, ?MODULE:pack3("string")),
      ?_assertEqual({ok,<<"eJwtjMEKwjAYg98l5yBNNxX7KrJDBSeFrS2zO429u//UwEdCINkQETzxQrj3N567gZhjRdhQl1Kn9G5HTtnMEbGVGaEt65MYpxKP9tQTj5T/K33vxjXboeQoybiw85QzdOWvNryG3fQBv0UkGA==">>},
                    ?MODULE:pack3(#{map => [{proplist,[{int,0},{atom,true},{float,0.4},{<<"binmap">>,#{1=>2,'fun'=>"not funny"}}]}],a=>2,g=>"15"})),
      ?_assertEqual({ok,<<"eJwlyUEKgCAURdG9vPEnfBYFfyvRwKBASI3Qkbj3jAZncm+FjxlqBC6nAM1POQTnldxXh0mw+xjcDa0g1PZXInQljZDsZhmt0HRc5M+d5dbaC9qMGGo=">>},
                    ?MODULE:pack3([{int,0},{atom,true},{float,0.4},{<<"binmap">>,#{1=>2,'fun'=>"not funny"}}]))
     ]
    }.

%% ------------------------------------
unpack3_test_() ->
    {"pack3/1 test",
     [
      ?_assertEqual({ok,"string"}, ?MODULE:unpack3(<<"eJyLNjQ01TE0NANiEx1DAxDbAEgbxwIAQkMFFg==">>)),
      ?_assertEqual({ok,#{<<"a">> => 2,
                          <<"g">> => "15",
                          <<"map">> =>
                              #{<<"proplist">> => #{<<"int">>=>0,<<"atom">>=>true,<<"float">>=>0.4,<<"binmap">>=>#{<<"1">>=>2,<<"fun">>=>"not funny"}}}
                         }},
                    ?MODULE:unpack3(<<"eJwtjMEKwjAYg98l5yBNNxX7KrJDBSeFrS2zO429u//UwEdCINkQETzxQrj3N567gZhjRdhQl1Kn9G5HTtnMEbGVGaEt65MYpxKP9tQTj5T/K33vxjXboeQoybiw85QzdOWvNryG3fQBv0UkGA==">>)),
      ?_assertEqual({ok,#{<<"int">>=>0,<<"atom">>=>true,<<"float">>=>0.4,<<"binmap">>=>#{<<"1">>=>2,<<"fun">>=>"not funny"}}},
                    ?MODULE:unpack3(<<"eJwlyUEKgCAURdG9vPEnfBYFfyvRwKBASI3Qkbj3jAZncm+FjxlqBC6nAM1POQTnldxXh0mw+xjcDa0g1PZXInQljZDsZhmt0HRc5M+d5dbaC9qMGGo=">>))
     ]
    }.

-endif.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 15.04.2017
%%% @doc Environment ping store server (callback)

-module(basiclib_ping_store_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1,
         %
         print/0,
         %
         store_u/2,
         find_u/1,
         delete_u/1,
         get_all_u/0,
         func_u/3,
         func_u/2,
         foldl_u/2,
         %
         store_t/3,
         refresh_t/2,
         find_t/1,
         delete_t/1,
         get_all_t/0,
         func_t/4,
         func_t/3,
         foldl_t/2,
         %
         lazy_t/2, lazy_t/3, lazy_t/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) -> exec(start_link,[Opts]).
% ------

print() -> exec(print,[]).

% ------

store_u(Key, Value) -> exec(store_u, [Key, Value]).

find_u(Key) -> exec(find_u, [Key]).

delete_u(Key) -> exec(delete_u, [Key]).

get_all_u() -> exec(get_all_u, []).

func_u(Key, FunStore, FunReply) -> exec(func_u, [Key, FunStore, FunReply]).

func_u(Key, FunStore) -> exec(func_u, [Key, FunStore]).

foldl_u(Fun, AccIn) -> exec(foldl_u, [Fun, AccIn]).

% ------

store_t(Key, Value, Timeout) -> exec(store_t, [Key, Value, Timeout]).

refresh_t(Key, Timeout) -> exec(refresh_t, [Key, Timeout]).

find_t(Key) -> exec(find_t, [Key]).

delete_t(Key) -> exec(delete_t, [Key]).

get_all_t() -> exec(get_all_t, []).

func_t(Key, Timeout, FunStore, FunReply) -> exec(func_t, [Key, Timeout, FunStore, FunReply]).

func_t(Key, Timeout, FunStore) -> exec(func_t, [Key, Timeout, FunStore]).

foldl_t(Fun, AccIn) -> exec(foldl_t, [Fun, AccIn]).

% ------

lazy_t(Key, FLazy) -> exec(lazy_t, [Key, FLazy]).

lazy_t(Key, FLazy, OptsMap) -> exec(lazy_t, [Key, FLazy, OptsMap]).

lazy_t(Key, FLazy, {TStore,TExpire,TUpdate}, {UseTran,Sync}) -> exec(lazy_t, [Key, FLazy, {TStore,TExpire,TUpdate}, {UseTran,Sync}]).

%% ====================================================================
%% Internal functions
%% ====================================================================

exec(Method, Args) -> erlang:apply(?BLstore_template,Method,[?MODULE|Args]).

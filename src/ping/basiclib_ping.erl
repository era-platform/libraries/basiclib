%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.03.2017
%%% @doc Implements ping node services (#126)
%%%        When net failure between nodes, then epmd process detects timedout connection after 60 sec.
%%%        It's too much for many reasons. Ping node hangs, Rpc:status hangs, Global name return previous pid.
%%%        Ping should be overriden by async and disconnecting.
%%%         It should be optimized to decrease processes to same node at same time.
%%%        Also service could explicitly disconnect timedout node.

-module(basiclib_ping).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ping/1, ping/2, ping/3,
         pong/0,
         ping_ok/1]).

-export([ping_open/2]).

-export([out/3, log/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

ping(Node) when is_atom(Node) ->
    net_adm:ping(Node).

% ping cached and async timeout

ping(Node, PingTimeout)
  when is_atom(Node), is_integer(PingTimeout), PingTimeout > 0 ->
    ping(Node, {PingTimeout,0}, undefined);

ping(Node, {PingTimeout,ReadCacheTimeout})
  when is_atom(Node), is_integer(PingTimeout), is_integer(ReadCacheTimeout), PingTimeout>0, ReadCacheTimeout>0 ->
    ping(Node, {PingTimeout,ReadCacheTimeout}, undefined).

ping(Node, {PingTimeout,0}, Opts)
  when is_atom(Node), is_integer(PingTimeout) ->
    do_ping(Node, PingTimeout, Opts);

ping(Node, {PingTimeout,ReadCacheTimeout}, Opts)
  when is_atom(Node) ->
    NowTS = ?BU:timestamp(),
    case catch ?BLpingstore:find_t(key(Node)) of
        {_,{R,TS}} when TS+ReadCacheTimeout >= NowTS -> R;
        _ -> do_ping(Node, PingTimeout, Opts)
    end.

% pong
pong() -> pong.

% ping ok (to store and not to do new ping requests on wide flow of requests)
ping_ok(Node) ->
    NowTS = ?BU:timestamp(),
    catch ?BLpingstore:store_t(key(Node), {pong,NowTS}, 10000).

% check ping all open nodes,
%  called from other nodes to fix full-mesh globals on wire disconnect
ping_open(T, Opts) ->
    NowTS = ?BU:timestamp(),
    Nodes = nodes(),
    {Self,Ref} = {self(),make_ref},
    % params
    case maps:get(set_group_leader,Opts,false) of
        true -> ?BLopts:set_group_leader();
        _ -> ok
    end,
    case maps:get(out,Opts,false) of
        false -> ok;
        Text when is_list(Text) -> ?OUT('$info',Text,[])
    end,
    TotalTimeout = maps:get(timeout,Opts,1000),
    % spawn pings
    NPids = lists:map(fun(N) -> {N,spawn_monitor(fun() -> Self ! {ok,Ref,N,?BLping:ping(N,T,Opts)} end)} end, Nodes),
    % receive results
    F = fun F(0,Acc) -> Acc;
            F(X,Acc) ->
                 Timeout = TotalTimeout + NowTS - ?BU:timestamp(),
                 receive
                     {ok,Ref,Node,Result} ->
                         F(X-1,lists:keystore(Node,1,Acc,{Node,Result}));
                     {'DOWN',_,process,Pid,_} ->
                         case lists:keyfind(Pid,2,Acc) of
                             false -> F(X,Acc);
                             {Node,_} -> F(X-1,lists:keystore(Node,1,Acc,{Node,down}))
                         end
                 after Timeout -> Acc
                 end end,
    F(length(NPids),NPids).

%% ====================================================================
%% Internal functions
%% ====================================================================

% -----
% @private
do_ping(Node, PingTimeout, Opts) when is_atom(Node) ->
    NowTS = ?BU:timestamp(),
    {_,R} = timer:tc(fun() -> do_ping_rpc(Node, PingTimeout, Opts) end),
    catch ?BLpingstore:store_t(key(Node), {R,NowTS}, 10000),
    R.

% alternative 1
%% do_ping_spawned(Node, PingTimeout, Opts) ->
%%     Self = self(),
%%     Ref = make_ref(),
%%     spawn(fun() -> Self ! {net_adm:ping(Node),Ref} end),
%%     receive
%%         {'pong'=R,Ref} -> on_ping_response(R,Node,Opts), R;
%%         {'pang'=R,Ref} -> on_ping_response(R,Node,Opts), R
%%     after PingTimeout ->
%%         R = 'timeout',
%%         on_ping_response(R,Node,Opts),
%%         R
%%     end.
%% alternative 2
%%do_ping_rpc(Node, PingTimeout, Opts) ->
%%    case ?BLrpc:call(Node, ?BLping, pong, [], PingTimeout) of
%%        {badrpc, timeout} -> on_ping_response(R=timeout,Node,Opts), R;
%%        {_, _} -> on_ping_response(R=pang,Node,Opts), R;
%%        pong=R -> on_ping_response(pong,Node,Opts), R
%%    end;
%% alternative 3
do_ping_rpc(Node, PingTimeout, Opts) ->
    case ?BLrpc:call(Node, erlang, node, [], PingTimeout) of
        Node -> on_ping_response(R=pong,Node,Opts), R;
        {badrpc, timeout} -> on_ping_response(R=timeout,Node,Opts), R;
        {badrpc, nodedown} -> on_ping_response(R=pang,Node,Opts), R;
        {_, _} -> on_ping_response(R=pang,Node,Opts), R
    end.

% on result
on_ping_response(_, _, undefined) -> ok;
on_ping_response(R, Node, Opts) when is_map(Opts) ->
    onresponse(R, maps:get(onresponse,Opts,undefined)),
    disconnect(R, Node, maps:get(disconnect,Opts,false), Opts),
    ok.
%
onresponse(R,F) when is_function(F,1) -> F(R);
onresponse(_,_) -> ok.
%
%disconnect('timeout',_Node,true) -> ok;
disconnect('timeout',Node,true,Opts) ->
    case lists:member(Node,nodes(connected)) of
        true ->
            ondisconnect(maps:get(ondisconnect,Opts,undefined), {true,Node}),
            ?LOG('$info',"Node '~s' ping timeout. Disconnect/connect...",[Node]),
            ?LOG('$info',"Disconnect node '~s': ~120p", [Node,catch net_kernel:disconnect(Node)]),
            spawn(fun() -> ?LOG('$info',"Connect node '~s': ~120p", [Node,net_kernel:connect_node(Node)]) end);
        false ->
            ondisconnect(maps:get(ondisconnect,Opts,undefined), {false,Node}),
            ok
    end;
disconnect(_,_,_,_) -> ok.

%% @private
ondisconnect(F, {true,Node}) when is_function(F,1) -> F(?BU:strbin("Node '~ts' ping timeout, attempting disconnect/connect...", [Node]));
ondisconnect(F, {false,Node}) when is_function(F,1) -> F(?BU:strbin("Node '~ts' ping timeout, not connected", [Node]));
ondisconnect(_,_) -> ok.

% -----
% @private
key(Node) -> {ping_result,Node}.


out(LogLevel, Fmt, Args) -> ?OUT(LogLevel,Fmt,Args).
log(LogLevel, Fmt, Args) -> ?OUT(LogLevel,Fmt,Args).
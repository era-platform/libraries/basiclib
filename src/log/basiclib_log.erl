%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.07.2016
%%% @doc Environment translator to log server

-module(basiclib_log).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([writeout/2, writeout/3,
         write/2, write/3,
         out/1, out/2,
         get_basic_log_dir/0,
         get_log_dir/1]).

-export([
    set_loglevel/1,
    get_loglevel/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

-ifdef(TEST).

writeout(LogFile, Arg) ->
    ?BLlogdummy:writeout(LogFile, Arg).

writeout(Level, LogFile, Arg) ->
    ?BLlogdummy:writeout(Level, LogFile, Arg).

write(LogFile, Arg) ->
    ?BLlogdummy:write(LogFile, Arg).

write(Level, LogFile, Arg) ->
    ?BLlogdummy:write(Level, LogFile, Arg).

out(Arg) ->
    ?BLlogdummy:out(Arg).

out(Level, Arg) ->
    ?BLlogdummy:out(Level, Arg).

get_basic_log_dir() -> ?BLlogdummy:get_basic_log_dir().

get_log_dir(X) -> ?BLlogdummy:get_log_dir(X).

-else.

%% -------------------
writeout(LogFile, Data) ->
    writeout('$force', LogFile, Data).

writeout(Level, LogFile, Data) ->
    ILevel = loglevel(Level),
    case loglevel(?CFG:max_loglevel()) of
        Max when Max < ILevel -> ok;
        _ ->
            Data1 = update_data(ILevel, Data),
            log(LogFile, Data1),
            console(Data1)
    end.

%% -------------------
write(LogFile, Data) ->
    write('$force',LogFile,Data).

write(Level, LogFile, Data) ->
    ILevel = loglevel(Level),
    case loglevel(?CFG:max_loglevel()) of
        Max when Max < ILevel -> ok;
        _ ->
            Data1 = update_data(ILevel, Data),
            log(LogFile, Data1)
    end.

%% -------------------
out(Data) ->
    out('$force', Data).

out(Level, Data) ->
    ILevel = loglevel(Level),
    case loglevel(?CFG:max_loglevel()) of
        Max when Max < ILevel -> ok;
        _ ->
            Data1 = update_data(ILevel, Data),
            console(Data1)
    end.

%% -------------------
get_basic_log_dir() ->
    ?CFG:basic_log_dir().

%% -------------------
get_log_dir(X) ->
    case ?CFG:log_dir_function() of
        F when is_function(F,1) -> F(X);
        _ -> filename:join(get_basic_log_dir(), X)
    end.

-endif.

%% ------------------------------
%% Set max loglevel 0-6, or atoms
%% ------------------------------
set_loglevel(LogLevel) ->
    %application:set_env(?APP,max_loglevel,loglevel(LogLevel)),
    ?CFG:set_env(max_loglevel,loglevel(LogLevel)),
    ?BU:to_atom_new(loglevelname(loglevel(LogLevel))).

%% ------------------------------
%% Get max loglevel 0-6
%% ------------------------------
get_loglevel() ->
    %case application:get_env(?APP,max_loglevel,{ok,4}) of {ok,LL} -> ?BU:to_atom_new(loglevelname(LL)) end,
    ?BU:to_atom_new(loglevelname(?CFG:get_env(max_loglevel,4))).

%% ====================================================================
%% Internal functions
%% ====================================================================

%%
loglevel('$force') -> 0;
loglevel('$crash') -> 1;
loglevel('$error') -> 2;
loglevel('$warning') -> 3;
loglevel('$info') -> 4;
loglevel('$trace') -> 5;
loglevel('$debug') -> 6;
%%
loglevel('FORCE') -> 0;
loglevel('CRASH') -> 1;
loglevel('ERROR') -> 2;
loglevel('WARNING') -> 3;
loglevel('INFO') -> 4;
loglevel('TRACE') -> 5;
loglevel('DEBUG') -> 6;
%%
loglevel(I) when is_integer(I) -> I.

%%
update_data(0, Data) -> Data;
update_data(LogLevel, Txt) when is_list(Txt) -> loglevelname(LogLevel) ++ "\t" ++ Txt;
update_data(LogLevel, {Fmt,Args}) when is_list(Fmt) -> {update_data(LogLevel, Fmt), Args}.

%%
loglevelname(0) -> "";
loglevelname(1) -> "CRASH";
loglevelname(2) -> "ERROR";
loglevelname(3) -> "WARNING";
loglevelname(4) -> "INFO";
loglevelname(5) -> "TRACE";
loglevelname(6) -> "DEBUG";
loglevelname(N) when N < 0 -> "LOWEST";
loglevelname(N) when N > 6 -> "HIGHEST".

%%
log(LogFile, Data) ->
    FLog = ?CFG:log_function(),
    FLog(LogFile, Data).

%%
console({Fmt, Args}) -> io:format("~ts"++Fmt++"~n",[?BU:log_date_pid(self())]++Args);
console(Text) -> io:format("~ts"++Text++"~n",[?BU:log_date_pid(self())]).


%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.11.2017
%%% @doc Log dummy for testing

-module(basiclib_log_dummy).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([writeout/2, writeout/3,
         write/2, write/3,
         out/1, out/2,
         get_basic_log_dir/0,
         get_log_dir/1]).

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

writeout(LogFile, {Fmt,Args}=Data) ->
    write(LogFile, Data),
    ?OUTC(Fmt,Args);
writeout(LogFile, Text) ->
    write(LogFile, Text),
    ?OUTC(Text,[]).

writeout(_Level, LogFile, Arg) ->
    write(LogFile, Arg).


write(_LogFile, {Fmt,Args}) ->
    ?BU:str(Fmt,Args);
write(_LogFile, Text) ->
    ?BU:str(Text,[]).

write(_Level, LogFile, Arg) ->
    write(LogFile, Arg).


out({Fmt,Args}) ->
    ?BU:str(Fmt,Args);
out(Text) ->
    ?BU:str(Text,[]).

out(_Level, Arg) ->
    out(Arg).


get_basic_log_dir() -> "log_dummy".

get_log_dir(X) -> filename:join(get_basic_log_dir(), ?BU:to_list(X)).

%% ====================================================================
%% Internal functions
%% ====================================================================




%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.01.2021
%%% @doc Configuration functions

-module(basiclib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([log_destination/1,
         boot_app_function/0,
         node_index/0,
         max_loglevel/0,
         log_function/0,
         basic_log_dir/0,
         log_dir_function/0]).

-export([get_env/1, get_env/2,
         get_all_env/0,
         set_env/2,
         unset_env/1,
         update_env/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% log_destination
log_destination(Default) ->
    get_env('log_destination', Default).

%% boot_app_function
boot_app_function() ->
    get_env('boot_app_function', undefined).
%%    fun(App,Opts) ->
%%        case ?APP_BOOT:is_boot_srv_running() of
%%            true -> ?APP_BOOT:update_cfg();
%%            false -> ?APP_BOOT:start()
%%        end end.

%% node_index
node_index() ->
    get_env('node_index', {fn,fun() -> erlang:phash2(node()) rem 1000000 end}).

%% max_loglevel
max_loglevel() ->
    get_env('max_loglevel', '$info').

%% log_function
log_function() ->
    get_env('log_function', fun(_LogFile, _Arg) -> ok end).

%% basic_log_dir
basic_log_dir() ->
    get_env('basic_log_dir', "log").

%% log_dir_function
log_dir_function() ->
    get_env('log_dir_function', fun(X) -> filename:join(basic_log_dir(), X) end).

%% ====================================================================
%% Internal functions
%% ====================================================================

get_env(Key) -> ?BU:get_env(?APP,Key).
get_env(Key,{fn,_}=Default) -> ?BU:get_env(?APP,Key,Default);
get_env(Key,Default) -> ?BU:get_env(?APP,Key,Default).
get_all_env() -> ?BU:get_all_env(?APP).
set_env(Key,Value) -> ?BU:set_env(?APP,Key,Value).
unset_env(Key) -> ?BU:unset_env(?APP,Key).
update_env(Opts) -> ?BU:update_env(?APP,Opts).

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 13.09.2017.
%%% @doc #412 Maps to erlang's ?M: with transforming params to unicode binary

-module(basiclib_wrapper_filelib).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(M, filelib).

-import(?BU, [ub/1]).

%% ====================================================================
%% API functions
%% ====================================================================

ensure_dir(Name) -> ?M:ensure_dir(ub(Name)).

file_size(Name) -> ?M:file_size(ub(Name)).

fold_files(Dir,RegExp,Recursive,Fun,AccIn) when is_binary(Dir) -> ?M:fold_files(Dir,RegExp,Recursive,Fun,AccIn);
fold_files(Dir,RegExp,Recursive,Fun,AccIn) when is_list(Dir) -> ?M:fold_files(ub(Dir),RegExp,Recursive,fun(FN,Acc) -> Fun(?BU:to_unicode_list(FN),Acc) end,AccIn).

is_dir(Name) -> ?M:is_dir(ub(Name)).

is_file(Name) -> ?M:is_file(ub(Name)).

is_regular(Name) -> ?M:is_regular(ub(Name)).

last_modified(Name) -> ?M:last_modified(ub(Name)).

wildcard(Wildcard) when is_binary(Wildcard) -> ?M:wildcard(?BU:to_unicode_list(Wildcard));
wildcard(Wildcard) when is_list(Wildcard) -> ?M:wildcard(Wildcard).
wildcard(Wildcard,Cwd) when is_binary(Wildcard) -> ?M:wildcard(?BU:to_unicode_list(Wildcard),Cwd);
wildcard(Wildcard,Cwd) -> ?M:wildcard(Wildcard,Cwd).

find_file(FileName, Dir) when is_binary(FileName) -> ?M:find_file(?BU:to_unicode_list(FileName), Dir);
find_file(FileName, Dir) when is_list(FileName) -> ?M:find_file(FileName, Dir).
find_file(FileName, Dir, Rules) when is_binary(FileName) -> ?M:find_file(?BU:to_unicode_list(FileName), Dir, Rules);
find_file(FileName, Dir, Rules) -> ?M:find_file(FileName, Dir, Rules).


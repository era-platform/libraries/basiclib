%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 13.09.2017.
%%% @doc #412 Maps to erlang's ?M: with transforming params to unicode binary

-module(basiclib_wrapper_file).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(M, file).

-import(?BU, [ub/1]).

%% ====================================================================
%% API functions
%% ====================================================================

change_mode(FN,Mode) -> ?M:change_mode(ub(FN),Mode).

change_owner(FN,Uid) -> ?M:change_owner(ub(FN),Uid).

change_time(FN,MTime) -> ?M:change_time(ub(FN),MTime).

close(IoDevice) -> ?M:close(IoDevice).

consult(FN) -> ?M:consult(ub(FN)).

copy(Source,{Destination,Modes}) -> ?M:copy(ub(Source),{ub(Destination),Modes});
copy(Source,Destination) -> ?M:copy(ub(Source),ub(Destination)).
copy(Source,Destination,ByteCount) -> ?M:copy(ub(Source),ub(Destination),ByteCount).

datasync(IoDevice) -> ?M:datasync(IoDevice).

del_dir(Dir) -> ?M:del_dir(ub(Dir)).

delete(FN) -> ?M:delete(ub(FN)).

eval(Filename) -> ?M:eval(ub(Filename)).
eval(Filename,Bindings) -> ?M:eval(ub(Filename),Bindings).

format_error(Reason) -> ?M:format_error(Reason).

get_cwd() -> ?M:get_cwd().
get_cwd(Drive) -> ?M:get_cwd(Drive).

list_dir(Dir) -> ?M:list_dir(ub(Dir)).
list_dir_all(Dir) -> ?M:list_dir_all(ub(Dir)).

make_dir(Dir) -> ?M:make_dir(ub(Dir)).

native_name_encoding() -> ?M:native_name_encoding().

open(File,Modes) when is_list(File) -> ?M:open(ub(File),Modes);
open(File,Modes) -> ?M:open(File,Modes).

position(IoDevice,Location) -> ?M:position(IoDevice,Location).

pread(IoDevice, LocNums) -> ?M:pread(IoDevice, LocNums).
pread(IoDevice, Location, Number) -> ?M:pread(IoDevice, Location, Number).

pwrite(IoDevice, LocBytes) -> ?M:pwrite(IoDevice, LocBytes).
pwrite(IoDevice, Location, Bytes) -> ?M:pwrite(IoDevice, Location, Bytes).

read(IoDevice,Number) -> ?M:read(IoDevice,Number).

read_file(FN) -> ?M:read_file(ub(FN)).

read_file_info(FN) -> ?M:read_file_info(ub(FN)).
read_file_info(FN,Opts) -> ?M:read_file_info(ub(FN),Opts).

read_line(IoDevice) -> ?M:read_line(IoDevice).

read_link(Name) -> ?M:read_link(ub(Name)).
read_link_all(Name) -> ?M:read_link_all(ub(Name)).
read_link_info(Name) -> ?M:read_link_info(ub(Name)).
read_link_info(Name,Opts) -> ?M:read_link_info(ub(Name),Opts).

rename(Source,Destination) -> ?M:rename(ub(Source),ub(Destination)).

set_cwd(Dir) when is_binary(Dir) -> ?M:set_cwd(?BU:to_unicode_list(Dir));
set_cwd(Dir) -> ?M:set_cwd(Dir).

truncate(IoDevice) -> ?M:truncate(IoDevice).

write(IoDevice,Bytes) -> ?M:write(IoDevice,Bytes).

write_file(FN,Bytes) -> ?M:write_file(ub(FN),Bytes).
write_file(FN,Bytes,Modes) -> ?M:write_file(ub(FN),Bytes,Modes).

write_file_info(FN,FileInfo) -> ?M:write_file_info(ub(FN),FileInfo).
write_file_info(FN,FileInfo,Opts) -> ?M:write_file_info(ub(FN),FileInfo,Opts).


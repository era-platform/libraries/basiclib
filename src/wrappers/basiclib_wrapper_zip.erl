%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 08.02.2019
%%% @doc An implementation of interface similar to erlang 'zip' module.
%%%   It uses a combination of external zip/unzip binaries and erlang's own zip:foldl() because:
%%%   - erlang's zip:create() and zip:unzip() do not handle unicode (well) and cannot be fixed without patching erlang source, so have to use external binaries.
%%%   - erlang's zip:foldl() also has bugs in unicode handling, but they can and are fixed in a wrapper.
%%%   A new function is introduced, create_from_dir(), which provides a much simpler and useful interface.

-module(basiclib_wrapper_zip).
-author(['Anton Makarov <anton@mastermak.ru>','Peter Bukashin <tbotc@yandex.ru>']).

-export([create/3,
         create_from_dir/2,
         unzip/2, extract/2,
         foldl/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%% Create archive. Use external zip command to create v1.0 archive.
%% Do not use built-in erlang zip:create() because it creates archives v2.0.
%% Also, zip:create() is incapable of working with any non-ascii filenames.
%% --------------------------------------
-spec create(Name::file:name(), FileList::[file:name()|{file:name(),binary()}|{file:name(),binary(),file:file_info()}]|dot, Options::[zip:create_option()]) ->
    {ok, FileName::file:filename()} | {ok, {FileName::file:filename(), binary()}} | {error, Reason::term()}.
%% --------------------------------------
create(Name, FileList, Options) ->
    case os:type() of
        {win32,_} -> zip:create(Name, FileList, Options);
        _ -> create_ext(Name, FileList, Options)
    end.

create_ext(Name, FileList, Options) ->
    case parse_zip_options(Options) of
        {ok, {cwd,CWD}} ->
            case os:find_executable("zip") of
                false -> {error, {not_found,<<"External 'zip' executable is not found in PATH!">>}};
                ZipCmd ->
                    ZipName = ?BU:to_unicode_list(Name),
                    AbsZipName = filename:absname(ZipName),
                    ZipFileList = make_file_list(FileList),
                    case ?BU:cmd_exec_args(ZipCmd, ["-r", AbsZipName | ZipFileList], [{cd,?BU:to_unicode_list(CWD)}]) of
                        {0,_} -> {ok, ZipName};
                        {_ExitCode,_Out}=Err -> {error,Err}
                    end
            end;
        {error,_}=OptErr -> OptErr
    end.

%% @private
parse_zip_options([]) -> {error, {bad_option,<<"empty options are not supported">>}};
parse_zip_options([{cwd,CWD}]) -> {ok, {cwd,CWD}};
parse_zip_options([Opt|_]=Opts) when is_list(Opts) ->
    case lists:keytake(compress, 1, Opts) of
        false -> {error, {bad_option,Opt}}; % any other option is not supported
        {value,{compress,all},Opts1} -> parse_zip_options(Opts1); % ignore {compress,all} - that's the default (in this interface).
        {value,{compress,_}=OptC,_} -> {error, {bad_option,OptC}} % not supported in this implementation.
    end.

%% @private
make_file_list(dot) -> ["."];
make_file_list(FileList) when is_list(FileList) ->
    [?BU:to_unicode_list(FN) || FN <- FileList].

%% --------------------------------------
%% Create archive. Use external zip command to create v1.0 archive.
%% Do not use built-in erlang zip:create() because it creates archives v2.0.
%% Also, zip:create() is incapable of working with any non-ascii filenames.
%% This variant packs the whole directory without having to explicitly provide its contents list.
%% --------------------------------------
-spec create_from_dir(Name::file:name(), Dir::file:name()) ->
    {ok, FileName::file:filename()} | {error, Reason::term()}.
%% --------------------------------------
create_from_dir(Name, Dir) ->
    create(Name, dot, [{cwd,Dir}]).

%% --------------------------------------
%% Extract archive contents. Use external unzip command to extract archives to disk folder.
%% Do not use built-in erlang zip:unzip() beause it works inadequately with non-ascii filenames, creating files with broken names.
%% --------------------------------------
-spec unzip(Archive::file:name()|binary(), Options:: [memory] | [{cwd, file:filename()}]) -> % | [{cwd, file:filename()},junk_paths]
    {ok, FileList::[file:name()]} | {ok, FileBinList::[{file:name(),binary()}]} |
    {error, Reason::term()} | {error, {ExitCode::integer(), Output::binary()}}.
%% --------------------------------------
unzip(Archive, Options) when is_list(Options) ->
    case os:type() of
        {win32,_} -> zip:unzip(Archive, Options);
        _ -> unzip_ext(Archive, Options)
    end.

unzip_ext(Archive, Options) when is_list(Options) ->
    case parse_unzip_options(Options) of
        {ok, memory} ->
            case ?MODULE:foldl(fun(FN,_,GetBinF,Acc) -> [{FN,GetBinF()}|Acc] end, [], Archive) of
                {ok, Acc1} -> {ok, lists:reverse(Acc1)};
                {error,_}=Err -> Err
            end;
        {ok, {cwd,CWD,Opts}} ->
            UCwd = ?BU:to_unicode_list(CWD),
            AbsArchiveStr = filename:absname(?BU:to_unicode_list(Archive)),
            JP = case lists:member(junk_paths,Opts) of true -> "-j"; false -> "" end, % -j: "junk paths" - ignore archive hierarchy, extract all into folder.
            Cmd = "unzip -o "++JP++" -d "++UCwd++" "++AbsArchiveStr++" 2>&1 </dev/null", % -o: overwrite without prompt
            case ?BU:cmd_exec(Cmd) of
                {0,Out} ->
                    LastChar = lists:last(UCwd),
                    UCwdB = case LastChar of $/ -> ?BU:to_binary(UCwd); _ -> <<(?BU:to_binary(UCwd))/binary,"/">> end,
                    Files = lists:filtermap(fun(Line) ->
                                                    case binary:match(Line, UCwdB) of
                                                        nomatch -> false;
                                                        {S,L} ->
                                                            LineTail = binary:part(Line, S+L, byte_size(Line)-S-L),
                                                            LineTailStr = string:trim(?BU:to_unicode_list(LineTail), trailing),
                                                            case lists:last(LineTailStr) of
                                                                $/ -> false; % ignore directories ending with slash
                                                                _ ->
                                                                    case filename:join(UCwd, LineTailStr) of
                                                                        AbsArchiveStr -> false; % don't include archive itself
                                                                        AbsFileStr -> {true, AbsFileStr}
                                                                    end
                                                            end
                                                    end
                                            end, binary:split(Out, <<"\n">>, [trim_all, global])),
                    {ok, Files};
                {_ExitCode,_Out}=Err ->
                    {error,Err}
            end;
        {error,_}=OptErr -> OptErr
    end.

%% --------------------------------------
%% @private
parse_unzip_options([]) -> {error, {bad_option,<<"empty options are not supported">>}};
parse_unzip_options([memory]) -> {ok, memory};
parse_unzip_options([{cwd,CWD}]) -> {ok, {cwd,CWD,[]}};
parse_unzip_options([{cwd,CWD},junk_paths]) -> {ok, {cwd,CWD,[junk_paths]}};
parse_unzip_options([Opt|_]) -> {error, {bad_option,Opt}}.

%% --------------------------------------
%% Extract archive contents. This is an alias of unzip/2.
%% --------------------------------------
extract(Archive, Options) -> unzip(Archive, Options).

%% --------------------------------------
%% Extract archive contents to memory in a foldl-style. Use built-in erlang zip:foldl() and fix filenames.
%% It is possible because no files with broken names are created, and filenames can easily be fixed in memory.
%% --------------------------------------
-spec foldl(Fun::fun((FIA::file:name(),GI::fun(),GB::fun(),AccIn::term())->AccOut::term()),Acc0::term(),Archive::file:name()|{file:name(),binary()}) ->
    {ok, Acc1::term()} | {error, Reason::term()}.
%% --------------------------------------
foldl(Fun, Acc0, Archive) ->
    FixFun = fun(FileName, GetInfoF, GetBinF, Acc) ->
                 FixFileName = ?BU:to_unicode_list(?BU:to_binary(FileName)),
                 Fun(FixFileName, GetInfoF, GetBinF, Acc)
             end,
    zip:foldl(FixFun, Acc0, Archive).

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% --------------------------------------
%% parse_zip_options test.
%% --------------------------------------
parse_zip_options_test_() ->
    {"parse_zip_options/1 test",
     [
      ?_assertMatch({error,{bad_option,_}}, parse_zip_options([])),
      ?_assertEqual({error,{bad_option,foobar}}, parse_zip_options([foobar])),
      ?_assertEqual({error,{bad_option,{foobar,5}}}, parse_zip_options([{foobar,5}])),
      ?_assertEqual({ok,{cwd,"/path"}}, parse_zip_options([{cwd,"/path"}])),
      ?_assertEqual({ok,{cwd,"/path"}}, parse_zip_options([{compress,all},{cwd,"/path"}])),
      ?_assertEqual({ok,{cwd,"/path"}}, parse_zip_options([{cwd,"/path"},{compress,all}])),
      ?_assertEqual({ok,{cwd,"/path"}}, parse_zip_options([{compress,all},{cwd,"/path"},{compress,all}])),
      ?_assertEqual({error,{bad_option,{compress,none}}}, parse_zip_options([{compress,none},{cwd,"/path"}])),
      ?_assertEqual({error,{bad_option,{compress,none}}}, parse_zip_options([{cwd,"/path"},{compress,none}])),
      ?_assertEqual({error,{bad_option,{compress,none}}}, parse_zip_options([{compress,all},{cwd,"/path"},{compress,none}])),
      ?_assertEqual({error,{bad_option,{compress,none}}}, parse_zip_options([{compress,none},{cwd,"/path"},{compress,all}]))
     ]}.

%% --------------------------------------
%% create zip archive from disk folder to disk archive test.
%% --------------------------------------
-define(REWZ_TEST_FOLDER, "/tmp/rewz_test").
-define(REWZ_TEST_FOLDER_SRC, ?REWZ_TEST_FOLDER++"/src").
-define(REWZ_TEST_FOLDER_DST, ?REWZ_TEST_FOLDER++"/dst").
-define(REWZ_TEST_FOLDER_ZIP, ?REWZ_TEST_FOLDER++"/archive.zip").

-define(REWZ_TEST_CONTENT_1,
        [
         {"abc.txt",<<"abc.txt content\nis muliti-line.\n">>},
         {"def.bin",<<0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15>>},
         {"ghi.sh",<<"#!/bin/sh\necho ghi\n">>}
        ]).
-define(REWZ_TEST_CONTENT_1_X,
        [
         {"xyz.ext",<<"must not be packed into archive because not listed.">>}
        ]).
-define(REWZ_TEST_CONTENT_2,
        [
         {"абв.txt",<<"абв.txt содержимое\nНе должно никак влиять.\n"/utf8>>},
         {"где.bin",<<15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0>>}
        ]).
-define(REWZ_TEST_CONTENT_2_X,
        [
         {"эюя.ext",<<"Не должен попасть в архив потому что не в списке.">>}
        ]).

create_test_() ->
    {"create/3 test",
     {setup,
      fun() -> wzip_test__cleanup_test_folder(force) end, %fun create_test__prepare_src_folder/1,
      fun wzip_test__cleanup_test_folder/1,
      [
       ?_assertEqual(true, create_test__body(latin, false)),
       ?_assertEqual(true, create_test__body(latin, true)),
       ?_assertEqual(true, create_test__body(unicode, false)),
       ?_assertEqual(true, create_test__body(unicode, true))
      ]}}.

%% @private
create_test__body(Variant, AddExtraFiles) ->
    wzip_test__cleanup_test_src_folder(),
    ok = create_test__prepare_src_folder(Variant),
    Files = files_in_folder(?REWZ_TEST_FOLDER_SRC),
    case AddExtraFiles of
        true -> ok = create_test__prepare_src_folder_extra(Variant);
        false -> ok end,
    create_test__remove_zip_archive(?REWZ_TEST_FOLDER_ZIP),
    create(?REWZ_TEST_FOLDER_ZIP, Files, [{cwd,?REWZ_TEST_FOLDER_SRC}]),
    create_test__check_zip_archive(?REWZ_TEST_FOLDER_ZIP, Variant).

%% @private
create_test__prepare_src_folder(latin) ->
    ok = ?BLfilelib:ensure_dir(?REWZ_TEST_FOLDER_SRC++"/"), % slash is required to create 'src' folder.
    wzip_test__write_files(?REWZ_TEST_CONTENT_1);
create_test__prepare_src_folder(unicode) ->
    create_test__prepare_src_folder(latin),
    wzip_test__write_files(?REWZ_TEST_CONTENT_2).

%% @private
create_test__prepare_src_folder_extra(latin) ->
    wzip_test__write_files(?REWZ_TEST_CONTENT_1_X);
create_test__prepare_src_folder_extra(unicode) ->
    create_test__prepare_src_folder_extra(latin),
    wzip_test__write_files(?REWZ_TEST_CONTENT_2_X).

%% @private
wzip_test__write_files(FCList) ->
    lists:foreach(fun({FN,FC}) ->
                      ok = file:write_file(?REWZ_TEST_FOLDER_SRC++"/"++FN, FC)
                  end, FCList).

%% @private
files_in_folder(Folder) ->
    Paths = ?BLfilelib:wildcard(?BU:to_unicode_list(Folder) ++ "/**"),
    {_Dirs, Files} = lists:partition(fun ?BLfilelib:is_dir/1, Paths),
    FI = length(Folder)+1, % also slash
    FilesSuf = lists:map(fun(FileName) -> string:slice(FileName,FI) end, Files),
    lists:sort(FilesSuf).

%% @private
wzip_test__cleanup_test_src_folder() ->
    ?BU:directory_delete(?REWZ_TEST_FOLDER_SRC).

%% @private
wzip_test__cleanup_test_dst_folder() ->
    ?BU:directory_delete(?REWZ_TEST_FOLDER_DST).

%% @private
wzip_test__cleanup_test_folder(_) ->
    ?BU:directory_delete(?REWZ_TEST_FOLDER).

%% @private
create_test__remove_zip_archive(ArchivePath) ->
    file:delete(ArchivePath).

%% @private
create_test__check_zip_archive(ArchivePath, Variant) ->
    % use erlang's zip:foldl() to extract content in memory
    {ok, C} = ?MODULE:foldl(fun(FileName, _GetInfoF, GetBinF, Acc) ->
                                [{FileName,GetBinF()} | Acc]
                            end, [], ArchivePath),
    A = case Variant of latin -> ?REWZ_TEST_CONTENT_1;
                        unicode -> ?REWZ_TEST_CONTENT_1 ++ ?REWZ_TEST_CONTENT_2 end,
    As = lists:sort(A),
    Cs = lists:sort(C),
    case Cs == As of
        true -> ok;
        false -> ?debugFmt("Cs = ~120tp", [Cs]),
                 ?debugFmt("!=As == ~120tp", [As]) end,
    Cs == As.

%% --------------------------------------
%% create zip archive from disk folder to disk archive test.
%% --------------------------------------
%create_from_dir_test_() ->
%    {"parse_zip_options/1 test",
%     [
%      ?_assertEqual(Files, create_from_dir(?REWZ_TEST_FOLDER++"/foo.zip",PackFolderPath))
%     ]}.

%% --------------------------------------
%% extract zip archive on disk to disk folder test.
%% --------------------------------------

-define(ZIP_ARCHIVE_UNZIP_TEST_LATIN,
<<16#50,16#4b,16#03,16#04,16#0a,16#00,16#00,16#00, 16#00,16#00,16#24,16#89,16#59,16#4e,16#0f,16#7a,
  16#82,16#f5,16#20,16#00,16#00,16#00,16#20,16#00, 16#00,16#00,16#07,16#00,16#1c,16#00,16#61,16#62,
  16#63,16#2e,16#74,16#78,16#74,16#55,16#54,16#09, 16#00,16#03,16#04,16#f7,16#73,16#5c,16#04,16#f7,
  16#73,16#5c,16#75,16#78,16#0b,16#00,16#01,16#04, 16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,16#00,
  16#00,16#61,16#62,16#63,16#2e,16#74,16#78,16#74, 16#20,16#63,16#6f,16#6e,16#74,16#65,16#6e,16#74,
  16#0a,16#69,16#73,16#20,16#6d,16#75,16#6c,16#69, 16#74,16#69,16#2d,16#6c,16#69,16#6e,16#65,16#2e,
  16#0a,16#50,16#4b,16#03,16#04,16#0a,16#00,16#00, 16#00,16#00,16#00,16#24,16#89,16#59,16#4e,16#88,
  16#e2,16#ce,16#ce,16#10,16#00,16#00,16#00,16#10, 16#00,16#00,16#00,16#07,16#00,16#1c,16#00,16#64,
  16#65,16#66,16#2e,16#62,16#69,16#6e,16#55,16#54, 16#09,16#00,16#03,16#04,16#f7,16#73,16#5c,16#04,
  16#f7,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01, 16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,
  16#00,16#00,16#00,16#01,16#02,16#03,16#04,16#05, 16#06,16#07,16#08,16#09,16#0a,16#0b,16#0c,16#0d,
  16#0e,16#0f,16#50,16#4b,16#03,16#04,16#0a,16#00, 16#00,16#00,16#00,16#00,16#24,16#89,16#59,16#4e,
  16#f5,16#a3,16#9e,16#8e,16#13,16#00,16#00,16#00, 16#13,16#00,16#00,16#00,16#06,16#00,16#1c,16#00,
  16#67,16#68,16#69,16#2e,16#73,16#68,16#55,16#54, 16#09,16#00,16#03,16#04,16#f7,16#73,16#5c,16#04,
  16#f7,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01, 16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,
  16#00,16#00,16#23,16#21,16#2f,16#62,16#69,16#6e, 16#2f,16#73,16#68,16#0a,16#65,16#63,16#68,16#6f,
  16#20,16#67,16#68,16#69,16#0a,16#50,16#4b,16#01, 16#02,16#1e,16#03,16#0a,16#00,16#00,16#00,16#00,
  16#00,16#24,16#89,16#59,16#4e,16#0f,16#7a,16#82, 16#f5,16#20,16#00,16#00,16#00,16#20,16#00,16#00,
  16#00,16#07,16#00,16#18,16#00,16#00,16#00,16#00, 16#00,16#01,16#00,16#00,16#00,16#a4,16#81,16#00,
  16#00,16#00,16#00,16#61,16#62,16#63,16#2e,16#74, 16#78,16#74,16#55,16#54,16#05,16#00,16#03,16#04,
  16#f7,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01, 16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,
  16#00,16#00,16#50,16#4b,16#01,16#02,16#1e,16#03, 16#0a,16#00,16#00,16#00,16#00,16#00,16#24,16#89,
  16#59,16#4e,16#88,16#e2,16#ce,16#ce,16#10,16#00, 16#00,16#00,16#10,16#00,16#00,16#00,16#07,16#00,
  16#18,16#00,16#00,16#00,16#00,16#00,16#00,16#00, 16#00,16#00,16#a4,16#81,16#61,16#00,16#00,16#00,
  16#64,16#65,16#66,16#2e,16#62,16#69,16#6e,16#55, 16#54,16#05,16#00,16#03,16#04,16#f7,16#73,16#5c,
  16#75,16#78,16#0b,16#00,16#01,16#04,16#e9,16#03, 16#00,16#00,16#04,16#e9,16#03,16#00,16#00,16#50,
  16#4b,16#01,16#02,16#1e,16#03,16#0a,16#00,16#00, 16#00,16#00,16#00,16#24,16#89,16#59,16#4e,16#f5,
  16#a3,16#9e,16#8e,16#13,16#00,16#00,16#00,16#13, 16#00,16#00,16#00,16#06,16#00,16#18,16#00,16#00,
  16#00,16#00,16#00,16#01,16#00,16#00,16#00,16#a4, 16#81,16#b2,16#00,16#00,16#00,16#67,16#68,16#69,
  16#2e,16#73,16#68,16#55,16#54,16#05,16#00,16#03, 16#04,16#f7,16#73,16#5c,16#75,16#78,16#0b,16#00,
  16#01,16#04,16#e9,16#03,16#00,16#00,16#04,16#e9, 16#03,16#00,16#00,16#50,16#4b,16#05,16#06,16#00,
  16#00,16#00,16#00,16#03,16#00,16#03,16#00,16#e6, 16#00,16#00,16#00,16#05,16#01,16#00,16#00,16#00,
  16#00>>
       ).


-define(ZIP_ARCHIVE_UNZIP_TEST_UNICODE,
<<16#50,16#4b,16#03,16#04,16#0a,16#00,16#00,16#00, 16#00,16#00,16#0c,16#88,16#59,16#4e,16#0f,16#7a,
  16#82,16#f5,16#20,16#00,16#00,16#00,16#20,16#00, 16#00,16#00,16#07,16#00,16#1c,16#00,16#61,16#62,
  16#63,16#2e,16#74,16#78,16#74,16#55,16#54,16#09, 16#00,16#03,16#f7,16#f4,16#73,16#5c,16#f7,16#f4,
  16#73,16#5c,16#75,16#78,16#0b,16#00,16#01,16#04, 16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,16#00,
  16#00,16#61,16#62,16#63,16#2e,16#74,16#78,16#74, 16#20,16#63,16#6f,16#6e,16#74,16#65,16#6e,16#74,
  16#0a,16#69,16#73,16#20,16#6d,16#75,16#6c,16#69, 16#74,16#69,16#2d,16#6c,16#69,16#6e,16#65,16#2e,
  16#0a,16#50,16#4b,16#03,16#04,16#0a,16#00,16#00, 16#00,16#00,16#00,16#0c,16#88,16#59,16#4e,16#88,
  16#e2,16#ce,16#ce,16#10,16#00,16#00,16#00,16#10, 16#00,16#00,16#00,16#07,16#00,16#1c,16#00,16#64,
  16#65,16#66,16#2e,16#62,16#69,16#6e,16#55,16#54, 16#09,16#00,16#03,16#f7,16#f4,16#73,16#5c,16#f7,
  16#f4,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01, 16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,
  16#00,16#00,16#00,16#01,16#02,16#03,16#04,16#05, 16#06,16#07,16#08,16#09,16#0a,16#0b,16#0c,16#0d,
  16#0e,16#0f,16#50,16#4b,16#03,16#04,16#0a,16#00, 16#00,16#00,16#00,16#00,16#0c,16#88,16#59,16#4e,
  16#f5,16#a3,16#9e,16#8e,16#13,16#00,16#00,16#00, 16#13,16#00,16#00,16#00,16#06,16#00,16#1c,16#00,
  16#67,16#68,16#69,16#2e,16#73,16#68,16#55,16#54, 16#09,16#00,16#03,16#f7,16#f4,16#73,16#5c,16#f7,
  16#f4,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01, 16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,
  16#00,16#00,16#23,16#21,16#2f,16#62,16#69,16#6e, 16#2f,16#73,16#68,16#0a,16#65,16#63,16#68,16#6f,
  16#20,16#67,16#68,16#69,16#0a,16#50,16#4b,16#03, 16#04,16#14,16#00,16#00,16#08,16#08,16#00,16#0c,
  16#88,16#59,16#4e,16#53,16#b2,16#dc,16#d2,16#47, 16#00,16#00,16#00,16#4b,16#00,16#00,16#00,16#0a,
  16#00,16#1c,16#00,16#d0,16#b0,16#d0,16#b1,16#d0, 16#b2,16#2e,16#74,16#78,16#74,16#55,16#54,16#09,
  16#00,16#03,16#f7,16#f4,16#73,16#5c,16#f7,16#f4, 16#73,16#5c,16#75,16#78,16#0b,16#00,16#01,16#04,
  16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,16#00, 16#00,16#0d,16#c8,16#b1,16#0d,16#80,16#30,16#0c,
  16#00,16#c1,16#3e,16#53,16#78,16#82,16#4c,16#97, 16#82,16#12,16#a8,16#91,16#98,16#80,16#1d,16#42,
  16#44,16#90,16#15,16#c0,16#33,16#bc,16#37,16#c2, 16#cd,16#4b,16#7f,16#54,16#4e,16#5a,16#2e,16#53,
  16#11,16#5f,16#30,16#2e,16#ba,16#cf,16#dc,16#28, 16#6f,16#5c,16#4f,16#1c,16#74,16#09,16#35,16#9e,
  16#d0,16#0f,16#93,16#88,16#32,16#a8,16#0c,16#a1, 16#85,16#aa,16#ef,16#be,16#fa,16#96,16#d3,16#0f,
  16#50,16#4b,16#03,16#04,16#0a,16#00,16#00,16#08, 16#00,16#00,16#0c,16#88,16#59,16#4e,16#73,16#c9,
  16#e0,16#b2,16#10,16#00,16#00,16#00,16#10,16#00, 16#00,16#00,16#0a,16#00,16#1c,16#00,16#d0,16#b3,
  16#d0,16#b4,16#d0,16#b5,16#2e,16#62,16#69,16#6e, 16#55,16#54,16#09,16#00,16#03,16#f7,16#f4,16#73,
  16#5c,16#f7,16#f4,16#73,16#5c,16#75,16#78,16#0b, 16#00,16#01,16#04,16#e9,16#03,16#00,16#00,16#04,
  16#e9,16#03,16#00,16#00,16#0f,16#0e,16#0d,16#0c, 16#0b,16#0a,16#09,16#08,16#07,16#06,16#05,16#04,
  16#03,16#02,16#01,16#00,16#50,16#4b,16#01,16#02, 16#1e,16#03,16#0a,16#00,16#00,16#00,16#00,16#00,
  16#0c,16#88,16#59,16#4e,16#0f,16#7a,16#82,16#f5, 16#20,16#00,16#00,16#00,16#20,16#00,16#00,16#00,
  16#07,16#00,16#18,16#00,16#00,16#00,16#00,16#00, 16#01,16#00,16#00,16#00,16#a4,16#81,16#00,16#00,
  16#00,16#00,16#61,16#62,16#63,16#2e,16#74,16#78, 16#74,16#55,16#54,16#05,16#00,16#03,16#f7,16#f4,
  16#73,16#5c,16#75,16#78,16#0b,16#00,16#01,16#04, 16#e9,16#03,16#00,16#00,16#04,16#e9,16#03,16#00,
  16#00,16#50,16#4b,16#01,16#02,16#1e,16#03,16#0a, 16#00,16#00,16#00,16#00,16#00,16#0c,16#88,16#59,
  16#4e,16#88,16#e2,16#ce,16#ce,16#10,16#00,16#00, 16#00,16#10,16#00,16#00,16#00,16#07,16#00,16#18,
  16#00,16#00,16#00,16#00,16#00,16#00,16#00,16#00, 16#00,16#a4,16#81,16#61,16#00,16#00,16#00,16#64,
  16#65,16#66,16#2e,16#62,16#69,16#6e,16#55,16#54, 16#05,16#00,16#03,16#f7,16#f4,16#73,16#5c,16#75,
  16#78,16#0b,16#00,16#01,16#04,16#e9,16#03,16#00, 16#00,16#04,16#e9,16#03,16#00,16#00,16#50,16#4b,
  16#01,16#02,16#1e,16#03,16#0a,16#00,16#00,16#00, 16#00,16#00,16#0c,16#88,16#59,16#4e,16#f5,16#a3,
  16#9e,16#8e,16#13,16#00,16#00,16#00,16#13,16#00, 16#00,16#00,16#06,16#00,16#18,16#00,16#00,16#00,
  16#00,16#00,16#01,16#00,16#00,16#00,16#a4,16#81, 16#b2,16#00,16#00,16#00,16#67,16#68,16#69,16#2e,
  16#73,16#68,16#55,16#54,16#05,16#00,16#03,16#f7, 16#f4,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01,
  16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03, 16#00,16#00,16#50,16#4b,16#01,16#02,16#1e,16#03,
  16#14,16#00,16#00,16#08,16#08,16#00,16#0c,16#88, 16#59,16#4e,16#53,16#b2,16#dc,16#d2,16#47,16#00,
  16#00,16#00,16#4b,16#00,16#00,16#00,16#0a,16#00, 16#18,16#00,16#00,16#00,16#00,16#00,16#01,16#00,
  16#00,16#00,16#a4,16#81,16#05,16#01,16#00,16#00, 16#d0,16#b0,16#d0,16#b1,16#d0,16#b2,16#2e,16#74,
  16#78,16#74,16#55,16#54,16#05,16#00,16#03,16#f7, 16#f4,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01,
  16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03, 16#00,16#00,16#50,16#4b,16#01,16#02,16#1e,16#03,
  16#0a,16#00,16#00,16#08,16#00,16#00,16#0c,16#88, 16#59,16#4e,16#73,16#c9,16#e0,16#b2,16#10,16#00,
  16#00,16#00,16#10,16#00,16#00,16#00,16#0a,16#00, 16#18,16#00,16#00,16#00,16#00,16#00,16#00,16#00,
  16#00,16#00,16#a4,16#81,16#90,16#01,16#00,16#00, 16#d0,16#b3,16#d0,16#b4,16#d0,16#b5,16#2e,16#62,
  16#69,16#6e,16#55,16#54,16#05,16#00,16#03,16#f7, 16#f4,16#73,16#5c,16#75,16#78,16#0b,16#00,16#01,
  16#04,16#e9,16#03,16#00,16#00,16#04,16#e9,16#03, 16#00,16#00,16#50,16#4b,16#05,16#06,16#00,16#00,
  16#00,16#00,16#05,16#00,16#05,16#00,16#86,16#01, 16#00,16#00,16#e4,16#01,16#00,16#00,16#00,16#00>>
       ).

unzip_test_() ->
    {"unzip/2 test",
     {setup,
      fun() -> wzip_test__cleanup_test_folder(force) end, %fun unzip_test__prepare_src_folder/1,
      fun wzip_test__cleanup_test_folder/1,
      [
       ?_assertEqual(true, unzip_test__body(latin)),
       ?_assertEqual(true, unzip_test__body(unicode))
      ]}}.

%% @private
unzip_test__body(Variant) ->
    wzip_test__cleanup_test_dst_folder(),
    ok = unzip_test__prepare_archive(Variant),
    R = unzip(?REWZ_TEST_FOLDER_ZIP, [{cwd,?REWZ_TEST_FOLDER_DST}]),
    case R of
        {ok,_} -> ok;
        _ -> ?debugFmt("unzip() -> ~120tp", [R]) end,
    unzip_test__check_dst_folder(?REWZ_TEST_FOLDER_DST, Variant).

%% @private
unzip_test__prepare_archive(Variant) ->
    ok = ?BLfilelib:ensure_dir(?REWZ_TEST_FOLDER++"/"), % slash is required to create folder itself.
    C = case Variant of
            latin -> ?ZIP_ARCHIVE_UNZIP_TEST_LATIN;
            unicode -> ?ZIP_ARCHIVE_UNZIP_TEST_UNICODE end,
    ok = file:write_file(?REWZ_TEST_FOLDER_ZIP, C).

%% @private
unzip_test__check_dst_folder(Folder, Variant) ->
    Paths = ?BLfilelib:wildcard(?BU:to_unicode_list(Folder) ++ "/**"),
    {_Dirs, Files} = lists:partition(fun ?BLfilelib:is_dir/1, Paths),
    FI = length(Folder)+1, % also slash
    FilesD = lists:map(fun(FileName) ->
                           FN = string:slice(FileName,FI),
                           {ok, FC} = file:read_file(FileName),
                           {FN,FC}
                       end, Files),
    A = case Variant of latin -> ?REWZ_TEST_CONTENT_1;
                        unicode -> ?REWZ_TEST_CONTENT_1 ++ ?REWZ_TEST_CONTENT_2 end,
    As = lists:sort(A),
    Ds = lists:sort(FilesD),
    case Ds == As of
        true -> ok;
        false -> ?debugFmt("Ds = ~120tp", [Ds]),
                 ?debugFmt("!=As == ~120tp", [As]) end,
    Ds == As.

-endif.

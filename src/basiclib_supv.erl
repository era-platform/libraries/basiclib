%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.10.2015
%%% @doc Basiclib app supv

-module(basiclib_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([init/1]).
-export([start_link/1]).
-export([start_child/1, restart_child/1, terminate_child/1, delete_child/1, get_childspec/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, StartArgs).

start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

get_childspec(Id) ->
    supervisor:get_childspec(?MODULE, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(StartArgs) ->
    Spec = [{?BLopts, {?BLopts, start_link, [StartArgs]}, permanent, 1000, worker, [?BLopts]},
            {?BLquota, {?BLquota, start_link, [[]]}, permanent, 1000, worker, [?BLquota]},
            {?BLmonitor, {?BLmonitor, start_link, [[{ets, ets:new(monitor,[public,set])},
                                                    {funlog, fun(Fmt,Args) -> ?LOG(Fmt,Args) end}]]}, permanent, 1000, worker, [?BLmonitor]},
            {?BLstore_srv, {?BLstore_srv, start_link, [[{ets_u, ets:new(ets_u,[public,set])},
                                                        {ets_t, ets:new(ets_t,[public,set])}]]}, permanent, 1000, worker, [?BLstore]},
            {?BLpingstore, {?BLpingstore, start_link, [[{ets_u, ets:new(ets_u,[public,set])},
                                                        {ets_t, ets:new(ets_t,[public,set])}]]}, permanent, 1000, worker, [?BLpingstore]},
            {?WORKERSUPV, {?WORKERSUPV, start_link, [[]]}, permanent, 1000, supervisor, [?WORKERSUPV]},

            {?ErrLoggerSrv, {?ErrLoggerSrv, start_link, [StartArgs]}, permanent, 1000, worker, [?ErrLoggerSrv]},
            {?FileCopier,{?FileCopier,start_link,[[]]},permanent, 1000, worker, [?FileCopier]}
           ],
    supervisor:check_childspecs(Spec),
    {ok, {{one_for_one, 10, 2}, Spec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

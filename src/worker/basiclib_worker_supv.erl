%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.12.2015
%%% @doc Common environment worker's supv

-module(basiclib_worker_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).

-export([get_default_worker_count/0,
    get_worker_name/1]).

-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Args) ->
    supervisor:start_link(?MODULE, Args).

get_default_worker_count() -> 32.

get_worker_name(0) -> basiclib_worker_0;
get_worker_name(1) -> basiclib_worker_1;
get_worker_name(2) -> basiclib_worker_2;
get_worker_name(3) -> basiclib_worker_3;
get_worker_name(4) -> basiclib_worker_4;
get_worker_name(5) -> basiclib_worker_5;
get_worker_name(6) -> basiclib_worker_6;
get_worker_name(7) -> basiclib_worker_7;
get_worker_name(8) -> basiclib_worker_8;
get_worker_name(9) -> basiclib_worker_9;
get_worker_name(10) -> basiclib_worker_10;
get_worker_name(11) -> basiclib_worker_11;
get_worker_name(12) -> basiclib_worker_12;
get_worker_name(13) -> basiclib_worker_13;
get_worker_name(14) -> basiclib_worker_14;
get_worker_name(15) -> basiclib_worker_15;
get_worker_name(16) -> basiclib_worker_16;
get_worker_name(17) -> basiclib_worker_17;
get_worker_name(18) -> basiclib_worker_18;
get_worker_name(19) -> basiclib_worker_19;
get_worker_name(20) -> basiclib_worker_20;
get_worker_name(21) -> basiclib_worker_21;
get_worker_name(22) -> basiclib_worker_22;
get_worker_name(23) -> basiclib_worker_23;
get_worker_name(24) -> basiclib_worker_24;
get_worker_name(25) -> basiclib_worker_25;
get_worker_name(26) -> basiclib_worker_26;
get_worker_name(27) -> basiclib_worker_27;
get_worker_name(28) -> basiclib_worker_28;
get_worker_name(29) -> basiclib_worker_29;
get_worker_name(30) -> basiclib_worker_30;
get_worker_name(31) -> basiclib_worker_31;
get_worker_name(A) -> ?BU:to_atom_new("basiclib_worker_" ++ ?BU:to_list(A)).


%% ====================================================================
%% Callbacks
%% ====================================================================

init(Args) ->
    WorkerCount = case ?BU:get_by_key(worker_count, Args, undefined) of
                      Count when is_integer(Count), Count>32, Count<256 -> Count;
                      _ -> get_default_worker_count()
                  end,
    ChildSpec = prepare_child_specs(lists:seq(0,WorkerCount-1), []),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

prepare_child_specs([], ChildSpecs) -> lists:reverse(ChildSpecs);
prepare_child_specs([F|Rest], ChildSpecs) ->
    Name = get_worker_name(F),
    Spec = {Name, {?WORKER, start_link, [Name]}, permanent, 1000, worker, [?WORKER]},
    prepare_child_specs(Rest, [Spec|ChildSpecs]).



%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.12.2015
%%% @doc Common environment worker srv

-module(basiclib_worker_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1, stop/1]).

-export([call_work/3, cast_work/3,
         call_work/2, cast_work/2,
         call_work_index/2, cast_work_index/2,
         call_workf/1, cast_workf/1,
         call_workf/2, cast_workf/2,
         call_workf_index/2, cast_workf_index/2,
         call_workf/3,
         call_workf_index/3]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(WORKER(A), erlang:phash2(A) rem ?WORKERSUPV:get_default_worker_count()).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Name) ->
    gen_server:start_link({local, Name}, ?MODULE, [{name, Name}], []).

stop(Name) ->
    gen_server:stop(Name).

%% -----------------------
%% Work M,F,A. Hashring automatically by Args to all workers
%% -----------------------
call_work(M,F,A) ->
    gen_server:call(?WORKERSUPV:get_worker_name(?WORKER(A)), {work,M,F,A}).

cast_work(M,F,A) ->
    gen_server:cast(?WORKERSUPV:get_worker_name(?WORKER(A)), {work,M,F,A}).

%% -----------------------
%% Work M,F,A. Hashring automatically by Hashcode to all workers
%% -----------------------
call_work(HashCode, {M,F,A}) ->
    gen_server:call(?WORKERSUPV:get_worker_name(?WORKER(HashCode)), {work,M,F,A}).

cast_work(HashCode, {M,F,A}) ->
    gen_server:cast(?WORKERSUPV:get_worker_name(?WORKER(HashCode)), {work,M,F,A}).

%% -----------------------
%% Work M,F,A. External hashring and worker count. Use worker index less then worker count.
%% -----------------------
call_work_index(Index, {M,F,A}) when is_integer(Index), Index>=0, Index=<32 ->
    gen_server:call(?WORKERSUPV:get_worker_name(Index), {work,M,F,A}).

cast_work_index(Index, {M,F,A}) when is_integer(Index), Index>=0, Index=<32 ->
    gen_server:cast(?WORKERSUPV:get_worker_name(Index), {work,M,F,A}).

%% -----------------------
%% Work F::function/0. Hashring automatically by Function value. All workers
%% -----------------------
call_workf(F) when is_function(F) ->
    gen_server:call(?WORKERSUPV:get_worker_name(?WORKER(F)), {workf,F}).

cast_workf(F) when is_function(F) ->
    gen_server:cast(?WORKERSUPV:get_worker_name(?WORKER(F)), {workf,F}).

%% -----------------------
%% Work F::function/0. Hashring automatically by hashcode. All workers
%% -----------------------
call_workf(HashCode,F) when is_function(F) ->
    gen_server:call(?WORKERSUPV:get_worker_name(?WORKER(HashCode)), {workf,F}).

cast_workf(HashCode, F) when is_function(F) ->
    gen_server:cast(?WORKERSUPV:get_worker_name(?WORKER(HashCode)), {workf,F}).

%% -----------------------
%% Work F::function/0. External hashring and worker count. Use worker index less then worker count.
%% -----------------------
call_workf_index(Index,F) when is_function(F) ->
    gen_server:call(?WORKERSUPV:get_worker_name(Index), {workf,F}).

cast_workf_index(Index, F) when is_function(F) ->
    gen_server:cast(?WORKERSUPV:get_worker_name(Index), {workf,F}).

%% -----------------------
%% Work F::function/0. Hashring automatically by hashcode. All workers. Timeout wait reply
%% -----------------------
call_workf(HashCode,F,Timeout) when is_function(F),is_integer(Timeout) ->
    gen_server:call(?WORKERSUPV:get_worker_name(?WORKER(HashCode)), {workf,F}, Timeout).

%% -----------------------
%% Work F::function/0. External hashring and worker count. Use worker index less then worker count.
%% Timeout wait reply
%% -----------------------
call_workf_index(Index,F,Timeout) when is_function(F),is_integer(Timeout) ->
    gen_server:call(?WORKERSUPV:get_worker_name(Index), {workf,F}, Timeout).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Name = ?BU:get_by_key(name,Opts),
    State1 = Name,
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call({work,M,F,A}, _From, State) ->
    Reply = case catch erlang:apply(M,F,A) of
                {'EXIT',Reason} -> {error,{worker,Reason}};
                R -> R
            end,
    {reply, Reply, State};

handle_call({workf,F}, _From, State) ->
    Reply = case catch F() of
                {'EXIT',Reason} -> {error,{worker,Reason}};
                R -> R
            end,
    {reply, Reply, State};

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({work,M,F,A}, State) ->
    catch erlang:apply(M,F,A),
    {noreply, State};

handle_cast({workf,F}, State) ->
    catch F(),
    {noreply, State};

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

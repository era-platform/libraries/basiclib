%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.12.2015
%%% @doc Common node storage server.
%%%        Store data by key.
%%%        Could be timebound.
%%%        Allow to delete, get, execute funs (make transactions), fold data.

-module(basiclib_store_srv_template).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/2,
         %
         print/1,
         size/1,
         %
         store_u/3,
         find_u/2,
         delete_u/2,
         get_all_u/1,
         func_u/4,
         func_u/3,
         foldl_u/3,
         clear_u/1,
         ensure_ets/2,
         %
         store_t/4,
         refresh_t/3,
         find_t/2,
         delete_t/2,
         get_all_t/1,
         func_t/5,
         func_t/4,
         foldl_t/3,
         clear_t/1,
         %
         lazy_t/3, lazy_t/4, lazy_t/5]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(trec, {
    value,
    ref,
    timeout, % timeout in ms
    timerref, % timer ref
    timeoutgs % greg second of timeout moment
  }).

-record(state, {
    regname,
    unlimit,
    timered,
    timerref
  }).

-define(CONTROLTIMER, erlang:send_after(10000, self(), {control_timer})).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% Start and register by local name
%% ------------------------------
start_link(M,Opts) ->
    gen_server:start_link({local,M}, ?MODULE, [{name,M}|Opts], []).

%% ------------------------------
%% DEBUG prints state
%% ------------------------------
print(M) -> gen_server:call(M, {print}).

%% ------------------------------
%% DEBUG returns count of items
%% ------------------------------
size(M) -> gen_server:call(M, {size}).

%% ------------------------------
%% Stores Key-Value for infinity. Operation is asynchronous.
%% ------------------------------
-spec store_u(Regname::atom(), Key::term(), Value::term()) -> ok.
%% ------------------------------
store_u(M, Key, Value) ->
    gen_server:cast(M, {store_u, Key, Value}).

%% ------------------------------
%% Search and return value by Key, stored for infinity
%% ------------------------------
-spec find_u(Regname::atom(), Key::term()) -> {Key::term(),Value::term()} | false.
%% ------------------------------
find_u(M, Key) ->
    gen_server:call(M, {find_u, Key}).

%% ------------------------------
%% Delete value by Key if exist and stored for infinity. Operation is asynchronous.
%% ------------------------------
-spec delete_u(Regname::atom(), Key::term()) -> ok.
%% ------------------------------
delete_u(M, Key) ->
    gen_server:cast(M, {delete_u, Key}).

%% ------------------------------
%% DEBUG. Returns all records, stored for infinity
%% ------------------------------
get_all_u(M) ->
    gen_server:call(M, {get_all_u}).

%% ------------------------------
%% Applies value-update function in process of storage. Applied to keys stored for infinity.
%%   Implements transaction change.
%%   If Key is not found then FunStore applies to 'undefined'
%%   Returns the value of FunReply, applied either to OldValue if arity of 1 or to Old and New values both if arity of 2.
%% ------------------------------
-spec func_u(Regname::atom(), Key::term(), FunStore::function(), FunReply::function()) -> term().
%% ------------------------------
func_u(M, Key, FunStore, FunReply)
  when is_function(FunStore,1) andalso (is_function(FunReply,1) orelse is_function(FunReply,2)) ->
      gen_server:call(M, {func_u, Key, FunStore, FunReply}).

%% ------------------------------
%% Applies value-update function in process of storage. Applied to keys stored for infinity.
%%   Implements transaction change.
%%   If Key is not found then FunStore applies to 'undefined'
%%   Operation is asynchronous.
%% ------------------------------
-spec func_u(Regname::atom(), Key::term(), FunStore::function()) -> ok.
%% ------------------------------
func_u(M, Key, FunStore)
  when is_function(FunStore,1) ->
      gen_server:cast(M, {func_u, Key, FunStore}).

%% ------------------------------
%% Applies foldl operation in store process. Applies it to all of records? stored for infinity.
%%   Function should have arity of 2. It is passed to ets:fold().
%% ------------------------------
-spec foldl_u(Regname::atom(), Fun::function(), AccIn::term()) -> AccOut::term().
%% ------------------------------
foldl_u(M, Fun, AccIn) ->
    gen_server:call(M, {foldl_u, Fun, AccIn}).

%% ------------------------------
%% Clears all records, stored without autodeletion timeout
%% ------------------------------
clear_u(M) ->
    gen_server:call(M, {clear_u}).

%%% ------------------------------------------------------------
%%% Limited storage. Autodeletion timer is used
%%% ------------------------------------------------------------

%% ------------------------------
%% Stores Key-Value that should be automatically deleted after Timeout. Operation is asynchronous.
%% ------------------------------
-spec store_t(Regname::atom(), Key::term(), Value::term(), Timeout::non_neg_integer()) -> ok.
%% ------------------------------
store_t(M, Key, Value, Timeout) ->
    gen_server:cast(M, {store_t, Key, Value, Timeout}).

refresh_t(M, Key, Timeout) ->
    gen_server:cast(M, {refresh_t, Key, Timeout}).

%% ------------------------------
%% Search and return value by Key, stored in auto-deletion mode
%% ------------------------------
-spec find_t(Regname::atom(), Key::term()) -> {Key::term(),Value::term()} | false.
%% ------------------------------
find_t(M, Key) ->
    gen_server:call(M, {find_t, Key}).

%% ------------------------------
%% Delete value by Key if exist and stored in auto-deletion mode
%% ------------------------------
-spec delete_t(Regname::atom(), Key::term()) -> ok.
%% ------------------------------
delete_t(M, Key) ->
    gen_server:cast(M, {delete_t, Key}).

%% ------------------------------
%% DEBUG. Returns all records, stored in auto-deletion mode
%% ------------------------------
get_all_t(M) ->
    gen_server:call(M, {get_all_t}).

%% ------------------------------
%% Applies value-update function in process of storage. Applied to keys stored in auto-deletion mode.
%%   Implements transaction change.
%%   If Key is not found then FunStore applies to 'undefined'
%%   Returns the value of FunReply, applied either to OldValue if arity of 1 or to Old and New values both if arity of 2.
%%   If value changed then new timer starts for new Timeout. Else previous timer continues.
%% ------------------------------
-spec func_t(Regname::atom(), Key::term(), TimeoutMs::non_neg_integer(), FunStore::function(), FunReply::function()) -> term().
%% ------------------------------
func_t(M, Key, Timeout, FunStore, FunReply)
  when is_function(FunStore,1) andalso (is_function(FunReply,1) orelse is_function(FunReply,2)) ->
      gen_server:call(M, {func_t, Key, Timeout, FunStore, FunReply}).

%% ------------------------------
%% Applies value-update function in process of storage. Applied to keys stored in auto-deletion mode.
%%   Implements transaction change.
%%   If Key is not found then FunStore applies to 'undefined'
%%   Operation is asynchronous.
%%   If value changed then new timer starts for new Timeout. Else previous timer continues.
%% ------------------------------
-spec func_t(Regname::atom(), Key::term(), TimeoutMs::non_neg_integer(), FunStore::function()) -> ok.
%% ------------------------------
func_t(M, Key, Timeout, FunStore)
  when is_function(FunStore,1) ->
      gen_server:cast(M, {func_t, Key, Timeout, FunStore}).

%% ------------------------------
%% Applies foldl operation in store process. Applies it to all of records? stored for infinity.
%%   Function should have arity of 2. It is passed to ets:fold().
%% ------------------------------
-spec foldl_t(Regname::atom(), Fun::function(), AccIn::term()) -> AccOut::term().
%% ------------------------------
foldl_t(M, Fun, AccIn) ->
    gen_server:call(M, {foldl_t, Fun, AccIn}).

%% ------------------------------
%% Clears all records, stored in auto-deletion mode.
%% ------------------------------
clear_t(M) ->
    gen_server:call(M, {clear_t}).

%% ------------------------------
%% Lazy function call. Return cached value or call lazy function and store its result for 5 seconds.
%% Caller should not mix same keys to lazy and other operations.
%% Note, that FLazy has arity 0 and called in storage process, so it should be extremally rapid to avoid storage unavailability.
%% Uses cache timeout 5000.
%% ------------------------------
-spec lazy_t(M::atom(), Key::term(), FLazy::function()) -> term().
%% ------------------------------
lazy_t(M, Key, FLazy) ->
    lazy_t(M, Key, FLazy, {10000,5000,1000}, {true,false}).

%% ------------------------------
%% Lazy function call. Return cached value or call lazy function and store its result for sometime.
%% Params:
%%   Key. Cached storage key. Should not be mixed to other operations than lazy.
%%   FLazy. Purposed function of arity 0.
%%   OptsMap. Lazy properties. Keys:
%%      store_timeout. After it, value would be automatically deleted from storage. Default 10000.
%%      expire_timeout. Should be less than store_timeout. Default 5000.
%%          If called after expire_timeout and before store_timeout from previous call then update process is started automatically.
%%      update_timeout. Should be less than store_timeout.  Default 1000.
%%          If update value process is async and it was already started ealier than update_timeout ms ago, then starts new update process automatically.
%%      store_tran. Default 'true'.
%%          If 'true' then FLazy is called from storage process. It should be extremally rapid to avoid storage unavailability.
%%          If 'false' then FLazy is called from caller process and not guarantee transaction.
%%          You should use false if FLazy could take a time to run.
%%      sync_update. Default 'false'
%%          If 'true' then caller waits for new FLazy call result after expire_timeout. But FLazy is called in caller process, not from storage process. So several processes could call FLazy at the same time when update.
%%          If 'false' then caller returns previous value but spawned process is started to call FLazy and update value.
%% ------------------------------
-spec lazy_t(M::atom(), Key::term(), FLazy::function(), OptsMap::map()) -> term().
%% ------------------------------
lazy_t(M, Key, FLazy, OptsMap) ->
    TStore = maps:get(store_timeout,OptsMap,10000),
    TExpire = maps:get(expire_timeout,OptsMap,5000),
    TUpdate = maps:get(update_timeout,OptsMap,1000),
    UseTran = maps:get(store_tran,OptsMap,true),
    SyncUpdate = maps:get(sync_update,OptsMap,false),
    lazy_t(M, Key, FLazy, {TStore,TExpire,TUpdate}, {UseTran,SyncUpdate}).

%% ------------------------------
%% Lazy function call. Return cached value or call lazy function and store its result for sometime.
%% Params:
%%   Key. Cached storage key. Should not be mixed to other operations than lazy.
%%   FLazy. Purposed function of arity 0.
%%   TStore. After it, value would be automatically deleted from storage. Default 10000.
%%   TExpire. Should be less than store_timeout. Default 5000.
%%       If called after expire_timeout and before store_timeout from previous call then update process is started automatically.
%%   TUpdate. Should be less than store_timeout.  Default 1000.
%%       If update value process is async and it was already started ealier than update_timeout ms ago, then starts new update process automatically.
%%   UseTran. Default 'true'.
%%       If 'true' then FLazy is called from storage process. It should be extremally rapid to avoid storage unavailability.
%%       If 'false' then FLazy is called from caller process and not guarantee transaction.
%%       You should use false if FLazy could take a time to run.
%%   SyncUpdate. Default 'false'
%%       If 'true' then caller waits for new FLazy call result after expire_timeout. But FLazy is called in caller process, not from storage process. So several processes could call FLazy at the same time when update.
%%       If 'false' then caller returns previous value but spawned process is started to call FLazy and update value.
%% ------------------------------
-spec lazy_t(M::atom(), Key::term(), FLazy::function(),
             {TStore::integer(),TExpire::integer(),TUpdate::integer()},
             {UseTran::boolean(),SyncUpdate::boolean()}) -> term().
%% ------------------------------
lazy_t(M, Key, FLazy, {TStore,TExpire,TUpdate}, {UseTran,false=SyncUpdate})
  when is_integer(TStore), is_integer(TExpire), is_integer(TUpdate), TStore>TExpire, TStore>TUpdate, TExpire>100, TUpdate>100,
       is_boolean(UseTran), is_boolean(SyncUpdate), is_function(FLazy,0) ->
    NowTS = ?BU:timestamp(),
    F = fun() -> {FLazy(),NowTS} end,
    FunStore = fun(undefined) when UseTran -> F(); % first, sync update in store proc
                  (undefined) -> undefined; % first, sync update in caller proc
                  ({R,TS}) when is_integer(TS),NowTS-TS>TExpire -> {R,{up,NowTS}}; % time left
                  ({R,{up,TS}}) when is_integer(TS),NowTS-TS>TUpdate -> {R,{up,NowTS}}; % up time left (update proc failed)
                  (R) -> R % up time norm, store time normal
               end,
    FunReply = fun({_,X}, {R,X}) -> {ok,R}; % not changed (by ext)
                  (_, undefined) -> up_sync; % undefined
                  (_, {_,{up,_}}) when SyncUpdate -> up_sync; % changed to up (by ext), use sync
                  (_, {R,{up,_}}) -> {up_async,R}; % changed to up (by ext), use async
                  (_, {R,_}) -> {ok,R} % other, undefined to sync result
               end,
    case ?MODULE:func_t(M, Key, TStore, FunStore, FunReply) of
        up_sync ->
            {Res,NowTS}=R = F(),
            ?MODULE:store_t(M, Key, R, TStore);
        {up_async,Res} ->
            spawn(fun() -> ?MODULE:store_t(M, Key, F(), TStore) end);
        {ok,Res} -> ok
    end,
    Res;

%%
lazy_t(M, Key, FLazy, {TStore,TExpire,TUpdate}, {UseTran,true=SyncUpdate})
    when is_integer(TStore), is_integer(TExpire), is_integer(TUpdate), TStore>TExpire, TStore>TUpdate, TExpire>100, TUpdate>100,
    is_boolean(UseTran), is_boolean(SyncUpdate), is_function(FLazy,0) ->
    NowTS = ?BU:timestamp(),
    case ?MODULE:find_t(M, Key) of
        {_,{Value0,TS}} when NowTS-TS < TExpire -> Value0;
        _ -> % false or expired
            FunCompute = fun() ->
                            Value = FLazy(),
                            NowTS1 = ?BU:timestamp(),
                            ?MODULE:store_t(M,Key,{Value,NowTS1},TStore),
                            Value
                         end,
            ?BLquota:request_quota(Key,self(),t),
            Value = receive
                        pass ->
                            case ?MODULE:find_t(M, Key) of
                                {_,{Value0,TS}} when NowTS-TS < TExpire -> Value0;
                                _ -> FunCompute() % false or expired

                            end
                    after TUpdate -> FunCompute()
                    end,
            ?BLquota:release_quota(Key,self()),
            Value
    end.

%% -------------------------------
%% Return ets of selected Key.
%%   This ets is created on first call and stores for infinity.
%%   Storage process is it's owner.
%% -------------------------------
ensure_ets(M, Key) ->
    gen_server:call(M, {ensure_ets, Key}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Opts) ->
    [RegName, UnlimitETS, TimeredETS] = ?BU:extract_required_props([name, ets_u, ets_t], Opts),
    State = #state{regname=RegName,
                   unlimit=UnlimitETS,
                   timered=TimeredETS,
                   timerref=?CONTROLTIMER},
    {ok, State}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

%% print state
handle_call({print}, _From, State) ->
    ?OUTC('$force', "Store '~s'. State: ~120p", [State#state.regname,State]),
    {reply, ok, State};

%% return size
handle_call({size}, _From, #state{unlimit=U,timered=T}=State) ->
    Reply = {ok,ets:info(U,size) + ets:info(T,size)},
    {reply, Reply, State};


%% ----
%% returns count of unlimit time data
handle_call({count_u}, _From, #state{unlimit=ETS}=State) ->
    Size = ets:info(ETS, size),
    ?OUTC('$force', "Store '~s'. Count of unlimit: ~120p", [State#state.regname,Size]),
    {reply, Size, State};

%% returns count of timered time data
handle_call({count_t}, _From, #state{timered=ETS}=State) ->
    Size = ets:info(ETS, size),
    ?OUTC('$force', "Store '~s'. Count of timered: ~120p", [State#state.regname,Size]),
    {reply, Size, State};


%% ----
%% find unlimit value
handle_call({find_u, Key}, _From, #state{unlimit=ETS}=State) ->
    Reply = case ets:lookup(ETS, Key) of
                [{Key,Value}] -> {Key,Value};
                [] -> false
            end,
    {reply, Reply, State};

%% find timered value
handle_call({find_t, Key}, _From, #state{timered=ETS}=State) ->
    Reply = case ets:lookup(ETS, Key) of
                [{Key,#trec{value=Value}}] -> {Key,Value};
                [] -> false
            end,
    {reply, Reply, State};


%% ----
%% returns all u objects
handle_call({get_all_u}, _From, #state{unlimit=ETS}=State) ->
    Reply = ets:tab2list(ETS),
    {reply, Reply, State};

%% returns all t objects
handle_call({get_all_t}, _From, #state{timered=ETS}=State) ->
    Reply = ets:tab2list(ETS),
    {reply, Reply, State};


%% ----
%% exec fun on u object
handle_call({func_u, Key, FunStore, FunReply}, _From, #state{unlimit=ETS}=State) ->
    PrevValue = case ets:lookup(ETS, Key) of
                    [{Key,Value}] -> Value;
                    [] -> undefined
                end,
    NewValue = FunStore(PrevValue),
    ets:insert(ETS, {Key,NewValue}),
    Reply = case erlang:fun_info(FunReply, arity) of
                {_,1} -> FunReply(PrevValue);
                {_,2} -> FunReply(PrevValue,NewValue)
            end,
    {reply, Reply, State};

%% exec fun on t object
handle_call({func_t, Key, Timeout, FunStore, FunReply}, _From, #state{timered=ETS}=State) ->
    {PrevValue,PrevRec} = case ets:lookup(ETS, Key) of
                              [{Key,#trec{value=Value}=Rec}] -> {Value,Rec};
                              [] -> {undefined,undefined}
                          end,
    case FunStore(PrevValue) of
        PrevValue -> NewValue = PrevValue;
        NewValue ->
            case PrevRec of
                #trec{timerref=TimerRef} -> erlang:cancel_timer(TimerRef);
                undefined -> ok
            end,
            Ref = make_ref(),
            NewRec = #trec{value=NewValue,
                           ref=Ref,
                           timeout=Timeout,
                           timerref = erlang:send_after(Timeout, self(), {store_timeout, Key, Ref}),
                           timeoutgs = ?BU:current_gregsecond() + (Timeout div 1000) + 1},
            ets:insert(ETS, {Key,NewRec})
    end,
    Reply = case erlang:fun_info(FunReply, arity) of
                {_,1} -> FunReply(PrevValue);
                {_,2} -> FunReply(PrevValue,NewValue)
            end,
    {reply, Reply, State};


%% ----
%% foldl on u store
handle_call({foldl_u, F, AccIn}, _From, #state{unlimit=ETS}=State) ->
    Reply = ets:foldl(F, AccIn, ETS),
    {reply, Reply, State};

%% foldl on t store
handle_call({foldl_t, F, AccIn}, _From, #state{timered=ETS}=State) ->
    Reply = ets:foldl(fun({Key,#trec{value=Value}}, Acc) -> F({Key,Value},Acc) end, AccIn, ETS),
    {reply, Reply, State};

%% clear u
handle_call({clear_u}, _From, #state{unlimit=ETS}=State) ->
    ets:delete_all_objects(ETS),
    {reply, ok, State};

%% clear t
handle_call({clear_t}, _From, #state{timered=ETS}=State) ->
    ets:delete_all_objects(ETS),
    {reply, ok, State};

%% ensure ets
handle_call({ensure_ets,Key}, _From, #state{unlimit=ETS}=State) ->
    Reply = case ets:lookup(ETS, {internal_ets,Key}) of
                [{_,{ets,Value}}] -> Value;
                [] ->
                    Value = ets:new(sub,[set,public]),
                    ets:insert(ETS, {{internal_ets,Key},{ets,Value}}),
                    Value
            end,
    {reply, Reply, State};

%% ----
%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

%% ------
%% store unlimit value
handle_cast({store_u, Key, Value}, #state{unlimit=ETS}=State) ->
    ets:insert(ETS, {Key, Value}),
    {noreply, State};

%% ------
%% delete unlimit value by key
handle_cast({delete_u, Key}, #state{unlimit=ETS}=State) ->
    ets:delete(ETS, Key),
    {noreply, State};

%% ------
%% store timered value
handle_cast({store_t, Key, Value, Timeout}, #state{timered=ETS}=State) ->
    case ets:lookup(ETS, Key) of
        [{Key,#trec{timerref=TimerRef}}] -> erlang:cancel_timer(TimerRef);
        [] -> ok
    end,
    Ref = make_ref(),
    Rec = #trec{value = Value,
                ref = Ref,
                timeout=Timeout,
                timerref = erlang:send_after(Timeout, self(), {store_timeout, Key, Ref}),
                timeoutgs = ?BU:current_gregsecond() + (Timeout div 1000) + 1},
    ets:insert(ETS, {Key, Rec}),
    {noreply, State};

%% ------
%% refresh timered value timeout
handle_cast({refresh_t, Key, Timeout}, #state{timered=ETS}=State) ->
    case ets:lookup(ETS, Key) of
        [{Key,#trec{value=Value, timerref=TimerRef}}] ->
            erlang:cancel_timer(TimerRef),
            Ref = make_ref(),
            Rec = #trec{value = Value,
                        ref = Ref,
                        timeout=Timeout,
                        timerref = erlang:send_after(Timeout, self(), {store_timeout, Key, Ref}),
                        timeoutgs = ?BU:current_gregsecond() + (Timeout div 1000) + 1},
            ets:insert(ETS, {Key, Rec});
        [] -> ok
    end,
    {noreply, State};

%% -------
%% delete timered value by key
handle_cast({delete_t, Key}, #state{timered=ETS}=State) ->
    case ets:lookup(ETS, Key) of
        [{Key,#trec{timerref=TimerRef}}] -> erlang:cancel_timer(TimerRef);
        [] -> ok
    end,
    ets:delete(ETS, Key),
    {noreply, State};


%% --------
%% exec fun on u object
handle_cast({func_u, Key, FunStore}, State) ->
    {_,_,State} = handle_call({func_u, Key, FunStore, fun(_) -> ok end}, undefined, State),
    {noreply, State};

%% exec fun on t object
handle_cast({func_t, Key, Timeout, FunStore}, State) ->
    {_,_,State} = handle_call({func_t, Key, Timeout, FunStore, fun(_) -> ok end}, undefined, State),
    {noreply, State};


%% -------
%% restart
handle_cast({restart}, State) ->
    {stop, State};


%% -------
%% other
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

%% timeout timered value
handle_info({store_timeout, Key, Ref}, #state{timered=ETS}=State) ->
    case ets:lookup(ETS, Key) of
        [{Key,#trec{ref=Ref}}] ->
            ets:delete(ETS, Key);
        [] -> ok;
        _ -> ok
    end,
    {noreply, State};

%% control_timer for hang expired
handle_info({control_timer}, #state{timered=ETS}=State) ->
    Now = ?BU:current_gregsecond(),
    Del = ets:foldl(fun({Key,#trec{timeoutgs=TimeoutGS}}, Acc) when TimeoutGS+5 < Now -> ets:delete(ETS, Key), Acc+1;
                       (_, Acc) -> Acc end, 0, ETS),
    case Del > 0 of
        true -> ?LOG('$info', "Store '~s'. control_timer removed ~p expired items", [State#state.regname,Del]);
        false -> ok
    end,
    {noreply, State#state{timerref=?CONTROLTIMER}};

%% other
handle_info(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================


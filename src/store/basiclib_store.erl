%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.01.2021
%%% @doc Local store facade.
%%%     If application is running then uses Store gen_server
%%%     Else uses custom store on application:put_env and get_env

-module(basiclib_store).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([store_u/2,
         find_u/1,
         delete_u/1,
         func_u/2, func_u/3,
         foldl_u/2]).

-export([store_t/3,
         find_t/1,
         delete_t/1,
         func_t/3, func_t/4,
         lazy_t/3, lazy_t/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

store_u(Key,Value) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:store_u(Key,Value);
        _ -> local_store_u(Key,Value)
    end.

find_u(Key) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:find_u(Key);
        _ -> local_find_u(Key)
    end.

delete_u(Key) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:delete_u(Key);
        _ -> local_delete_u(Key)
    end.

func_u(Key,FunStore) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:func_u(Key,FunStore);
        _ -> local_func_u(Key,FunStore)
    end.

func_u(Key,FunStore,FunReply) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:func_u(Key,FunStore,FunReply);
        _ -> local_func_u(Key,FunStore,FunReply)
    end.

foldl_u(Fun, AccIn) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:foldl_u(Fun, AccIn);
        _ -> local_foldl_u(Fun, AccIn)
    end.

%% ----------------------------------------------------

store_t(Key,Value,Timeout) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:store_t(Key,Value,Timeout);
        _ -> local_store_t(Key,Value,Timeout)
    end.

find_t(Key) ->
    case catch ?BLstore_srv:find_t(Key) of
        {'EXIT',_} -> local_find_t(Key);
        T -> T
    end.

delete_t(Key) ->
    case whereis(?BLstore_srv) of
        Pid when is_pid(Pid) -> ?BLstore_srv:delete_t(Key);
        _ -> local_delete_t(Key)
    end.

func_t(Key,Timeout,FunStore) ->
    case catch ?BLstore_srv:func_t(Key,Timeout,FunStore) of
        {'EXIT',_} -> local_func_t(Key,Timeout,FunStore);
        T -> T
    end.

func_t(Key,Timeout,FunStore,FunReply) ->
    case catch ?BLstore_srv:func_t(Key,Timeout,FunStore,FunReply) of
        {'EXIT',_} -> local_func_t(Key,Timeout,FunStore,FunReply);
        T -> T
    end.

lazy_t(Key,Fun,Opts) ->
    case catch ?BLstore_srv:lazy_t(Key,Fun,Opts) of
        {'EXIT',_} -> local_lazy_t(Key,Fun,Opts);
        T -> T
    end.

lazy_t(Key,Fun,Timeouts,SyncOpts) ->
    case catch ?BLstore_srv:lazy_t(Key,Fun,Timeouts,SyncOpts) of
        {'EXIT',_} -> local_lazy_t(Key,Fun,Timeouts,SyncOpts);
        T -> T
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------------------------
%% Non-timered surrogate
%% -----------------------------------------------------
local_store_u(Key,Value) ->
    %application:set_env(?APP,{store_surrogate,Key},{Value,0}),
    ?CFG:set_env({store_surrogate,Key},{Value,0}),
    ok.

local_find_u(Key) ->
    %case application:get_env(?APP,{store_surrogate,Key}) of
    case ?CFG:get_env({store_surrogate,Key}) of
        undefined -> false;
        {ok, {Value,_}} -> {Key,Value}
    end.

local_delete_u(Key) ->
    %application:unset_env(?APP,{store_surrogate,Key}),
    ?CFG:unset_env({store_surrogate,Key}),
    ok.

local_func_u(Key,FunStore) ->
    Value0 = case local_find_u(Key) of
                 false -> undefined;
                 {_,Value} -> Value
             end,
    case FunStore(Value0) of
        Value0 -> ok;
        Value1 -> local_store_u(Key,Value1)
    end.

local_func_u(Key,FunStore,FunReply) ->
    Value0 = case local_find_u(Key) of
                 false -> undefined;
                 {_,Value} -> Value
             end,
    case FunStore(Value0) of
        Value0 -> ok;
        Value1 ->
            local_store_u(Key,Value1),
            case FunReply of
                _ when is_function(FunReply,1) -> FunReply(Value0);
                _ when is_function(FunReply,2) -> FunReply(Value0,Value1)
            end
    end.

local_foldl_u(Fun,AccIn) ->
    %Envs = application:get_all_env(?APP),
    Envs = ?CFG:get_all_env(),
    lists:foldl(fun({{store_surrogate,Key},{Value,0}}, Acc) -> Fun({Key,Value},Acc);
                   (_, Acc) -> Acc
                end, AccIn, Envs).

%% -----------------------------------------------------
%% Timered surrogate
%% -----------------------------------------------------
local_store_t(Key,Value,Timeout) ->
    NowTS = ?BU:timestamp(),
    ExpireTS = NowTS + Timeout,
    %application:set_env(?APP,{store_surrogate,Key},{Value,ExpireTS}),
    ?CFG:set_env({store_surrogate,Key},{Value,ExpireTS}),
    local_filter_timeout(NowTS),
    ok.

%% -------------
local_find_t(Key) ->
    %case application:get_env(?APP,{store_surrogate,Key}) of,
    case ?CFG:get_env({store_surrogate,Key}) of
        undefined -> false;
        {ok, {Value,ExpireTS}} ->
            case ?BU:timestamp() of
                TS when TS < ExpireTS -> {Key,Value};
                _ ->
                    %application:unset_env(?APP,{store_surrogate,Key}),
                    ?CFG:unset_env({store_surrogate,Key}),
                    false
            end
    end.

%% -------------
local_delete_t(Key) ->
    %application:unset_env(?APP,{store_surrogate,Key}),
    ?CFG:unset_env({store_surrogate,Key}),
    ok.

%% -------------
local_func_t(Key,Timeout,FunStore) ->
    Value0 = case local_find_t(Key) of
                 false -> undefined;
                 {_,Value} -> Value
             end,
    case FunStore(Value0) of
        Value0 -> ok;
        Value1 -> local_store_t(Key,Timeout,Value1)
    end.

%% -------------
local_func_t(Key,Timeout,FunStore,FunReply) ->
    Value0 = case local_find_t(Key) of
                 false -> undefined;
                 {_,Value} -> Value
             end,
    case FunStore(Value0) of
        Value0 -> ok;
        Value1 ->
            local_store_t(Key,Value1,Timeout),
            case FunReply of
                _ when is_function(FunReply,1) -> FunReply(Value0);
                _ when is_function(FunReply,2) -> FunReply(Value0,Value1)
            end
    end.

%% -------------
local_lazy_t(Key,Fun,OptsMap) ->
    TStore = maps:get(store_timeout,OptsMap,10000),
    TExpire = maps:get(expire_timeout,OptsMap,5000),
    TUpdate = maps:get(update_timeout,OptsMap,1000),
    UseTran = maps:get(store_tran,OptsMap,true),
    SyncUpdate = maps:get(sync_update,OptsMap,false),
    lazy_t(Key,Fun,{TStore,TExpire,TUpdate},{UseTran,SyncUpdate}).

%% -------------
local_lazy_t(Key,Fun,{TStore,TExpire,_TUpdate},{_UseTran,_SyncUpdate}) ->
    NowTS = ?BU:timestamp(),
    F = fun() ->
            Res = Fun(),
            local_store_t(Key,{lazy,Res,NowTS+TExpire,NowTS+TStore},TStore),
            Res
        end,
    case local_find_t(Key) of
        {_,{lazy,Value,RefreshTS,_ExpireTS}} when NowTS < RefreshTS -> Value;
        {_,{lazy,Value,RefreshTS,ExpireTS}} ->
            Max = case (NowTS-RefreshTS) / (ExpireTS-RefreshTS) of
                      A when A < 0.25 -> 10000;
                      A when A < 0.5 -> 1000;
                      A when A < 0.75 -> 100;
                      A when A < 1 -> 10;
                      _ -> 1
                  end,
            case ?BU:random(Max) of
                0 -> F();
                _ -> Value
            end;
        {_,Value} -> Value;
        false -> F()
    end.

%% -----------------------------------------------------------------------
local_filter_timeout(NowTS) ->
    %case application:get_env(?APP,{store_surrogate_filter_tick}) of
    case ?CFG:get_env({store_surrogate_filter_tick}) of
        undefined -> do_filter_timeout(NowTS);
        {ok, TS} when NowTS - TS > 600000 -> do_filter_timeout(NowTS);
        _ -> ok
    end.

do_filter_timeout(NowTS) ->
    %application:set_env(?APP,{store_surrogate_filter_tick},NowTS),
    %Envs = application:get_all_env(?APP),
    ?CFG:set_env({store_surrogate_filter_tick},NowTS),
    Envs = ?CFG:get_all_env(),
    spawn(fun() ->
            lists:foreach(fun({{store_surrogate,Key},{_Value,ExpireTS}}) when ExpireTS < NowTS ->
                                %application:unset_env(?APP,{store_surrogate,Key});
                                ?CFG:unset_env({store_surrogate,Key});
                             (_) -> ok
                          end, Envs)
          end).

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.04.2017
%%% @doc Solve issue of erlang.otp when rpc:call could exceed selected timeout when destination server is unaccessible.

-module(basiclib_rpc).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([call/5,
         call/4,
         cast/4,
         multicall/5,
         multicall/4,
         pinfo/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

cast(Node, M, F, A) ->
    rpc:cast(Node, M, F, A).

call(Node, M, F, A, Timeout) ->
    {Self,Ref} = {self(),make_ref()},
    Pid = spawn(fun() -> Self ! {ok,Ref,rpc:call(Node, M, F, A, Timeout)} end),
    receive
        {ok,Ref,Result} -> Result;
        % XXX how DOWN is going to be received if Pid is not monitored ? use spawn_monitor() above!
        {'DOWN',_,process,Pid,_} -> {badrpc, error}
    after Timeout+100 ->
        exit(Pid,kill),
        {badrpc, spawn_timeout}
    end.

call(Node, M, F, A) ->
    rpc:call(Node, M, F, A).

multicall(Nodes, M, F, A, Timeout) ->
    rpc:multicall(Nodes, M, F, A, Timeout).

multicall(Nodes, M, F, A) ->
    rpc:multicall(Nodes, M, F, A).

pinfo(Pid,Key) ->
    rpc:pinfo(Pid,Key).

%% ====================================================================
%% Internal functions
%% ====================================================================




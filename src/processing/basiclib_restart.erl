%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.06.2017
%%% @doc #218 Functions of reboot of system elements (nodes, servers, sites, system)

-module(basiclib_restart).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------
%% Performing
%% ------------------------------------

% ----------
% Stops current node/server in spawned process, could be aborted
stop_node_spawned(MapOpts) when is_map(MapOpts) ->
    [Ref,Delay,CmdLine] = ?BU:maps_get([ref,delay,cmdline],MapOpts),
    stop_node_spawned(Ref,Delay,CmdLine).

stop_node_spawned(Ref,Delay) ->
    stop_node_spawned(Ref,Delay,undefined).

stop_node_spawned(Ref,Delay,CmdLine) when is_binary(Delay) ->
    T = case catch binary:split(Delay,<<"-">>,[]) of
            {'EXIT',_} -> 7000;
            [V] -> ?BU:to_int(V);
            [Min,Max] -> {rnd,?BU:to_int(Min),?BU:to_int(Max)}
        end,
    stop_node_spawned(Ref,T,CmdLine);
stop_node_spawned(Ref,{rnd,Min,Max},CmdLine) when Min < Max -> stop_node_spawned(Ref,?BU:random(Min,Max),CmdLine);
stop_node_spawned(Ref,{rnd,Min,_},CmdLine) -> stop_node_spawned(Ref,Min,CmdLine);
stop_node_spawned(Ref,Delay,CmdLine) when is_integer(Delay), Delay < 5000 -> stop_node_spawned(Ref,5000,CmdLine);
stop_node_spawned(Ref,Delay,CmdLine) when is_integer(Delay) ->
    ?BLopts:set_group_leader(),
    ?OUT('$info', "RPC: STOP NODE. Wait ~p ms (~p)", [Delay, Ref]),
    Pid = spawn(fun() -> receive
                             {abort,Ref} -> ok;
                             {abort,Ref,CallerPid} -> CallerPid ! {aborted,Ref}, ok
                         after Delay ->
                             do_restart(Ref,CmdLine)
                         end,
                         ?BLstore:delete_t({stop_node_spawned,Ref})
                end),
    ?BLstore:store_t({stop_node_spawned,Ref},Pid,Delay+500),
    {ok,Pid,Ref}.

% @private
do_restart(_Ref,false) ->
    ?OUT('$info', "STOP NODE '~s' (~p)", [node(),_Ref]),
    ?BU:stop_node(0);
do_restart(Ref,CmdLine) when is_binary(CmdLine) ->
    case ?BU:to_bool(B=CmdLine) of
        false when B== <<"false">>; B== <<"0">>; B== <<"FALSE">>; B== <<"False">> ->
            do_restart(Ref,false);
        %true -> bash platform restart command
        _ ->
            ?OUT('$info', "RESTART SERVER '~ts' by cmd '~ts' (~tp)", [?BU:node_addr(node()),CmdLine,Ref]),
            ?OUT('$info', "~120tp", [?BU:os_cmd(?BU:to_list(CmdLine))])
    end.

% ----------
% Abort stop_node spawned request
abort_stop_node(Ref) ->
    ?BLopts:set_group_leader(),
    ?OUT('$info', "RPC: ABORT STOP NODE (~p)", [Ref]),
    case ?BLstore:find_t({stop_node_spawned,Ref}) of
        false -> {error,not_found};
        {_,Pid} when is_pid(Pid) ->
            case process_info(Pid,status) of
                undefined -> {error,not_found};
                _ ->
                    Pid ! {abort,Ref,self()},
                    receive {aborted,Ref} -> ok
                    after 500 -> {error,timeout}
                    end end end.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.10.2016
%%% @doc Implements services to call same parallel queries to nodes (inside current site, or cross site)

-module(basiclib_multicall).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([apply/2,
         call_direct/3, call_direct/4]).
-export([multicall/2]).
-export([map_result_rpc_multicall/1]).

%% ====================================================================
%% Define
%% ===========================в=========================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------------
%% calls erlang:apply directly in parallel
%% ------------------------------------------
apply([],_Timeout) -> [];
apply([{M,F,A}|_]=MFAs,Timeout)
    when is_atom(M), is_atom(F), is_list(A), is_integer(Timeout) ->
    do_call_direct(MFAs,undefined,Timeout);
%%
apply([{_K,M,F,A}|_]=MFAs,Timeout)
    when is_atom(M), is_atom(F), is_list(A), is_integer(Timeout) ->
    do_call_direct(MFAs,undefined,Timeout);
%%
apply([Fun|_]=Funs,Timeout) when is_function(Fun,0), is_integer(Timeout) ->
    do_call_direct(Funs,undefined,Timeout);
%%
apply([{_K,Fun}|_]=Funs,Timeout) when is_function(Fun,0), is_integer(Timeout) ->
    do_call_direct(Funs,undefined,Timeout).

%% ------------------------------------------
%% calls nodes directly in parallel through rpc:call
%% ------------------------------------------
call_direct(Nodes,{M,F,A}=MFA,Timeout)
  when is_list(Nodes), is_atom(M), is_atom(F), is_list(A), is_integer(Timeout) ->
    do_call_direct(Nodes,MFA,undefined,Timeout).

call_direct(Nodes,{M,F,A}=MFA,Timeout,Opts)
    when is_list(Nodes), is_atom(M), is_atom(F), is_list(A), is_integer(Timeout), is_map(Opts) ->
    do_call_direct(Nodes,MFA,undefined,Timeout,Opts).

%% ------------------------------------------
%% make async multiplex fun apply
%% ------------------------------------------
-spec multicall(List::[{Key::term(),F::function()} | {Key::term(),F::function(),Args::list()}], Timeout::integer()) ->
          [{Key::term(),Res::term()}].
%% ------------------------------------------
multicall(List,Timeout) when is_list(List) ->
    {Self,Ref} = {self(),make_ref()},
    % make spawned applies
    Res = lists:foldl(fun({Key,F},Acc) when is_function(F,0) ->
                              {_Pid,MonRef} = spawn_monitor(fun() -> Self ! {ok,Ref,Key,F()} end),
                              Acc#{MonRef=>Key,Key=>MonRef};
                         ({Key,F},Acc) when is_function(F,1) ->
                              {_Pid,MonRef} = spawn_monitor(fun() -> Self ! {ok,Ref,Key,F(Key)} end),
                              Acc#{MonRef=>Key,Key=>MonRef};
                         ({Key,F,Args},Acc) when is_function(F,1), is_list(Args) ->
                              {_Pid,MonRef} = spawn_monitor(fun() -> Self ! {ok,Ref,Key,F(Args)} end),
                              Acc#{MonRef=>Key,Key=>MonRef}
                      end, #{}, List),
    % wait for responses
    Fwait = fun Fwait(0,Acc,_) -> Acc;
                Fwait(N,Acc,Map) ->
                    receive {ok,Ref,Key,NRes} ->
                                MonRef = maps:get(Key,Map),
                                Acc1 = lists:keyreplace(Key, 1, Acc, {Key,NRes}),
                                Fwait(N-1,Acc1,maps:without([Key,MonRef], Map));
                            {'DOWN',MonRef,process,_,_} ->
                                case maps:get(MonRef,Map,error) of
                                    error -> Fwait(N,Acc,Map);
                                    Key ->
                                        Acc1 = lists:keyreplace(Key, 1, Acc, {Key,down}), %[{Key,down}|Acc]
                                        Fwait(N-1,Acc1,maps:without([Key,MonRef], Map))
                                end
                    after Timeout -> Acc
                    end end,
    Fwait(length(List),List,Res).

%% ------------------------------------------
%% extracts multicall result in format: {OkResults, ErrorNodes}.
%% ------------------------------------------
-spec map_result_rpc_multicall(Answ::[{Node::atom(),Res::term()}]) -> {[OkRes::term()],[ErrNode::atom()]}.
%% ------------------------------------------
map_result_rpc_multicall(Answ) ->
    ErrNodes = lists:filtermap(fun({N,undefined}) -> {true,N}; (_) -> false end, Answ),
    OkNodesRes = lists:filtermap(fun({_,undefined}) -> false; ({_,Res}) -> {true,Res} end, Answ),
    {OkNodesRes,ErrNodes}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% --------------------------------------------------------
% @private caller
% calls nodes directly through rpc:call
% --------------------------------------------------------
do_call_direct(Nodes,{M,F,A},Default,Timeout) ->
    Fcall = fun(DestNode) when is_atom(DestNode) ->
                    case ?BLping:ping(DestNode, {1000,1000}, #{disconnect => true}) of
                        timeout -> Default;
                        pang -> Default;
                        pong ->
                            case catch ?BLrpc:call(DestNode, M, F, A, Timeout) of
                                {'EXIT',_} -> Default;
                                {badrpc,_} -> Default;
                                R -> R
                            end end;
               (_) -> undefined
            end,
     do_call(Nodes,Fcall,Default,Timeout).

do_call_direct(Nodes,{M,F,A},Default,Timeout,PingOpts) when is_map(PingOpts) ->
    UsePing = maps:get(use_ping,PingOpts,true),
    FunRpc = fun(DestNode) ->
                    % rpc:call/5 doesn't hang on tcp misconnect
                    case catch rpc:call(DestNode, M, F, A, Timeout) of
                        {'EXIT',_} -> Default;
                        {badrpc,_} -> Default;
                        R -> R
                    end
             end,
    Fcall = fun(DestNode) when not is_atom(DestNode) -> undefined;
               (DestNode) when not UsePing -> FunRpc(DestNode);
               (DestNode) when UsePing ->
                    PingTimeout = maps:get(ping_timeout,PingOpts,1000),
                    PingCacheTimeout = maps:get(cache_timeout,PingOpts,1000),
                    case ?BLping:ping(DestNode, {PingTimeout,PingCacheTimeout}, PingOpts) of
                        timeout -> Default;
                        pang -> Default;
                        pong -> FunRpc(DestNode)
                    end
            end,
    do_call(Nodes,Fcall,Default,Timeout).

%%
do_call_direct(FunsOrMFAs,Default,Timeout) ->
    Fcall = fun({M,F,A}) when is_atom(M) -> erlang:apply(M,F,A);
        ({_K,M,F,A}) when is_atom(M) -> erlang:apply(M,F,A);
        (Fun) when is_function(Fun,0) -> Fun();
        ({_K,Fun}) when is_function(Fun,0) -> Fun()
            end,
    do_call(FunsOrMFAs,Fcall,Default,Timeout).

%% ====================================================================
%%
%% ====================================================================

% @private caller
do_call(Dests,Fcall,Default,Timeout) ->
    {SelfPid,Ref} = {self(), make_ref()},
    Fquery = fun(Owner,Dest) -> query_remote(Owner, Dest, Fcall) end,
    ?BU:spawn_fun(fun() -> query_collect({SelfPid,Ref},Dests,Fquery,Timeout) end),
    receive {result,Ref,R} -> R
    after Timeout+1000 -> [{Dest,Default} || Dest <- Dests]
    end.

%% --------------------------------------------------------
%% --- sub ---
%% @private spawn1
query_collect({OwnerPid,OwnerRef},Dests,Fquery,Timeout) ->
    {SelfPid,Ref} = {self(), make_ref()},
    F1 = fun(Dest, Acc) ->
                ?BU:spawn_fun(fun() -> Fquery({SelfPid,Ref},Dest) end),
                [{Dest,undefined} | Acc]
         end,
    L = lists:reverse(lists:foldl(F1, [], Dests)),
    L1 = gather(L, Ref, Timeout, ?BU:timestamp(), length(L)),
    OwnerPid ! {result,OwnerRef,L1}.

%% @private spawn1
gather(L, _, _, _, 0) -> L;
gather(L, Ref, TotalTimeout, TS, I) ->
    Timeout = case TotalTimeout - (?BU:timestamp() - TS) of
                  _N when _N < 0 -> 0;
                  T -> T
              end,
    receive {result,Ref,{D,R}} -> gather(lists:keyreplace(D, 1, L, {D,R}), Ref, TotalTimeout, TS, I-1)
    after Timeout -> L
    end.

%% @private spawn2
query_remote({OwnerPid,OwnerRef}, DestNode, Fcall) ->
    Res = Fcall(DestNode),
    OwnerPid ! {result,OwnerRef,{DestNode,Res}}.
%% --- end sub ---

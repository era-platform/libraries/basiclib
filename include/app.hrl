%% ====================================================================
%% Types
%% ====================================================================

-type proplist() :: [{Key::binary()|atom(),Value::term()}].

-type json_term() :: [{binary() | atom(), json_term()}]
                        | [{},...]
                        | [json_term()] | []
                        | map()
                        | true | false | null
                        | integer() | float()
                        | binary() | atom()
                        | calendar:datetime().

%% ====================================================================
%% Modules
%% ====================================================================

-define(APP, basiclib).
-define(SUPV, basiclib_supv).
-define(CFG, basiclib_config).

-define(BU, basiclib_utils).
-define(BLcoll, basiclib_collection_select).
-define(BLhtmlentity, basiclib_htmlentity).
-define(BLmodifier, basiclib_modifier).
-define(BLpwdhash, basiclib_pwdhash).
-define(BLurlencode, basiclib_urlencode).
-define(BLuuid, basiclib_uuid).
-define(BLwav, basiclib_wav).

-define(BLlog, basiclib_log).
-define(BLlogdummy, basiclib_log_dummy).

-define(BLping, basiclib_ping).
-define(BLpingstore, basiclib_ping_store_srv).

-define(BLmulticall, basiclib_multicall).
-define(BLrestart, basiclib_restart).
-define(BLrpc, basiclib_rpc).

-define(BLopts, basiclib_srv).
-define(BLquota, basiclib_quota_srv).
-define(BLleader, basiclib_leader_svc_srv).
-define(BLtrace, basiclib_trace).

-define(BLmonitor, basiclib_monitor_srv).
-define(BLmonitor_template, basiclib_monitor_srv_template).

-define(BLstore, basiclib_store).
-define(BLstore_srv, basiclib_store_srv).
-define(BLstore_template, basiclib_store_srv_template).

-define(BLfile, basiclib_wrapper_file).
-define(BLfilelib, basiclib_wrapper_filelib).
-define(BLzip, basiclib_wrapper_zip).

-define(WORKERSUPV, basiclib_worker_supv).
-define(WORKER, basiclib_worker_srv).

-define(ErrLoggerSrv, basiclib_error_logger_srv).

-define(FileCopier, basiclib_file_copier_srv).

%% ====================================================================
%% Log, Out
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Text), ?BLlog:write(?CFG:log_destination(?LOGFILE), Text)).
-define(LOG(Fmt,Args), ?BLlog:write(?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,LogFile,Fmt,Args), ?BLlog:write(Level, LogFile, {Fmt,Args})).

-define(OUTC(Text), ?BLlog:out(Text)).
-define(OUTC(Fmt,Args), ?BLlog:out({Fmt,Args})).
-define(OUTC(Level,Fmt,Args), ?BLlog:out(Level,{Fmt,Args})).

-define(OUT(Text), ?BLlog:writeout(?CFG:log_destination(?LOGFILE), Text)).
-define(OUT(Fmt,Args), ?BLlog:writeout(?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,LogFile,Fmt,Args), ?BLlog:writeout(Level, LogFile, {Fmt,Args})).
